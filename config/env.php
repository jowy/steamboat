<?php

return [
    'APP_NAME' => env('APP_NAME', 'Laravel'),
    'APP_ENV' => env('APP_ENV', 'local'),
    'APP_KEY' => env('APP_KEY', null),
    'APP_DEBUG' => env('APP_DEBUG', true),
    'APP_URL' => env('APP_URL', 'http://localhost'),
	
	'LOG_CHANNEL' => env('LOG_CHANNEL', 'stack'),
    
    'DB_CONNECTION' => env('DB_CONNECTION', 'mysql'),
    'DB_HOST' => env('DB_HOST', '127.0.0.1'),
    'DB_PORT' => env('DB_PORT', '3306'),
    'DB_DATABASE' => env('DB_DATABASE', 'remark'),
    'DB_USERNAME' => env('DB_USERNAME', 'root'),
    'DB_PASSWORD' => env('DB_PASSWORD', null),
    
    'BROADCAST_DRIVER' => env('BROADCAST_DRIVER', 'log'),
    'CACHE_DRIVER' => env('CACHE_DRIVER', 'file'),
    'SESSION_DRIVER' => env('SESSION_DRIVER', 'file'),
    'SESSION_LIFETIME' => env('SESSION_LIFETIME', '120'),
    'QUEUE_DRIVER' => env('QUEUE_DRIVER', 'sync'),
    
    'REDIS_HOST' => env('REDIS_HOST', '127.0.0.1'),
    'REDIS_PASSWORD' => env('REDIS_PASSWORD', null),
    'REDIS_PORT' => env('REDIS_PORT', 6379),
    
    'MAIL_DRIVER' => env('MAIL_DRIVER', 'smtp'),
    'MAIL_HOST' => env('MAIL_HOST', 'mailtrap.io'),
    'MAIL_PORT' => env('MAIL_PORT', 2525),
    'MAIL_USERNAME' => env('MAIL_USERNAME', null),
    'MAIL_PASSWORD' => env('MAIL_PASSWORD', null),
    'MAIL_ENCRYPTION' => env('MAIL_ENCRYPTION', null),
	'MAIL_FROM_ADDRESS' => env('MAIL_FROM_ADDRESS', null),
	'MAIL_FROM_NAME' => env('MAIL_FROM_NAME', null),

    'AWS_ACCESS_KEY_ID' => env('AWS_ACCESS_KEY_ID', null),
    'AWS_SECRET_ACCESS_KEY' => env('AWS_SECRET_ACCESS_KEY', null),
    'AWS_DEFAULT_REGION' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    'AWS_BUCKET' => env('AWS_BUCKET', null),

    'PUSHER_APP_ID' => env('PUSHER_APP_ID', null),
    'PUSHER_APP_KEY' => env('PUSHER_KEY', null),
    'PUSHER_APP_SECRET' => env('PUSHER_SECRET', null),
    'PUSHER_APP_CLUSTER' => env('PUSHER_APP_CLUSTER', 'mt1'),

    'GOOGLE_MAP_API_KEY' => env('GOOGLE_MAP_API_KEY', 'AIzaSyDC-VFkMya1-3gCY3J0J9FkGgSI_s5ljmw'),

    'FACEBOOK_APPID' => env('FACEBOOK_APPID', null),
    'FACEBOOK_SECRET' => env('FACEBOOK_SECRET', null),
    'FACEBOOK_REDIRECT' => env('FACEBOOK_REDIRECT', null),
];