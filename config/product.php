<?php
/**
 * Created by PhpStorm.
 * User: hm0001
 * Date: 26/3/2019
 * Time: 7:45 PM
 */

return [
    'label' => [
        'btl' => 'btl',
        'ctn' => 'ctn',
        'gram' => 'gram',
        'kg' => 'kg',
        'nos' => 'nos',
        'pack' => 'pack',
        'tray' => 'tray',
    ]
];
