<?php

use Illuminate\Database\Seeder;
use App\Models\Page;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getPages() as $key => $page) {
            if (!Page::where('page_tag', '=', strtolower($page['page_tag']))->exists()) {
                $p = new Page();
                $p->page_tag = $page['page_tag'];
                $p->page_description_en = $page['page_description_en'];
                $p->page_description_cn = $page['page_description_cn'];
                $p->title_en = $page['title_en'];
                $p->title_cn = $page['title_cn'];
                $p->content_en = $page['content_en'];
                $p->content_cn = $page['content_cn'];
                $p->mobile_content_en = $page['mobile_content_en'];
                $p->mobile_content_cn = $page['mobile_content_cn'];
				$p->sharing_title_en = isset($page['sharing_title_en']) ? $page['sharing_title_en'] : null;;
                $p->sharing_title_cn = isset($page['sharing_title_cn']) ? $page['sharing_title_cn'] : null;;
                $p->sharing_content_en = isset($page['sharing_content_en']) ? $page['sharing_content_en'] : null;;
                $p->sharing_content_cn = isset($page['sharing_content_cn']) ? $page['sharing_content_cn'] : null;;
                $p->sharing_image_en = isset($page['sharing_image_en']) ? $page['sharing_image_en'] : null;;
                $p->sharing_image_cn = isset($page['sharing_image_cn']) ? $page['sharing_image_cn'] : null;
                $p->has_share = isset($page['has_share']) ? $page['has_share'] : 0;
                $p->is_system = isset($page['is_system']) ? $page['is_system'] : 0;
                $p->save();
            }
        }
    }

    public function getPages() {
        $arr = array();

        $arr[] = [
            'page_tag' => 'about-us',
            'page_description_en' => 'About Us',
            'page_description_cn' => '关于我们',
            'title_en' => 'Title',
            'title_cn' => '标题',
            'content_en' => 'Content',
            'content_cn' => '内容',
            'mobile_content_en' => 'Mobile Content',
            'mobile_content_cn' => '手机内容',
        ];
		
		$arr[] = [
            'page_tag' => 'invite',
            'page_description_en' => 'Invite Page',
            'page_description_cn' => 'Invite Page',
            'title_en' => 'Invite Page',
            'title_cn' => 'Invite Page',
            'content_en' => 'Invite Page Content',
            'content_cn' => 'Invite Page Content',
            'mobile_content_en' => 'Invite Page Content',
            'mobile_content_cn' => 'Invite Page Content',
            'sharing_title_en' => 'Invite Page',
            'sharing_title_cn' => 'Invite Page',
            'sharing_content_en' => 'Invite Page Content For Share',
            'sharing_content_cn' => 'Invite Page Content For Share',
            'sharing_image_en' => '/img/share.png',
            'sharing_image_cn' => '/img/share.png',
            'has_share' => 0,
            'is_system' => 1,
        ];

        $arr[] = [
            'page_tag' => 'share',
            'page_description_en' => 'Sharing Page',
            'page_description_cn' => 'Sharing Page',
            'title_en' => 'Sharing Page',
            'title_cn' => 'Sharing Page',
            'content_en' => 'Sharing Page Content',
            'content_cn' => 'Sharing Page Content',
            'mobile_content_en' => 'Sharing Page Content',
            'mobile_content_cn' => 'Sharing Page Content',
            'sharing_title_en' => 'Sharing Page',
            'sharing_title_cn' => 'Sharing Page',
            'sharing_content_en' => 'Sharing Page Content For Share',
            'sharing_content_cn' => 'Sharing Page Content For Share',
            'sharing_image_en' => '/img/share.png',
            'sharing_image_cn' => '/img/share.png',
            'has_share' => 1,
            'is_system' => 1
        ];

        return $arr;
    }
}
