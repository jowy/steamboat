<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getSettings() as $key => $setting) {
            if (!Setting::where('setting', '=', $setting['setting'])->exists()) {
                $s = new Setting();
                $s->type = $setting['type'];
                $s->setting = $setting['setting'];
                $s->value = $setting['value'];
                $s->default_value = $setting['default_value'];
                $s->params = $setting['params'];
                $s->description_en = $setting['description_en'];
                $s->description_cn = $setting['description_cn'];
                $s->save();
            }
        }
    }
    
    public function getSettings() {
        $arr = array();

        //Site Name EN
        $arr[] = [
            'type' => Setting::TYPE_TEXT,
            'setting' => 'site_name_en',
            'value' => null,
            'default_value' => 'Site Name',
            'params' => null,
            'description_en' => 'Website Name(EN)',
            'description_cn' => '网站名称（英文）',
        ];

        //Site Name CN
        $arr[] = [
            'type' => Setting::TYPE_TEXT,
            'setting' => 'site_name_cn',
            'value' => null,
            'default_value' => '网站名称',
            'params' => null,
            'description_en' => 'Website Name(CN)',
            'description_cn' => '网站名称（中文）',
        ];

        //Slogan EN
        $arr[] = [
            'type' => Setting::TYPE_TEXT,
            'setting' => 'slogan_en',
            'value' => null,
            'default_value' => null,
            'params' => null,
            'description_en' => 'Website Slogan(EN)',
            'description_cn' => '网站标语（英文）',
        ];

        //Slogan CN
        $arr[] = [
            'type' => Setting::TYPE_TEXT,
            'setting' => 'slogan_cn',
            'value' => null,
            'default_value' => null,
            'params' => null,
            'description_en' => 'Website Slogan(CN)',
            'description_cn' => '网站标语（中文）',
        ];

        //Logo English for Admin Navigation Bar
        $arr[] = [
            'type' => Setting::TYPE_IMAGE,
            'setting' => 'admin_navigation_logo_en',
            'value' => null,
            'default_value' => '/logo.png',
            'params' => json_encode(['image_width' => 145, 'image_height' => 40]),
            'description_en' => 'Admin Navigation Bar Logo(EN)',
            'description_cn' => '后台导航商标（英文）',
        ];

        //Logo Chinese for Admin Navigation Bar
        $arr[] = [
            'type' => Setting::TYPE_IMAGE,
            'setting' => 'admin_navigation_logo_cn',
            'value' => null,
            'default_value' => '/logo.png',
            'params' => json_encode(['image_width' => 145, 'image_height' => 40]),
            'description_en' => 'Admin Navigation Bar Logo(CN)',
            'description_cn' => '后台导航商标（中文）',
        ];

        //Logo English for Admin Login Page
        $arr[] = [
            'type' => Setting::TYPE_IMAGE,
            'setting' => 'admin_login_logo_en',
            'value' => null,
            'default_value' => '/logo.png',
            'params' => json_encode(['image_width' => 145, 'image_height' => 40]),
            'description_en' => 'Admin Login Page Logo(EN)',
            'description_cn' => '后台登入页面商标（英文）',
        ];

        //Logo Chinese for Admin Login Page
        $arr[] = [
            'type' => Setting::TYPE_IMAGE,
            'setting' => 'admin_login_logo_cn',
            'value' => null,
            'default_value' => '/logo.png',
            'params' => json_encode(['image_width' => 145, 'image_height' => 40]),
            'description_en' => 'Admin Login Page Logo(CN)',
            'description_cn' => '后台登入页面商标（中文）',
        ];

        //Logo English for User Navigation Bar
        $arr[] = [
            'type' => Setting::TYPE_IMAGE,
            'setting' => 'user_navigation_logo_en',
            'value' => null,
            'default_value' => '/logo.png',
            'params' => json_encode(['image_width' => 290, 'image_height' => 80]),
            'description_en' => 'Navigation Bar Logo(EN)',
            'description_cn' => '导航商标（英文）',
        ];

        //Logo Chinese for User Navigation Bar
        $arr[] = [
            'type' => Setting::TYPE_IMAGE,
            'setting' => 'user_navigation_logo_cn',
            'value' => null,
            'default_value' => '/logo.png',
            'params' => json_encode(['image_width' => 290, 'image_height' => 80]),
            'description_en' => 'Navigation Bar Logo(CN)',
            'description_cn' => '导航商标（中文）',
        ];

        //Logo English for User Login Page
        $arr[] = [
            'type' => Setting::TYPE_IMAGE,
            'setting' => 'user_login_logo_en',
            'value' => null,
            'default_value' => '/logo.png',
            'params' => json_encode(['image_width' => 290, 'image_height' => 80]),
            'description_en' => 'Login Page Logo(EN)',
            'description_cn' => '登入页面商标（英文）',
        ];

        //Logo Chinese for User Login Page
        $arr[] = [
            'type' => Setting::TYPE_IMAGE,
            'setting' => 'user_login_logo_cn',
            'value' => null,
            'default_value' => '/logo.png',
            'params' => json_encode(['image_width' => 290, 'image_height' => 80]),
            'description_en' => 'Login Page Logo(CN)',
            'description_cn' => '登入页面商标（中文）',
        ];

        //Favourite Icon
        $arr[] = [
            'type' => Setting::TYPE_IMAGE,
            'setting' => 'favicon',
            'value' => null,
            'default_value' => '/favicon.ico',
            'params' => json_encode(['image_width' => 50, 'image_height' => 50]),
            'description_en' => 'Favourite Icon',
            'description_cn' => '书签商标',
        ];
		
		//Home page slider switch
        $arr[] = [
            'type' => Setting::TYPE_SELECT,
            'setting' => 'user_homepage_slider',
            'value' => 1,
            'default_value' => 1,
            'params' => json_encode(['options' => [0 => 'No', 1 => 'Yes']]),
            'description_en' => 'Dashboard Slider Enable?',
            'description_cn' => '用户首页是否显示横幅?',
        ];

        //Tax name
        $arr[] = [
            'type' => Setting::TYPE_TEXT,
            'setting' => 'tax_name',
            'value' => null,
            'default_value' => 'Service charges',
            'params' => '',
            'description_en' => 'Tax name',
            'description_cn' => '税务名称',
        ];


        //Tax percentage
        $arr[] = [
            'type' => Setting::TYPE_TEXT,
            'setting' => 'tax_percentage',
            'value' => null,
            'default_value' => 0,
            'params' => json_encode(['classes' => 'percentage-input']),
            'description_en' => 'Tax percentage',
            'description_cn' => '税务巴仙率',
        ];

        //Table duration
        $arr[] = [
            'type' => Setting::TYPE_TEXT,
            'setting' => 'table_duration',
            'value' => 120,
            'default_value' => 120,
            'params' => json_encode(['classes' => 'number-input']),
            'description_en' => 'Table duration',
            'description_cn' => '桌子持续时间',
        ];

        return $arr;
    }
}
