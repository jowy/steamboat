<?php

use Illuminate\Database\Seeder;

class PricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('prices')->truncate();
        \DB::table('prices')->insert([
            'id' => 1,
            'type' => \App\Models\Price::KID_TYPE,
            'name_en' => 'Kid',
            'per_pax_price' => 19.90,
            'is_system' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        \DB::table('prices')->insert([
            'id' => 2,
            'type' => \App\Models\Price::ADULT_TYPE,
            'name_en' => 'Adult',
            'per_pax_price' => 29.90,
            'is_system' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        \DB::table('prices')->insert([
            'id' => 3,
            'type' => \App\Models\Price::SENIOR_TYPE,
            'name_en' => 'Senior',
            'per_pax_price' => 9.90,
            'is_system' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        \DB::table('prices')->insert([
            'id' => 4,
            'type' => \App\Models\Price::FREE_TYPE,
            'name_en' => 'Free',
            'per_pax_price' => 0.00,
            'is_system' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
