<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getMenus() as $key => $menu) {
            if (!Menu::where('name_en', '=', $menu['name_en'])->exists()) {
                $s = new Menu();
                $s->menu_tag = $menu['menu_tag'];
                $s->name_en = $menu['name_en'];
                $s->name_cn = $menu['name_cn'];
                $s->tier = $menu['tier'];
                $s->save();
            }
        }
    }
    
    public function getMenus() {
        $arr = array();

        //Left Sidebar top menu
        $arr[] = [
            'menu_tag' => 'frontend-sidebar-top-menu',
            'name_en' => 'Left Sidebar Top Menu',
            'name_cn' => '左边导航栏顶部菜单',
            'tier' => 2,
        ];

        //Left Sidebar bottom menu
        $arr[] = [
            'menu_tag' => 'frontend-sidebar-bottom-menu',
            'name_en' => 'Left Sidebar Bottom Menu',
            'name_cn' => '左边导航栏底部菜单',
            'tier' => 2,
        ];
        
        return $arr;
    }
}
