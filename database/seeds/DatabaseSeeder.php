<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		if (config('env.APP_DEBUG') !== true) {
            dd('db:seed is not allowed');
        }
		
        //Add default user
        \DB::table('user')->delete();
		\DB::statement('ALTER TABLE `user` AUTO_INCREMENT=1');
        $u = new \App\Models\User();
        $u->username = 'origin';
        $u->name = 'Origin';
        $u->email = 'origin@example.com';
        $u->password = 'qweasd';
        $u->password2 = 'qweasd';
        $u->first_login = 1;
        $u->save();

        //Add default super admin
        \DB::table('admin')->delete();
		\DB::statement('ALTER TABLE `admin` AUTO_INCREMENT=1');
        $u = new \App\Models\Admin();
        $u->username = 'admin';
        $u->password = 'qweasd';
        $u->email = 'admin@admin.com';
        $u->name = 'Administrator';
        $u->account_type = \App\Models\Admin::ACCOUNT_TYPE_SUPERADMIN;
        $u->save();
		
		//Add default super admin
        $u = new \App\Models\Admin();
        $u->username = 'dev';
        $u->password = 'qweasd';
        $u->email = 'developer@testspots.com';
        $u->name = 'Developer';
        $u->account_type = \App\Models\Admin::ACCOUNT_TYPE_SUPERADMIN;
        $u->save();

        $request = request();
        if ($request->has('test') && $request->get('test') == 1) {
            $i = new InstallSeeder();
            $i->run();
        }
    }
}
