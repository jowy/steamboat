<?php

use Illuminate\Database\Seeder;
use App\Models\Bank;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getBanks() as $var) {

            $count = Bank::where('name_en', '=', $var['name_en'])->where('country_id', '=', $var['country_id'])->count();
            if (!$count) {
                $c = new Bank();

                foreach ($var as $k => $v) {
                    $c->$k = $v;
                }

                $c->save();
            }
        }
    }

    public function getBanks() {
        $arr = array();

        $my = \App\Models\Country::where('country_code_alpha_2', '=', 'my')->first();
        if ($my) {
            $arr[] = ['country_id' => $my->id, 'name_en' => 'Malayan Banking Berhad (MBB)', 'name_cn' => 'Malayan Banking Berhad (MBB)'];
            $arr[] = ['country_id' => $my->id, 'name_en' => 'Standard Chartered Bank Malaysia Berhad (SCB)', 'name_cn' => 'Standard Chartered Bank Malaysia Berhad (SCB)'];
            $arr[] = ['country_id' => $my->id, 'name_en' => 'Public Bank Berhad (PBB)', 'name_cn' => 'Public Bank Berhad (PBB)'];
            $arr[] = ['country_id' => $my->id, 'name_en' => 'CIMB Bank Berhad (CIMB)', 'name_cn' => 'CIMB Bank Berhad (CIMB)'];
            $arr[] = ['country_id' => $my->id, 'name_en' => 'Bank Islam Malaysia Berhad (BIMB)', 'name_cn' => 'Bank Islam Malaysia Berhad (BIMB)'];
            $arr[] = ['country_id' => $my->id, 'name_en' => 'Hong Leong Bank Berhad (HLBB)', 'name_cn' => 'Hong Leong Bank Berhad (HLBB)'];
            $arr[] = ['country_id' => $my->id, 'name_en' => 'United Overseas Bank (UOB)', 'name_cn' => 'United Overseas Bank (UOB)'];
            $arr[] = ['country_id' => $my->id, 'name_en' => 'Bank Simpanan Malaysia (BSN)', 'name_cn' => 'Bank Simpanan Malaysia (BSN)'];
            $arr[] = ['country_id' => $my->id, 'name_en' => 'Rashid Hussein Bank (RHB)', 'name_cn' => 'Rashid Hussein Bank (RHB)'];
        }

        return $arr;
    }
}
