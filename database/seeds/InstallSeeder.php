<?php

use Illuminate\Database\Seeder;
use App\Models\Slider;

class InstallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (config('env.APP_DEBUG') !== true) {
            dd('Install is not allow');
        }

        $request = request();

        $s = new SettingSeeder();
        $p = new PageSeeder();
        $m = new MenuSeeder();
        $c = new CountrySeeder();
		$b = new BankSeeder();
		$prices = new PricesTableSeeder();

        $s->run();
        $p->run();
        $m->run();
        $c->run();
		$b->run();
		$prices->run();

        if (!$request->has('test') || $request->get('test') != 1) {
            $d = new DatabaseSeeder();
            $d->run();
        }

		$this->insertDemoSlider();
		$this->insertDemoAnnouncement();
    }
	
	public function insertDemoSlider() {
		//Add Default Slider
		for ($i = 1; $i <= 3; $i++) {
			\DB::table('slider')->insert([
				'title_en' => 'Demo Slider ' . $i,
				'title_cn' => '示范焦点图 ' . $i,
				'description_en' => 'Demo Slider ' . $i . ' Description',
				'description_cn' => '示范焦点图 ' . $i . ' 简介',
				'image_en' => '/img/slider' . $i . '.jpg',
				'image_cn' => '/img/slider' . $i . '.jpg',
				'url_en' => null,
				'url_cn' => null,
				'new_tab_en' => 0,
				'new_tab_cn' => 0,
				'start_at' => '2017-01-01 00:00:00',
				'end_at' => '2030-12-31 23:59:59',
				'created_at' => \Carbon\Carbon::now(),
				'updated_at' => \Carbon\Carbon::now(),
			]);
		}
	}

    public function insertDemoAnnouncement() {
        \DB::table('announcement')->insert([
            'title_en' => 'Demo Announcement',
            'title_cn' => '示范公告 ',
            'content_en' => '<p>Demo Announcement Content</p>',
            'content_cn' => '<p>示范公告内容</p>',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
