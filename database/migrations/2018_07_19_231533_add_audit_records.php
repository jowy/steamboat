<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAuditRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_records', function (Blueprint $table) {
            $table->engine = 'innodb';
            $table->bigIncrements('id');
            $table->unsignedInteger('admin_id')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->string('function_tag', 255)->nullable()->default(null);
            $table->unsignedInteger('operation')->nullable()->default(null);
            $table->longText('before_data')->nullable()->default(null);
            $table->longText('after_data')->nullable()->default(null);
            $table->longText('created_data')->nullable()->default(null);
            $table->longText('deleted_data')->nullable()->default(null);
            $table->text('params')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('admin_id')->references('id')->on('admin')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audit_records', function (Blueprint $table) {
            $table->dropForeign('audit_records_admin_id_foreign');
        });
        Schema::dropIfExists('audit_records');
    }
}
