<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prices', function (Blueprint $table) {
            $table->string('type')->nullable()->change();
            $table->string('name_en')->after('type');
            $table->unsignedTinyInteger('is_system')->default(0)->after('per_pax_price');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prices', function (Blueprint $table) {
            $table->string('type')->nullable(false)->change();
            $table->dropColumn('name_en');
            $table->dropColumn('is_system');
            $table->dropSoftDeletes();
        });
    }
}
