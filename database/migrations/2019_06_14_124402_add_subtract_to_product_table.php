<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubtractToProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product', function (Blueprint $table) {
            $table->decimal('subtract_quantity', 18, \App\Constants::$decimal_point)->nullable()->after('ala_carte_price');
            $table->unsignedInteger('subtract_product_id')->nullable()->after('ala_carte_price');

            $table->foreign('subtract_product_id')->references('id')->on('product')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product', function (Blueprint $table) {
            $table->dropColumn(['subtract_quantity', 'subtract_product_id']);
        });
    }
}
