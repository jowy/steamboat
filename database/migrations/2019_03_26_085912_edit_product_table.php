<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product', function (Blueprint $table) {
            $table->string('quantity_label')->after('product_name_cn');
            $table->boolean('is_ala_carte')->default(false)->after('product_image_cn');
            $table->renameColumn('product_price', 'ala_carte_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product', function (Blueprint $table) {
            $table->string('quantity_label');
            $table->boolean('is_ala_carte');
            $table->renameColumn('ala_carte_price', 'product_price');
        });
    }
}
