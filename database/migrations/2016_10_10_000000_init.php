<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Init extends Migration
{
    protected $dbname = null;
    public function __construct() {
        $this->dbname = config('database.connections.mysql.database');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->makeAdmin(1);
        $this->makeSetting(1);
        $this->makeSlider(1);
        $this->makePage(1);//Must add before menu
        $this->makeAnnouncement(1);
        $this->makeCountry(1);
        $this->makeMenu(1);
        $this->makeBank(1);//Must add after country
        $this->makeCategory(1);
        $this->makeUser(1);//Must add after country
        $this->makeUserBonus(1);//Must add after user
        $this->makeUserTransaction(1);//Must add after user
        $this->makeUserTopup(1);//Must add after user, admin, country, bank and company bank
        $this->makeUserWithdrawal(1);//Must add after user, admin, country and bank
        $this->makeUserCreditManage(1);//Must add after user, admin, country and bank
        $this->makeBranch(1);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->makeUserCreditManage(2);//Must drop before user and admin
        $this->makeUserWithdrawal(2);//Must drop before user, admin, country and bank
        $this->makeUserTopup(2);//Must drop before user, admin, country, bank and company bank
        $this->makeUserTransaction(2);//Must drop before user
        $this->makeUserBonus(2);//Must drop before user
        $this->makeUser(2);//Must drop before country
        $this->makeCategory(2);
        $this->makeBank(2);//Must drop before country
        $this->makeMenu(2);//Must drop before page
        $this->makeAnnouncement(2);
        $this->makePage(2);
        $this->makeSetting(2);
        $this->makeSlider(2);
        $this->makeAdmin(2);
        $this->makeBranch(2);
		$this->makeCountry(2);
    }

    protected function dropForeigns($tables) {
        foreach ($tables as $db) {
            $foreigns = \DB::select("SELECT TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_SCHEMA = '" . $this->dbname . "' AND REFERENCED_TABLE_NAME = '" . $db . "'");
            foreach ($foreigns as $f) {
                Schema::table($db, function (Blueprint $table) use ($db, $f) {
                    $table->dropForeign($f->CONSTRAINT_NAME);
                });
            }
        }
    }

    protected function dropTables($tables) {
        foreach ($tables as $db) {
            Schema::dropIfExists($db);
        }
    }

    protected function dropForeignsAndTables($tables) {
        $this->dropForeigns($tables);
        $this->dropTables($tables);
    }

    public function makeAdmin($type) {
        if ($type == 1) {
            Schema::create('permission_groups', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->increments('id');
                $table->string('group_name', 255);
                $table->timestamps();
            });
            Schema::create('permission_groups_permissions', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->increments('id');
                $table->unsignedInteger('permission_group_id');
                $table->text('permission_tag');
                $table->timestamps();
            });
            Schema::table('permission_groups_permissions', function (Blueprint $table) {
                $table->foreign('permission_group_id')->references('id')->on('permission_groups')->onDelete('cascade');
            });

            Schema::create('admin', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->increments('id');
                $table->string('username', 64);
                $table->string('email', 255);
                $table->string('name', 255);
                $table->string('password', 255);
                $table->string('lang', 24)->default('en');
                $table->rememberToken();
                $table->unsignedTinyInteger('account_type');
                $table->unsignedInteger('permission_group_id')->nullable()->default(null);
                $table->timestamps();
                $table->softDeletes();
            });
            Schema::table('admin', function (Blueprint $table) {
                $table->foreign('permission_group_id')->references('id')->on('permission_groups')->onDelete('set null');
            });

            Schema::create('admin_password_resets', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->string('username', 64)->index();
                $table->string('token')->index();
                $table->timestamp('created_at')->nullable();
            });

        } else if ($type == 2) {
            Schema::table('permission_groups_permissions', function (Blueprint $table) {
                $table->dropForeign('permission_groups_permissions_permission_group_id_foreign');
            });
            Schema::table('admin', function (Blueprint $table) {
                $table->dropForeign('admin_permission_group_id_foreign');
            });
            $tables = ['permission_groups_permissions', 'admin_password_resets', 'admin', 'permission_groups'];
            $this->dropTables($tables);
        }
    }

    public function makeSetting($type) {
        if ($type == 1) {
            Schema::create('setting', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->increments('id');
                $table->unsignedInteger('type');
                $table->string('setting', 255);
                $table->text('value')->nullable()->default(null);
                $table->text('default_value')->nullable()->default(null);
                $table->text('params')->nullable()->default(null);
                $table->string('description_en', 255);
                $table->string('description_cn', 255);
                $table->timestamps();
                $table->softDeletes();
            });
        } else if ($type == 2) {
            $tables = ['setting'];
            $this->dropTables($tables);
        }
    }

    public function makeSlider($type) {
        if ($type == 1) {
            Schema::create('slider', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->increments('id');
                $table->string('title_en', 255);
                $table->string('title_cn', 255);
                $table->text('description_en')->nullable()->default(null);
                $table->text('description_cn')->nullable()->default(null);
                $table->text('image_en')->nullable()->default(null);
                $table->text('image_cn')->nullable()->default(null);
                $table->text('url_en')->nullable()->default(null);
				$table->string('url_text_en', 255)->nullable()->default(null);
                $table->text('url_cn')->nullable()->default(null);
				$table->string('url_text_cn', 255)->nullable()->default(null);
                $table->unsignedTinyInteger('new_tab_en')->default(0);
                $table->unsignedTinyInteger('new_tab_cn')->default(0);
                $table->dateTime('start_at');
                $table->dateTime('end_at');
                $table->timestamps();
            });
        } else if ($type == 2) {
            $tables = ['slider'];
            $this->dropTables($tables);
        }
    }

    public function makePage($type) {
        if ($type == 1) {
            Schema::create('page', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->increments('id');
                $table->string('page_tag', 64)->unique();
                $table->string('page_description_en', 255);
                $table->string('page_description_cn', 255);
                $table->string('title_en', 255);
                $table->string('title_cn', 255);
                $table->text('content_en');
                $table->text('content_cn')->nullable()->default(null);
                $table->text('mobile_content_en');
                $table->text('mobile_content_cn')->nullable()->default(null);
                $table->string('sharing_title_en', 255)->nullable()->default(null);
                $table->string('sharing_title_cn', 255)->nullable()->default(null);
                $table->string('sharing_content_en', 255)->nullable()->default(null);
                $table->string('sharing_content_cn', 255)->nullable()->default(null);
                $table->text('sharing_image_en')->nullable()->default(null);
                $table->text('sharing_image_cn')->nullable()->default(null);
                $table->unsignedTinyInteger('has_share')->default(0);
                $table->unsignedTinyInteger('is_system')->default(0);
                $table->timestamps();
            });
        } else if ($type == 2) {
            $tables = ['page'];
            $this->dropTables($tables);
        }
    }

    public function makeAnnouncement($type) {
        if ($type == 1) {
            Schema::create('announcement', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->increments('id');
                $table->string('title_en', 255);
                $table->string('title_cn', 255);
                $table->text('content_en');
                $table->text('content_cn')->nullable()->default(null);
                $table->timestamps();
            });
        } else if ($type == 2) {
            $tables = ['announcement'];
            $this->dropTables($tables);
        }
    }

    public function makeCountry($type) {
        if ($type == 1) {
            Schema::create('country', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->increments('id');
                $table->string('country_code_alpha_2', 16)->nullable()->default(null);
                $table->string('country_code_alpha_3', 16)->nullable()->default(null);
                $table->unsignedInteger('country_code_number')->nullable()->default(null);
                $table->text('country_name', 255)->nullable()->default(null);
                $table->text('country_name_en', 255)->nullable()->default(null);
                $table->text('country_name_cn', 255)->nullable()->default(null);
                $table->string('ext', 64)->nullable()->default(null);
                $table->string('currency_code', 16)->nullable()->default(null);
                $table->decimal('currency_actual_rate', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('currency_in_rate', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('currency_out_rate', 18, \App\Constants::$decimal_point)->default(0);
                $table->string('currency_prefix', 255)->nullable()->default(null);
                $table->string('currency_suffix', 255)->nullable()->default(null);
                $table->unsignedTinyInteger('status');
                $table->timestamps();
            });
            Schema::create('country_locations', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->bigIncrements('id');
                $table->unsignedInteger('country_id');
                $table->unsignedBigInteger('parent_id')->nullable()->default(null);
                $table->unsignedInteger('sorting')->default(0);
                $table->string('location_name_en', 255);
                $table->string('location_name_cn', 255);
                $table->timestamps();
            });
            Schema::table('country_locations', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('country')->onDelete('cascade');
                $table->foreign('parent_id')->references('id')->on('country_locations')->onDelete('cascade');
            });
        } else if ($type == 2) {
            Schema::table('country_locations', function (Blueprint $table) {
                $table->dropForeign('country_locations_country_id_foreign');
                $table->dropForeign('country_locations_parent_id_foreign');
            });
            $tables = ['country_locations', 'country'];
            $this->dropTables($tables);
        }
    }

    public function makeMenu($type) {
        if ($type == 1) {
            Schema::create('menu', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->increments('id');
                $table->string('menu_tag', 64);
                $table->string('name_en', 255);
                $table->string('name_cn', 255);
                $table->unsignedInteger('tier');
                $table->timestamps();
            });
            Schema::create('menu_items', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->bigIncrements('id');
                $table->unsignedInteger('menu_id');
                $table->unsignedBigInteger('parent_id')->nullable()->default(null);
                $table->unsignedTinyInteger('type');
                $table->unsignedInteger('page_id')->nullable()->default(null);
                $table->unsignedInteger('sorting')->default(0);
                $table->string('url', 255)->nullable()->default(null);
                $table->string('text_en', 255);
                $table->string('text_cn', 255);
                $table->unsignedTinyInteger('open_in_new_tab')->default(0);
                $table->timestamps();
            });
            Schema::table('menu_items', function (Blueprint $table) {
                $table->foreign('menu_id')->references('id')->on('menu')->onDelete('cascade');
                $table->foreign('parent_id')->references('id')->on('menu_items')->onDelete('cascade');
                $table->foreign('page_id')->references('id')->on('page')->onDelete('cascade');
            });
        } else if ($type == 2) {
            $tables = ['menu_items', 'menu'];
            Schema::table('menu_items', function (Blueprint $table) {
                $table->dropForeign('menu_items_menu_id_foreign');
                $table->dropForeign('menu_items_parent_id_foreign');
                $table->dropForeign('menu_items_page_id_foreign');
            });
            $this->dropTables($tables);
        }
    }

    public function makeBank($type) {
        if ($type == 1) {
            Schema::create('bank', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->increments('id');
                $table->unsignedInteger('country_id')->nullable()->default(null);
                $table->string('name_en', 255);
                $table->string('name_cn', 255);
                $table->timestamps();
            });
            Schema::create('company_bank', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->increments('id');
                $table->unsignedInteger('bank_id')->nullable()->default(null);
                $table->unsignedInteger('country_id')->nullable()->default(null);
                $table->string('name_en', 255);
                $table->string('name_cn', 255);
                $table->string('account_name', 255)->nullable()->default(null);
                $table->string('account_number', 255)->nullable()->default(null);
                $table->timestamps();
            });
            Schema::table('company_bank', function (Blueprint $table) {
                $table->foreign('bank_id')->references('id')->on('bank')->onDelete('set null');
                $table->foreign('country_id')->references('id')->on('country')->onDelete('set null');
            });
            Schema::table('bank', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('country')->onDelete('set null');
            });
        } else if ($type == 2) {
            $tables = ['company_bank', 'bank'];
            Schema::table('company_bank', function (Blueprint $table) {
                $table->dropForeign('company_bank_bank_id_foreign');
                $table->dropForeign('company_bank_country_id_foreign');
            });
            Schema::table('bank', function (Blueprint $table) {
                $table->dropForeign('bank_country_id_foreign');
            });
            $this->dropTables($tables);
        }
    }

    public function makeCategory($type) {
        if ($type == 1) {
            Schema::create('category', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->increments('id');
                $table->unsignedInteger('parent_id')->nullable()->default(null);
                $table->unsignedInteger('sorting')->default(0);
                $table->string('name_en', 255);
                $table->string('name_cn', 255)->nullable()->default(null);
                $table->timestamps();
                $table->softDeletes();
            });
            Schema::table('category', function (Blueprint $table) {
                $table->foreign('parent_id')->references('id')->on('category')->onDelete('set null');
            });
        } else if ($type == 2) {
            $tables = ['category'];
            $this->dropForeignsAndTables($tables);
        }
    }

    public function makeUser($type) {
        if ($type == 1) {
            Schema::create('user', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->increments('id');
                $table->string('username', 64)->unique()->index();
                $table->string('email', 255);
                $table->string('name', 255)->nullable()->default(null);
                $table->unsignedTinyInteger('gender')->default(0);
                $table->string('password', 255);
                $table->string('password2', 255);
                $table->text('avatar')->nullable()->default(null);
                $table->string('lang', 24)->default('en');
                $table->decimal('credit_1', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('credit_2', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('credit_3', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('credit_4', 18, \App\Constants::$decimal_point)->default(0);
				$table->unsignedInteger('bank_id')->nullable()->default(null);
                $table->string('bank_account_name', 255)->nullable()->default(null);
                $table->string('bank_account_number', 255)->nullable()->default(null);
                $table->unsignedInteger('sponsor_user_id')->nullable()->default(null);
                $table->unsignedInteger('placement_user_id')->nullable()->default(null);
                $table->unsignedInteger('left_user_id')->nullable()->default(null);
                $table->unsignedInteger('right_user_id')->nullable()->default(null);
                $table->unsignedInteger('country_id')->nullable()->default(null);
                $table->string('national_id', 255)->nullable()->default(null);
                $table->unsignedInteger('contact_country_id')->nullable()->default(null);
                $table->string('contact_number', 64)->nullable()->default(null);
                $table->date('date_of_birth')->nullable()->default(null);
				$table->unsignedBigInteger('sponsor_size')->default(0);
				$table->unsignedBigInteger('direct_sponsor_pax')->default(0);
                $table->unsignedBigInteger('facebook_id')->nullable()->default(null);
                $table->unsignedBigInteger('google_id')->nullable()->default(null);
                $table->unsignedTinyInteger('first_login')->default(0);
                $table->unsignedTinyInteger('freeze')->default(0);
				$table->longText('sponsor_ids')->nullable()->default(null);
                $table->longText('placement_ids')->nullable()->default(null);
                $table->dateTime('new_login_at')->nullable()->default(null);
                $table->dateTime('last_login_at')->nullable()->default(null);
                $table->rememberToken();
                $table->timestamps();

            });
            Schema::table('user', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('country')->onDelete('set null');
                $table->foreign('contact_country_id')->references('id')->on('country')->onDelete('set null');
				$table->foreign('bank_id')->references('id')->on('bank')->onDelete('set null');
                $table->foreign('sponsor_user_id')->references('id')->on('user')->onDelete('set null');
                $table->foreign('placement_user_id')->references('id')->on('user')->onDelete('set null');
                $table->foreign('left_user_id')->references('id')->on('user')->onDelete('set null');
                $table->foreign('right_user_id')->references('id')->on('user')->onDelete('set null');
            });

            Schema::create('password_resets', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->string('email', 64)->index();
                $table->string('token')->index();
                $table->timestamp('created_at')->nullable();
            });
        } else if ($type == 2) {
            $tables = ['password_resets', 'user'];
            $this->dropForeignsAndTables($tables);
        }
    }

    public function makeUserBonus($type) {
        if ($type == 1) {
            Schema::create('user_bonus', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->bigIncrements('id');
                $table->unsignedInteger('user_id')->nullable()->default(null);
                $table->unsignedInteger('bonus_type');
                $table->decimal('amount', 18, \App\Constants::$decimal_point);
                $table->text('params')->nullable()->default(null);
                $table->uuid('related_key')->nullable()->default(null);
                $table->timestamps();
            });
            Schema::table('user_bonus', function (Blueprint $table) {
                $table->foreign('user_id')->references('id')->on('user')->onDelete('set null');
            });
        } else if ($type == 2) {
            $tables = ['user_bonus'];
            $this->dropForeignsAndTables($tables);
        }
    }

    public function makeUserTransaction($type) {
        if ($type == 1) {
            Schema::create('user_transactions', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->bigIncrements('id');
                $table->unsignedInteger('user_id')->nullable()->default(null);
                $table->unsignedTinyInteger('credit_type');
                $table->unsignedInteger('transaction_type');
                $table->decimal('amount', 18, \App\Constants::$decimal_point);
                $table->unsignedTinyInteger('adjust_description')->default(0);
                $table->string('description_en', 255)->nullable()->default(null);
                $table->string('description_cn', 255)->nullable()->default(null);
                $table->text('remark')->nullable()->default(null);
                $table->text('params')->nullable()->default(null);
                $table->uuid('related_key')->nullable()->default(null);
                $table->timestamps();
            });
            Schema::table('user_transactions', function (Blueprint $table) {
                $table->foreign('user_id')->references('id')->on('user')->onDelete('set null');
            });
        } else if ($type == 2) {
            $tables = ['user_transactions'];
            $this->dropForeignsAndTables($tables);
        }
    }

    public function makeUserTopup($type) {
        if ($type == 1) {
            Schema::create('user_topup', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->bigIncrements('id');
                $table->unsignedInteger('user_id')->nullable()->default(null);
                $table->unsignedInteger('admin_id')->nullable()->default(null);
                $table->unsignedInteger('country_id')->nullable()->default(null);
                $table->unsignedInteger('bank_id')->nullable()->default(null);
                $table->unsignedInteger('company_bank_id')->nullable()->default(null);
                $table->decimal('currency_actual_rate', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('currency_in_rate', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('currency_out_rate', 18, \App\Constants::$decimal_point)->default(0);
                $table->string('bank_name_en', 255)->nullable()->default(null);
                $table->string('bank_name_cn', 255)->nullable()->default(null);
                $table->string('bank_account_name', 255)->nullable()->default(null);
                $table->string('bank_account_number', 255)->nullable()->default(null);
                $table->unsignedTinyInteger('credit_type');
                $table->decimal('amount', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('admin_fees', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('amount_after_admin_fees', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('local_currency_amount', 18, \App\Constants::$decimal_point)->default(0);
                $table->text('receipt')->nullable()->default(null);
                $table->date('admin_response_date')->nullable()->default(null);
                $table->text('admin_remark')->nullable()->default(null);
                $table->text('user_remark')->nullable()->default(null);
                $table->unsignedTinyInteger('status')->default(0);
				$table->uuid('related_key')->nullable()->default(null);
				$table->text('params')->nullable()->default(null);
                $table->timestamps();
            });
            Schema::table('user_topup', function (Blueprint $table) {
                $table->foreign('user_id')->references('id')->on('user')->onDelete('set null');
                $table->foreign('admin_id')->references('id')->on('admin')->onDelete('set null');
                $table->foreign('country_id')->references('id')->on('country')->onDelete('set null');
                $table->foreign('bank_id')->references('id')->on('bank')->onDelete('set null');
                $table->foreign('company_bank_id')->references('id')->on('company_bank')->onDelete('set null');
            });
			Schema::create('user_topup_receipts', function (Blueprint $table) {
				$table->engine = 'innodb';
				$table->bigIncrements('id');
				$table->unsignedInteger('user_id')->nullable()->default(null);
				$table->unsignedBigInteger('user_topup_id')->nullable()->default(null);
				$table->text('receipt')->nullable()->default(null);
				$table->timestamps();
			});
			Schema::table('user_topup_receipts', function (Blueprint $table) {
                $table->foreign('user_id')->references('id')->on('user')->onDelete('set null');
				$table->foreign('user_topup_id')->references('id')->on('user_topup')->onDelete('set null');
            });
        } else if ($type == 2) {
            $tables = ['user_topup', 'user_topup_receipts'];
            $this->dropForeignsAndTables($tables);
        }
    }

    public function makeUserWithdrawal($type) {
        if ($type == 1) {
            Schema::create('user_withdrawal', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->bigIncrements('id');
                $table->unsignedInteger('user_id')->nullable()->default(null);
                $table->unsignedInteger('admin_id')->nullable()->default(null);
                $table->unsignedInteger('country_id')->nullable()->default(null);
                $table->unsignedInteger('bank_id')->nullable()->default(null);
                $table->decimal('currency_actual_rate', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('currency_in_rate', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('currency_out_rate', 18, \App\Constants::$decimal_point)->default(0);
                $table->string('bank_name_en', 255)->nullable()->default(null);
                $table->string('bank_name_cn', 255)->nullable()->default(null);
                $table->string('bank_account_name', 255)->nullable()->default(null);
                $table->string('bank_account_number', 255)->nullable()->default(null);
                $table->unsignedTinyInteger('credit_type');
                $table->decimal('amount', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('admin_fees', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('amount_after_admin_fees', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('local_currency_amount', 18, \App\Constants::$decimal_point)->default(0);
                $table->date('admin_response_date')->nullable()->default(null);
                $table->text('admin_remark')->nullable()->default(null);
                $table->text('user_remark')->nullable()->default(null);
                $table->unsignedTinyInteger('status')->default(0);
                $table->timestamps();
            });
            Schema::table('user_withdrawal', function (Blueprint $table) {
                $table->foreign('user_id')->references('id')->on('user')->onDelete('set null');
                $table->foreign('admin_id')->references('id')->on('admin')->onDelete('set null');
                $table->foreign('country_id')->references('id')->on('country')->onDelete('set null');
                $table->foreign('bank_id')->references('id')->on('bank')->onDelete('set null');
            });
        } else if ($type == 2) {
            $tables = ['user_withdrawal'];
            $this->dropForeignsAndTables($tables);
        }
    }

    public function makeUserCreditManage($type) {
        if ($type == 1) {
            Schema::create('credit_manage', function (Blueprint $table) {
                $table->engine = 'innodb';
                $table->bigIncrements('id');
                $table->unsignedInteger('user_id')->nullable()->default(null);
                $table->unsignedInteger('admin_id')->nullable()->default(null);
                $table->unsignedTinyInteger('action_type');
                $table->decimal('amount', 18, \App\Constants::$decimal_point)->default(0);
                $table->decimal('cash_received', 18, \App\Constants::$decimal_point)->default(0);
                $table->unsignedTinyInteger('adjust_description')->default(0);
                $table->string('description_en', 255)->nullable()->default(null);
                $table->string('description_cn', 255)->nullable()->default(null);
                $table->text('remark')->nullable()->default(null);
                $table->timestamps();
            });
            Schema::table('credit_manage', function (Blueprint $table) {
                $table->foreign('user_id')->references('id')->on('user')->onDelete('set null');
                $table->foreign('admin_id')->references('id')->on('admin')->onDelete('set null');
            });
        } else if ($type == 2) {
            $tables = ['credit_manage'];
            $this->dropForeignsAndTables($tables);
        }
    }
	
	public function makeBranch($type) {
        if ($type == 1) {
            Schema::create('branch', function (Blueprint $table) {
				$table->engine = 'innodb';
				$table->increments('id');
				$table->string('name_en', 255);
				$table->string('name_cn', 255)->nullable()->default(null);
				$table->string('email', 255)->nullable()->default(null);
				$table->string('contact_number', 255)->nullable()->default(null);
				$table->unsignedInteger('country_id')->nullable()->default(null);
				$table->text('address')->nullable()->default(null);
				$table->string('latitude', 255)->nullable()->default(null);
				$table->string('longitude', 255)->nullable()->default(null);
				$table->timestamps();
				$table->softDeletes();

				$table->foreign('country_id')->references('id')->on('country')->onDelete('set null');
			});
        } else if ($type == 2) {
            $tables = ['branch'];
            $this->dropForeignsAndTables($tables);
        }
    }
}
