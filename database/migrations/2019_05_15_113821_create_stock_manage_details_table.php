<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockManageDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_manage_details', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->unsignedBigInteger('stock_manage_id');
            $table->unsignedInteger('branch_stock_id');
            $table->decimal('quantity', 18, \App\Constants::$decimal_point);
            $table->timestamps();

            $table->foreign('stock_manage_id')->references('id')->on('stock_manages')->onDelete('cascade');
            $table->foreign('branch_stock_id')->references('id')->on('branch_stock');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_manage_details');
    }
}
