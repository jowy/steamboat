<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentMethod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->decimal('pax_total', 18, \App\Constants::$decimal_point)->default(0)->after('per_pax_price');
            $table->decimal('ala_carte_total', 18, \App\Constants::$decimal_point)->default(0)->after('pax_total');
            $table->decimal('sub_total', 18, \App\Constants::$decimal_point)->default(0)->after('ala_carte_total');

            $table->string('tax_name', 255)->nullable()->default(null)->after('per_pax_price');
            $table->decimal('tax_percentage', 5, 2)->default(0)->after('tax_name');
            $table->decimal('tax_total', 18, \App\Constants::$decimal_point)->default(0)->after('tax_percentage');
            $table->decimal('discount_total', 18, \App\Constants::$decimal_point)->default(0)->after('tax_total');

            $table->unsignedTinyInteger('payment_method')->default(0)->after('grand_total');
        });

        \DB::table('sales')->update(['payment_method' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->dropColumn('pax_total');
            $table->dropColumn('ala_carte_total');
            $table->dropColumn('sub_total');

            $table->dropColumn('tax_name');
            $table->dropColumn('tax_percentage');
            $table->dropColumn('tax_total');
            $table->dropColumn('discount_total');

            $table->dropColumn('payment_method');
        });
    }
}
