<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditBranchStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branch_stock', function (Blueprint $table) {
            $table->decimal('quantity', 18, \App\Constants::$decimal_point)->default(0.00)->change();
        });

        Schema::table('branch_stock_transaction', function (Blueprint $table) {
            $table->decimal('quantity', 18, \App\Constants::$decimal_point)->default(0.00)->change();
        });

        Schema::table('order_details', function (Blueprint $table) {
            $table->decimal('quantity', 18, \App\Constants::$decimal_point)->default(0.00)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branch_stock', function (Blueprint $table) {
            $table->bigInteger('quantity')->default(0)->change();
        });

        Schema::table('branch_stock_transaction', function (Blueprint $table) {
            $table->bigInteger('quantity')->default(0)->change();
        });

        Schema::table('order_details', function (Blueprint $table) {
            $table->integer('quantity')->default(0)->change();
        });
    }
}
