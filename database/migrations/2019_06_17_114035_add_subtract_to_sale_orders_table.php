<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubtractToSaleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_orders', function (Blueprint $table) {
            $table->unsignedInteger('branch_stock_id')->nullable()->change();
            $table->unsignedInteger('subtract_branch_stock_id')->nullable()->after('branch_stock_id');
            $table->unsignedInteger('subtract_product_id')->nullable()->after('branch_stock_id');
            $table->unsignedInteger('product_id')->nullable()->after('branch_stock_id');
            $table->unsignedInteger('subtract_product_name')->nullable()->after('product_name');
            $table->integer('subtract_total_quantity')->nullable()->after('quantity');

            $table->foreign('subtract_branch_stock_id')->references('id')->on('branch_stock')->onDelete('set null');
            $table->foreign('subtract_product_id')->references('id')->on('product')->onDelete('set null');
            $table->foreign('product_id')->references('id')->on('product')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_orders', function (Blueprint $table) {
            $table->dropForeign('sale_orders_product_id_foreign');
            $table->dropForeign('sale_orders_subtract_branch_stock_id_foreign');
            $table->dropForeign('sale_orders_subtract_product_id_foreign');
            $table->dropColumn(['subtract_branch_stock_id', 'subtract_product_id', 'product_id', 'subtract_product_name', 'subtract_total_quantity']);
            $table->unsignedInteger('branch_stock_id')->change();
        });
    }
}
