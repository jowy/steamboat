<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->engine = 'innodb';
            $table->increments('id');
            $table->unsignedInteger('sorting')->default(0);
            $table->string('product_code', 64)->nullable()->default(null);
            $table->string('product_name_en', 64)->nullable()->default(null);
            $table->string('product_name_cn', 64)->nullable()->default(null);
            $table->string('product_short_description_en', 255)->nullable()->default(null);
            $table->string('product_short_description_cn', 255)->nullable()->default(null);
            $table->text('product_description_en')->nullable()->default(null);
            $table->text('product_description_cn')->nullable()->default(null);
            $table->text('product_image_en')->nullable()->default(null);
            $table->text('product_image_cn')->nullable()->default(null);
            $table->decimal('product_price', 18, \App\Constants::$decimal_point)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('product_category', function (Blueprint $table) {
            $table->engine = 'innodb';
            $table->increments('id');
            $table->unsignedInteger('product_id')->nullable()->default(null);
            $table->unsignedInteger('category_id')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('product')->onDelete('set null');
            $table->foreign('category_id')->references('id')->on('category')->onDelete('set null');
        });

        Schema::create('product_images', function (Blueprint $table) {
            $table->engine = 'innodb';
            $table->increments('id');
            $table->unsignedInteger('product_id')->nullable()->default(null);
            $table->text('path')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('product')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_images');
        Schema::dropIfExists('product_category');
        Schema::dropIfExists('product');
    }
}
