<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStockForBranch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_stock', function (Blueprint $table) {
            $table->engine = 'innodb';
            $table->increments('id');
            $table->unsignedInteger('branch_id')->nullable()->default(null);
            $table->unsignedInteger('product_id')->nullable()->default(null);
            $table->string('product_name_en', 255)->nullable()->default(null);
            $table->string('product_name_cn', 255)->nullable()->default(null);
            $table->bigInteger('quantity')->default(0);
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branch')->onDelete('set null');
            $table->foreign('product_id')->references('id')->on('product')->onDelete('set null');
        });
        Schema::create('branch_stock_transaction', function (Blueprint $table) {
            $table->engine = 'innodb';
            $table->increments('id');
            $table->unsignedInteger('branch_id')->nullable()->default(null);
            $table->unsignedInteger('product_id')->nullable()->default(null);
            $table->unsignedInteger('stock_id')->nullable()->default(null);
            $table->string('product_name_en', 255)->nullable()->default(null);
            $table->string('product_name_cn', 255)->nullable()->default(null);
            $table->unsignedInteger('transaction_type');
            $table->bigInteger('quantity')->default(0);
            $table->text('remark')->nullable()->default(null);
            $table->text('params')->nullable()->default(null);
            $table->uuid('related_key')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branch')->onDelete('set null');
            $table->foreign('product_id')->references('id')->on('product')->onDelete('set null');
            $table->foreign('stock_id')->references('id')->on('branch_stock')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_stock_transaction');
        Schema::dropIfExists('branch_stock');
    }
}
