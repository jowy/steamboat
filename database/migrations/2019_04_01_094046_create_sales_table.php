<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->unsignedInteger('branch_id')->nullable();
            $table->unsignedBigInteger('staff_id')->nullable();
            $table->unsignedBigInteger('branch_table_id')->nullable();
            $table->string('table_no');
            $table->integer('table_duration');
            $table->string('sale_ref_id');
            $table->integer('total_pax');
            $table->decimal('per_pax_price', 18, \App\Constants::$decimal_point)->default(0);
            $table->decimal('grand_total', 18, \App\Constants::$decimal_point)->default(0);
            $table->decimal('paid', 18, \App\Constants::$decimal_point)->default(0);
            $table->boolean('is_paid')->default(false);
            $table->dateTime('start_at');
            $table->dateTime('finish_at');
            $table->timestamps();
            $table->softDeletes();

            $table->index(['branch_id', 'staff_id', 'branch_table_id', 'sale_ref_id']);
            $table->foreign('branch_id')->references('id')->on('branch')->onDelete('cascade');
            $table->foreign('staff_id')->references('id')->on('staff')->onDelete('set null');
            $table->foreign('branch_table_id')->references('id')->on('branch_tables')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
