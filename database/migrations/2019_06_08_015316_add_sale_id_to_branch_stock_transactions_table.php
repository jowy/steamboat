<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSaleIdToBranchStockTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branch_stock_transaction', function (Blueprint $table) {
            $table->unsignedBigInteger('sale_id')->nullable()->after('stock_id');

            $table->foreign('sale_id')->references('id')->on('sales')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branch_stock_transaction', function (Blueprint $table) {
             $table->dropForeign('branch_stock_transaction_sale_id_foreign');

            $table->dropColumn('sale_id');
        });
    }
}
