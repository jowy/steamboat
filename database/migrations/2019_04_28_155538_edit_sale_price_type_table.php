<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditSalePriceTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_details', function (Blueprint $table) {
            $table->decimal('per_pax_price', 18, \App\Constants::$decimal_point)->default(0)->after('pax');
            $table->string('price_type')->after('pax_type');
        });

        /*Schema::table('sales', function (Blueprint $table) {
            $table->dropColumn('per_pax_price');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_details', function (Blueprint $table) {
            $table->dropColumn('per_pax_price');
            $table->dropColumn('price_type');
        });

        /*Schema::table('sales', function (Blueprint $table) {
            $table->decimal('per_pax_price', 18, \App\Constants::$decimal_point)->default(0);
        });*/
    }
}
