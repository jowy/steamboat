<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlaCarteToSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->text('ala_carte_remark')->nullable()->after('is_join_sale');
            $table->string('ala_carte_name')->nullable()->after('is_join_sale');
            $table->boolean('is_ala_carte_sale')->default(false)->after('is_join_sale');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->dropColumn(['is_ala_carte_sale', 'ala_carte_remark', 'ala_carte_name']);
            /*$table->dropColumn('ala_carte_remark');
            $table->dropColumn('ala_carte_name');*/
        });
    }
}
