<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchWalletTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_wallet_transactions', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->unsignedInteger('branch_id')->nullable();
            $table->unsignedInteger('transaction_type');
            $table->date('daily_sale_date')->nullable();
            $table->decimal('amount', 18, \App\Constants::$decimal_point);
            $table->unsignedTinyInteger('adjust_description')->default(0);
            $table->string('description', 255)->nullable()->default(null);
            $table->text('remark')->nullable()->default(null);
            $table->text('params')->nullable()->default(null);
            $table->uuid('related_key')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branch')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_wallet_transactions');
    }
}
