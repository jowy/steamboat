<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_orders', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->unsignedBigInteger('sale_id');
            $table->unsignedInteger('branch_stock_id');
            $table->string('product_name');
            $table->integer('quantity');
            $table->decimal('ala_carte_price', 18, \App\Constants::$decimal_point)->default(0);
            $table->decimal('total_price', 18, \App\Constants::$decimal_point)->default(0);
            $table->timestamps();

            $table->index(['sale_id', 'branch_stock_id']);
            $table->foreign('sale_id')->references('id')->on('sales')->onDelete('cascade');
            $table->foreign('branch_stock_id')->references('id')->on('branch_stock');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_orders');
    }
}
