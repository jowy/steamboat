<?php

function pushFlash($key, $value) {
    $values = Session::get($key, []);
    $values[] = $value;
    Session::flash($key, $values);
}

function addError($msg) {
    if (!is_array($msg)) {
        pushFlash('flash_error', $msg);
    } else {
        foreach ($msg as $key => $var) {
            if (is_array($var)) {
                foreach ($var as $k => $v) {
                    pushFlash('flash_error', $v);
                }
            } else {
                pushFlash('flash_error', $var);
            }
        }
    }
}

function addInfo($msg) {
    if (!is_array($msg)) {
        pushFlash('flash_info', $msg);
    } else {
        foreach ($msg as $key => $var) {
            if (is_array($var)) {
                foreach ($var as $k => $v) {
                    pushFlash('flash_info', $v);
                }
            } else {
                pushFlash('flash_info', $var);
            }
        }
    }
}

function addSuccess($msg) {
    if (!is_array($msg)) {
        pushFlash('flash_success', $msg);
    } else {
        foreach ($msg as $key => $var) {
            if (is_array($var)) {
                foreach ($var as $k => $v) {
                    pushFlash('flash_success', $v);
                }
            } else {
                pushFlash('flash_success', $var);
            }
        }
    }
}

function addValidatorMsg($arr) {
    foreach ($arr->getMessages() as $key => $var) {
        foreach ($var as $k => $v) {
            addError($v);
        }
    }
}

function touchFolder($path) {
    $path = public_path($path);

    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }
}

function generateRandomUniqueName($length = 40) {
    return str_random($length);
}

function generateRandomHtmlId($length = 20) {
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getValidImageMimeType() {
    return array(
        'image/gif', 'image/jpg', 'image/jpeg', 'image/png'
    );
}

function isImage($name) {
    if (\Input::hasFile($name)) {
        $images_mimes = getValidImageMimeType();

        if (in_array(\Input::file($name)->getMimeType(), $images_mimes)) {
            return true;
        }
    }

    return false;
}


function convertMimeToExt($mime) {
    if ($mime == 'image/jpeg')
        $extension = '.jpg';
    elseif ($mime == 'image/png')
        $extension = '.png';
    elseif ($mime == 'image/gif')
        $extension = '.gif';
    else
        $extension = '';

    return $extension;
}

function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds') {
    $sets = array();
    if (strpos($available_sets, 'l') !== false)
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    if (strpos($available_sets, 'u') !== false)
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    if (strpos($available_sets, 'd') !== false)
        $sets[] = '23456789';
    if (strpos($available_sets, 's') !== false)
        $sets[] = '!@#$%&*?';

    $all = '';
    $password = '';
    foreach ($sets as $set) {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }

    $all = str_split($all);
    for ($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];

    $password = str_shuffle($password);

    if (!$add_dashes)
        return $password;

    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while (strlen($password) > $dash_len) {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}

function notEmpty($value) {
    if ($value != '' && $value != null && $value != false) {
        return true;
    } else {
        return false;
    }
}

function isEmpty($value) {
    return notEmpty($value) === true ? false : true;
}

function notDigit($value) {
    if ($value != '' && $value != null && $value != false && $value != true && $value >= 0 && ctype_digit((string)$value)) {
        return false;
    } else {
        return true;
    }
}

function isDigit($value) {
    if ($value != '' && $value != null && $value != false && $value != true && $value >= 0 && ctype_digit((string)$value)) {
        return true;
    } else {
        return false;
    }
}

function isFund($value) {
    return preg_match_all('/[\d](.[\d]{1,2})?/im' ,$value) ? true : false;
}

function notFund($value) {
    return preg_match_all('/[\d](.[\d]{1,2})?/im' ,$value) ? false : true;
}

function isPercentage($value) {
    if (is_numeric($value) && $value <= 100) {
        return true;
    } else {
        return false;
    }
}

function notPercentage($value) {
    if (is_numeric($value) && $value <= 100) {
        return false;
    } else {
        return true;
    }
}

function isNumber($value) {
    return ctype_digit((string)$value) ? true : false;
}

function notNumber($value) {
    return ctype_digit((string)$value) ? false : true;
}

function isPost() {
    return strtolower(request()->method()) == 'post' ? true : false;
}

function isAjax() {
    return request()->ajax() ? true : false;
}

function string_contains($str, array $arr) {
    foreach($arr as $a) {
        if (mb_stripos($str,$a) !== false) return true;
    }
    return false;
}

function writeErrorLog($log) {
    if ($log instanceof \Exception) {
        Log::error(
            "ERROR CODE = " . $log->getCode() . ": \n" . "Unexpected ERROR on " . $log->getFile() . " LINE: " . $log->getLine() . ", " . $log->getMessage() . "\n"
        );
    } else {
        Log::error($log);
    }
}

function toFund($value) {
    return number_format($value, 2);
}

function buildLinkHtml($arr) {
    $url = isset($arr['url']) ? $arr['url'] : '#';
    $description = isset($arr['description']) ? $arr['description'] : 'button';
    $color = isset($arr['color']) ? $arr['color'] : '';
    $target = isset($arr['target']) ? $arr['target'] : '_self';
    $is_button = isset($arr['is_button']) && $arr['is_button'] === false ? '' : 'btn btn-primary';

    return sprintf('<a href="%s" class="%s %s" target="%s">%s</a>', $url, $is_button, $color, $target, $description);
}

function buildRemoteLinkHtml($arr) {
    $url = isset($arr['url']) ? $arr['url'] : '#';
    $description = isset($arr['description']) ? $arr['description'] : 'button';
    $color = isset($arr['color']) ? $arr['color'] : '';
    $modal = isset($arr['modal']) ? $arr['modal'] : '#remote-modal-large';

    return sprintf('<a href="%s" data-toggle="modal" data-target="%s" class="btn btn-primary %s">%s</a>', $url, $modal, $color, $description);
}

function buildRemoteLink($arr) {
    $url = isset($arr['url']) ? $arr['url'] : '#';
    $description = isset($arr['description']) ? $arr['description'] : 'button';
    $color = isset($arr['color']) ? $arr['color'] : '';
    return sprintf('<a href="%s" class="btn btn-primary %s">%s</a>', $url, $color, $description);
}

function buildConfirmationLinkHtml($arr) {
    $url = isset($arr['url']) ? $arr['url'] : '#';
    $description = isset($arr['description']) ? $arr['description'] : 'button';
    $color = isset($arr['color']) ? $arr['color'] : '';
    $title = isset($arr['title']) ? $arr['title'] : trans('common.confirm') . '?';
    $content = isset($arr['content']) ? $arr['content'] : trans('common.are_you_sure') . '?';
    $redirect = isset($arr['redirect']) && in_array($arr['redirect'], [true, 1, '1', 'yes']) ? 'yes' : 'no';
    $modal = isset($arr['modal']) ? $arr['modal'] : '#confirm-modal';
    $successcall = isset($arr['successcall']) ? $arr['successcall'] : 'default';

    return sprintf('<a data-href="%s" class="btn btn-primary btn-danger %s" data-redirect="%s" data-toggle="modal" data-target="%s" data-header="%s" data-body="%s" data-successcall="%s">%s</a>', $url, $color, $redirect, $modal, $title, $content, $successcall, $description);
}

function makeResponse($code, $message = null, $data = array()) {
	if ($message instanceof \Exception) {
        $debug = config('env.APP_DEBUG');
        if ($debug === true) {
            $message = $message->getFile() . ' / ' . $message->getLine() . ' . ' . $message->getMessage();
        } else {
            $message = $message->getMessage();
        }
    }

    if ($code === true) {
	    $code = 200;
    } else if ($code === false) {
	    $code = 422;
    }

	if (request()->ajax() || request()->wantsJson()) {
		if ($message != null) {
			$data['message'] = $message;
		} else {
			switch ($code) {
				case 422:
					$data['message'] = trans('common.something_went_wrong');
				break;
				case 200:
					$data['message'] = trans('common.operation_success');
				break;
				default:
					$data['message'] = trans('common.unknown_error');
				break;
			}
		}
		
		return response()->json($data)->setStatusCode($code);
	} else {
		switch ($code) {
			case 200:
				if ($message != null) {
					addSuccess($message);
				} else {
					addSuccess(trans('common.operation_success'));
				}
			break;
			default:
				if ($message != null) {
					addError($message);
				} else {
					addError(trans('common.something_went_wrong'));
				}
				
				if (isset($data['errors'])) {
					foreach ($data['errors'] as $field => $data1) {
						if (is_array($data1)) {
							foreach ($data1 as $data2) {
								addError($data2);
							}
						} else {
							addError($data1);
						}
					}
				}
			break;
		}
		
		return redirect()->back()->withInput();
	}
}

function fundFormat($fund, $decimal = 'default') {
    if (!is_numeric($fund)) {
        $fund = 0;
    }

    if ($decimal === 'default') {
        $decimal = \App\Constants::$decimal_point;
    }

    return number_format($fund, $decimal);
}

function isImageExtension($ext, $trim = false) {
    $imgs_ext = ['png', 'jpg', 'jpeg', 'bmp', 'gif'];

    if ($trim === true) {
        $name = strtolower($ext);
        $split = explode('.', $name);
        return in_array(end($split), $imgs_ext);
    } else {
        return in_array(strtolower($ext), $imgs_ext);
    }
}

function slugify($text) {
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

// a function for comparing two float numbers
// float 1 - The first number
// operator - The operator. Valid options are =, <=, <, >=, >, <>, eq, lt, lte, gt, gte, ne
// float 2 - The number to compare against the first
function compareFloatNumbers($float1, $operator, $float2)
{
    switch (\App\Constants::$decimal_point) {
        case 1: $epsilon = 0.1; break;
        case 2: $epsilon = 0.01; break;
        case 3: $epsilon = 0.001; break;
        case 4: $epsilon = 0.0001; break;
        case 5: $epsilon = 0.00001; break;
        case 6: $epsilon = 0.000001; break;
        case 7: $epsilon = 0.0000001; break;
        case 8: $epsilon = 0.00000001; break;
        case 9: $epsilon = 0.000000001; break;
        case 10: $epsilon = 0.0000000001; break;
    }

    $float1 = (float)$float1;
    $float2 = (float)$float2;

    switch ($operator)
    {
        // equal
        case "=":
        case "eq":
            {
                if (abs($float1 - $float2) < $epsilon) {
                    return true;
                }
                break;
            }
        // less than
        case "<":
        case "lt":
            {
                if (abs($float1 - $float2) < $epsilon) {
                    return false;
                }
                else
                {
                    if ($float1 < $float2) {
                        return true;
                    }
                }
                break;
            }
        // less than or equal
        case "<=":
        case "lte":
            {
                if (compareFloatNumbers($float1, '<', $float2) || compareFloatNumbers($float1, '=', $float2)) {
                    return true;
                }
                break;
            }
        // greater than
        case ">":
        case "gt":
            {
                if (abs($float1 - $float2) < $epsilon) {
                    return false;
                }
                else
                {
                    if ($float1 > $float2) {
                        return true;
                    }
                }
                break;
            }
        // greater than or equal
        case ">=":
        case "gte":
            {
                if (compareFloatNumbers($float1, '>', $float2) || compareFloatNumbers($float1, '=', $float2)) {
                    return true;
                }
                break;
            }
        case "<>":
        case "!=":
        case "ne":
            {
                if (abs($float1 - $float2) > $epsilon) {
                    return true;
                }
                break;
            }
        default:
            {
                die("Unknown operator '".$operator."' in compareFloatNumbers()");
            }
    }

    return false;
}

function addAuditLog($description, $function_tag, $operation, $model = null) {
    return \App\Models\AuditRecords::addLog($description, $function_tag, $operation, $model);
}

function dt($model) {
    return new \App\Helpers\DataTableHelper($model);
}

function safeFileFormat() {
    return ['png', 'jpg', 'jpeg', 'bmp', 'gif', 'pdf', 'xlsx', 'docx'];
}

function isSafeFileFormat($ext) {
    $ext = str_replace('.', '', $ext);
    return in_array($ext, safeFileFormat());
}

function getClassName($class) {
    $str = get_class($class);
    $arr = explode('\\', $str);
    return end($arr);
}

function isValidImageExtension($name) {
    return ends_with($name, ['.jpg', '.jpeg', '.gif', '.jpg', '.png', '.bmp']);
}

function append_resource_names($str)
{
    return [
        'names' => [
            'index' => $str.'.index',
            'create' => $str.'.create',
            'store' => $str.'.store',
            'show' => $str.'.show',
            'edit' => $str.'.edit',
            'update' => $str.'.update',
            'patch' => $str.'.patch',
            'destroy' => $str.'.destroy',
        ],
    ];
}
