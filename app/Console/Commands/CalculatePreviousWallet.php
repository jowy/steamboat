<?php

namespace App\Console\Commands;

use App\Models\Branch;
use App\Models\BranchWalletTransaction;
use App\Models\Sale;
use Illuminate\Console\Command;

class CalculatePreviousWallet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculate_previous_wallet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate branch`s previous sale and add back to wallet';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $begin = new \DateTime('2019-04-28');
        $end = new \DateTime('2019-05-14'); # It will stop at 13th instead of 14th
        $branches = Branch::all();

        try {
            \DB::beginTransaction();

            $interval = \DateInterval::createFromDateString('1 day');
            $period = new \DatePeriod($begin, $interval, $end);

            foreach ($period as $dt) {
                $runDate = $dt->format("Y-m-d");
                foreach ($branches as $branch) {
                    # To avoid duplicate cronjob
                    if (!BranchWalletTransaction::whereTransactionType(BranchWalletTransaction::TYPE_DAILY_SALES)->whereBranchId($branch->id)->whereDate('daily_sale_date', $runDate)->exists()) {
                        $grandTotalSales = Sale::whereBranchId($branch->id)->whereDate('start_at', $runDate)->sum('grand_total');

                        if ($grandTotalSales > 0) {
                            $grandTotalSales = abs($grandTotalSales);
                            $branch->credit_1 += $grandTotalSales;
                            $branch->save();
                        }

                        // Create branch wallet transaction
                        $branchWalletTransaction = new BranchWalletTransaction();
                        $branchWalletTransaction->branch_id = $branch->id;
                        $branchWalletTransaction->transaction_type = BranchWalletTransaction::TYPE_DAILY_SALES;
                        $branchWalletTransaction->daily_sale_date = $runDate;
                        $branchWalletTransaction->amount = $grandTotalSales;
                        $branchWalletTransaction->remark = 'Branch: ' . $branch->name_en . ' daily sales for ' . $runDate;
                        $branchWalletTransaction->save();
                    }
                }
            }

            \DB::commit();
            $this->info("Successfully " . $this->signature . ' at ' . now()->format('Y-m-d H:i:s'));
        } catch (\Exception $exception) {
            \DB::rollBack();
            \Log::info($this->signature . ': '. $exception->getMessage());
            $this->info("Failed to " . $this->signature . ' at ' . now()->format('Y-m-d H:i:s'));
        }
    }
}
