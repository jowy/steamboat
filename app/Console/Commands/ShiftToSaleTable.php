<?php

namespace App\Console\Commands;

use App\Models\Sale;
use App\Models\SaleTable;
use Illuminate\Console\Command;

class ShiftToSaleTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shiftToSaleTable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Shift all the sales to sale table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sales = Sale::orderBy('id', 'asc')->get();

        $isMultipleTable = false;
        $sameBranchId = null;
        $sameStaffId = null;
        $sameTotalPax = null;
        $sameGrandTotal = null;
        $samePaid = null;
        $sameStartAt = null;

        $sameDatas = [];
        $count = 0;
        foreach ($sales as $sale) {
            if ($isMultipleTable) {
                # Calculate the datetime different
                $startDateTime1 = new \DateTime($sameStartAt);
                $startDateTime2 = new \DateTime($sale->start_at);
                $startDateTimeDiff = $startDateTime2->getTimestamp() - $startDateTime1->getTimestamp();

                # Determine is within same sale
                if ($sale->branch_id == $sameBranchId && $sale->staff_id == $sameStaffId && $sale->total_pax == $sameTotalPax && $sale->grand_total == $sameGrandTotal && $sale->paid == $samePaid && $startDateTimeDiff <= 1) {
                    $sameDatas[$count][] = $sale->id;

                    $sameBranchId = $sale->branch_id;
                    $sameStaffId = $sale->staff_id;
                    $sameTotalPax = $sale->total_pax;
                    $sameGrandTotal = $sale->grand_total;
                    $samePaid = $sale->paid;
                    $sameStartAt = $sale->start_at;
                } else {
                    $isMultipleTable = false;
                    $count++;

                    $sameBranchId = $sale->branch_id;
                    $sameStaffId = $sale->staff_id;
                    $sameTotalPax = $sale->total_pax;
                    $sameGrandTotal = $sale->grand_total;
                    $samePaid = $sale->paid;
                    $sameStartAt = $sale->start_at;

                    $sameDatas[$count][] = $sale->id;
                }
            } else {
                $count++;
                $isMultipleTable = true;
                $sameBranchId = $sale->branch_id;
                $sameStaffId = $sale->staff_id;
                $sameTotalPax = $sale->total_pax;
                $sameGrandTotal = $sale->grand_total;
                $samePaid = $sale->paid;
                $sameStartAt = $sale->start_at;

                $sameDatas[$count][] = $sale->id;
            }
        }

        try {
            \DB::beginTransaction();

            foreach ($sameDatas as $sameData) {
                if (count($sameData) > 1) {
                    // Multiple
                    $isFirst = true;
                    $minId = min($sameData);
                    foreach ($sameData as $saleId) {
                        $sale = Sale::find($saleId);

                        # Shift the table id and no to the sale table
                        $saleTable = new SaleTable();
                        $saleTable->sale_id = $minId;
                        $saleTable->branch_table_id = $sale->branch_table_id;
                        $saleTable->table_no = $sale->table_no;
                        $saleTable->save();

                        # Delete all except the first one
                        if (!$isFirst) {
                            $sale->delete();
                        }
                        $isFirst = false;
                    }
                } else {
                    // Only one
                    $sale = Sale::find($sameData[0]);

                    # Shift the table id and no to the sale table
                    $saleTable = new SaleTable();
                    $saleTable->sale_id = $sale->id;
                    $saleTable->branch_table_id = $sale->branch_table_id;
                    $saleTable->table_no = $sale->table_no;
                    $saleTable->save();
                }
            }
            \DB::commit();
            echo "Shift success";
        } catch (\Exception $exception) {
            \DB::rollBack();
            \Log::info('shift_to_sale_table_failed: '. $exception->getMessage());
            echo "Failed to shift";
        }
    }
}
