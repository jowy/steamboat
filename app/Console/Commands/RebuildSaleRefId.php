<?php

namespace App\Console\Commands;

use App\Models\Sale;
use Illuminate\Console\Command;

class RebuildSaleRefId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rebuild_sale_ref_id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuild all sale ref id again';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sales = Sale::withTrashed()->get();

        try {
            \DB::beginTransaction();

            foreach ($sales as $sale) {
                $defaultCount = 100000;
                $totalCounts = Sale::where('id', '<=', $sale->id)->withTrashed()->count();

                $sale->sale_ref_id = ($defaultCount + $totalCounts + 1);
                $sale->save();
            }
            \DB::commit();
            $this->info("Successfully " . $this->signature . ' at ' . now()->format('Y-m-d H:i:s'));
        } catch (\Exception $exception) {
            \DB::rollBack();
            \Log::info($this->signature . ': '. $exception->getMessage());
            $this->info("Failed to " . $this->signature . ' at ' . now()->format('Y-m-d H:i:s'));
        }
    }
}
