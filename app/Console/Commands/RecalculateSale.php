<?php

namespace App\Console\Commands;

use App\Models\Sale;
use App\Models\SaleTable;
use Illuminate\Console\Command;

class RecalculateSale extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recalculate_sale';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recalculate sale';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            \DB::beginTransaction();

            foreach (Sale::orderBy('id', 'ASC')->get() as $sale) {
                $sale->recalculateSale();
                $sale->save();
            }

            \DB::commit();
            $this->info("Successfully " . $this->signature . ' at ' . now()->format('Y-m-d H:i:s'));
        } catch (\Exception $exception) {
            \DB::rollBack();
            \Log::info($this->signature . ': '. $exception->getMessage());
            $this->info("Failed to " . $this->signature . ' at ' . now()->format('Y-m-d H:i:s'));
        }
    }
}
