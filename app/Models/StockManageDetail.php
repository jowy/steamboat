<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockManageDetail extends Model
{
    public function branch_stock()
    {
        return $this->belongsTo(BranchStock::class, 'branch_stock_id');
    }
}
