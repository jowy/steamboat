<?php

namespace App\Models;

use Input;

class PermissionGroupPermission extends BaseModels {
    public function getAuditDescription() {
        return null;
    }

    public function needAudit() {
        return false;
    }

    protected $table = 'permission_groups_permissions';

    protected $fillable = [
        'permission_tag'
    ];

    protected $dates = ['deleted_at'];

    protected $rules = array(
        'permission_group_id' => 'required|exists:permission_groups,id',
        'permission_tag' => 'required|isPermissionTag',
    );

    public static function boot() {
        parent::boot();
    }

    public function permissionGroup() {
        return $this->belongsTo('App\Models\PermissionGroup', 'permission_group_id', 'id');
    }

    public function setPermissionTagAttribute($value) {
        $this->attributes['permission_tag'] = strtolower($value);
    }

    //All the permissions for backend should set at here
    public static function getPermissionsLists() {
        $lang = app()->getLocale();
        $arr = @include(base_path('resources/lang/' . $lang . '/permission.php'));

        if (!$arr || count($arr) < 0) {
            return [];
        }

        return $arr;
    }

    public static function isValidPermission($permission_tag) {
        $permission_tag = strtolower($permission_tag);

        if (array_key_exists($permission_tag, static::getPermissionsLists())) {
            return true;
        } else {
            return false;
        }
    }

}