<?php

namespace App\Models;

use Input;

class Setting extends BaseModels {

    const TYPE_TEXT = 1;
    const TYPE_TEXTAREA = 2;
    const TYPE_EDITOR = 3;
    const TYPE_SELECT = 4;
    const TYPE_IMAGE = 5;

    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->description_en;
    }

    static $settings = null;

    protected $table = 'setting';

    protected $fillable = [
        'value',
    ];

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    public static $rules = [
        'value' => '',
    ];

    public static function boot() {
        parent::boot();
    }

    public static function getSettings() {
        if (static::$settings == null) {
            $settings = array();
            foreach (static::all() as $key => $var) {
                $settings[$var->setting] = $var->value != null && $var->value != '' ? $var->value : $var->default_value;
            }

            static::$settings = $settings;
        }

        return static::$settings;
    }

    public function getValue() {
        if ($this->value == null || $this->value == '') {
            return $this->default_value;
        } else {
            return $this->value;
        }
    }

    public function getDescription() {
        if (app()->getLocale() == 'cn') {
            return $this->description_cn;
        } else {
            return $this->description_en;
        }
    }

    public function explainValue($hide_image = false, $shorten = false) {
        $params = $this->convertParams();

        switch ($this->type) {
            case static::TYPE_TEXT:
                return $shorten === true ? mb_substr($this->getValue(), 0, 20) : $this->getValue();
            break;
            case static::TYPE_TEXTAREA:
                return $shorten === true ? mb_substr(strip_tags($this->getValue()), 0, 20) : $this->getValue();
            break;
            case static::TYPE_EDITOR:
                return $shorten === true ? mb_substr(strip_tags($this->getValue()), 0, 20) : $this->getValue();
            break;
            case static::TYPE_SELECT:
                if ($this->getValue() == null) {
                    return null;
                }

                $options = array();

                if (isset($params['options'])) {
                    $options = $params['options'];
                }

                if (isset($options[$this->getValue()])) {
                    return $options[$this->getValue()];
                } else {
                    return trans('common.unknown');
                }
            break;
            case static::TYPE_IMAGE:
                if ($hide_image === true) {
                    return trans('common.image');
                } else {
                    return $this->getValue();
                }
            break;
        }
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_value') && Input::get('filter_value') != '') {
            $query->where('filter_value', 'like', '%' . Input::get('filter_value') . '%');
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }

}