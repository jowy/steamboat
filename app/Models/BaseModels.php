<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

abstract class BaseModels extends Model implements Auditable {
	
	public function needAudit() {
		return true;
	}

    public static function boot() {
		static::deleting(function ($m) {
			if ($m->needAudit() === true && !app()->runningInConsole() && defined('IN_ADMIN') && IN_ADMIN === true) {
				$admin = \Auth::guard('admin')->user();
				
				AuditRecords::addLog($admin->username . ' deleted ' . $m->getAuditDescription(), 'delete_' . $m->table, AuditRecords::OPERATION_DELETE, $m);
			}
		});
		
		static::created(function ($m) {
			if ($m->needAudit() === true && !app()->runningInConsole() && defined('IN_ADMIN') && IN_ADMIN === true) {
				$admin = \Auth::guard('admin')->user();
				
				AuditRecords::addLog($admin->username . ' created ' . $m->getAuditDescription(), 'create_' . $m->table, AuditRecords::OPERATION_CREATE, $m);
			}
		});
		
		static::updated(function ($m) {
			if ($m->needAudit() === true && !app()->runningInConsole() && defined('IN_ADMIN') && IN_ADMIN === true) {
				if ($m->exists() && $m->isDirty()) {
					$admin = \Auth::guard('admin')->user();

					AuditRecords::addLog($admin->username . ' edited ' . $m->getAuditDescription(), 'edit_' . $m->table, AuditRecords::OPERATION_UPDATE, $m);
				}
			}
		});
		
		static::bootTraits();
	}
	
    public function scopeGetRecords($query) {
        return $query;
    }

    public function setAttribute($key, $value) {
        $cc = Str::studly($key);
        $method = 'get' . $cc . 'Lists';
        if (method_exists(get_called_class(), $method)) {
            $arr = $this->$method();
            if (!array_key_exists($value, $arr)) {
                throw new \Exception(trans('common.invalid_attribute', ['attribute' => $cc]));
            }
        }

        parent::setAttribute($key, $value);
    }

    public function setParamsAttribute($value) {
        if (is_array($value)) {
            $this->attributes['params'] = json_encode($value);
        } else if (is_array(json_decode($value, true))) {
            $this->attributes['params'] = $value;
        } else if (is_null($value)) { 
			$this->attributes['params'] = null;
		} else {
            $this->attributes['params'] = json_encode($value);
        }
    }

    public function convertParams() {
        $params = json_decode($this->params, true);
        if (!is_array($params)) {
            $params = array();
        }

        return $params;
    }

    public function getCreatedTimesAgo() {
        return $this->created_at->diffForHumans();
    }

    public function getUpdatedTimesAgo() {
        return $this->updated_at->diffForHumans();
    }

    public function setReceiptAttribute($file) {
        if ($file instanceof \Illuminate\Http\UploadedFile) {
            $now = \Carbon\Carbon::now()->format('Y/m/');
            $path = 'uploads/receipt/' . $now;
            $filename = \Carbon\Carbon::now()->format('d_') . generateRandomUniqueName();

            if (isImageExtension($file->getClientOriginalExtension())) {
                $img = \Image::make($file);
                $mime = $img->mime();
                $ext = convertMimeToExt($mime);

                touchFolder($path);

                $img = $img->save($path . $filename . $ext, 90);
            } else {
                $ext = '.' . $file->getClientOriginalExtension();
                $file->move($path, $filename . $ext);
            }

            $this->attributes['receipt'] = $path . $filename . $ext;
        } else if (is_string($file)) {
            $this->attributes['receipt'] = $file;
        }
    }
}