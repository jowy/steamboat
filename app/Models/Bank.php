<?php

namespace App\Models;

use Input;

class Bank extends BaseModels {
    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->name_en;
    }

    protected $table = 'bank';

    protected $fillable = [
        'country_id',
        'name_en',
        'name_cn',
    ];

    public static $rules = [
        'country_id' => 'required|exists:country,id,status,1',
        'name_en' => 'required|min:1|max:255',
        'name_cn' => 'required|min:1|max:255',
    ];

    public static function boot() {
        parent::boot();
    }

    public function country() {
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    public function getName() {
        if (app()->getLocale() == 'cn') {
            return $this->name_cn;
        } else {
            return $this->name_en;
        }
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function scopeGetSelects($query) {
        $arr = array();
        foreach ($query->get() as $key => $var) {
            $arr[$var->id] = $var->getName();
        }

        return $arr;
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_country_id') && Input::get('filter_country_id') != '') {
            $query->where('country_id', '=', Input::get('filter_country_id'));
        }

        if (Input::filled('filter_name') && Input::get('filter_name') != '') {
            $query->where(function ($q) {
                $q->where('name_en', 'like', '%' . Input::get('filter_name') . '%')
                    ->orWhere('name_cn', 'like', '%' . Input::get('filter_name') . '%');
            });
        }

        if (Input::filled('filter_name_en') && Input::get('filter_name_en') != '') {
            $query->where('name_en', 'like', '%' . Input::get('filter_name_en') . '%');
        }

        if (Input::filled('filter_name_cn') && Input::get('filter_name_cn') != '') {
            $query->where('name_cn', 'like', '%' . Input::get('filter_name_cn') . '%');
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after_date')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_updated_before_date')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before_date') . ' 23:59:59');
        }
    }

}