<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleDetail extends Model
{
    public function getPriceTypeNameAttribute()
    {
        if (empty($this->price_type_name)) {
            if (!empty($this->price_type)) {
                $price = Price::whereType($this->price_type)->withTrashed()->first();
                return $price->name_en;
            }
            # Then will go for very first default stage price 29.90
            return 'Default Pax';
        }
        return $this->price_type_name;
    }

    public function getPerPaxPriceAttribute($value)
    {
        if ($this->price_type_name == 'Default Pax' && empty($this->price_type)) {
            return 29.90;
        }
        return $value;
    }
}
