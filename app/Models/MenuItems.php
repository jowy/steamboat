<?php

namespace App\Models;

use Input;

class MenuItems extends BaseModels {
    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->text_en;
    }

    const TYPE_DISABLE = 0;
    const TYPE_PAGE = 1;
    const TYPE_LINK = 2;

    const OPEN_IN_CURRENT_TAB = 0;
    const OPEN_IN_NEW_TAB = 1;

    protected $table = 'menu_items';

    protected $fillable = [
        'menu_id', 'parent_id', 'type', 'page_id', 'sorting', 'url', 'text_en', 'text_cn', 'open_in_new_tab'
    ];

    protected $dates = [];

    public static $rules = array(
        'menu_id' => 'required|exists:menu,id',
        'parent_id' => 'exists:menu_items,id',
        'type' => 'required|in:0,1,2',//0 mean disable, 1 mean page, 2 mean custom url
        'page_id' => 'required_if:type,1|exists:page,id',
        'sorting' => 'isNumber',
        'url' => 'required_if:type,2|url',
        'text_en' => 'required|max:255',
        'text_cn' => 'required|max:255',
        'open_in_new_tab' => 'required|in:0,1',//0 mean open in current tab, 1 mean open in new tab
    );

    public static function boot() {
        parent::boot();
    }

    public function menu() {
        return $this->belongsTo('App\Models\Menu', 'menu_id', 'id');
    }

    public function parent() {
        return $this->belongsTo('App\Models\MenuItems', 'parent_id', 'id');
    }

    public function sons() {
        return $this->hasMany('App\Models\MenuItems', 'parent_id', 'id')->orderBy('sorting', 'ASC');
    }

    public function page() {
        return $this->hasOne('App\Models\Page', 'id', 'page_id');
    }
    
    public function getText() {
        $field = 'text_' . app()->getLocale();
        if (isset($this->$field) && $this->$field) {
            return $this->$field;
        } else {
            return $this->text_en;
        }
    }

    public function getTextWithLink() {
        $text = $this->getText();

        if ($this->getLink() != null) {
            $text .= ' ( ' . $this->getLink() . ' )';
        }

        return $text;
    }

    public static function getTypeLists() {
        return [
            static::TYPE_DISABLE => trans('common.disable'),
            static::TYPE_PAGE => trans('common.page'),
            static::TYPE_LINK => trans('common.url'),
        ];
    }

    public static function getOpenInNewTabLists() {
        return [
            static::OPEN_IN_CURRENT_TAB => trans('common.no'),
            static::OPEN_IN_NEW_TAB => trans('common.yes'),
        ];
    }

    public function getOpenInNewTab($html = false) {
        switch ($this->open_in_new_tab) {
            case static::OPEN_IN_CURRENT_TAB:
                if ($html === true) {
                    return '_self';
                } else {
                    return false;
                }
                break;
            case static::OPEN_IN_NEW_TAB:
                if ($html === true) {
                    return '_blank';
                } else {
                    return true;
                }
                break;
            default:
                if ($html === true) {
                    return '_self';
                } else {
                    return null;
                }
                break;
        }
    }

    public function getLink($html = false) {
        switch ($this->type) {
            case static::TYPE_DISABLE: return $html === true ? 'javascript:;' : null; break;
            case static::TYPE_PAGE:
                if ($this->page) {
                    return route('user.page', ['page_tag' => $this->page->page_tag]);
                } else {
                    if ($html === true) {
                        return 'javascript:;';
                    } else {
                        return null;
                    }
                }
                break;
            case static::TYPE_LINK:
                if ($this->url && $this->url != null) {
                    return $this->url;
                } else {
                    if ($html === true) {
                        return 'javascript:;';
                    } else {
                        return null;
                    }
                }
            default: return $html === true ? 'javascript:;' : null; break;
        }
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function scopeGetFilteredResults($query) {
        return $query;
    }

}