<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    use SoftDeletes;

    const FREE_PAX_COUNT = 10;
    const PER_PAX_PRICE = 29.90;
    //const TABLE_DURATION = 7200; # In secs

    protected $dates = ['start_at', 'finish_at', 'created_at', 'updated_at'];

    public static function boot() {
        parent::boot();
        static::creating(function ($m) {
            /*if (!isset($m->sale_ref_id) || $m->sale_ref_id == false || $m->sale_ref_id == null || $m->sale_ref_id == '') {
                $c = 100000 + (static::count() + 1);

                $m->sale_ref_id = $c;
            }*/

            $startAt = \Carbon\Carbon::now();
            $finishAt =  \Carbon\Carbon::parse($startAt)->addSeconds($m->table_duration);

            $m->start_at = $startAt;
            $m->finish_at = $finishAt;
        });

        static::created(function ($model) {
            if (!isset($model->sale_ref_id) || $model->sale_ref_id == false || $model->sale_ref_id == null || $model->sale_ref_id == '') {
                $defaultCount = 100000;
                $totalCounts = Sale::where('id', '<=', $model->id)->withTrashed()->count();

                $model->sale_ref_id = ($defaultCount + $totalCounts + 1);
                $model->save();
            }
            if ($model->first_login == 0) {
//                \Mail::send('user.welcome_email', ['model' => $model], function ($m) use ($model) {
//                    //
//                    $m->from(config('env.MAIL_FROM_ADDRESS'), config('env.MAIL_FROM_NAME'));
//
//                    $m->to($model->email, $model->getName())->subject('Welcome from ' . config('env.APP_NAME'));
//
//                });
            }
        });
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function staff()
    {
        return $this->belongsTo(Staff::class, 'staff_id');
    }

    public function sale_tables()
    {
        return $this->hasMany(SaleTable::class, 'sale_id');
    }

    public function sale_details()
    {
        return $this->hasMany(SaleDetail::class, 'sale_id');
    }

    public function sale_orders()
    {
        return $this->hasMany(SaleOrder::class, 'sale_id');
    }

    public function totalPaxPriceWithoutFreePax()
    {
        $totalPriceWithoutFreePax = 0;
        foreach ($this->sale_details as $sale_detail) {
            $perPaxPrice = $sale_detail->per_pax_price;
            if ($sale_detail->pax_type == config('sale.pax_type.free') && $sale_detail->price_type == Price::ADULT_TYPE) {
                $perPaxPrice = SaleDetail::whereSaleId($this->id)->wherePaxType( config('sale.pax_type.pay'))->wherePriceType(Price::ADULT_TYPE)->first()->per_pax_price;
            }
            $totalPriceWithoutFreePax += ($sale_detail->pax * $perPaxPrice);
        }
        return (float) $totalPriceWithoutFreePax;
    }

    public function totalFreePaxPrice()
    {
        if ($saleDetail = $this->sale_details()->wherePaxType(config('sale.pax_type.free'))->wherePriceType(Price::ADULT_TYPE)->first()) {
            $adultPaxPrice = SaleDetail::whereSaleId($this->id)->wherePaxType( config('sale.pax_type.pay'))->wherePriceType(Price::ADULT_TYPE)->first()->per_pax_price;
            return (float) $saleDetail->pax * $adultPaxPrice;
        }
        return 0;
    }

    public function totalOrderPrice()
    {
        $totalPrice = 0;
        if (count($this->sale_orders) > 0) {
            foreach($this->sale_orders as $sale_order) {
                $totalPrice += $sale_order->total_price;
            }
            return (float) $totalPrice;
        }
        return $totalPrice;
    }

    public function calculateChange()
    {
        return (float) ($this->paid - $this->grand_total);
    }

    public static function calculateTotalPaxPrice($payPax, $perPaxPrice)
    {
        return (float) $payPax * $perPaxPrice;
    }

    public static function calculateFreePax($totalPax, $freePaxCount)
    {
        return (int) floor($totalPax / ($freePaxCount + 1));  # Add one is because start from the new one only consider free
    }

    public function scopeNotPaid($query)
    {
        return $query->whereIsPaid(false);
    }

    public function scopeNotJoinSale($query)
    {
        return $query->whereIsJoinSale(false);
    }

    public function scopeIsPaid($query)
    {
        return $query->whereIsPaid(true);
    }

    public function getAllTableAttribute()
    {
        return implode(", ", $this->sale_tables()->pluck("table_no")->toArray());
    }

    public function getRoundAdjustmentAttribute()
    {
        $roundAdjustment = ($this->grand_total - $this->sub_total);
        if ($this->tax_total > 0) {
            $roundAdjustment += $this->tax_total;
        }
        if ($this->discount_total > 0 && is_null($this->discount_percentage)) {
            $roundAdjustment += $this->discount_total;
        }
        return (float) $roundAdjustment;
    }

    public static function getPaymentMethodLists() {
        $arr = array();
        $arr[1] = trans('common.cash');
        $arr[2] = trans('common.credit_card');
        return $arr;
    }

    /**
     * Set the round up grand total.
     *
     * @param  string  $value
     * @return void
     */
    public function setGrandTotalAttribute($value)
    {
        $this->attributes['grand_total'] = self::customRound($value);
    }

    public function explainPaymentMethod() {
        $arr = static::getPaymentMethodLists();

        return isset($arr[$this->payment_method]) ? $arr[$this->payment_method] : trans('common.unknown');
    }

    public function explainDiscountType() {
        if (is_null($this->discount_percentage)) {
            return trans('common.fixed');
        } else {
            return trans('common.percentage');
        }
    }

    /**
     * Recalculate entire sale
     */
    public function recalculateSale() {
        $setting = Setting::getSettings();

        $totalPax = 0;
        $subTotal = 0;
        $paxTotal = 0;
        $alaCarteTotal = 0;
        // Calculate total pax price
        foreach ($this->sale_details as $sale_detail) {
            $subTotal += $sale_detail->total_pax_price;
            $totalPax += $sale_detail->pax;
            $paxTotal += $sale_detail->total_pax_price;
        }

        // Calculate ala carte price
        if (count($this->sale_orders) > 0) {
            foreach ($this->sale_orders as $sale_order) {
                $subTotal += $sale_order->total_price;
                $alaCarteTotal += $sale_order->total_price;
            }
        }

        $this->total_pax = $totalPax;
        $this->pax_total = $paxTotal;
        $this->ala_carte_total = $alaCarteTotal;

        // Calculate tax, if there is any
        if ($this->tax_percentage > 0) {
            $this->tax_total = ($subTotal / 100) * $this->tax_percentage;
        }

        // Discount by percentage, before tax.
        if (!is_null($this->discount_percentage)) {
            $this->sub_total = ($subTotal - $this->discount_total);
            $grandTotal = $subTotal;
            $grandTotal -= $this->discount_total;

            #OVERRIDE THE PRICE HERE
            if ($this->tax_total > 0) {
                $grandTotal += $this->tax_total;
            }
        } else {
            $this->sub_total = $subTotal;
            $grandTotal = $subTotal;

            #OVERRIDE THE PRICE HERE
            if ($this->tax_total > 0) {
                $grandTotal += $this->tax_total;
            }

            if ($this->discount_total > 0) {  // Discount by fixed price.
                $grandTotal = self::customRound($grandTotal);
                $grandTotal = $grandTotal - $this->discount_total;
            }
        }

        $this->grand_total = $grandTotal;
    }

    /**
     * Custom round up value.
     *
     * @return float|int
     */
    public static function customRound($value)
    {
        $rangeDefinitions = [
            [[0.00, 0.02], 0.00],
            [[0.03, 0.07], 0.05],
            [[0.08, 0.09], 0.10],
        ];

        $no = explode('.', number_format($value, 2, '.', ''));
        $integer = floor($no[0]);
        $firstDecimal = substr($no[1],0,1) / 10;
        $secondDecimal = substr($no[1],-1,1) / 100;

        $result = $value;
        foreach ($rangeDefinitions as $range) {
            if ($secondDecimal >= $range[0][0] && $secondDecimal <= $range[0][1]) {
                $result = $integer + $firstDecimal + $range[1];
            }
        }
        return $result;
    }

    /**
     * Filter results
     *
     * @param $query
     */
    public function scopeGetFilteredResults($query)
    {
        if (\Input::filled('staff_id') && \Input::get('staff_id') != '') {
            $query->where(function ($q) {
                $q->whereStaffId(\Input::get('staff_id'));
            });
        }

        if (\Input::filled('sale_ref_id') && \Input::get('sale_ref_id') != '') {
            $query->where(function ($q) {
                $q->where('sale_ref_id' , 'LIKE', '%'.\Input::get('sale_ref_id').'%');
            });
        }

        if (\Input::filled('filter_credit_card') && \Input::get('filter_credit_card') != '') {
            $query->where(function ($q) {
                $q->where('remark' , 'LIKE', '%'.\Input::get('filter_credit_card').'%');
            });
        }

        if (\Input::filled('filter_payment_method')) {
            $query->wherePaymentMethod(\Input::get('filter_payment_method'));
        }

        if (\Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', \Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (\Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', \Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (\Input::filled('filter_created_after_datetime')) {
            $query->where('created_at', '>=', \Input::get('filter_created_after_datetime'));
        }

        if (\Input::filled('filter_created_before_datetime')) {
            $query->where('created_at', '<=', \Input::get('filter_created_before_datetime'));
        }

        if (\Input::filled('filter_payment_method')) {
            $query->where('payment_method', '=', \Input::get('filter_payment_method'));
        }
    }
}
