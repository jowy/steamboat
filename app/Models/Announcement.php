<?php

namespace App\Models;

use Input;

class Announcement extends BaseModels {

    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->title_en;
    }

    protected $table = 'announcement';

    protected $fillable = [
        'title_en', 'title_cn',
        'content_en', 'content_cn',
    ];

    protected $dates = [];

    public static $rules = [
        'title_en' => 'required|max:255',
        'title_cn' => 'required|max:255',
        'content_en' => '',
        'content_cn' => '',
    ];

    public function readed() {
        return $this->hasMany('App\Models\AnnouncementStatus', 'announcement_id', 'id');
    }

    public function getTitle() {
        $field = 'title_' . app()->getLocale();
        return $this->$field;
    }

    public function getContent() {
        $field = 'content_' . app()->getLocale();
        return $this->$field;
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }

}