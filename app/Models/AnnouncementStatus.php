<?php

namespace App\Models;

use Input;

class AnnouncementStatus extends BaseModels {

    public function getAuditDescription() {
        return null;
    }

    public function needAudit() {
        return false;
    }

    protected $table = 'announcement_status';

    protected $fillable = [
    ];

    protected $dates = ['created_at', 'updated_at'];

    public static $rules = array(
        'user_id' => 'required|exists:user,id',
        'announcement_id' => 'required|exists:announcement,id',
    );

    public static function boot() {
        parent::boot();
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function announcement() {
        return $this->belongsTo('App\Models\Announcement', 'announcement_id', 'id');
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function scopeGetFilteredResults($query) {
        return $query;
    }

}