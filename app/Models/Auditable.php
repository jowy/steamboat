<?php

namespace App\Models;

interface Auditable {
	public function getAuditDescription();
	public function needAudit();
}