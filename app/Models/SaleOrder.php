<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleOrder extends Model
{
    public function sale()
    {
        return $this->belongsTo(Sale::class, 'sale_id');
    }

    public static function calculateTotalPrice($alaCartePrice, $quantity)
    {
        $total = (float) $alaCartePrice * (int) $quantity;
        return (float) $total;
    }

    public static function createOrUpdateOrder($sale, $orderDetails, $request)
    {
        foreach ($orderDetails as $key => $value) {
            $product = Product::whereIsAlaCarte(true)->whereId($value['product_id'])->first();

            $isSubtractProduct = false;
            if (!is_null($product->subtract_product_id) && !is_null($product->subtract_quantity)) {
                $isSubtractProduct = true;
            }

            $unitQuantity = $value['quantity'];
            if ($isSubtractProduct) {
                $branchStock = BranchStock::whereProductId($product->subtract_product_id)->whereBranchId(\Auth::guard('staff')->user()->branch_id)->first();
                $subtractQuantity = ($product->subtract_quantity * $unitQuantity);

                if ($saleOrder = SaleOrder::whereSaleId($sale->id)->whereProductId($product->id)->whereBranchStockId($branchStock->id)->whereNotNull('subtract_product_id')->whereNotNull('subtract_branch_stock_id')->first()) {
                    $subtractTotalQuantity = (($saleOrder->quantity + $unitQuantity) * $product->subtract_quantity);

                    // Update sale order
                    $saleOrder->product_name = $product->product_name_en;
                    $saleOrder->subtract_product_name = $product->subtract_product->product_name_en;
                    $saleOrder->quantity += $unitQuantity;
                    $saleOrder->subtract_total_quantity = $subtractTotalQuantity;
                    $saleOrder->ala_carte_price = $product->ala_carte_price;
                    $saleOrder->total_price = SaleOrder::calculateTotalPrice($product->ala_carte_price, ($saleOrder->quantity + $unitQuantity));
                    $saleOrder->save();
                } else {
                    // Create new sale order
                    $saleOrder = new self();
                    $saleOrder->sale_id = $sale->id;
                    $saleOrder->product_id = $product->id;
                    $saleOrder->subtract_product_id = $product->subtract_product_id;
                    $saleOrder->subtract_branch_stock_id = $branchStock->id;
                    $saleOrder->product_name = $product->product_name_en;
                    $saleOrder->subtract_product_name = $product->subtract_product->product_name_en;
                    $saleOrder->quantity = $unitQuantity;
                    $saleOrder->subtract_total_quantity = $subtractQuantity;
                    $saleOrder->ala_carte_price = $product->ala_carte_price;
                    $saleOrder->total_price = SaleOrder::calculateTotalPrice($product->ala_carte_price, $unitQuantity);
                    $saleOrder->save();
                }

                # Deduct branch stock
                $branchStock->quantity -= $subtractQuantity;
                $branchStock->save();
            } else {
                $branchStock = BranchStock::whereProductId($product->id)->whereBranchId(\Auth::guard('staff')->user()->branch_id)->first();
                $unitQuantity = $value['quantity'];

                if ($saleOrder = SaleOrder::whereSaleId($sale->id)->whereBranchStockId($branchStock->id)->first()) {
                    $totalQuantity = ($saleOrder->quantity + $unitQuantity);

                    // Update sale order
                    $saleOrder->product_name = ($branchStock->product && $branchStock->product->product_name_en) ? $branchStock->product->product_name_en : $branchStock->product_name_en;
                    $saleOrder->quantity += $unitQuantity;
                    $saleOrder->ala_carte_price = $branchStock->product->ala_carte_price;
                    $saleOrder->total_price = SaleOrder::calculateTotalPrice($branchStock->product->ala_carte_price, $totalQuantity);
                    $saleOrder->save();
                } else {
                    // Create new sale order
                    $saleOrder = new SaleOrder();
                    $saleOrder->sale_id = $sale->id;
                    $saleOrder->branch_stock_id = $branchStock->id;
                    $saleOrder->product_id = $product->id;
                    $saleOrder->product_name = ($branchStock->product && $branchStock->product->product_name_en) ? $branchStock->product->product_name_en : $branchStock->product_name_en;
                    $saleOrder->quantity = $unitQuantity;
                    $saleOrder->ala_carte_price = $branchStock->product->ala_carte_price;
                    $saleOrder->total_price = SaleOrder::calculateTotalPrice($branchStock->product->ala_carte_price, $unitQuantity);
                    $saleOrder->save();
                }

                # Deduct branch stock
                $branchStock->quantity -= $unitQuantity;
                $branchStock->save();
            }

            $remark = 'Staff: '.\Auth::guard('staff')->user()->name.' make an sale from sale reference id: '.$sale->sale_ref_id;

            if ($isSubtractProduct) {
                $remark .= ' (Is subtract product: '.$saleOrder->product_name.', subtract quantity: '.$product->subtract_quantity.')';
            }

            # Store branch stock transaction
            $branchStockTransaction = new BranchStockTransaction();
            $branchStockTransaction->branch_id = \Auth::guard('staff')->user()->branch_id;
            $branchStockTransaction->product_id = $branchStock->product_id;
            $branchStockTransaction->stock_id = $branchStock->id;
            $branchStockTransaction->sale_id = $sale->id;
            $branchStockTransaction->product_name_en = $saleOrder->product_name;
            $branchStockTransaction->transaction_type = BranchStockTransaction::TYPE_STAFF_ORDER;
            $branchStockTransaction->quantity = ($isSubtractProduct) ? $subtractQuantity : $unitQuantity;
            $branchStockTransaction->remark = $remark;
            $branchStockTransaction->params = ($request) ? json_encode($request->all(), true) : null;
            $branchStockTransaction->save();
        }
    }
}
