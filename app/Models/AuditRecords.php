<?php

namespace App\Models;

use Input;

/**
 * Class Audit
 * @package App\Models
 */
class AuditRecords extends BaseModels
{
    public function getAuditDescription() {
        return null;
    }

    public function needAudit() {
        return false;
    }

    const OPERATION_CREATE = 1;
    const OPERATION_UPDATE = 2;
    const OPERATION_DELETE = 3;
    const OPERATION_LOGIN = 4;

    protected $table = 'audit_records';

    protected $fillable = [
        'admin_id', 'description', 'function_tag', 'operation', 'params',
        'before_data', 'after_data', 'created_data', 'deleted_data',
    ];

    public static $rules = [
        'admin_id' => 'required|exists:admin,id',
        'description' => 'required|min:1|max:255',
        'function_tag' => 'required',
        'operation' => 'required|in:1,2,3,4',
        'params' => '',
        'before_data' => '',
        'after_data' => '',
        'created_data' => '',
        'deleted_data' => '',
    ];

    public static function boot() {
        parent::boot();
    }

    public function admin() {
        return $this->belongsTo('App\Models\Admin', 'admin_id', 'id');
    }

    public function convertParams() {
        return json_decode($this->params, true);
    }

    public function setParamsAttribute($value) {
        if (is_array($value)) {
            $this->attributes['params'] = json_encode($value);
        } else if (is_array(json_decode($value, true))) {
            $this->attributes['params'] = $value;
        } else {
            $this->attributes['params'] = json_encode($value);
        }
    }

    public function convertBeforeData() {
        return json_decode($this->before_data, true);
    }

    public function setBeforeDataAttribute($value) {
        if (is_array($value)) {
            $this->attributes['before_data'] = json_encode($value);
        } else if (is_array(json_decode($value, true))) {
            $this->attributes['before_data'] = $value;
        } else {
            $this->attributes['before_data'] = json_encode($value);
        }
    }

    public function convertAfterData() {
        return json_decode($this->after_data, true);
    }

    public function setAfterDataAttribute($value) {
        if (is_array($value)) {
            $this->attributes['after_data'] = json_encode($value);
        } else if (is_array(json_decode($value, true))) {
            $this->attributes['after_data'] = $value;
        } else {
            $this->attributes['after_data'] = json_encode($value);
        }
    }

    public function convertCreatedData() {
        return json_decode($this->created_data, true);
    }

    public function setCreatedDataAttribute($value) {
        if (is_array($value)) {
            $this->attributes['created_data'] = json_encode($value);
        } else if (is_array(json_decode($value, true))) {
            $this->attributes['created_data'] = $value;
        } else {
            $this->attributes['created_data'] = json_encode($value);
        }
    }

    public function convertDeletedData() {
        return json_decode($this->deleted_data, true);
    }

    public function setDeletedDataAttribute($value) {
        if (is_array($value)) {
            $this->attributes['deleted_data'] = json_encode($value);
        } else if (is_array(json_decode($value, true))) {
            $this->attributes['deleted_data'] = $value;
        } else {
            $this->attributes['deleted_data'] = json_encode($value);
        }
    }

    public static function addLog($description, $function_tag, $operation, $model = null) {
        $log = new AuditRecords();
        $log->admin_id = \Auth::guard('admin')->user()->id;
        $log->description = $description;
        $log->function_tag = $function_tag;

        $no_changes = false;

        switch ($operation) {
            case static::OPERATION_CREATE:
                $log->operation = static::OPERATION_CREATE;
                if ($model) {
                    $log->created_data = $model;
                }
                break;
            case static::OPERATION_UPDATE:
                $log->operation = static::OPERATION_UPDATE;
                if ($model) {
                    $dirty = $model->getDirty();

                    if (count($dirty) > 0) {
                        $original = $model->getOriginal();

                        $after = array();
                        $before = array();

                        foreach ($dirty as $key => $var) {
                            $after[$key] = $var;
                            if (isset($original[$key])) {
                                $before[$key] = $original[$key];
                            } else {
                                $before[$key] = '-';
                            }
                        }

                        $log->before_data = $before;
                        $log->after_data = $after;
                    } else {
                        $no_changes = true;
                    }
                }
                break;
            case static::OPERATION_DELETE:
                $log->operation = static::OPERATION_DELETE;

                $log->deleted_data = $model;
                break;
            case static::OPERATION_LOGIN:
                $log->operation = static::OPERATION_LOGIN;
                break;
            default: throw new \Exception(trans('common.unknown_error')); break;
        }

        if ($no_changes === false) {
            $log->save();
            return $log;
        } else {
            return false;
        }
    }

    public function explainFunctionTag() {
        return trans('audit.' . $this->function_tag);
    }

    public function explainOperation() {
        switch ($this->operation) {
            case static::OPERATION_CREATE: return trans('audit.create'); break;
            case static::OPERATION_UPDATE: return trans('audit.edit'); break;
            case static::OPERATION_DELETE: return trans('audit.delete'); break;
            case static::OPERATION_LOGIN: return trans('audit.login'); break;
            default: return trans('common.unknown'); break;
        }
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_admin_username') && Input::get('filter_admin_username') != '') {
            $query->whereHas('admin', function ($q) {
                $q->where('username', 'like', '%' . Input::get('filter_admin_username') . '%');
            });
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after_date')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_updated_before_date')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before_date') . ' 23:59:59');
        }
    }
}