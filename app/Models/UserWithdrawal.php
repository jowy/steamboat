<?php

namespace App\Models;

use Input;

class UserWithdrawal extends BaseModels {
    public function getAuditDescription() {
        return getClassName($this) . ' ' . ($this->user ? ' for ' . $this->user->username : '#' . $this->id);
    }

    protected $table = 'user_withdrawal';

    protected $fillable = [

    ];

    protected $dates = [
        'created_at', 'updated_at', 'admin_response_date'
    ];

    public static $rules = [
        'user_id' => 'required|exists:user,id',
        'admin_id' => 'nullable|exists:admin,id',
        'country_id' => 'required|exists:country,id,status,1',
        'bank_id' => 'required|exists:bank,id,status,1',
        'currency_actual_rate' => 'required|isFund',
        'currency_in_rate' => 'required|isFund',
        'currency_out_rate' => 'required|isFund',
        'bank_name_en' => 'required|min:1|max:255',
        'bank_name_cn' => 'required|min:1|max:255',
        'bank_account_name' => 'required|min:1|max:255',
        'bank_account_number' => 'required|min:1|max:255',
        'credit_type' => 'required|isUserCreditType',
        'amount' => 'required|isFund',
        'admin_fees' => 'required|isFund',
        'amount_after_admin_fees' => 'required|isFund',
        'admin_response_date' => 'required|date',
        'admin_remark' => 'nullable',
        'user_remark' => 'nullable',
        'status' => 'required|in:0,1,2',
    ];

    public static function boot() {
        parent::boot();
        static::saving(function ($m) {
            $m->amount_after_admin_fees = $m->amount - $m->admin_fees;
            $m->local_currency_amount = ($m->amount_after_admin_fees * $m->currency_out_rate);
        });
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function admin() {
        return $this->belongsTo('App\Models\Admin', 'admin_id', 'id');
    }

    public function country() {
        return $this->belongsTo('App\Models\Country', 'country_id', 'id');
    }

    public function bank() {
        return $this->belongsTo('App\Models\Bank', 'bank_id', 'id');
    }

    public function companyBank() {
        return $this->belongsTo('App\Models\CompanyBank', 'company_bank_id', 'id');
    }

    public function getBankName() {
        if ($this->bank) {
            return $this->bank->getName();
        }
        $field = 'bank_name_' . app()->getLocale();
        return isset($this->$field) && !isEmpty($this->$field) ? $this->$field : $this->bank_name_en;
    }

    public function explainCreditType() {
        return User::explainCreditType($this->credit_type);
    }

    public static function getStatusLists($empty = false) {
        $arr = array();
        if ($empty === true) {
            $arr[] = trans('common.all');
        }

        $arr[0] = trans('common.pending');
        $arr[1] = trans('common.completed');
        $arr[2] = trans('common.refunded');

        return $arr;
    }

    public function explainStatus() {
        $arr = static::getStatusLists();

        return isset($arr[$this->status]) ? $arr[$this->status] : trans('common.unknown');
    }

    public function isDone() {
        return in_array($this->status, [1,2]);
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_username') && Input::get('filter_username') != '') {
            $query->whereHas('user', function ($q) {
                $q->where('username', '=', Input::get('filter_username'));
            });
        }

        if (Input::filled('filter_amount_from') && Input::get('filter_amount_from') != '') {
            $query->where('amount', '>=', Input::get('filter_amount_from'));
        }

        if (Input::filled('filter_amount_to') && Input::get('filter_amount_to') != '') {
            $query->where('amount', '<=', Input::get('filter_amount_to'));
        }

        if (Input::filled('filter_status') && Input::get('filter_status') != '') {
            $query->where('status', '=', Input::get('filter_status'));
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }

}