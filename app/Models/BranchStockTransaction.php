<?php

namespace App\Models;

use Input;

class BranchStockTransaction extends BaseModels {

    public function getAuditDescription() {
        return getClassName($this) . ' #' . $this->id;
    }

    public function needAudit() {
        return false;
    }

    const TYPE_ADMIN_ADD = 1;
    const TYPE_ADMIN_DEDUCT = 2;
    const TYPE_STAFF_ORDER = 3;
    const TYPE_STAFF_REFUND = 4;
    const TYPE_RESTOCK_OUT = 5;
    const TYPE_RESTOCK_IN = 6;
    const TYPE_RESTOCK_REFUND = 7;

    protected $table = 'branch_stock_transaction';

    protected $fillable = [

    ];

    public static $rules = [
        'branch_id' => 'required|exists:branch,id',
        'product_id' => 'required|exists:product,id',
        'stock_id' => 'required|exists:branch_stock,id',
        'product_name_en' => 'required|min:1|max:255',
        'product_name_cn' => 'required|min:1|max:255',
        'transaction_type' => 'required|in:1,2,3,4,5,6,7',
        'quantity' => 'required|isNumber',
    ];

    public static function boot() {
        parent::boot();
    }

    public function branch() {
        return $this->belongsTo('App\Models\Branch', 'branch_id', 'id')->withTrashed();
    }

    public function product() {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id')->withTrashed();
    }

    public function stock() {
        return $this->belongsTo('App\Models\BranchStock', 'stock_id', 'id');
    }

    public function getName() {
        if (app()->getLocale() == 'cn') {
            return $this->product_name_cn;
        } else {
            return $this->product_name_en;
        }
    }

    public function explainTransactionType() {
        $params = json_decode($this->params, true);
        if (!is_array($params)) {
            $params = array();
        }

        switch ($this->transaction_type) {
            case static::TYPE_ADMIN_ADD: return trans('stock.type_admin_add'); break;
            case static::TYPE_ADMIN_DEDUCT: return trans('stock.type_admin_deduct'); break;
            case static::TYPE_STAFF_ORDER: return trans('stock.type_user_order'); break;
            case static::TYPE_STAFF_REFUND: return trans('stock.type_user_refund'); break;
            case static::TYPE_RESTOCK_OUT: return trans('stock.type_restock_out'); break;
            case static::TYPE_RESTOCK_IN: return trans('stock.type_restock_in'); break;
            case static::TYPE_RESTOCK_REFUND: return trans('stock.type_restock_refund'); break;
            default: return trans('common.unknown'); break;
        }
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function fillProduct(Product $product) {
        $this->product_id = $product->id;
        $this->product_name_en = $product->product_name_en;
        $this->product_name_cn = $product->product_name_cn;
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_name') && Input::get('filter_name') != '') {
            $query->where(function ($q) {
                $q->where('product_name_en', 'like', '%' . Input::get('filter_name') . '%')
                    ->orWhere('product_name_cn', 'like', '%' . Input::get('filter_name') . '%');
            });
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after_date')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_updated_before_date')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before_date') . ' 23:59:59');
        }
    }

}
