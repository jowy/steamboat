<?php

namespace App\Models;

use Input;

class Menu extends BaseModels {
    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->name_en;
    }

    protected $table = 'menu';

    protected $fillable = [
        'name_en', 'name_cn', 'tier',
    ];

    protected $dates = [];

    public static $rules = array(
        'name_en' => 'required|max:255',
        'name_cn' => 'required|max:255',
        'tier' => 'required|isNumber',
    );

    public static function boot() {
        parent::boot();
    }

    public function items() {
        return $this->hasMany('App\Models\MenuItems', 'menu_id', 'id');
    }

    public function childrens() {
        return $this->hasMany('App\Models\MenuItems', 'menu_id', 'id')->whereNull('parent_id')->orderBy('sorting', 'ASC');
    }

    public function getName() {
        $field = 'name_' . app()->getLocale();
        if (isset($this->$field) && $this->$field) {
            return $this->$field;
        } else {
            return $this->name_en;
        }
    }

    public function setMenuTagAttribute($value) {
        $this->attributes['menu_tag'] = slugify(trim(strtolower($value)));
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function scopeGetFilteredResults($query) {
        return $query;
    }

}