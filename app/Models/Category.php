<?php

namespace App\Models;

use Input;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends BaseModels {
    use SoftDeletes;

    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->name_en;
    }

    protected $table = 'category';

    protected $fillable = [
        'parent_id', 'sorting', 'name_en', 'name_cn'
    ];

    protected $dates = [];

    public static $rules = array(
        'parent_id' => 'exists:country_locations,id',
        'sorting' => 'isNumber',
        'name_en' => 'required|max:255',
        'name_cn' => 'nullable|max:255',
    );

    public static function boot() {
        parent::boot();
    }

    public function parent() {
        return $this->belongsTo('App\Models\Category', 'parent_id', 'id');
    }

    public function sons() {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id')->orderBy('sorting', 'ASC');
    }

    public static function getCategories() {
        $field = 'name_' . app()->getLocale();
        return static::whereNull('parent_id')->orderBy('sorting', 'ASC')->pluck($field, 'id');
    }

    public function getName() {
        $field = 'name_' . app()->getLocale();
        if (isset($this->$field) && $this->$field) {
            return $this->$field;
        } else {
            return $this->name_en;
        }
    }

    public function getNames() {
        $arr = array();
        if ($this->name_en) {
            $arr[] = $this->name_en;
        }

        if ($this->name_cn) {
            $arr[] = $this->name_cn;
        }

        return implode(' / ', $arr);
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function scopeGetFilteredResults($query) {
        return $query;
    }

}