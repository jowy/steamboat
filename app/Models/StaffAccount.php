<?php

namespace App\Models;

use Input;

class StaffAccount extends BaseModels
{
    public function getAuditDescription() {
        return null;
    }

    public function needAudit() {
        return false;
    }

    protected $table = 'staff_account';

    protected $dates = [
        'created_at', 'updated_at',
    ];

    public function branch() {
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }

    public function staff() {
        return $this->belongsTo(Staff::class, 'staff_id', 'id');
    }

    public static function getTypeLists() {
        $arr = array();
        $arr[1] = trans('common.cash_in');
        $arr[2] = trans('common.cash_out');
        return $arr;
    }

    public function explainType() {
        $arr = static::getTypeLists();

        return isset($arr[$this->type]) ? $arr[$this->type] : trans('common.unknown');
    }

    public function scopeGetFilteredResults($query)
    {
        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after_date')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_updated_before_date')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before_date') . ' 23:59:59');
        }
    }
}
