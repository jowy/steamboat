<?php

namespace App\Models;

use Input;

class ProductCategory extends BaseModels {
    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->name_en;
    }

    public function needAudit() {
        return false;
    }

    protected $table = 'product_category';

    protected $fillable = [
        'product_id', 'category_id',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public static $rules = array(
        'product_id' => 'required|exists:product,id',
        'category_id' => 'required|exists:category,id',
    );

    public static function boot() {
        parent::boot();
    }

    public function product() {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id')->withTrashed();
    }

    public function category() {
        return $this->belongsTo('App\Models\Category', 'category_id', 'id')->withTrashed();
    }

    public function getProductName() {
        return $this->product ? $this->product->getProductName() : trans('common.unknown');
    }

    public function getCategoryName() {
        return $this->category ? $this->category->getName() : trans('common.unknown');
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function scopeGetFilteredResults($query) {
        return $query;
    }

}