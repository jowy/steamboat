<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BranchTable extends Model
{
    use SoftDeletes;

    protected $softDelete = true;

    protected $fillable = ['table_no', 'pax'];

    public function sale_tables()
    {
        return $this->hasMany(SaleTable::class, 'branch_table_id');
    }
}
