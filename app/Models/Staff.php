<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Input;

class Staff extends Authenticatable
{
    use SoftDeletes;

    protected $softDelete = true;

    protected $fillable = [
        'name',
        'email',
        'contact_number',
    ];

    public static $rules = [
        'branch_id' => 'required|exists:branch,id',
        'name' => 'required|min:1|max:255',
        'username' => 'required|min:2|alpha_dash|unique:staff,username',
        'password' => 'required|min:6|confirmed',
        'contact_number' => 'max:150',
        'email' => 'email|max:200',
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function scopeGetFilteredResults($query)
    {
        if (Input::filled('filter_name') && Input::get('filter_name') != '') {
            $query->where(function ($q) {
                $q->where('name_en', 'like', '%' . Input::get('filter_name') . '%')
                    ->orWhere('name_cn', 'like', '%' . Input::get('filter_name') . '%');
            });
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after_date')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_updated_before_date')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before_date') . ' 23:59:59');
        }
    }
}
