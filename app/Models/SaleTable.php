<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaleTable extends Model
{
    use SoftDeletes;

    public function sale()
    {
        return $this->belongsTo(Sale::class, 'sale_id');
    }

    public function table()
    {
        return $this->belongsTo(BranchTable::class, 'branch_table_id')->withTrashed();
    }
}
