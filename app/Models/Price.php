<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Price extends Model
{
    use SoftDeletes;

    const KID_TYPE = 'kid';
    const ADULT_TYPE = 'adult';
    const SENIOR_TYPE = 'senior';
    const FREE_TYPE = 'free';

    public function scopeNotSystem($query) {
        $query->where('is_system', '=', 0);
    }

    public static $rules = [
        'name_en' => 'required|max:100',
        'per_pax_price' => 'required|isFund'
    ];
}
