<?php

namespace App\Models;

use Input;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Branch extends Authenticatable
{
    use SoftDeletes;

    protected $softDelete = true;

    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->name_en;
    }

    protected $table = 'branch';

    protected $fillable = [
        'name_en',
        'name_cn',
        'country_id',
        'address',
        'receipt_address',
        'latitude',
        'longitude',
        'contact_number',
        'email',
    ];

    public static $rules = [
        'name_en' => 'required|min:1|max:255',
        //'name_cn' => 'required|min:1|max:255',
        'username' => 'required|min:2|alpha_dash|unique:branch,username',
        'password' => 'required|min:6|confirmed',
        'country_id' => 'required|exists:country,id,status,1',
        'address' => 'required',
        'receipt_address' => 'required',
        'latitude' => 'required',
        'longitude' => 'required',
        'contact_number' => '',
        'email' => 'email',
    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    public static function boot() {
        parent::boot();
    }

    public function country() {
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    public function staffs() {
        return $this->hasMany('App\Models\Staff', 'branch_id');
    }

    public function getName() {
        $field = 'name_' . app()->getLocale();
        return !isEmpty($this->$field) ? $this->$field : $this->name_en;
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function scopeGetSelects($query) {
        $arr = array();
        foreach ($query->get() as $key => $var) {
            $arr[$var->id] = $var->getName();
        }

        return $arr;
    }

    public function scopeGetForSelects($query) {
        return $query->GetSelects();
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_name') && Input::get('filter_name') != '') {
            $query->where(function ($q) {
                $q->where('name_en', 'like', '%' . Input::get('filter_name') . '%')
                    ->orWhere('name_cn', 'like', '%' . Input::get('filter_name') . '%');
            });
        }

        if (Input::filled('filter_name_en') && Input::get('filter_name_en') != '') {
            $query->where('name_en', 'like', '%' . Input::get('filter_name_en') . '%');
        }

        if (Input::filled('filter_name_cn') && Input::get('filter_name_cn') != '') {
            $query->where('name_cn', 'like', '%' . Input::get('filter_name_cn') . '%');
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after_date')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_updated_before_date')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before_date') . ' 23:59:59');
        }
    }

    /**
     * Manage branch stock record automatically
     *
     * @param  int $product_id
     * @param  int $transaction_type
     * @param  int $quantity
     * @param  array $extra_params
     * @param null $remark
     * @return \App\Models\Branch
     * @throws \Exception
     */
    public function manageStock($product_id, $transaction_type, $quantity, $extra_params = array(), $remark = null) {
        if ($product_id instanceof Product) {
            $product = $product_id;
            $product_id = $product->id;
        } else {
            $product = Product::find($product_id);
        }

        if ($quantity <= 0) {
            throw new \Exception(trans('common.unknown_error') . ' :manageStock Incorrect Quantity');
        }

        if (!$product) {
            throw new \Exception(trans('common.record_not_found'));
        }

        $stock = BranchStock::lockForUpdate()->where('branch_id', '=', $this->id)->where('product_id', '=', $product_id)->first();
        if (!$stock) {
            $stock = new BranchStock();
            $stock->branch_id = $this->id;
            $stock->fillProduct($product);
            $stock->quantity = 0;
        }

        $adjust_quantity = 0;
        switch ($transaction_type) {
            case BranchStockTransaction::TYPE_RESTOCK_IN://THIS SHOULD NEVER HAPPEND
                throw new \Exception('Unknown stock action');
                break;
            case BranchStockTransaction::TYPE_ADMIN_ADD:
            case BranchStockTransaction::TYPE_USER_REFUND:
            case BranchStockTransaction::TYPE_RESTOCK_REFUND:
                $adjust_quantity = $quantity;
                $stock->quantity = $stock->quantity + $quantity;
                break;
            case BranchStockTransaction::TYPE_ADMIN_DEDUCT:
            case BranchStockTransaction::TYPE_USER_ORDER:
            case BranchStockTransaction::TYPE_RESTOCK_OUT:
                $adjust_quantity = 0 - $quantity;
                $stock->quantity = $stock->quantity - $quantity;
                break;
            default: throw new \Exception(trans('common.unknown_error') . ' :manageStock Unknown transaction type'); break;
        }

        if ($stock->quantity < 0) {
            throw new \Exception(trans('common.unknown_error') . ' :manageStock Negative Stock');
        }

        if ($adjust_quantity == 0) {
            throw new \Exception(trans('common.unknown_error') . ' :manageStock unknown quantity');
        }

        $params = array();
        foreach ($product->attributes as $key => $value) {
            $params[$key] = (string) $value;
        }

        if (count($extra_params) > 0) {
            foreach ($extra_params as $key => $var) {
                if (!isset($params[$key])) {
                    $params[$key] = $var;
                } else {
                    throw new \Exception('manage stock error, invalid params');
                }
            }
        }

        $stock->save();

        $transaction = new BranchStockTransaction();
        $transaction->branch_id = $this->id;
        $transaction->stock_id = $stock->id;

        $transaction->fillProduct($product);

        $transaction->transaction_type = $transaction_type;
        $transaction->quantity = $adjust_quantity;
        $transaction->remark = $remark;
        $transaction->params = $params;
        $transaction->save();

        return $this;
    }

}
