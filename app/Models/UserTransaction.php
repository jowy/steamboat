<?php

namespace App\Models;

use Input;

class UserTransaction extends BaseModels {
    public function getAuditDescription() {
        return null;
    }

    public function needAudit() {
        return false;
    }

    protected $table = 'user_transactions';

    protected $fillable = [

    ];

    protected $dates = [];

    public static $rules = [
        'user_id' => 'required|exists:user,id',
        'amount' => 'required|isFund',
        'transaction_type' => 'required|isUserTransaction',
        'credit_type' => 'required|isUserCreditType',
        'adjust_description' => 'in:0,1',
        'description_en' => '',
        'description_cn' => '',
        'params' => '',
    ];

    public static function boot() {
        parent::boot();
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public static function getTransactionTypeLists($empty = false, $params = array()) {
        $arr = array();
        if ($empty === true) {
            $arr[''] = trans('common.all');
        }

        $arr[101] = trans('transaction.credit_transfer_out', $params);
        $arr[102] = trans('transaction.credit_transfer_in', $params);

        $arr[400] = trans('transaction.topup', $params);
        $arr[401] = trans('transaction.withdrawal', $params);
        $arr[402] = trans('transaction.withdrawal_refund', $params);

        $arr[901] = trans('common.admin_add_credit_1');
        $arr[902] = trans('common.admin_deduct_credit_1');
        $arr[903] = trans('common.admin_add_credit_2');
        $arr[904] = trans('common.admin_deduct_credit_2');
        $arr[905] = trans('common.admin_add_credit_3');
        $arr[906] = trans('common.admin_deduct_credit_3');
        $arr[907] = trans('common.admin_add_credit_4');
        $arr[908] = trans('common.admin_deduct_credit_4');

        return $arr;
    }

    public static function getFilterTransactionType($empty = false) {
        $arr = array();

        if ($empty === true) {
            $arr[''] = trans('common.all');
        }

        $arr[999] = trans('common.admin_related');

        return $arr;
    }

    public static function getFilterTransactionTypeIds($type) {
        switch ($type) {
            case 999: return [901, 902, 903, 904, 905, 906, 907, 908]; break;
            default: return []; break;
        }
    }

    public function explainTransactionType() {
        $params = $this->convertParams();
        if (!isset($params['credit_type'])) {
            $params['credit_type'] = trans('common.credit_' . $this->credit_type);
        }

        if ($this->adjust_description == 1) {
            $field = 'description_' . app()->getLocale();
            return $this->$field;
        } else {
            $arr = static::getTransactionTypeLists(false, $params);
            if (isset($arr[$this->transaction_type])) {
                return $arr[$this->transaction_type];
            } else {
                return trans('common.invalid_user_transaction');
            }
        }
    }

    public function explainCreditType() {
        return User::explainCreditType($this->credit_type);
    }

    public function explainCreditAction() {
        if ($this->amount > 0) {
            return trans('common.credit');
        } else {
            return trans('common.debit');
        }
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_amount_from') && Input::get('filter_amount_from') != '') {
            $query->where('amount', '>=', Input::get('filter_amount_from'));
        }

        if (Input::filled('filter_amount_to') && Input::get('filter_amount_to') != '') {
            $query->where('amount', '<=', Input::get('filter_amount_to'));
        }

        if (Input::filled('filter_transaction_type') && Input::get('filter_transaction_type') != '') {
            $query->where('transaction_type', '=', Input::get('filter_transaction_type'));
        }

        if (Input::filled('filter_transaction_group') && Input::get('filter_transaction_group') != '') {
            $ids = static::getFilterTransactionTypeIds(Input::get('filter_transaction_group'));
            if (count($ids)) {
                $query->whereIn('transaction_type', $ids);
            }
        }

        if (Input::filled('filter_action') && Input::get('filter_action') != '') {
            if (Input::get('filter_action') == 1) {
                $query->where('amount', '>=', 0);
            } else if (Input::get('filter_action') == 2) {
                $query->where('amount', '<', 0);
            }
        }

        if (Input::filled('filter_credit_type') && Input::get('filter_credit_type') != '') {
            $query->where('credit_type', '=', Input::get('filter_credit_type'));
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }

}
