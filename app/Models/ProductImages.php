<?php

namespace App\Models;

use Input;

class ProductImages extends BaseModels {
    const IMAGE_WIDTH = 1200;
    const IMAGE_HEIGHT = 700;

    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->name_en;
    }

    public function needAudit() {
        return false;
    }

    protected $table = 'product_images';

    protected $fillable = [
        'product_id', 'path',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public static $rules = array(
        'product_id' => 'required|exists:product,id',
        'path' => 'required',
    );

    public static function boot() {
        parent::boot();
    }

    public function product() {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id')->withTrashed();
    }

    public function setPathAttribute($file)
    {
        if ($file instanceof \Illuminate\Http\UploadedFile) {
            $now = \Carbon\Carbon::now()->format('Y/m/');
            $path = 'uploads/product_image/' . $now;
            $filename = \Carbon\Carbon::now()->format('d_') . generateRandomUniqueName();

            if (isImageExtension($file->getClientOriginalExtension())) {
                $img = \Image::make($file);
                $mime = $img->mime();
                $ext = convertMimeToExt($mime);

                touchFolder($path);

                $img = $img->fit(static::IMAGE_WIDTH, static::IMAGE_HEIGHT)->save($path . $filename . $ext, 90);
            } else {
                throw new \Exception(trans('common.invalid_image'));
            }

            $this->attributes['path'] = $path . $filename . $ext;
        } else if (is_string($file)) {
            $this->attributes['path'] = $file;
        }
    }

    public function getImage($asset = true) {
        return $this->getPath($asset);
    }

    public function getPath($asset = true) {
        return isset($this->path) && !isEmpty($this->path) ? ($asset ? asset($this->path) : $this->path) : null;
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function scopeGetFilteredResults($query) {
        return $query;
    }

}