<?php

namespace App\Models;

use Input;

class PermissionGroup extends BaseModels {
    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->group_name;
    }

    protected $table = 'permission_groups';

    protected $fillable = [
        'group_name',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public static $rules = [
        'group_name' => 'required|unique:permission_groups,group_name',
        'permissions' => 'required',
        'permissions.*' => 'required|isPermissionTag'
    ];

    public static function boot() {
        parent::boot();
    }

    public function permissions() {
        return $this->hasMany('App\Models\PermissionGroupPermission', 'permission_group_id', 'id');
    }

    public function scopeGetGroups($query) {
        return $query;
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_group_name') && Input::get('filter_group_name') != '') {
            $query->where('group_name', 'like', '%' . Input::get('filter_group_name') . '%');
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }

}