<?php

namespace App\Models;

use Input;

class Page extends BaseModels {
    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->title_en;
    }

    protected $table = 'page';

    protected $fillable = [

    ];

    protected $dates = [];

    public static $rules = array(
        'page_tag' => 'required|min:1|max:64|unique:page,page_tag',
        'page_description_en' => 'required|min:1|max:255',
        'page_description_cn' => 'min:1|max:255',
        'title_en' => 'required|min:1|max:255',
        'title_cn' => 'min:1|max:255',
        'content_en' => 'required',
        'content_cn' => '',
        'mobile_content_en' => 'required',
        'mobile_content_cn' => '',
    );

    public static function boot() {
        parent::boot();
    }

    public function getUrl() {
        return route('user.page', ['page_tag' => $this->page_tag]);
    }

    public function getPageDescription() {
        $field = 'page_description_' . app()->getLocale();
        if (isset($this->$field) && $this->$field) {
            return $this->$field;
        } else {
            return $this->page_description_en;
        }
    }

    public function getTitle() {
        $field = 'title_' . app()->getLocale();
        if (isset($this->$field) && $this->$field) {
            return $this->$field;
        } else {
            return $this->title_en;
        }
    }

    public function getContent() {
        $field = 'content_' . app()->getLocale();
        if (isset($this->$field)) {
            return $this->$field;
        } else {
            return $this->content_en;
        }
    }

    public function getMobileContent() {
        $field = 'mobile_content_' . app()->getLocale();
        if (isset($this->$field)) {
            return $this->$field;
        } else {
            return $this->content_en;
        }
    }

    public function getSharingTitle() {
        $field = 'sharing_title_' . app()->getLocale();
        if (isset($this->$field)) {
            return $this->$field;
        } else {
            return $this->sharing_title_en;
        }
    }

    public function getSharingContent() {
        $field = 'sharing_content_' . app()->getLocale();
        if (isset($this->$field)) {
            return $this->$field;
        } else {
            return $this->sharing_content_en;
        }
    }

    public function getSharingImage() {
        $field = 'sharing_image_' . app()->getLocale();
        if (isset($this->$field)) {
            return $this->$field;
        } else {
            return $this->sharing_image_en;
        }
    }

    public function setSharingImageEnAttribute($file) {
        if ($file instanceof \Illuminate\Http\UploadedFile) {
            $img = \Image::make($file);
            $mime = $img->mime();
            $ext = convertMimeToExt($mime);

            $now = \Carbon\Carbon::now()->format('Y/m/');
            $path = 'uploads/share/' . $now;
            $filename = \Carbon\Carbon::now()->format('d_') . generateRandomUniqueName();

            touchFolder($path);

            $img = $img->save($path . $filename . $ext, 90);

            $this->attributes['sharing_image_en'] = $path . $filename . $ext;
        } else if (is_string($file)) {
            $this->attributes['sharing_image_en'] = $file;
        }
    }

    public function setSharingImageCnAttribute($file) {
        if ($file instanceof \Illuminate\Http\UploadedFile) {
            $img = \Image::make($file);
            $mime = $img->mime();
            $ext = convertMimeToExt($mime);

            $now = \Carbon\Carbon::now()->format('Y/m/');
            $path = 'uploads/share/' . $now;
            $filename = \Carbon\Carbon::now()->format('d_') . generateRandomUniqueName();

            touchFolder($path);

            $img = $img->save($path . $filename . $ext, 90);

            $this->attributes['sharing_image_cn'] = $path . $filename . $ext;
        } else if (is_string($file)) {
            $this->attributes['sharing_image_cn'] = $file;
        }
    }

    public static function getPagesForSelect() {
        $field = 'page_description_' . app()->getLocale();
        return static::pluck($field, 'id');
    }

    public function setPageTagAttribute($value) {
        $this->attributes['page_tag'] = slugify(trim(strtolower($value)));
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function scopeGetFilteredResults($query) {
        return $query;
    }

}