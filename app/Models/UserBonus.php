<?php

namespace App\Models;

use Input;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBonus extends BaseModels {
    public function getAuditDescription() {
        return null;
    }

    public function needAudit() {
        return false;
    }

    protected $table = 'user_bonus';

    protected $fillable = [
        'user_id', 'bonus_type', 'amount', 'params', 'related_key',
    ];

    public static $rules = [
        'user_id' => 'required|exists:user,id',
        'bonus_type' => 'required|isUserBonusType',
        'amount' => 'required|isFund',
        'params' => '',
        'related_key' => '',
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public static function getBonusTypeLists($empty = false, $params = array()) {
        $arr = array();
        if ($empty === true) {
            $arr[''] = trans('common.all');
        }

        return $arr;
    }

    public function explainBonusType() {
        $params = json_decode($this->params, true);
        if (!is_array($params)) {
            $params = array();
        }
        $arr = static::getBonusTypeLists(false, $params);
        if (isset($arr[$this->bonus_type])) {
            return $arr[$this->bonus_type];
        } else {
            return trans('common.unknown');
        }
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_username') && Input::get('filter_username') != '') {
            $query->whereHas('user', function ($q) {
                $q->where('username', 'like', '%' . Input::get('filter_username') . '%');
            });
        }

        if (Input::filled('filter_bonus_type') && Input::get('filter_bonus_type') != '') {
            $query->where('bonus_type', '=', Input::get('filter_bonus_type'));
        }

        if (Input::filled('filter_amount_from') && Input::get('filter_amount_from') != '') {
            $query->where('amount', '>=', Input::get('filter_amount_from'));
        }

        if (Input::filled('filter_amount_to') && Input::get('filter_amount_to') != '') {
            $query->where('amount', '<=', Input::get('filter_amount_to'));
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after_date')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_updated_before_date')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before_date') . ' 23:59:59');
        }
    }

}