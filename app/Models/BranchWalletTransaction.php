<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BranchWalletTransaction extends BaseModels
{
    public function getAuditDescription() {
        return getClassName($this) . ' #' . $this->id;
    }

    public function needAudit() {
        return false;
    }

    const TYPE_ADMIN_ADD = 1;
    const TYPE_ADMIN_DEDUCT = 2;
    const TYPE_BRANCH_ADD = 3;
    const TYPE_BRANCH_DEDUCT = 4;
    const TYPE_DAILY_SALES = 5;
    const TYPE_SUPPLIER_PAID = 6;

    public static function boot() {
        parent::boot();
    }

    public function branch() {
        return $this->belongsTo('App\Models\Branch', 'branch_id', 'id')->withTrashed();
    }

    public function explainTransactionType() {
        $params = json_decode($this->params, true);
        if (!is_array($params)) {
            $params = array();
        }

        switch ($this->transaction_type) {
            case static::TYPE_ADMIN_ADD: return trans('stock.type_admin_add_wallet', $params); break;
            case static::TYPE_ADMIN_DEDUCT: return trans('stock.type_admin_deduct_wallet', $params); break;
            case static::TYPE_BRANCH_ADD: return trans('stock.type_branch_add_wallet', $params); break;
            case static::TYPE_BRANCH_DEDUCT: return trans('stock.type_branch_deduct_wallet', $params); break;
            case static::TYPE_DAILY_SALES: return trans('stock.type_daily_sales'); break;
            case static::TYPE_SUPPLIER_PAID: return trans('stock.type_supplier_paid'); break;
            default: return trans('common.unknown'); break;
        }
    }
}
