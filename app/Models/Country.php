<?php

namespace App\Models;

use Input;

class Country extends BaseModels {

    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->country_name_en;
    }

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    protected $table = 'country';

    protected $fillable = [
        'currency_actual_rate',
        'currency_in_rate',
        'currency_out_rate',
        'status',
        'currency_prefix',
        'currency_suffix',
    ];

    public static $rules = [
        'status' => 'in:0,1',
        'currency_actual_rate' => 'required|isFund',
        'currency_in_rate' => 'required|isFund',
        'currency_out_rate' => 'required|isFund',
        'currency_prefix' => 'max:255',
        'currency_suffix' => 'max:255',
    ];

    public static function boot() {
        parent::boot();
    }

    public function locations() {
        return $this->hasMany('App\Models\CountryLocations', 'country_id', 'id');
    }

    public function states() {
        return $this->hasMany('App\Models\CountryLocations', 'country_id', 'id')->whereNull('parent_id')->orderBy('sorting', 'ASC');
    }

    public static function getStatusLists($empty = false) {
        $arr = array();
        if ($empty === true) {
            $arr[''] = trans('common.all');
        }
        $arr[static::STATUS_INACTIVE] = trans('common.inactive');
        $arr[static::STATUS_ACTIVE] = trans('common.active');

        return $arr;
    }

    public function explainStatus() {
        $arr = static::getStatusLists();
        return isset($arr[$this->status]) ? $arr[$this->status] : null;
    }

    public function getCountryName() {
        if (app()->getLocale() == 'cn') {
            return $this->country_name_cn;
        } else {
            return $this->country_name_en;
        }
    }

    public function getName() {
        return $this->getCountryName();
    }

    public function getActualRate() {
        return fundFormat($this->currency_actual_rate, \App\Constants::$decimal_point);
    }

    public function getInRate() {
        return fundFormat($this->currency_in_rate, \App\Constants::$decimal_point);
    }

    public function getOutRate() {
        return fundFormat($this->currency_out_rate, \App\Constants::$decimal_point);
    }

    //Passin amount must be USD
    public function buildCurrency($amount) {
        $amount = $amount * $this->currency_in_rate;

        return $this->currency_prefix .fundFormat($amount, \App\Constants::$decimal_point) . $this->currency_suffix;
    }
	
	//NO MATTER WHAT VALUE, WONT FORMAT
    public function explainCurrency($amount) {
        $arr = [];
        if ($this->currency_prefix) {
            $arr[] = $this->currency_prefix;
        }
        $arr[] = $amount;
        if ($this->currency_suffix) {
            $arr[] = $this->currency_suffix;
        }
        return implode(' ', $arr);
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function scopeGetCountries($query) {
        return $query->where('status', '=', Country::STATUS_ACTIVE)->orderBy('id', 'ASC');
    }

    public function scopeGetSelects($query) {
        $arr = array();
        foreach ($query->get() as $key => $var) {
            $arr[$var->id] = $var->getCountryName();
        }

        return $arr;
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_country_name') && Input::get('filter_country_name') != '') {
            $query->where(function ($q) {
                $q->where('country_name_en', 'like', '%' . Input::get('filter_country_name') . '%')
                    ->orWhere('country_name_cn', 'like', '%' . Input::get('filter_country_name') . '%');
            });
        }

        if (Input::filled('filter_country_name_en') && Input::get('filter_country_name_en') != '') {
            $query->where('country_name_en', '=', Input::get('filter_country_name_en'));
        }

        if (Input::filled('filter_country_name_cn') && Input::get('filter_country_name_cn') != '') {
            $query->where('country_name_cn', '=', Input::get('filter_country_name_cn'));
        }

        if (Input::filled('filter_status') && Input::get('filter_status') != '') {
            $query->where('status', '=', Input::get('filter_status'));
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after_date')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_updated_before_date')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before_date') . ' 23:59:59');
        }
    }

}