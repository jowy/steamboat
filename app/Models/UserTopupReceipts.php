<?php

namespace App\Models;

use Input;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserTopupReceipts extends BaseModels {

    public function needAudit() {
        return false;
    }

    public function getAuditDescription() {
        return getClassName($this) . ' #' . $this->id;
    }

    protected $table = 'user_topup_receipts';

    protected $fillable = [

    ];

    public static $rules = [
        'user_id' => 'required|exists:users,id',
        'user_topup_id' => 'required|exists:user_topup,id',
        'receipt' => 'required',
    ];

    public static function boot() {
        parent::boot();
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function topup() {
        return $this->belongsTo('App\Models\UserTopup', 'user_topup_id', 'id');
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function setReceiptAttribute($file) {
        if (!isSafeFileFormat($file->getClientOriginalExtension())) {
            throw new \Exception(trans('common.unsupported_file_format'));
        }

        if ($file instanceof \Illuminate\Http\UploadedFile) {
            $mime = $file->getMimeType();
            $ext = '.' . $file->getClientOriginalExtension();
            $now = \Carbon\Carbon::now()->format('Y/m/');
            $path = 'uploads/topup_receipt/' . $now;
            $filename = \Carbon\Carbon::now()->format('d_') . generateRandomUniqueName();

            touchFolder($path);

            if (isImageExtension($file->getClientOriginalExtension())) {
                $img = \Image::make($file);
                $img = $img->save($path . $filename . $ext, 90);
            } else {
                $file->move($path, $filename . $ext);
            }

            $this->attributes['receipt'] = $path . $filename . $ext;
        }
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after_date')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_updated_before_date')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before_date') . ' 23:59:59');
        }
    }

}