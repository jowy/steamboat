<?php

namespace App\Models;

use Input;

class CompanyBank extends BaseModels {

    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->name_en;
    }

    protected $table = 'company_bank';

    protected $fillable = [
        'bank_id',
        'account_name',
        'account_number',
    ];

    public static $rules = [
        'bank_id' => 'required|exists:bank,id',
        'account_name' => 'required|min:1|max:255',
        'account_number' => 'required|min:1|max:255',
        'country_id' => 'nullable|exists:country,id,status,1',
        'name_en' => 'nullable|min:1|max:255',
        'name_cn' => 'nullable|min:1|max:255',
    ];

    public static function boot() {
        parent::boot();
    }

    public function country() {
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    public function bank() {
        return $this->hasOne('App\Models\Bank', 'id', 'bank_id');
    }

    public function getName() {
        if ($this->bank) {
            return $this->bank->getName();
        }
        $field = 'name_' . app()->getLocale();
        return isset($this->$field) && !isEmpty($this->$field) ? $this->$field : $this->name_en;
    }

    public function getBankName() {
        return $this->getName();
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function scopeGetSelects($query) {
        $arr = array();
        foreach ($query->get() as $key => $var) {
            $arr[$var->id] = $var->getName();
        }

        return $arr;
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_country_id') && Input::get('filter_country_id') != '') {
            $query->where('country_id', '=', Input::get('filter_country_id'));
        }

        if (Input::filled('filter_name') && Input::get('filter_name') != '') {
            $query->where(function ($q) {
                $q->where('name_en', 'like', '%' . Input::get('filter_name') . '%')
                    ->orWhere('name_cn', 'like', '%' . Input::get('filter_name') . '%');
            });
        }

        if (Input::filled('filter_name_en') && Input::get('filter_name_en') != '') {
            $query->where('name_en', 'like', '%' . Input::get('filter_name_en') . '%');
        }

        if (Input::filled('filter_name_cn') && Input::get('filter_name_cn') != '') {
            $query->where('name_cn', 'like', '%' . Input::get('filter_name_cn') . '%');
        }

        if (Input::filled('filter_account_name') && Input::get('filter_account_name') != '') {
            $query->where('account_name', 'like', '%' . Input::get('filter_account_name') . '%');
        }

        if (Input::filled('filter_account_number') && Input::get('filter_account_number') != '') {
            $query->where('account_number', 'like', '%' . Input::get('filter_account_number') . '%');
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after_date')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_updated_before_date')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before_date') . ' 23:59:59');
        }
    }

}