<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierTransaction extends Model
{
    public function getAuditDescription() {
        return null;
    }

    public function needAudit() {
        return false;
    }

    protected $fillable = [

    ];

    protected $dates = [];

    public static $rules = [
        'user_id' => 'required|exists:user,id',
        'amount' => 'required|isFund',
        'transaction_type' => 'required|isUserTransaction',
        'credit_type' => 'required|isUserCreditType',
        'adjust_description' => 'in:0,1',
        'description_en' => '',
        'description_cn' => '',
        'params' => '',
    ];

    public static function boot() {
        parent::boot();
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public static function getTransactionTypeLists($empty = false, $params = array()) {
        $arr = array();
        if ($empty === true) {
            $arr[''] = trans('common.all');
        }

        $arr[103] = trans('transaction.owe');
        $arr[104] = trans('transaction.paid');

        return $arr;
    }

    public static function getFilterTransactionType($empty = false) {
        $arr = array();

        if ($empty === true) {
            $arr[''] = trans('common.all');
        }

        $arr[999] = trans('common.admin_related');

        return $arr;
    }

    public static function getFilterTransactionTypeIds($type) {
        switch ($type) {
            case 999: return [901, 902, 903, 904, 905, 906, 907, 908]; break;
            default: return []; break;
        }
    }

    public function explainTransactionType() {
        /*$params = $this->convertParams();
        if (!isset($params['credit_type'])) {
            $params['credit_type'] = trans('common.credit_' . $this->credit_type);
        }*/
        $params = [];

        if ($this->adjust_description == 1) {
            $field = 'description_' . app()->getLocale();
            return $this->$field;
        } else {
            $arr = static::getTransactionTypeLists(false);
            if (isset($arr[$this->transaction_type])) {
                return $arr[$this->transaction_type];
            } else {
                return trans('common.invalid_user_transaction');
            }
        }
    }

    public function explainCreditAction() {
        if ($this->amount > 0) {
            return trans('common.credit');
        } else {
            return trans('common.debit');
        }
    }

    public static function getPaymentMethodLists()
    {
        $arr = [];
        $arr[1] = trans('common.cash');
        $arr[2] = trans('common.credit_card');
        $arr[3] = trans('common.cheque');
        return $arr;
    }

    public function explainPaymentMethod()
    {
        $arr = static::getPaymentMethodLists();

        return isset($arr[$this->payment_method]) ? $arr[$this->payment_method] : "-";
    }

    public function scopeGetFilteredResults($query) {

    }
}
