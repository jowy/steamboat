<?php

namespace App\Models;

use Input;

class Slider extends BaseModels {

    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->title_en;
    }

    const IMAGE_WIDTH = 1920;
    const IMAGE_HEIGHT = 1080;

    protected $table = 'slider';

    protected $fillable = [
        'title_en', 'description_en', 'url_en', 'url_text_en', 'new_tab_en',
        'title_cn', 'description_cn', 'url_cn', 'url_text_cn', 'new_tab_cn',
        'start_at', 'end_at',
    ];

    public static $rules = [
        'title_en' => 'required|max:255',
        'title_cn' => 'required|max:255',
        'description_en' => '',
        'description_cn' => '',
        'url_en' => '',
        'url_text_en' => '',
        'url_cn' => '',
        'url_text_cn' => '',
        'new_tab_en' => 'required|in:0,1',
        'new_tab_cn' => 'required|in:0,1',
        'start_at' => 'required|date',
        'end_at' => 'required|after:start_at',
    ];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetAvailableSliders($query) {
        $now = new \Carbon\Carbon();
        $query->where('start_at', '<=', $now)->where('end_at', '>=', $now)->orderBy('id', 'DESC');

        return $query;
    }

    public function setTitleEnAttribute($value) {
        $this->attributes['title_en'] = trim($value);
    }
    public function setTitleCnAttribute($value) {
        $this->attributes['title_cn'] = trim($value);
    }

    public function setUrlEnAttribute($value) {
        $this->attributes['url_en'] = trim($value);
    }
    public function setUrlCnAttribute($value) {
        $this->attributes['url_cn'] = trim($value);
    }

    public function setUrlTextEnAttribute($value) {
        $this->attributes['url_text_en'] = trim($value);
    }
    public function setUrlTextCnAttribute($value) {
        $this->attributes['url_text_cn'] = trim($value);
    }

    public function setImageEnAttribute($file) {
        if ($file instanceof \Illuminate\Http\UploadedFile) {
            $img = \Image::make($file);
            $mime = $img->mime();
            $ext = convertMimeToExt($mime);

            $now = new \Carbon\Carbon();

            $path = 'uploads/slider/' . $now->format('Y/m') . '/';
            $filename = generateRandomUniqueName();

            touchFolder($path);

            $img = $img->fit(static::IMAGE_WIDTH, static::IMAGE_HEIGHT)->save($path . $filename . $ext, 90);

            $this->attributes['image_en'] = '/' . $path . $filename . $ext;
        }
    }

    public function setImageCnAttribute($file) {
        if ($file instanceof \Illuminate\Http\UploadedFile) {
            $img = \Image::make($file);
            $mime = $img->mime();
            $ext = convertMimeToExt($mime);

            $now = new \Carbon\Carbon();

            $path = 'uploads/slider/' . $now->format('Y/m') . '/';
            $filename = generateRandomUniqueName();

            touchFolder($path);

            $img = $img->fit(static::IMAGE_WIDTH, static::IMAGE_HEIGHT)->save($path . $filename . $ext, 90);

            $this->attributes['image_cn'] = '/' . $path . $filename . $ext;
        }
    }

    public function getTitles() {
        $arr = array();

        if ($this->title_en != '' && $this->title_en != null) {
            $arr[] = $this->title_en;
        }
        if ($this->title_cn != '' && $this->title_cn != null) {
            $arr[] = $this->title_cn;
        }

        return implode(' / ', $arr);
    }

    public function getImage() {
        if (app()->getLocale() == 'cn') {
            return $this->image_cn;
        } else {
            return $this->image_en;
        }
    }

    public function getTitle() {
        if (app()->getLocale() == 'cn') {
            return $this->title_cn;
        } else {
            return $this->title_en;
        }
    }

    public function getDescription() {
        if (app()->getLocale() == 'cn') {
            return $this->description_cn;
        } else {
            return $this->description_en;
        }
    }

    public function getUrl($html = false) {
        if (app()->getLocale() == 'cn') {
            $link = $this->url_cn;
        } else {
            $link = $this->url_en;
        }

        if ($html === true) {
            return $link != null && $link != '' ? $link : 'javascript:;';
        } else {
            return $link != null && $link != '' ? $link : null;
        }
    }

    public function getUrlText() {
        if (app()->getLocale() == 'cn') {
            $text = $this->url_text_cn;
        } else {
            $text = $this->url_text_en;
        }

        return $text;
    }

    public function getNewTab($html = false) {
        if (app()->getLocale() == 'cn') {
            $newtab = $this->new_tab_cn;
        } else {
            $newtab = $this->new_tab_en;
        }

        if ($html) {
            return $newtab == 1 ? '_blank' : '_self';
        } else {
            return $newtab;
        }
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_title') && Input::get('filter_title') != '') {
            $query->where(function ($q) {
                $q->where('title_en', 'like', '%' . Input::get('filter_title') . '%');
                $q->orWhere('title_cn', 'like', '%' . Input::get('filter_title') . '%');
            });
        }

        if (Input::filled('filter_start_after')) {
            $query->where('start_at', '>=', Input::get('filter_start_after'));
        }

        if (Input::filled('filter_start_before')) {
            $query->where('start_at', '<=', Input::get('filter_start_before'));
        }

        if (Input::filled('filter_end_after')) {
            $query->where('end_at', '>=', Input::get('filter_end_after'));
        }

        if (Input::filled('filter_end_before')) {
            $query->where('end_at', '<=', Input::get('filter_end_before'));
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }

}