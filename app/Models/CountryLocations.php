<?php

namespace App\Models;

use Input;

class CountryLocations extends BaseModels {

    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->location_name_en;
    }

    protected $table = 'country_locations';

    protected $fillable = [
        'country_id', 'parent_id', 'sorting', 'url', 'location_name_en', 'location_name_cn'
    ];

    protected $dates = [];

    public static $rules = array(
        'country_id' => 'required|exists:country,id',
        'parent_id' => 'exists:country_locations,id',
        'sorting' => 'isNumber',
        'location_name_en' => 'required|max:255',
        'location_name_cn' => 'required|max:255',
    );

    public static function boot() {
        parent::boot();
    }

    public function country() {
        return $this->belongsTo('App\Models\Country', 'country_id', 'id');
    }

    public function parent() {
        return $this->belongsTo('App\Models\CountryLocations', 'parent_id', 'id');
    }

    public function sons() {
        return $this->hasMany('App\Models\CountryLocations', 'parent_id', 'id')->orderBy('sorting', 'ASC');
    }
    
    public function getText() {
        $field = 'location_name_' . app()->getLocale();
        if (isset($this->$field) && $this->$field) {
            return $this->$field;
        } else {
            return $this->text_en;
        }
    }

    public function getLocationName() {
        return $this->getText();
    }

    public function getName() {
        return $this->getText();
    }

    public function getTextWithLink() {
        return $this->getText();
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function scopeGetFilteredResults($query) {
        return $query;
    }

}