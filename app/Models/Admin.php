<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Audit;
use Validator;
use Input;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class Admin extends Authenticatable
{
    use Notifiable, SoftDeletes;

    const ACCOUNT_TYPE_SUPERADMIN = 1;
    const ACCOUNT_TYPE_ADMIN = 2;

	public function needAudit() {
        return true;
    }

    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->username;
    }

    protected static $userPermissions = null;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    public static $rules = [
        'username' => 'required|max:64|notEmail|isUsername|unique:admin,username',
        'email' => 'required|email|unique:admin,email',
        'name' => 'required',
        'password' => 'required|isPassword|confirmed',
        'permission_group_id' => 'exists:permission_groups,id',
    ];

    public static function defaultUserType() {
        return static::ACCOUNT_TYPE_ADMIN;
    }

    public function getDefaultUserType() {
        return static::defaultUserType();
    }

    public function permissionGroup() {
        return $this->hasOne('App\Models\PermissionGroup', 'id', 'permission_group_id');
    }

    public function setPasswordAttribute($password) {
        if(Hash::needsRehash($password)) {
            $password = bcrypt($password);
        }
        $this->attributes['password'] = $password;
    }

    public function setEmailAttribute($value) {
        $this->attributes['email'] = trim(strtolower($value));
    }

    public function setUsernameAttribute($value) {
        $this->attributes['username'] = trim(strtolower($value));
    }

    public function isSuperAdmin() {
        return $this->account_type == static::ACCOUNT_TYPE_SUPERADMIN;
    }

    public function isAdmin() {
        return $this->account_type == static::ACCOUNT_TYPE_SUPERADMIN || $this->account_type == static::ACCOUNT_TYPE_ADMIN;
    }

    public function scopeGetAdmin($query) {
        return $query->where('account_type', '=', static::getDefaultUserType());
    }

    public function getPermissions() {
        if (static::$userPermissions === null && static::$userPermissions !== false) {
            $this->load(['permissionGroup', 'permissionGroup.permissions']);
            if ($this->permission_group_id != null && $this->permissionGroup) {
                $lists = PermissionGroupPermission::getPermissionsLists();
                $arr = array();

                foreach ($this->permissionGroup->permissions as $key => $var) {
                    if (isset($lists[$var->permission_tag])) {
                        $arr[$var->permission_tag] = $lists[$var->permission_tag];
                    } else {
                        $arr[$var->permission_tag] = sprintf('Unknown Permission %s', $var->permission_tag);
                    }
                }

                static::$userPermissions = $arr;
            } else {
                static::$userPermissions = false;
            }
        }

        return static::$userPermissions;
    }

    public function hasPermission($permission) {
        $permission = strtolower($permission);

        if ($this->isSuperAdmin()) {
            return true;
        }

        if (!$this->isAdmin()) {
            return false;
        }

        $my_permissions = $this->getPermissions();

        if ($my_permissions == false || $my_permissions == null) {
            return false;
        }

        if (isset($my_permissions[$permission])) {
            return true;
        }

        return false;
    }

    public function hasAnyPermission($permissions) {
        if ($this->isSuperAdmin()) {
            return true;
        }

        if (!$this->isAdmin()) {
            return false;
        }

        if (is_string($permissions)) {
            $permissions = explode(',', $permissions);
        }

        $my_permissions = $this->getPermissions();

        if ($my_permissions == false || $my_permissions == null) {
            return false;
        }

        $has = false;

        foreach ($permissions as $key => $var) {
            if (isset($my_permissions[$var])) {
                $has = true;
                break;
            }
        }

        return $has;
    }

    public function getAvatar($asset = true) {
        return $asset === true ? asset('img/avatar_admin.png') : 'img/avatar_admin.png';
    }

    public function getGroupName() {
        return $this->permissionGroup ? $this->permissionGroup->group_name : '-';
    }
	
	public function isDev() {
		return $this->username == 'dev';
	}

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id')) {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_username')) {
            $query->where('username', 'like', "%" . Input::get('filter_username') . "%");
        }

        if (Input::filled('filter_name')) {
            $query->where('name', 'like', "%" . Input::get('filter_name') . "%");
        }

        if (Input::filled('filter_email')) {
            $query->where('email', 'like', "%" . Input::get('filter_email') . "%");
        }

        if (Input::filled('filter_group_name') && Input::get('filter_group_name') != '') {
            $query->whereHas('permissionGroup', function ($q) {
                $q->where('group_name', 'like', '%' . Input::get('filter_group_name') . '%');
            });
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::filled('filter_status')) {
            if (Input::get('filter_status') == 1) {
                $query->where('deleted_at', '=', NULL);
            } else if (Input::get('filter_status') == 2) {
                $query->whereNotNull('deleted_at');
            }
        }

        return $query;
    }
}
