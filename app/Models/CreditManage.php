<?php

namespace App\Models;

use Input;

class CreditManage extends BaseModels {
    public function getAuditDescription() {
        return getClassName($this) . ' ' . ($this->user ? ' for ' . $this->user->username : '#' . $this->id);
    }

    protected $table = 'credit_manage';

    protected $fillable = [

    ];

    public static $rules = [
        'user_id' => 'required|exists:users,id',
        'admin_id' => 'required|exists:admin,id',
        'action_type' => 'required|isCreditManageType',
        'amount' => 'required|isFund',
        'adjust_description' => 'required|in:0,1',
        'description_en' => '',
        'description_cn' => '',
        'remark' => '',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function admin() {
        return $this->belongsTo('App\Models\Admin', 'admin_id', 'id');
    }

    public static function getActionTypeLists($empty = false, $params = array()) {
        $arr = array();

        if ($empty === true) {
            $arr[''] = trans('common.all');
        }

        $arr[1] = trans('common.admin_add_credit_1', $params);
        $arr[2] = trans('common.admin_deduct_credit_1', $params);
//        $arr[3] = trans('common.admin_add_credit_2', $params);
//        $arr[4] = trans('common.admin_deduct_credit_2', $params);
//        $arr[5] = trans('common.admin_add_credit_3', $params);
//        $arr[6] = trans('common.admin_deduct_credit_3', $params);
//        $arr[7] = trans('common.admin_add_credit_4', $params);
//        $arr[8] = trans('common.admin_deduct_credit_4', $params);

        return $arr;
    }

    public function explainActionType() {
        $params = json_decode($this->params, true);
        if (!is_array($params)) {
            $params = array();
        }
        $arr = static::getActionTypeLists(false, $params);
        if (isset($arr[$this->action_type])) {
            return $arr[$this->action_type];
        } else {
            return trans('common.unknown');
        }
    }

    public function explainCreditDebit() {
        if ($this->amount < 0) {
            return trans('common.debit');
        } else {
            return trans('common.credit');
        }
    }

    public function explainAdjustDescription() {
        if ($this->adjust_description == 1) {
            return trans('common.yes');
        } else {
            return trans('common.no');
        }
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_username') && Input::get('filter_username') != '') {
            $query->whereHas('user', function ($q) {
                $q->where('username', 'like', '%' . Input::get('filter_username') . '%');
            });
        }

        if (Input::filled('filter_admin') && Input::get('filter_admin') != '') {
            $query->whereHas('admin', function ($q) {
                $q->where('name', 'like', '%' . Input::get('filter_admin') . '%');
            });
        }

        if (Input::filled('filter_credit_debit') && Input::get('filter_credit_debit') != '') {
            if (Input::get('filter_credit_debit') == 2) {
                $query->where('amount', '<', 0);
            } else {
                $query->where('amount', '>=', 0);
            }
        }

        if (Input::filled('filter_amount_from')) {
            $query->where('amount', '>=', Input::get('filter_amount_from'));
        }

        if (Input::filled('filter_amount_to')) {
            $query->where('amount', '<=', Input::get('filter_amount_to'));
        }

        if (Input::filled('filter_action_type') && Input::get('filter_action_type') != '') {
            $query->where('action_type', '=', Input::get('filter_action_type'));
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after_date')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_updated_before_date')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before_date') . ' 23:59:59');
        }

        return $query;
    }

}