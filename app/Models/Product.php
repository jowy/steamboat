<?php

namespace App\Models;

use Input;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends BaseModels {
    use SoftDeletes;

    const IMAGE_WIDTH = 1200;
    const IMAGE_HEIGHT = 1200;

    public function getAuditDescription() {
        return getClassName($this) . ' ' . $this->product_name_en;
    }

    protected $table = 'product';

    protected $fillable = [

    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    public static $rules = array(
        'product_name_en' => 'required|string|max:255',
        'product_name_cn' => 'nullable|string|max:255',
        'product_short_description_en' => 'nullable|string|max:255',
        'product_short_description_cn' => 'nullable|string|max:255',
        'product_description_en' => 'nullable',
        'product_description_cn' => 'nullable',
        'product_details_en' => 'nullable',
        'product_details_cn' => 'nullable',
        'product_image_en' => 'nullable',
        'product_image_cn' => 'nullable',
        //'product_price' => 'required|isFund',
        'sorting' => 'nullable|isNumber',
        'is_new_product' => 'required|boolean',
    );

    public static function boot() {
        parent::boot();

        /*static::update(function ($model) {
            if($model->isDirty('price')){
                // price has changed
            }
        });*/
    }

    public function categories() {
        return $this->hasMany('App\Models\ProductCategory', 'product_id', 'id');
    }

    public function images() {
        return $this->hasMany('App\Models\ProductImages', 'product_id', 'id');
    }

    public function branch_stocks() {
        return $this->hasMany('App\Models\BranchStock', 'product_id', 'id');
    }

    public function subtract_product()
    {
        return $this->belongsTo('App\Models\Product', 'subtract_product_id', 'id');
    }

    public function packagePrices() {
        return $this->hasMany('App\Models\PackageProduct', 'product_id', 'id')->orderBy('id', 'ASC');
    }

    public function setProductImageEnAttribute($file)
    {
        if ($file instanceof \Illuminate\Http\UploadedFile) {
            $now = \Carbon\Carbon::now()->format('Y/m/');
            $path = 'uploads/product/' . $now;
            $filename = \Carbon\Carbon::now()->format('d_') . generateRandomUniqueName();

            if (isImageExtension($file->getClientOriginalExtension())) {
                $img = \Image::make($file);
                $mime = $img->mime();
                $ext = convertMimeToExt($mime);

                touchFolder($path);

                $img = $img->fit(static::IMAGE_WIDTH, static::IMAGE_HEIGHT)->save($path . $filename . $ext, 90);
            } else {
                throw new \Exception(trans('common.invalid_image'));
            }

            $this->attributes['product_image_en'] = $path . $filename . $ext;
        } else if (is_string($file)) {
            $this->attributes['product_image_en'] = $file;
        }
    }

    public function setProductImageCnAttribute($file) {
        if ($file instanceof \Illuminate\Http\UploadedFile) {
            $now = \Carbon\Carbon::now()->format('Y/m/');
            $path = 'uploads/product/' . $now;
            $filename = \Carbon\Carbon::now()->format('d_') . generateRandomUniqueName();

            if (isImageExtension($file->getClientOriginalExtension())) {
                $img = \Image::make($file);
                $mime = $img->mime();
                $ext = convertMimeToExt($mime);

                touchFolder($path);

                $img = $img->fit(static::IMAGE_WIDTH, static::IMAGE_HEIGHT)->save($path . $filename . $ext, 90);
            } else {
                throw new \Exception(trans('common.invalid_image'));
            }

            $this->attributes['product_image_cn'] = $path . $filename . $ext;
        } else if (is_string($file)) {
            $this->attributes['product_image_cn'] = $file;
        }
    }

    public function getName() {
        return $this->getProductName();
    }

    public function getProductName() {
        $field = 'product_name_' . app()->getLocale();
        if (isset($this->$field) && $this->$field && !isEmpty($this->$field)) {
            return $this->$field;
        } else {
            return $this->product_name_en;
        }
    }

    public function getImage($asset = false) {
        return $this->getProductImage($asset);
    }

    public function getProductImage($asset = false) {
        $field = 'product_image_' . app()->getLocale();
        $value = isset($this->$field) && $this->$field != null ? $this->$field : $this->image_en;

        return $asset === true ? asset($value) : $value;
    }

    public function getShortDescription() {
        return $this->getProductShortDescription();
    }

    public function getProductShortDescription() {
        $field = 'product_short_description_' . app()->getLocale();
        $value = isset($this->$field) && $this->$field != null ? $this->$field : $this->product_short_description_en;

        return $value;
    }

    public function getDescription() {
        return $this->getProductDescription();
    }

    public function getProductDescription() {
        $field = 'product_description_' . app()->getLocale();
        $value = isset($this->$field) && $this->$field != null ? $this->$field : $this->product_description_en;

        return $value;
    }

    public function getDetails($strip = false) {
        return $this->getProductDetails($strip);
    }

    public function getProductDetails($strip = false) {
        $field = 'product_details_' . app()->getLocale();
        $value = isset($this->$field) && $this->$field != null ? $this->$field : $this->product_details_en;

        if ($strip === true) {
            $value = strip_tags($value);
        }

        return $value;
    }

    public function canDelete() {
        //FOR NOW, ALL PRODUCT IS SAFE TO DELETE
        return true;
    }

    public function scopeGetProducts($query) {
        return $query;
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_product_name') && Input::get('filter_product_name') != '') {
            $query->where('product_name_en', 'LIKE', '%' . Input::get('filter_product_name') . '%')
                ->orWhere('product_name_cn', 'LIKE', '%' . Input::get('filter_product_name') . '%');
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }
    }

    public function scopeSorting($query) {
        return $query->orderBy('sorting', 'ASC');
    }
}
