<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Input;

class Supplier extends Model
{
    use SoftDeletes;

    protected $softDelete = true;

    protected $fillable = [
        'name',
        'contact_number',
        'remark',
        'contact_number'
    ];

    public static $rules = [
        'name' => 'required|min:1|max:255',
        'remark' => 'nullable|max:1500',
        'contact_number' => 'max:150',
    ];

    public function supplier_accounts()
    {
        return $this->hasMany(SupplierAccount::class, 'supplier_id');
    }

    public function supplier_branch_account()
    {
        return $this->hasOne(SupplierAccount::class, 'supplier_id')->whereBranchId(\Auth::guard('branch')->id()); // Order it by anything you want
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_name') && Input::get('filter_name') != '') {
            $query->where(function ($q) {
                $q->where('name_en', 'like', '%' . Input::get('filter_name') . '%')
                    ->orWhere('name_cn', 'like', '%' . Input::get('filter_name') . '%');
            });
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after_date')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_updated_before_date')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before_date') . ' 23:59:59');
        }
    }
}
