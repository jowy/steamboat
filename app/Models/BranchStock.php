<?php

namespace App\Models;

use Input;

class BranchStock extends BaseModels {

    public function getAuditDescription() {
        return getClassName($this) . ' #' . $this->product_name_en;
    }

    public function needAudit() {
        return false;
    }

    protected $table = 'branch_stock';

    protected $fillable = [

    ];

    public static $rules = [
        'branch_id' => 'required|exists:branch,id',
        'product_id' => 'required|exists:product,id',
        'product_name_en' => 'required|min:1|max:255',
        'product_name_cn' => 'required|min:1|max:255',
        'quantity' => 'required|isNumber',
    ];

    public static function boot() {
        parent::boot();
    }

    public function branch() {
        return $this->belongsTo('App\Models\Branch', 'branch_id', 'id')->withTrashed();
    }

    public function product() {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id')->withTrashed();
    }

    public function transactions() {
        return $this->hasMany('App\Models\BranchStockTransaction', 'stock_id', 'id');
    }

    public function getName() {
        if (app()->getLocale() == 'cn') {
            return $this->product_name_cn;
        } else {
            return $this->product_name_en;
        }
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function fillProduct(Product $product) {
        $this->product_id = $product->id;
        $this->product_name_en = $product->product_name_en;
        $this->product_name_cn = $product->product_name_cn;
    }

    public function scopeGetFilteredResults($query) {
        if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_name') && Input::get('filter_name') != '') {
            $query->where(function ($q) {
                $q->where('product_name_en', 'like', '%' . Input::get('filter_name') . '%')
                    ->orWhere('product_name_cn', 'like', '%' . Input::get('filter_name') . '%');
            });
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after_date')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_updated_before_date')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before_date') . ' 23:59:59');
        }
    }

}