<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockManage extends Model
{
    use SoftDeletes;

    public static function getActionLists()
    {
        $arr = [];
        $arr[1] = trans('common.deposit');
        $arr[2] = trans('common.withdrawal');
        return $arr;
    }

    public function explainAction()
    {
        $arr = static::getActionLists();

        return isset($arr[$this->action]) ? $arr[$this->action] : trans('common.unknown');
    }

    public static function getPlaceLists()
    {
        $arr = [];
        $arr[1] = trans('common.hall');
        $arr[2] = trans('common.kitchen');
        $arr[3] = trans('common.other');
        return $arr;
    }

    public function explainPlace()
    {
        $arr = static::getPlaceLists();

        return isset($arr[$this->place]) ? $arr[$this->place] : trans('common.unknown');
    }

    public function stock_manage_details()
    {
        return $this->hasMany(StockManageDetail::class, 'stock_manage_id');
    }
}
