<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;
use Input;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const DEFAULT_PASSWORD = '123123';
    const DEFAULT_PASSWORD2 = '123123';

    const AVATAR_WIDTH = 500;
    const AVATAR_HEIGHT = 500;

    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'last_login_at', 'new_login_at',
    ];

    protected $dates = [
        'created_at', 'updated_at', 'bvt', 'last_login_at', 'new_login_at', 'last_roi_at',
    ];

    public static function boot() {
        parent::boot();
        static::creating(function ($m) {
            if (!isset($m->password) || $m->password == false || $m->password == null || $m->password == '') {
                $m->password = static::DEFAULT_PASSWORD;
            }

            if (!isset($m->password2) || $m->password2 == false || $m->password2 == null || $m->password2 == '') {
                $m->password2 = static::DEFAULT_PASSWORD2;
            }
        });

        static::created(function ($model) {
            if ($model->first_login == 0) {
//                \Mail::send('user.welcome_email', ['model' => $model], function ($m) use ($model) {
//                    //
//                    $m->from(config('env.MAIL_FROM_ADDRESS'), config('env.MAIL_FROM_NAME'));
//
//                    $m->to($model->email, $model->getName())->subject('Welcome from ' . config('env.APP_NAME'));
//
//                });
            }
        });
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function country() {
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    public function contactCountry() {
        return $this->hasOne('App\Models\Country', 'id', 'contact_country_id');
    }

    public function bank() {
        return $this->hasOne('App\Models\Bank', 'id', 'bank_id');
    }

    public function sponsor() {
        return $this->belongsTo('App\Models\User', 'sponsor_user_id', 'id');
    }

    public function placement() {
        return $this->belongsTo('App\Models\User', 'placement_user_id', 'id');
    }

    public function sponsorTreeUsers() {
        return $this->hasMany('App\Models\User', 'sponsor_user_id', 'id');
    }

    public function placementTreeUsers() {
        return $this->hasMany('App\Models\User', 'placement_user_id', 'id');
    }

    public function leftUser() {
        return $this->hasOne('App\Models\User', 'id', 'left_user_id');
    }

    public function rightUser() {
        return $this->hasOne('App\Models\User', 'id', 'right_user_id');
    }

    public function transactions() {
        return $this->hasMany('App\Models\UserTransaction', 'user_id', 'id');
    }

    public function bonus() {
        return $this->hasMany('App\Models\UserBonus', 'user_id', 'id');
    }

    public function setPasswordAttribute($password) {
        if(Hash::needsRehash($password)) {
            $password = bcrypt($password);
        }
        $this->attributes['password'] = $password;
    }

    public function setPassword2Attribute($password) {
        if(Hash::needsRehash($password)) {
            $password = bcrypt($password);
        }
        $this->attributes['password2'] = $password;
    }

    public function setEmailAttribute($value) {
        $this->attributes['email'] = trim(strtolower($value));
    }

    public function setUsernameAttribute($value) {
        $this->attributes['username'] = trim(strtolower($value));
    }

    public function setLangAttribute($value) {
        if (array_key_exists(strtolower($value), \App\Constants::getLanguages())) {
            $this->attributes['lang'] = strtolower($value);
        } else {
            $this->attributes['lang'] = config('app.locale');
        }
    }

    public function setAvatarAttribute($file) {
        if ($file instanceof \Illuminate\Http\UploadedFile) {
            $img = \Image::make($file);
            $mime = $img->mime();
            $ext = convertMimeToExt($mime);

            $now = \Carbon\Carbon::now()->format('Y/m/');
            $path = 'uploads/avatar/' . $now;
            $filename = \Carbon\Carbon::now()->format('d_') . generateRandomUniqueName();

            touchFolder($path);

            $img = $img->fit(static::AVATAR_WIDTH, static::AVATAR_HEIGHT)->save($path . $filename . $ext, 90);

            $this->attributes['avatar'] = $path . $filename . $ext;
        } else if (is_string($file)) {
            $this->attributes['avatar'] = $file;
        }
    }

    public function setSponsorIdsAttribute($value) {
        if (is_array($value)) {
            if (count($value)) {
                $this->attributes['sponsor_ids'] = implode(',', array_unique($value));
            } else {
                $this->attributes['sponsor_ids'] = null;
            }
        } else if (is_string($value) || is_integer($value)) {
            $this->attributes['sponsor_ids'] = trim(trim($value, ','));
        } else {
            if ($value == null) {
                $this->attributes['sponsor_ids'] = null;
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        }
    }

    public function addIdToSponsorIds($value) {
        $ids = $this->getSponsorIdsAsArray();
        if (is_array($value)) {
            $ids = array_unique(array_merge($ids, $value));
        } else if ((is_string($value) || is_integer($value)) && isNumber($value)) {
            $ids[] = $value;
            $ids = array_unique($ids);
        } else {
            throw new \Exception(trans('common.unknown_error'));
        }

        $this->sponsor_ids = $ids;
    }

    public function getSponsorIdsAsArray($add_self = false) {
        if ($this->sponsor_ids == null) {
            $r = array();
        } else {
            $r = explode(',', $this->sponsor_ids);
        }

        if ($add_self === true) {
            $r[] = $this->id;
        }
        return $r;
    }

    public function getTotalSponsorSize() {
        return count($this->getSponsorIdsAsArray());
    }

    public function isIdInSponsor($id) {
        return in_array($id, $this->getSponsorIdsAsArray());
    }

    public function forceSetSponsorIds($value) {
        if (is_array($value)) {
            if (count($value)) {
                $this->attributes['sponsor_ids'] = trim(trim(implode(',', array_unique($value)), ','));
            } else {
                $this->attributes['sponsor_ids'] = null;
            }
        } else if (is_string($value) || is_integer($value)) {
            $this->attributes['sponsor_ids'] = trim(trim($value, ','));
        } else {
            if ($value == null) {
                $this->attributes['sponsor_ids'] = null;
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        }
    }

    public function forceSetPlacementIds($value) {
        if (is_array($value)) {
            if (count($value)) {
                $this->attributes['placement_ids'] = trim(trim(implode(',', array_unique($value)), ','));
            } else {
                $this->attributes['placement_ids'] = null;
            }
        } else if (is_string($value) || is_integer($value)) {
            $this->attributes['placement_ids'] = trim(trim($value, ','));
        } else {
            if ($value == null) {
                $this->attributes['placement_ids'] = null;
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        }
    }

    public function setPlacementIdsAttribute($value) {
        if (is_array($value)) {
            if (count($value)) {
                $this->attributes['placement_ids'] = implode(',', array_unique($value));
            } else {
                $this->attributes['placement_ids'] = null;
            }
        } else if (is_string($value) || is_integer($value)) {
            $this->attributes['placement_ids'] = trim(trim($value, ','));
        } else {
            if ($value == null) {
                $this->attributes['placement_ids'] = null;
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        }
    }

    public function addIdToPlacementIds($value) {
        $ids = $this->getPlacementIdsAsArray();
        if (is_array($value)) {
            $ids = array_unique(array_merge($ids, $value));
        } else if ((is_string($value) || is_integer($value)) && isNumber($value)) {
            $ids[] = $value;
            $ids = array_unique($ids);
        } else {
            throw new \Exception(trans('common.unknown_error'));
        }

        $this->placement_ids = $ids;
    }

    public function getPlacementIdsAsArray($add_self = false) {
        if ($this->placement_ids == null) {
            $r = array();
        } else {
            $r = explode(',', $this->placement_ids);
        }

        if ($add_self === true) {
            $r[] = $this->id;
        }
        return $r;
    }

    public function getTotalPlacementSize() {
        return count($this->getPlacementIdsAsArray());
    }

    public function isIdInPlacement($id) {
        return in_array($id, $this->getPlacementIdsAsArray());
    }

    public function getAvatar($asset = true) {
        if ($this->avatar != null && $this->avatar != '') {
            return $asset === true ? asset($this->avatar) : $this->avatar;
        } else {
            if ($this->gender == 1) {
                return $asset === true ? asset('img/avatar_female.png') : 'img/avatar_female.png';
            } else {
                return $asset === true ? asset('img/avatar_male.png') : 'img/avatar_male.png';
            }
        }
    }

    public function getCredit($id) {
        $field = 'credit_' . $id;
        return $this->$field;
    }
	
	public function explainGender() {
        return \App\Constants::explainGender($this->gender);
    }

    public function scopeGetUsers($query) {
        return $query;
    }
	
	public function getName() {
        if ($this->name != null) {
            return $this->name;
        } else {
            return $this->username;
        }
    }
	
	public function explainFreeze() {
        if ($this->freeze == 1) {
            return trans('common.banned');
        } else {
            return trans('common.active');
        }
    }

    public static function getCreditTypes($empty = false) {
        $arr = array();
        if ($empty === true) {
            $arr[''] = trans('common.all');
        }

        $arr[1] = trans('common.credit_1');
        $arr[2] = trans('common.credit_2');
        $arr[3] = trans('common.credit_3');
        $arr[4] = trans('common.credit_4');
        $arr[5] = trans('common.credit_5');

        return $arr;
    }

    public static function explainCreditType($credit_type) {
        $arr = static::getCreditTypes();
        if (isset($arr[$credit_type])) {
            return $arr[$credit_type];
        } else {
            return trans('common.invalid_credit_type');
        }
    }

    public function explainPackage() {
        if ($this->package) {
            return $this->package->getName();
        } else {
            return trans('common.unknown');
        }
    }

    public function isOrigin() {
        return $this->username == 'origin' || $this->id == 1;
    }

    public function buildContactNumber() {
        $str = '';
        if ($this->contactCountry) {
            $str .= $this->contactCountry->ext;
        }
        $str .= $this->contact_number;
        return $str;
    }

    public static function findForPassport($username) {
        return User::where('username', '=', $username)->first();
    }

    public function isInSamePlacementTree(User $user) {
        if ($user->id == $this->id) {
            return true;
        }

        if (in_array($user->id, $this->getPlacementIdsAsArray(true))) {
            return true;
        }

        $ids = array();
        if ($this->placement_user_id != null) {
            $start = $this->placement_user_id;
            $have_upline = true;
            $i = 0;

            while ($have_upline) {
                if ($i == 0) {
                    $user_now = User::find($start);
                } else {
                    if ($user_now->placement_user_id == null) {
                        $have_upline = false;
                        break;
                    }

                    $user_now = User::find($user_now->placement_user_id);
                }

                if (!$user_now) {
                    break;
                }

                if ($user_now) {
                    $ids[] = $user_now->id;
                }

                $i++;
            }
        }

        if (count($ids) <= 0) {
            return false;
        }

        if (in_array($user->id, $ids)) {
            return true;
        }

        return false;
    }
	
	public function getInviteLink() {
        return route('user.invite.share', ['username' => $this->username, 'timestamp' => time()]);
    }

    public function scopeGetFilteredResults($query) {
		if (Input::filled('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::filled('filter_username') && Input::get('filter_username') != '') {
            $query->where('username', 'like', '%' . Input::get('filter_username') . '%');
        }

        if (Input::filled('filter_sponsor_username') && Input::get('filter_sponsor_username') != '') {
            $query->whereHas('sponsor', function ($q) {
                $q->where('username', 'like', '%' . Input::get('filter_sponsor_username') . '%');
            });
        }

        if (Input::filled('filter_email') && Input::get('filter_email') != '') {
            $query->where('email', '=', Input::get('filter_email'));
        }

        if (Input::filled('filter_gender') && Input::get('filter_gender') != '') {
            $query->where('gender', '=', Input::get('filter_gender'));
        }

        if (Input::filled('filter_can_withdraw') && Input::get('filter_can_withdraw') != '') {
            $query->where('can_withdraw', '=', Input::get('filter_can_withdraw'));
        }

        if (Input::filled('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::filled('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::filled('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::filled('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::filled('filter_created_after_date')) {
            $query->where('created_at', '>=', Input::get('filter_created_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_created_before_date')) {
            $query->where('created_at', '<=', Input::get('filter_created_before_date') . ' 23:59:59');
        }

        if (Input::filled('filter_updated_after_date')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after_date') . ' 00:00:00');
        }

        if (Input::filled('filter_updated_before_date')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before_date') . ' 23:59:59');
        }
        
        return $query;
    }
}
