<?php

namespace App\Helpers;

final class DataTableHelper {

    protected $model = null;
    protected $request = null;
    protected $dt = null;

	public function __construct($model) {
        $this->model = $model;
        $this->request = request();
        $this->dt = new \Yajra\DataTables\EloquentDataTable($model);
    }

    public function getDt() {
	    return $this->dt;
    }

    public function setup($setup) {
        $this->dt = $setup;
        return $this;
    }

    public function render($columns = array()) {
        if ($this->request->get('export_excel') == 1) {
            //increase max execution time of this script to 30 min:
            ini_set('max_execution_time', 1800);
            //increase Allowed Memory Size of this script:
            ini_set('memory_limit','3072M');

            $microtime = microtime(true);
            $milliseconds = sprintf("%03d", ($microtime - floor($microtime)) * 1000);
            $name = \Carbon\Carbon::now()->format('Y-m-d-H-i-s-') . $milliseconds;
            return \Excel::create($name, function($excel) use ($name, $columns) {
                $excel->sheet($name, function($sheet) use ($name, $columns) {
                    $sheet->setOrientation('landscape');

                    ob_start();
                    $dtc = clone $this->dt;
                    $dtc_data = $dtc->make(true)->getData();
                    $total_records = $dtc_data->recordsFiltered;
                    unset($dtc, $dtc_data);
                    ob_end_flush();
                    $per_page = 2500;
                    $this->request->merge(['length' => $per_page]);

                    if (!count($columns) && $this->request->has('whitelists') && $this->request->get('whitelists')) {
                        $columns = explode(',', $this->request->get('whitelists'));
                        if (count($columns)) {
                            $sheet->appendRow($columns);
                        }
                    }
					
					//A to AZ, should be enough already
                    $sheet->setColumnFormat(array(
                        'A:AZ' => '@',
                    ));

                    foreach (array_keys($columns, 'actions', true) as $key) {
                        unset($columns[$key]);
                    }

                    foreach (array_keys($columns, 'action', true) as $key) {
                        unset($columns[$key]);
                    }

                    if ($total_records > $per_page) {
                        $pages = (int) ceil($total_records / $per_page);
                        for($i = 1; $i <= $pages; $i++) {
                            $dt = clone $this->dt;
                            $start = (int) ($i - 1) * $per_page;
                            $this->request->merge(['start' => $start]);
                            $page = $dt->make(true);
                            $data = $page->getData();
                            foreach ($data->data as $row) {
                                ob_start();
                                $row = collect($row);
                                if (count($columns)) {
                                    $real_row = array();
                                    foreach ($columns as $key => $var) {
                                        if (!isset($real[$var])) {
                                            $real_row[$var] = '';
                                        }
                                        if ($row->has($var)) {
                                            $real_row[$var] = (string) $row->get($var);
                                        }
                                    }
                                    $sheet->appendRow($real_row);
                                } else {
                                    $sheet->appendRow($row->toArray());
                                }
                                ob_end_flush();
                            }
                            unset($dt, $page, $data);//Release ram?
                        }
                    } else {
                        $page = $this->dt->make(true);
                        $data = $page->getData();
                        foreach ($data->data as $row) {
                            ob_start();
                            $row = collect($row);
                            if (count($columns)) {
                                $real_row = array();
                                foreach ($columns as $key => $var) {
                                    if (!isset($real[$var])) {
                                        $real_row[$var] = '';
                                    }
                                    if ($row->has($var)) {
                                        $real_row[$var] = $row->get($var);
                                    }
                                }
                                $sheet->appendRow($real_row);
                            } else {
                                $sheet->appendRow($row->toArray());
                            }
                            ob_end_flush();
                        }
                    }
                });
            })->download('xls');
        } else {
            return $this->dt->make(true);
        }
    }
	
}