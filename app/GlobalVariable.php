<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Menu;
use App\Models\MenuItems;

class GlobalVariable {
    protected static $_G = null;
    protected static $__G = null;

    public static function buildMenu() {
        $arr = array();

        //maximum 5 tiers for now, should be enough
        $withs = ['childrens', 'childrens.sons', 'childrens.sons.sons', 'childrens.sons.sons.sons', 'childrens.sons.sons.sons.sons'];

        $menus = Menu::with($withs)->orderBy('id', 'ASC')->get();
        foreach ($menus as $key => $var) {
            $arr[$var->menu_tag] = $var;
        }

        return $arr;
    }

    public static function init(Request $request) {
        if (static::$_G === null) {
            $arr = array();
            $arr['route'] = $request->route()->getName();
            $arr['params'] = $request->route()->parameters();
            $arr['user'] = $request->user('staff');
			$arr['unread_announcement'] = 0;
            if ($arr['user']) {
				$arr['unread_announcement'] = \App\Models\Announcement::whereHas('readed', function ($m) use ($arr) {
                    $m->where('user_id', '=', $arr['user']->id);
                }, '<=', 0)->count();
            }
            $arr['lang'] = app()->getLocale();
            $arr['locale'] = $arr['lang'];
            $arr['langs'] = \App\Constants::getLanguages();
            $arr['setting'] = \App\Models\Setting::getSettings();
            $arr['menus'] = static::buildMenu();

            static::$_G = $arr;
        }

        return static::$_G;
    }

    public static function init_admin(Request $request) {
        if (static::$__G === null) {
            $arr = array();
            $arr['route'] = $request->route()->getName();
            $arr['params'] = $request->route()->parameters();
            $arr['admin'] = $request->user('admin');
            $arr['lang'] = app()->getLocale();
            $arr['locale'] = $arr['lang'];
            $arr['langs'] = \App\Constants::getLanguages();
            $arr['setting'] = \App\Models\Setting::getSettings();

            static::$__G = $arr;
        }

        return static::$__G;
    }
	
	public static function init_simple(Request $request) {
        if (static::$__G === null) {
            $arr = array();
            $arr['lang'] = app()->getLocale();
            $arr['locale'] = $arr['lang'];
            $arr['langs'] = \App\Constants::getLanguages();
            $arr['setting'] = \App\Models\Setting::getSettings();

            static::$__G = $arr;
        }

        return static::$__G;
    }
}