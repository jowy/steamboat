<?php

namespace App;

class Constants {

    public static $decimal_point = 2;
    public static $default_country_id = 133;
    public static $row_per_page = 30;

    public static function getLanguages($empty = false) {
        $arr = array();
        if ($empty === true) {
            $arr[''] = trans('common.please_select');
        }
		
        $arr = array_merge($arr, config('app.locales'));
        
        return $arr;
    }

    public static function getGenders($empty = false) {
        $arr = array();
        if ($empty === true) {
            $arr[''] = trans('common.please_select');
        }
        $arr[0] = trans('common.male');
        $arr[1] = trans('common.female');

        return $arr;
    }
	
	public static function explainGender($gender) {
        $arr = static::getGenders();
        if (isset($arr[$gender])) {
            return $arr[$gender];
        } else {
            return trans('common.unknown');
        }
    }

    public static function getCountries($get = true) {
        $c = \App\Models\Country::where('status', '=', \App\Models\Country::STATUS_ACTIVE);
        if ($get === true) {
            return $c->get();
        } else {
            return $c;
        }
    }

    public static function getCountryForSelect() {
        $c = static::getCountries(false);
        if (app()->getLocale() == 'cn') {
            return $c->pluck('country_name_cn', 'id');
        } else {
            return $c->pluck('country_name_en', 'id');
        }
    }

    public static function getCountryWithFlag() {
        $c = static::getCountries(false);
        $lists = array();
        $field = 'country_name_' . app()->getLocale();
        foreach ($c->get() as $country) {
            if ($country->ext != null) {
                $lists[$country->id]['icon'] = 'flag-icon-' . strtolower($country->country_code_alpha_2);
                $lists[$country->id]['id'] = $country->id;
                $lists[$country->id]['name'] = $country->$field;
                $lists[$country->id]['ext'] = $country->ext;
            }
        }

        return $lists;
    }
	
	public static function getYesNoForSelect() {
        return [
            0 => trans('common.no'),
            1 => trans('common.yes'),
        ];
    }
	
	public static function explainYesNo($key) {
        $arr = static::getYesNoForSelect();

        if (isset($arr[$key])) {
            return $arr[$key];
        } else {
            return trans('common.unknown');
        }
    }

    public static function getFlagClassByLanguage($language) {
        switch ($language) {
            default: return 'flag-icon-us'; break;
            case 'cn': return 'flag-icon-cn'; break;
        }
    }

    public static function getEnabledDisabled() {
        $arr = array();
        $arr[0] = trans('common.disabled');
        $arr[1] = trans('common.enabled');
        return $arr;
    }

    public static function explainEnabledDisabled($value) {
        $arr = static::getEnabledDisabled();
        return isset($arr[$value]) ? $arr[$value] : trans('common.unknown');
    }

    public static function buildCurrencyAmount($amount, $country) {
        $str = '';

        if ($country->currency_prefix != null) {
            $str .= $country->currency_prefix . ' ';
        }

        $str .= fundFormat($amount);

        if ($country->currency_suffix != null) {
            $str .= ' ' . $country->currency_suffix;
        }

        return $str;
    }
}