<?php

namespace App\Http\Controllers\Branch;

use App\Http\Controllers\UserBaseController;
use App\Models\BranchStock;
use App\Models\BranchStockTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StockController extends UserBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $dt = dt(BranchStock::whereBranchId(\Auth::guard('branch')->id())->with('product'));

            return $dt->setup(
                $dt->getDt()
                    ->addColumn('is_ala_carte', function ($m) {
                        if ($m->product->is_ala_carte) {
                            return trans('common.yes');
                        } else {
                            return trans('common.no');
                        }
                    })
                    ->addColumn('ala_carte_price', function ($m) {
                        if ($m->product->is_ala_carte) {
                            return fundFormat($m->product->ala_carte_price);
                        } else {
                            return "-";
                        }

                    })
                    ->addColumn('actions', function ($model) {
                        $actions = buildLinkHtml([
                            'url' => route('branch.stock.edit', ['id' => $model->id]),
                            'description' => '<i class="fa fa-eye"></i>',
                        ]);
                        return $actions;
                    })->rawColumns(['actions'])
            )->render();
        } else {
            return view('branch.stock.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $model = BranchStockTransaction::whereBranchId(\Auth::guard('branch')->id())
                        ->whereStockId($id);

            $dt = dt($model);

            return $dt->setup(
                $dt->getDt()
                    ->editColumn('transaction_type', function ($m) {
                        return $m->explainTransactionType();
                    })
                    ->addColumn('actions', function ($model) {
                        $actions = buildLinkHtml([
                            'url' => route('branch.stock.edit', ['id' => $model->id]),
                            'description' => '<i class="fa fa-eye"></i>',
                        ]);
                        return $actions;
                    })->rawColumns(['actions'])
            )->render();
        } else {
            if ($branchStock = BranchStock::whereId($id)->whereBranchId(\Auth::guard('branch')->id())->first()) {
                return view('branch.stock.edit', compact('branchStock'));
            }
        }
        return redirect()->route('branch.stock.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        if ($request->ajax()) {
            // Validator
            $rules = [
                'in_out_type' => 'required|in:add,deduct',
                'in_out_qty' => 'required|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
                'remark' => 'nullable|max:200',
            ];

            $this->validate($request, $rules);

            \DB::beginTransaction();
            try {
                $branchStock = BranchStock::whereId($id)->first();
                $transactionType = BranchStockTransaction::TYPE_RESTOCK_IN;
                if ($request->input('in_out_type') == 'deduct') {
                    $request->merge(['in_out_qty' => -1 * abs($request->input('in_out_qty'))]);
                    $transactionType = BranchStockTransaction::TYPE_RESTOCK_OUT;
                }
                $branchStock->quantity = $branchStock->quantity + $request->input('in_out_qty');
                $branchStock->save();

                $branchStockTranscation = new BranchStockTransaction();
                $branchStockTranscation->branch_id = \Auth::guard('branch')->id();
                $branchStockTranscation->product_id = $branchStock->product_id;
                $branchStockTranscation->stock_id = $branchStock->id;
                $branchStockTranscation->product_name_en = $branchStock->product_name_en;
                $branchStockTranscation->transaction_type = $transactionType;
                $branchStockTranscation->quantity = $request->get('in_out_qty');
                $branchStockTranscation->remark = $request->get('remark');
                $branchStockTranscation->params = $request;
                $branchStockTranscation->save();

                \DB::commit();
                addSuccess(trans('common.stock_updated_success'));
                return makeResponse(true, trans('common.stock_updated_success'));
            } catch (\Exception $exception) {
                \DB::rollBack();
                return makeResponse(false, $exception->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
