<?php

namespace App\Http\Controllers\Branch;

use App\Http\Controllers\UserBaseController;
use App\Models\BranchWalletTransaction;
use Illuminate\Http\Request;

class WalletController extends UserBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $model = BranchWalletTransaction::whereBranchId(\Auth::guard('branch')->id());

            $dt = dt($model);

            return $dt->setup(
                $dt->getDt()
                    ->editColumn('transaction_type', function ($m) {
                        return $m->explainTransactionType();
                    })
                    /*->addColumn('actions', function ($model) {
                        $actions = buildLinkHtml([
                            'url' => route('branch.stock.edit', ['id' => $model->id]),
                            'description' => '<i class="fa fa-eye"></i>',
                        ]);
                        return $actions;
                    })->rawColumns(['actions'])*/
            )->render();
        } else {
            $user = \Auth::guard('branch')->user();
            return view('branch.wallet.index', compact('user'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        if ($request->ajax()) {
            // Validator
            $rules = [
                'in_out_type' => 'required|in:add,deduct',
                'in_out_wallet' => 'required|isFund',
                'remark' => 'nullable|max:200',
            ];

            $this->validate($request, $rules);

            \DB::beginTransaction();
            try {
                $user = \Auth::guard('branch')->user();
                if ($request->input('in_out_type') == 'deduct') {
                    $transactionType = BranchWalletTransaction::TYPE_BRANCH_DEDUCT;
                    $request->merge(['in_out_wallet' => -1 * abs($request->input('in_out_wallet'))]);
                } else {
                    $transactionType = BranchWalletTransaction::TYPE_BRANCH_ADD;
                    $request->merge(['in_out_wallet' => abs($request->input('in_out_wallet'))]);
                }
                $user->credit_1 = $user->credit_1 + $request->input('in_out_wallet');
                $user->save();

                // Create branch wallet transaction
                $branchWalletTransaction = new BranchWalletTransaction();
                $branchWalletTransaction->branch_id = $user->id;
                $branchWalletTransaction->transaction_type = $transactionType;
                $branchWalletTransaction->amount = $request->input('in_out_wallet');
                $branchWalletTransaction->remark = $request->input('remark');
                $branchWalletTransaction->params = json_encode($request->all(), true);
                $branchWalletTransaction->save();

                \DB::commit();
                addSuccess(trans('common.operation_success'));
                return makeResponse(true, trans('common.operation_success'));
            } catch (\Exception $exception) {
                \DB::rollBack();
                return makeResponse(false, $exception->getMessage());
            }
        }
    }
}
