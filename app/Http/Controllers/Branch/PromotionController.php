<?php

namespace App\Http\Controllers\Branch;

use App\Http\Controllers\UserBaseController;
use App\Models\Promotion;
use Illuminate\Http\Request;
use DataTables;

class PromotionController extends UserBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (isAjax()) {
            $promotion = Promotion::query();

            return DataTables::of($promotion)
                /*->filter(function ($model) {
                    return $model->GetFilteredResults();
                })*/
                ->editColumn('free_pax_count', function ($model) {
                    return ($model->free_pax_count) ? $model->free_pax_count : '-' ;
                })
                ->addColumn('actions', function ($model) {
                    $actions = buildLinkHtml([
                        'url' => route('branch.promotion.edit', ['id' => $model->id]),
                        'description' => '<i class="fa fa-eye"></i>',
                    ]);
                    /*$actions .= buildConfirmationLinkHtml([
                        'url' => route('branch.promotion.delete', ['id' => $model->id]),
                        'description' => '<i class="fa fa-trash"></i>',
                    ]);*/
                    return $actions;
                })
                ->make(true);
        } else {
            return view('branch.promotion.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $promotion = new Promotion();
        return view('branch.promotion.form', compact('promotion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = ['free_pax_count' => 'nullable|integer|max:100'];

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            if (Promotion::whereBranchId(\Auth::guard('branch')->id())->exists()) {
                return makeResponse(false, trans('common.error'));
            }
            $model = new Promotion();
            $model->branch_id = \Auth::guard('branch')->id();
            if ($request->get('free_pax_count') && $request->get('free_pax_count') > 0) {
                $model->free_pax_count = $request->get('free_pax_count');
            } else {
                $model->free_pax_count = null;
            }
            $model->save();

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Promotion $promotion
     * @return \Illuminate\Http\Response
     */
    public function edit(Promotion $promotion)
    {
        $mode = 'edit';
        return view('branch.promotion.form', compact('mode', 'promotion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promotion $promotion)
    {
        $rules = ['free_pax_count' => 'nullable|integer|max:100'];

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            if ($promotion->branch_id != \Auth::guard('branch')->id()) {
                return makeResponse(false, trans('common.error'));
            }

            if ($request->get('free_pax_count') && $request->get('free_pax_count') > 0) {
                $promotion->free_pax_count = $request->get('free_pax_count');
            } else {
                $promotion->free_pax_count = null;
            }
            $promotion->save();

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }
}
