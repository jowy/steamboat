<?php

namespace App\Http\Controllers\Branch;

use App\Http\Controllers\UserBaseController;
use App\Models\BranchStock;
use App\Models\BranchStockTransaction;
use App\Models\StockManage;
use App\Models\StockManageDetail;
use Illuminate\Http\Request;

class StockManageController extends UserBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $dt = dt(StockManage::whereBranchId(\Auth::guard('branch')->id()));

            return $dt->setup(
                $dt->getDt()
                    ->editColumn('action', function ($m) {
                        return $m->explainAction();
                    })
                    ->editColumn('place', function ($m) {
                        return $m->explainPlace();
                    })
                    ->addColumn('actions', function ($model) {
                        $actions = buildLinkHtml([
                            'url' => route('branch.stock-manage.show', ['id' => $model->id]),
                            'description' => '<i class="fa fa-eye"></i>',
                        ]);
                        return $actions;
                    })->rawColumns(['actions'])
            )->render();
        } else {
            return view('branch.stock-manage.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stockManage = new StockManage();
        $stocks = BranchStock::whereBranchId(\Auth::guard('branch')->id())->pluck('product_name_en', 'id');

        return view('branch.stock-manage.create', compact('stockManage', 'stocks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            # Validator
            $rules = [
                'action_id' => 'required|integer|in:' . implode(',', array_keys(StockManage::getActionLists())),
                'place' => 'required|integer|in:' . implode(',', array_keys(StockManage::getPlaceLists())),
                'stock_manage_detail' => 'required',
                'stock_manage_detail.*' => 'required|array',
                'stock_manage_detail.*.stock_id' => 'required|exists:branch_stock,id,branch_id,'.\Auth()->guard('branch')->id(),
                'stock_manage_detail.*.quantity' => 'required|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
                'remark' => 'max:2000'
            ];
            $this->validate($request, $rules);

            \DB::beginTransaction();
            try {
                // Create new stock manage
                $stockManage = new StockManage();
                $stockManage->branch_id = \Auth::guard('branch')->id();
                $stockManage->action = $request->get('action_id');
                $stockManage->place = $request->get('place');
                $stockManage->remark = $request->get('remark');
                $stockManage->save();

                // Create stock manage detail
                foreach ($request->input('stock_manage_detail') as $key => $value) {
                    $branchStock = BranchStock::find($value['stock_id']);

                    $quantity = abs($value['quantity']);
                    $transactionType = BranchStockTransaction::TYPE_RESTOCK_IN;
                    if ($request->get('action_id') == 2) { # Withdrawal action
                        $quantity = -1 * abs($value['quantity']);
                        $transactionType = BranchStockTransaction::TYPE_RESTOCK_OUT;

                        # Stock quantity cannot be negative balance
                        if ($branchStock->quantity < $value['quantity']) {
                            throw new \Exception(trans('common.cannot_bigger_than_stock_balance', ['stock' => $branchStock->product_name_en, 'balance' => $branchStock->quantity]));
                        }
                    }

                    $stockManageDetail = new StockManageDetail();
                    $stockManageDetail->stock_manage_id = $stockManage->id;
                    $stockManageDetail->branch_stock_id = $branchStock->id;
                    $stockManageDetail->quantity = $quantity;
                    $stockManageDetail->save();

                    // Update branch stock quantity
                    $branchStock->quantity += $quantity;
                    $branchStock->save();

                    // Create branch stock transaction
                    $branchStockTransaction = new BranchStockTransaction();
                    $branchStockTransaction->branch_id = \Auth::guard('branch')->id();
                    $branchStockTransaction->product_id = $branchStock->product_id;
                    $branchStockTransaction->stock_id = $branchStock->id;
                    $branchStockTransaction->product_name_en = $branchStock->product_name_en;
                    $branchStockTransaction->transaction_type = $transactionType;
                    $branchStockTransaction->quantity = $quantity;
                    $branchStockTransaction->remark = 'Branch: ' . \Auth::guard('branch')->user()->name_en . ' restock on stock manage id: ' . $stockManage->id;
                    $branchStockTransaction->params = json_encode($request->all(), true);
                    $branchStockTransaction->save();
                }

                \DB::commit();

                addSuccess(trans('common.operation_success'));
                return makeResponse(true, trans('common.operation_success'));
            } catch (Exception $exception) {
                \DB::rollback();
                return makeResponse(false, $exception->getMessage());
            }
        }
        return response()->json(trans('common.error'), JsonResponse::HTTP_BAD_REQUEST);
    }

    public function show(StockManage $stockManage)
    {
        return view('branch.stock-manage.show', compact('stockManage'));
    }
}
