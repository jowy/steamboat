<?php

namespace App\Http\Controllers\Branch;

use App\Http\Controllers\UserBaseController;
use App\Models\BranchWalletTransaction;
use App\Models\Supplier;
use App\Models\SupplierAccount;
use App\Models\SupplierTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupplierController extends UserBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $dt = dt(Supplier::with('supplier_branch_account'));

            return $dt->setup(
                $dt->getDt()
                    ->addColumn('account_credit', function ($m) {
                        return fundFormat($m->supplier_branch_account->account_credit);
                    })
                    ->addColumn('actions', function ($model) {
                        $actions = buildLinkHtml([
                            'url' => route('branch.supplier.edit', ['id' => $model->id]),
                            'description' => '<i class="fa fa-eye"></i>',
                        ]);
                        return $actions;
                    })->rawColumns(['actions'])
            )->render();
        } else {
            return view('branch.supplier.index');
        }
    }

    public function edit(Request $request, Supplier $supplier)
    {
        if ($request->ajax()) {
            $model = SupplierTransaction::whereBranchId(\Auth::guard('branch')->id())
                ->whereSupplierId($supplier->id);

            $dt = dt($model);

            return $dt->setup(
                $dt->getDt()
                    ->editColumn('payment_method', function ($m) {
                        return $m->explainPaymentMethod();
                    })
                    ->editColumn('transaction_type', function ($m) {
                        return $m->explainTransactionType();
                    })
            )->render();
        } else {
            return view('branch.supplier.edit', compact('supplier'));
        }
    }

    public function update(Request $request, Supplier $supplier)
    {
        if ($request->ajax()) {
            // Validator
            $rules = [
                'in_out_type' => 'required|in:add,deduct',
                'adjustment_credit' => 'required|isFund',
                'payment_method' => 'required|integer|in:' . implode(',', array_keys(SupplierTransaction::getPaymentMethodLists())),
                'remark' => 'nullable|max:200',
            ];

            $this->validate($request, $rules);

            \DB::beginTransaction();
            try {
                $transactionType = 103;
                if ($request->input('in_out_type') == 'deduct') {
                    $request->merge(['adjustment_credit' => -1 * abs($request->input('adjustment_credit'))]);
                    $transactionType = 104;
                }

                $supplierAccount = SupplierAccount::whereSupplierId($supplier->id)->whereBranchId(\Auth::guard('branch')->id())->first();
                $supplierAccount->account_credit += $request->input('adjustment_credit');
                $supplierAccount->save();

                $supplierTranscation = new SupplierTransaction();
                $supplierTranscation->branch_id = \Auth::guard('branch')->id();
                $supplierTranscation->supplier_id = $supplier->id;
                $supplierTranscation->transaction_type = $transactionType;
                $supplierTranscation->payment_method = $request->get('payment_method');
                $supplierTranscation->amount = $request->get('adjustment_credit');
                $supplierTranscation->remark = $request->get('remark');
                $supplierTranscation->params = $request;
                $supplierTranscation->save();

                // Deduct from wallet if pay by cash
                if ($request->input('in_out_type') == 'deduct' && $request->get('payment_method') == 1) {
                    $user = \Auth::guard('branch')->user();
                    $user->credit_1 += $request->get('adjustment_credit');
                    $user->save();

                    // Create branch wallet transaction
                    $branchWalletTransaction = new BranchWalletTransaction();
                    $branchWalletTransaction->branch_id = $user->id;
                    $branchWalletTransaction->transaction_type = BranchWalletTransaction::TYPE_SUPPLIER_PAID;
                    $branchWalletTransaction->amount = $request->get('adjustment_credit');
                    $branchWalletTransaction->remark = 'Branch: ' . $user->name_en . ' pay to supplier ' . $supplier->name;
                    $branchWalletTransaction->params = json_encode($request->all(), true);
                    $branchWalletTransaction->save();
                }

                \DB::commit();
                addSuccess(trans('common.operation_success'));
                return makeResponse(true, trans('common.operation_success'));
            } catch (\Exception $exception) {
                \DB::rollBack();
                return makeResponse(false, $exception->getMessage());
            }
        }
        return redirect()->route('branch.supplier.index');
    }
}
