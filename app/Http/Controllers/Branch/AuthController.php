<?php

namespace App\Http\Controllers\Branch;

use App\Http\Controllers\UserBaseController;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends UserBaseController
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/branch';

    public function login()
    {
        return view('branch.auth.login');
    }

    public function authenticate(Request $request)
    {
        $this->validate($request, [
           'username' => 'required',
           'password' => 'required'
        ]);

        $login = \Auth::guard('branch')->attempt([
            'username' => $request->input('username'),
            'password' => $request->input('password')
        ], $request->has('remember'));

        if (!$login) {
            return $this->sendFailedLoginResponse($request);
        }

        return makeResponse(true, trans('common.login_success'));
    }

    public function logout()
    {
        \Auth::guard('branch')->logout();

        return redirect()->route('branch.login');
    }

    /**
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return makeResponse(false, trans('common.authentication_failed'));
    }
}
