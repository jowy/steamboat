<?php

namespace App\Http\Controllers\Branch;

use App\Http\Controllers\UserBaseController;
use App\Models\Branch;
use App\Models\Country;
use Illuminate\Http\Request;

class BranchController extends UserBaseController
{
    public function profile()
    {
        $countries = Country::GetCountries()->GetSelects();
        $profile = \Auth::guard('branch')->user();

        return view('branch.branch.profile', compact('countries', 'profile'));
    }

    public function updateProfile(Request $request)
    {
        if ($request->ajax()) {
            $profile = \Auth::guard('branch')->user();

            if (!$profile) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $rules = Branch::$rules;
            unset($rules['username']);
            $rules = array_merge($rules, [
                'password' => 'nullable|min:6|confirmed'
            ]);
            $this->validate($request, $rules);

            $profile->fill($request->all());
            if ($request->input('password')) {
                $profile->password = bcrypt($request->input('password'));
            }
            $profile->save();

            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        }
    }
}
