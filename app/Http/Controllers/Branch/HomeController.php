<?php

namespace App\Http\Controllers\Branch;

use App\Http\Controllers\UserBaseController;
use Illuminate\Http\Request;

class HomeController extends UserBaseController
{
    public function index()
    {
        return view('branch.dashboard');
    }
}
