<?php

namespace App\Http\Controllers\Branch;

use App\Http\Controllers\UserBaseController;
use App\Models\BranchTable;
use Illuminate\Http\Request;

class BranchTableController extends UserBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $dt = dt(BranchTable::whereBranchId(\Auth::guard('branch')->id()));

            return $dt->setup(
                $dt->getDt()
                    ->addColumn('actions', function ($model) {
                        $actions = buildLinkHtml([
                            'url' => route('branch.branch-table.edit', ['id' => $model->id]),
                            'description' => '<i class="fa fa-eye"></i>',
                        ]);
                        $actions .= buildConfirmationLinkHtml([
                            'url' => route('branch.branch-table.delete', ['id' => $model->id]),
                            'description' => '<i class="fa fa-trash"></i>',
                        ]);
                        return $actions;
                    })->rawColumns(['actions'])
            )->render();
        } else {
            return view('branch.table.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branchTable = new BranchTable();

        return view('branch.table.form', compact('branchTable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        // Validation
        $rules = [
            'table_no' => 'required|max:100|unique:branch_tables,id,NULL,id,branch_id,'.\Auth::guard('branch')->id(),
            'pax' => 'nullable|integer|max:100',
        ];

        $this->validate($request, $rules);

        // Store table
        $branchTable = new BranchTable($request->all());
        $branchTable->branch_id = \Auth::guard('branch')->id();
        $branchTable->save();

        addSuccess(trans('common.operation_success'));
        return makeResponse(true, trans('common.operation_success'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param BranchTable $branchTable
     * @return \Illuminate\Http\Response
     */
    public function edit(BranchTable $branchTable)
    {
        if ($branchTable->branch_id = \Auth::guard('branch')->id()) {
            $mode = 'edit';
            return view('branch.table.form', compact('branchTable', 'mode'));
        }
        return redirect()->route('branch.table.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param BranchTable $branchTable
     * @return
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, BranchTable $branchTable)
    {
        // Validation
        $rules = [
            'table_no' => 'required|max:100|unique:branch_tables,id,'.$branchTable->id.',id,branch_id,'.\Auth::guard('branch')->id(),
            'pax' => 'nullable|integer|max:100',
        ];

        $this->validate($request, $rules);

        // Store table
        $branchTable->fill($request->all());
        $branchTable->save();

        addSuccess(trans('common.operation_success'));
        return makeResponse(true, trans('common.operation_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id) {
        try {
            $model = BranchTable::whereId($id)->whereBranchId(\Auth::guard('branch')->id())->first();

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            if ($model->delete()) {
                return makeResponse(true, trans('common.operation_success'));
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }
}
