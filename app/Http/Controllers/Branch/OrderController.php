<?php

namespace App\Http\Controllers\Branch;

use App\Http\Controllers\UserBaseController;
use App\Models\BranchStock;
use App\Models\BranchStockTransaction;
use App\Models\BranchWalletTransaction;
use App\Models\Inventory;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Supplier;
use App\Models\SupplierAccount;
use App\Models\SupplierTransaction;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends UserBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $dt = dt(Order::whereBranchId(\Auth::guard('branch')->id())->with('supplier'));

            return $dt->setup(
                $dt->getDt()
                    ->editColumn('total_order_cost', function ($m) {
                        return fundFormat($m->total_order_cost);
                    })
                    ->addColumn('actions', function ($model) {
                        $actions = buildLinkHtml([
                            'url' => route('branch.order.show', ['id' => $model->id]),
                            'description' => '<i class="fa fa-eye"></i>',
                        ]);
                        return $actions;
                    })->rawColumns(['actions'])
            )->render();
        } else {
            return view('branch.order.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $order = new Order();
        $suppliers = Supplier::pluck('name', 'id');
        $products = Product::whereNull('subtract_product_id')
            ->whereNull('subtract_quantity')->pluck('product_name_en', 'id');

        return view('branch.order.create', compact('order', 'suppliers', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            # Validator
            $rules = [
                'select_supplier' => 'required|exists:suppliers,id',
                'order_stock_detail' => 'required',
                'order_stock_detail.*' => 'required|array',
                'order_stock_detail.*.product_id' => 'required|exists:product,id',
                'order_stock_detail.*.quantity' => 'required|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
                'order_stock_detail.*.total_price' => 'required|isFund',
                //'total_order_cost' => 'required|isFund',
                'payment_method' => 'required|integer|in:' . implode(',', array_keys(SupplierTransaction::getPaymentMethodLists())),
                'paid' => 'nullable|isFund',
                'remark' => 'max:2000'
            ];
            $this->validate($request, $rules);

            \DB::beginTransaction();
            try {
                // Create new return stock
                $order = new Order();
                $order->supplier_id = $request->get('select_supplier');
                $order->branch_id = \Auth::guard('branch')->id();
                $order->order_ref_id = time() . mt_rand() . \Auth::guard('branch')->id();
                $order->remark = $request->get('remark');
                $order->save();

                $totalOrderCost = 0;
                // Create order stock detail
                foreach ($request->input('order_stock_detail') as $key => $value) {
                    $product = Product::find($value['product_id']);

                    $orderDetail = new OrderDetail();
                    $orderDetail->order_id = $order->id;
                    $orderDetail->product_id = $product->id;
                    $orderDetail->product_name = $product->product_name_en;
                    $orderDetail->quantity = $value['quantity'];
                    $orderDetail->quantity_label = $product->quantity_label;
                    $orderDetail->total_price = $value['total_price'];
                    $orderDetail->save();

                    $totalOrderCost += $value['total_price'];

                    // Create or update branch stock
                    if (!$branchStock = BranchStock::whereProductId($orderDetail->product_id)->whereBranchId(\Auth::guard('branch')->id())->first()) {
                        $branchStock = new BranchStock();
                        $branchStock->product_id = $orderDetail->product_id;
                        $branchStock->branch_id = \Auth::guard('branch')->id();
                        $branchStock->product_name_en = $orderDetail->product_name;
                        $branchStock->quantity = $orderDetail->quantity;
                        $branchStock->save();
                    } else {
                        $branchStock->product_name_en = $orderDetail->product_name;
                        $branchStock->quantity = $branchStock->quantity + $orderDetail->quantity;
                        $branchStock->save();
                    }

                    // Create stock transaction
                    $branchStockTransaction = new BranchStockTransaction();
                    $branchStockTransaction->branch_id = \Auth::guard('branch')->id();
                    $branchStockTransaction->product_id = $branchStock->product_id;
                    $branchStockTransaction->stock_id = $branchStock->id;
                    $branchStockTransaction->product_name_en = $branchStock->product_name_en;
                    $branchStockTransaction->transaction_type = BranchStockTransaction::TYPE_RESTOCK_IN;
                    $branchStockTransaction->quantity = $value['quantity'];
                    $branchStockTransaction->remark =  'Branch: '.\Auth::guard('branch')->user()->name_en.' received order stock: '.$order->order_ref_id;
                    $branchStockTransaction->params = json_encode($request->all(), true);
                    $branchStockTransaction->save();
                }

                $supplier = Supplier::find($request->get('select_supplier'));
                $supplierAccount = SupplierAccount::whereSupplierId($order->supplier_id)->
                whereBranchId(\Auth::guard('branch')->id())
                    ->first();

                // Create supplier transaction
                if ($totalOrderCost > 0) {
                    $totalOrderCost = abs($totalOrderCost);

                    $orderSupplierTranscation = new SupplierTransaction();
                    $orderSupplierTranscation->branch_id = \Auth::guard('branch')->id();
                    $orderSupplierTranscation->supplier_id = $order->supplier_id;
                    $orderSupplierTranscation->order_id = $order->id;
                    $orderSupplierTranscation->transaction_type = 103;
                    $orderSupplierTranscation->amount = $totalOrderCost;
                    $orderSupplierTranscation->remark = 'Order stock from '.$supplier->name.', order reference id: '.$order->order_ref_id;
                    $orderSupplierTranscation->params = json_encode($request->all(), true);
                    $orderSupplierTranscation->save();

                    // Update order cost
                    $order->total_order_cost = $totalOrderCost;
                    $order->save();

                    // Update supplier account
                    $supplierAccount->account_credit += $totalOrderCost;
                    $supplierAccount->save();
                }

                if ($request->get('paid') > 0) {
                    // Convert to negative
                    $paid = -1 * abs($request->get('paid'));

                    $orderSupplierTranscation = new SupplierTransaction();
                    $orderSupplierTranscation->branch_id = \Auth::guard('branch')->id();
                    $orderSupplierTranscation->supplier_id = $order->supplier_id;
                    $orderSupplierTranscation->order_id = $order->id;
                    $orderSupplierTranscation->transaction_type = 104;
                    $orderSupplierTranscation->payment_method = $request->get('payment_method');
                    $orderSupplierTranscation->amount = $paid;
                    $orderSupplierTranscation->remark = 'Order stock from ' . $supplier->name . ', order reference id: ' . $order->order_ref_id;
                    $orderSupplierTranscation->params = json_encode($request->all(), true);
                    $orderSupplierTranscation->save();

                    // Update supplier account
                    $supplierAccount->account_credit += $paid;
                    $supplierAccount->save();

                    // Deduct from wallet if pay by cash
                    if ($request->get('payment_method') == 1) {
                        $user = \Auth::guard('branch')->user();
                        $user->credit_1 += $paid;
                        $user->save();

                        // Create branch wallet transaction
                        $branchWalletTransaction = new BranchWalletTransaction();
                        $branchWalletTransaction->branch_id = $user->id;
                        $branchWalletTransaction->transaction_type = BranchWalletTransaction::TYPE_SUPPLIER_PAID;
                        $branchWalletTransaction->amount = $paid;
                        $branchWalletTransaction->remark = 'Branch: ' . $user->name_en . ' pay order for order ref id: ' . $order->order_ref_id;
                        $branchWalletTransaction->params = json_encode($request->all(), true);
                        $branchWalletTransaction->save();
                    }
                }

                \DB::commit();

                addSuccess(trans('common.order_stock_success'));
                return makeResponse(true, trans('common.order_stock_success'));
            } catch (Exception $exception) {
                \DB::rollback();
                return makeResponse(false, $exception->getMessage());
            }
        }
        return response()->json(trans('common.error'), JsonResponse::HTTP_BAD_REQUEST);
    }

    public function show(Order $order)
    {
        return view('branch.order.show', compact('order'));
    }

    public function printInvoice(Order $order)
    {
        if ($order->branch_id == \Auth::guard('branch')->id()) {
            return view('branch.order.invoice', compact('order'));
        }
        return redirect()->route('branch.order.index');
    }
}
