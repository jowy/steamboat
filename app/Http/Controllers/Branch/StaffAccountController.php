<?php

namespace App\Http\Controllers\Branch;

use App\Http\Controllers\UserBaseController;
use App\Models\Sale;
use App\Models\Staff;
use App\Models\StaffAccount;
use Illuminate\Http\Request;

class StaffAccountController extends UserBaseController
{
    public function summary() {
        return view('branch.staff.account.summary');
    }

    public function summaryPost(Request $request) {
        $this->validate($request, [
            'start_at' => 'required|date',
            'end_at' => 'required|date',
            'staff_id' => 'required|exists:staff,id,branch_id,' . \Auth::guard('branch')->user()->id,
        ]);

        try {
            $me = \Auth::guard('branch')->user();

            $staff = Staff::where('branch_id', '=', $me->id)->find($request->get('staff_id'));

            $started = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request->get('start_at') . ':00');
            $ended = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request->get('end_at') . ':00');

            $sales = Sale::where('staff_id', '=', $staff->id)
                ->where('created_at', '>=', $started)
                ->where('created_at', '<=', $ended);

            $accounts = StaffAccount::where('staff_id', '=', $staff->id)
                ->where('created_at', '>=', $started)
                ->where('created_at', '<=', $ended)
                ->orderBy('id', 'ASC')
                ->get();

            $balance = $sales->sum('grand_total') - $accounts->sum('amount');

            return view('staff_account_receipt', [
                'started' => $started,
                'ended' => $ended,
                'sales' => $sales,
                'accounts' => $accounts,
                'balance' => $balance,
                'cash_collected' => $request->get('cash_collected', 0),
                'staff' => $staff,
            ]);
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function index(Request $request) {
        return view('branch.staff.account.index');
    }

    public function indexDt(Request $request) {
        $dt = dt(StaffAccount::with(['staff', 'branch'])->where('branch_id', '=', \Auth::guard('branch')->user()->id)->GetRecords());

        return $dt->setup(
            $dt->getDt()
                ->editColumn('staff_id', function ($m) {
                    return $m->staff ? $m->staff->username : '-';
                })
                ->editColumn('branch_id', function ($m) {
                    return $m->branch ? $m->branch->username : '-';
                })
                ->editColumn('type', function ($m) {
                    return $m->explainType();
                })
                ->filter(function ($model) {
                    return $model->GetFilteredResults();
                })
                ->addColumn('actions', function ($m) {
//                    return buildLinkHtml([
//                        'url' => route('staff.account.receipt', ['id' => $m->id]),
//                        'description' => '<i class="fa fa-file-o"></i>',
//                        'target' => '_blank'
//                    ]);
                })
        )->render();
    }
}
