<?php

namespace App\Http\Controllers\Branch;

use App\Http\Controllers\UserBaseController;
use App\Models\Sale;
use App\Models\Staff;
use Illuminate\Http\Request;

class SaleController extends UserBaseController
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $dt = dt(Sale::whereBranchId(\Auth::guard('branch')->id())->withTrashed()->with(['staff' => function ($query) {
                $query->withTrashed();
            }]));

            return $dt->setup(
                $dt->getDt()
                    ->editColumn('grand_total', function ($m) {
                        return 'RM '.fundFormat($m->grand_total);
                    })
                    ->editColumn('paid', function ($m) {
                        return 'RM '.fundFormat($m->paid);
                    })
                    ->editColumn('tax_total', function ($m) {
                        return 'RM ' . fundFormat($m->tax_total);
                    })
                    ->editColumn('discount_total', function ($m) {
                        return 'RM ' . fundFormat($m->discount_total);
                    })
                    ->editColumn('payment_method', function ($m) {
                        return $m->explainPaymentMethod();
                    })
                    ->addColumn('change', function ($m) {
                        return 'RM '.fundFormat($m->calculateChange());
                    })
                    ->editColumn('created_at', function ($m) {
                        return $m->created_at->format('Y-m-d');
                    })
                    ->addColumn('is_cancelled', function ($m) {
                        if (is_null($m->deleted_at)) {
                            return trans('common.no');
                        }
                        return trans('common.yes');
                    })
                    ->filter(function ($model) {
                        return $model->GetFilteredResults();
                    })
                    ->addColumn('actions', function ($model) {
                        $actions = buildLinkHtml([
                            'url' => route('branch.sale.show', ['id' => $model->id]),
                            'description' => '<i class="fa fa-eye"></i>',
                        ]);
                        /*$actions .= buildLinkHtml([
                            'url' => route('branch.sale.edit', ['id' => $model->id]),
                            'description' => '<i class="fa fa-pencil"></i>',
                            'color' => 'btn-success'
                        ]);*/
                        if ($model->is_paid == 0 && is_null($model->deleted_at)) {
                            $actions .= buildConfirmationLinkHtml([
                                'url' => route('branch.sale.delete', ['id' => $model->id]),
                                'description' => '<i class="fa fa-trash"></i>',
                                'color' => 'btn-danger'
                            ]);
                        }
                        return $actions;
                    })->rawColumns(['actions'])
            )->render();
        } else {
            $staffs = Staff::whereBranchId(\Auth::guard('branch')->id())->pluck('name', 'id');

            return view('branch.sale.index', compact('staffs'));
        }
    }

    public function indexWidget(Request $request)
    {
        if ($request->ajax()) {
            $sales = Sale::whereBranchId(\Auth::guard('branch')->id())->GetFilteredResults()->get();

            $totalSales = 0;
            $grandTotal = 0;
            $totalPax = 0;
            foreach ($sales as $sale) {
                $totalSales++;
                $grandTotal += $sale->grand_total;
                $totalPax += $sale->total_pax;
            }

            return response()->json([
                'total_sales' => $totalSales,
                'grand_total' => number_format($grandTotal, 2),
                'total_pax' => $totalPax,
            ]);
        }
    }

    public function show(Sale $sale)
    {
        return view('branch.sale.show', compact('sale'));
    }

    public function receipt(Sale $sale)
    {
        return view('sale_receipt', compact('sale'));
    }

    public function delete(Request $request, $id) {
        $model = Sale::whereBranchId(\Auth::guard('branch')->id())->GetFilteredResults()->find($id);

        if (!$model) {
            return makeResponse(false, trans('common.record_not_found'));
        }

        $model->delete();
        return makeResponse(true, trans('common.operation_success'));
    }
}
