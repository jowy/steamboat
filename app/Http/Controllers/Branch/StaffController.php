<?php

namespace App\Http\Controllers\Branch;

use App\Http\Controllers\UserBaseController;
use App\Models\Staff;
use Illuminate\Http\Request;
use DataTables;

class StaffController extends UserBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $staffs = Staff::query()->whereBranchId(\Auth::guard('branch')->id());

            return DataTables::of($staffs)
                ->filter(function ($model) {
                    return $model->GetFilteredResults();
                })
                ->addColumn('actions', function ($model) {
                    $actions = buildLinkHtml([
                        'url' => route('branch.staff.edit', ['id' => $model->id]),
                        'description' => '<i class="fa fa-eye"></i>',
                    ]);
                    $actions .= buildConfirmationLinkHtml([
                        'url' => route('branch.staff.delete', ['id' => $model->id]),
                        'description' => '<i class="fa fa-trash"></i>',
                    ]);
                    $actions .= buildLinkHtml([
                        'url' => route('branch.staff.login', ['id' => $model->id]),
                        'description' => '<i class="fa fa-key"></i>',
                        'color' => 'btn-info',
                    ]);
                    return $actions;
                })
                ->make(true);
        } else {
            return view('branch.staff.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $staff = new Staff();
        return view('branch.staff.form', compact('staff'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = Staff::$rules;
        unset($rules['branch_id']);
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $name_exists = Staff::where('name', '=', $request->get('name'))->first();
            if ($name_exists) {
                return makeResponse(false, trans('common.staff_name_exists', ['name' => $request->get('name')]));
            }

            $model = new Staff($request->all());
            $model->branch_id = \Auth::guard('branch')->id();
            $model->username = $request->get('username');
            $model->password = bcrypt($request->get('password'));
            $model->save();

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Staff $staff
     * @return \Illuminate\Http\Response
     */
    public function edit(Staff $staff)
    {
        $mode = 'edit';
        return view('branch.staff.form', compact('mode', 'staff'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Staff $staff
     * @return
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Staff $staff)
    {
        $rules = Staff::$rules;
        $rules = array_merge($rules, [
            'username' => 'required|min:2|alpha_dash|unique:staff,username,'.$staff->id,
            'password' => 'nullable|min:6|confirmed'
        ]);
        unset($rules['branch_id']);
        $this->validate($request, $rules);

        try {
            $name_en_exists = Staff::where('name', '=', $request->get('name'))->where('id', '!=', $staff->id)->first();
            if ($name_en_exists) {
                return makeResponse(false, trans('common.staff_name_exists', ['name' => $request->get('name')]));
            }

            $staff->fill($request->all());
            $staff->username = $request->input('username');
            if ($request->input('password')) {
                $staff->password = bcrypt($request->input('password'));
            }
            $staff->save();

            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id) {
        try {
            $model = Staff::whereId($id)->whereBranchId(\Auth::guard('branch')->id())->first();

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            if ($model->delete()) {
                return makeResponse(true, trans('common.operation_success'));
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }

    public function login(Request $request, $id)
    {
        try {
            $model = Staff::where('branch_id', '=', $this->_G['user']->id)->find($id);

            if ($model) {
                \Auth::guard('staff')->login($model);

                return redirect()->route('staff.sale.index');
            } else {
                throw new \Exception(trans('common.branch_not_found'));
            }
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }
}
