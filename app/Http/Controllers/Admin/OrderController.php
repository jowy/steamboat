<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use App\Models\Order;
use Illuminate\Http\Request;
use DataTables;

class OrderController extends AdminBaseController
{
    public function index()
    {
        return view('admin.order.index');
    }

    public function indexDt(Request $request)
    {
        $model = Order::with(['branch', 'supplier']);

        return DataTables::of($model)
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildLinkHtml([
                    'url' => route('admin.order.show', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>',
                ]);
                return $actions;
            })
            ->make(true);
    }

    public function show(Order $order)
    {
        return view('admin.order.show', compact('order'));
    }

    public function invoice(Order $order)
    {
        return view('admin.order.invoice', compact('order'));
    }
}
