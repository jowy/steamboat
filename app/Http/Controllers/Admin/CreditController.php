<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Input;
use DataTables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminBaseController;
use App\Models\User;
use App\Models\UserTransaction;
use App\Models\CreditManage;
use App\Constants;

class CreditController extends AdminBaseController
{
    public function index(Request $request) {
        return view('admin.credit.index');
    }

    public function indexDt(Request $request) {
        $dt = dt(CreditManage::with(['user', 'admin'])->GetRecords());

        return $dt->setup(
            $dt->getDt()
            ->editColumn('user_id', function ($model) {
                return $model->user ? $model->user->username : trans('common.user_not_found');
            })
            ->editColumn('admin_id', function ($model) {
                return $model->admin ? $model->admin->username : trans('common.admin_not_found');
            })
            ->editColumn('action_type', function ($model) {
                return $model->explainActionType();
            })
            ->editColumn('amount', function ($model) {
                return fundFormat($model->amount, \App\Constants::$decimal_point);
            })
			->editColumn('cash_received', function ($model) {
                return fundFormat($model->cash_received, \App\Constants::$decimal_point);
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';
                $actions .= buildRemoteLinkHtml([
                    'url' => route('admin.credit.details', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>',
                ]);
                return $actions;
            })
            ->rawColumns(['actions'])
        )->render();
    }

    public function details(Request $request, $id) {
        $model = CreditManage::with(['admin'])->GetRecords()->find($id);

        return view('admin.credit.details', ['model' => $model]);
    }

    public function create(Request $request) {
        return view('admin.credit.create');
    }

    public function createPost(Request $request) {
        $rules = CreditManage::$rules;
        unset($rules['user_id']);
        unset($rules['admin_id']);
        $rules['username'] = 'required|exists:user,username';
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();
            $model = new CreditManage();

            $user = User::lockForUpdate()->where('username', '=', $request->get('username'))->first();

            if (!$user) {
                throw new \Exception(trans('common.user_not_found'));
            }

            if (!$request->has('amount') || $request->get('amount') == '' || $request->get('amount') <= 0) {
                throw new \Exception(trans('common.invalid_amount'));
            }

            if ($request->has('adjust_description') && $request->get('adjust_description') == 1) {
                if (!$request->has('description_en') || $request->get('description_en') == '') {
                    throw new \Exception(trans('common.english_description_is_required'));
                }

                if (!$request->has('description_cn') || $request->get('description_cn') == '') {
                    throw new \Exception(trans('common.chinese_description_is_required'));
                }
            }

            $amount = $request->get('amount');

            $model->admin_id = \Auth::guard('admin')->user()->id;
            $model->user_id = $user->id;
            $model->adjust_description = $request->get('adjust_description');
            $model->description_en = $request->has('description_en') && $request->get('description_en') != '' ? $request->get('description_en') : null;
            $model->description_cn = $request->has('description_cn') && $request->get('description_cn') != '' ? $request->get('description_cn') : null;
            $model->remark = $request->has('remark') && $request->get('remark') != '' ? $request->get('remark') : null;
            $model->cash_received = $request->has('cash_received') && !isEmpty($request->get('cash_received')) ? $request->get('cash_received', 0) : 0;

            $ut = new UserTransaction();
            $ut->user_id = $user->id;
            if ($model->remark != null) {
                $ut->params = ['remark' => $model->remark];
            }
            $ut->adjust_description = $request->get('adjust_description');
            $ut->description_en = $request->has('description_en') && $request->get('description_en') != '' ? $request->get('description_en') : null;
            $ut->description_cn = $request->has('description_cn') && $request->get('description_cn') != '' ? $request->get('description_cn') : null;

            switch ($request->get('action_type')) {
                case 1://Admin add credit 1
                    $user->credit_1 = $user->credit_1 + $amount;

                    $model->amount = $amount;
                    $model->action_type = 1;

                    $ut->amount = $amount;
                    $ut->transaction_type = 901;
                    $ut->credit_type = 1;

                    break;
                case 2://Admin deduct credit 1
                    $user->credit_1 = $user->credit_1 - $amount;

                    $model->amount = 0 - $amount;
                    $model->action_type = 2;

                    $ut->amount = 0 - $amount;
                    $ut->transaction_type = 902;
                    $ut->credit_type = 1;

                    break;
                case 3://Admin add credit 2
                    $user->credit_2 = $user->credit_2 + $amount;

                    $model->amount = $amount;
                    $model->action_type = 3;

                    $ut->amount = $amount;
                    $ut->transaction_type = 903;
                    $ut->credit_type = 2;

                    break;
                case 4://Admin deduct credit 2
                    $user->credit_2 = $user->credit_2 - $amount;

                    $model->amount = 0 - $amount;
                    $model->action_type = 4;

                    $ut->amount = 0 - $amount;
                    $ut->transaction_type = 904;
                    $ut->credit_type = 2;

                    break;
                case 5://Admin add credit 3
                    $user->credit_3 = $user->credit_3 + $amount;

                    $model->amount = $amount;
                    $model->action_type = 5;

                    $ut->amount = $amount;
                    $ut->transaction_type = 905;
                    $ut->credit_type = 3;

                    break;
                case 6://Admin deduct credit 3
                    $user->credit_3 = $user->credit_3 - $amount;

                    $model->amount = 0 - $amount;
                    $model->action_type = 6;

                    $ut->amount = 0 - $amount;
                    $ut->transaction_type = 906;
                    $ut->credit_type = 3;

                    break;
                case 7://Admin add credit 4
                    $user->credit_4 = $user->credit_4 + $amount;

                    $model->amount = $amount;
                    $model->action_type = 7;

                    $ut->amount = $amount;
                    $ut->transaction_type = 907;
                    $ut->credit_type = 4;

                    break;
                case 8://Admin deduct credit 4
                    $user->credit_4 = $user->credit_4 - $amount;

                    $model->amount = 0 - $amount;
                    $model->action_type = 8;

                    $ut->amount = 0 - $amount;
                    $ut->transaction_type = 908;
                    $ut->credit_type = 4;

                    break;
                default: throw new \Exception(trans('common.unknown_error')); break;
            }

            $model->save();
            $ut->save();
            $user->save();

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function checkUserName(Request $request) {
        $u = User::where('username', '=', $request->get('username'))->first();

        if ($u) {
            return makeResponse(true, trans('common.operation_success'), ['name' => $u->getName()]);
        } else {
            return makeResponse(false, trans('common.user_not_found'));
        }
    }
}
