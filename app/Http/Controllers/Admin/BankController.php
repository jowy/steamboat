<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Input;
use DataTables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminBaseController;
use App\Models\Bank;
use App\Models\Country;

class BankController extends AdminBaseController
{
    public function index(Request $request) {
        $countries = Country::GetCountries()->GetSelects();
        return view('admin.bank.index', ['countries' => $countries]);
    }

    public function indexDt(Request $request) {
        $dt = dt(Bank::GetRecords()->with(['country']));

        return $dt->setup(
            $dt->getDt()
            ->editColumn('country_id', function ($m) {
                return $m->country ? $m->country->getCountryName() : '-';
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildRemoteLinkHtml([
                    'url' => route('admin.bank.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>',
                    'modal' => '#remote-modal',
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('admin.bank.delete', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                ]);
                return $actions;
            })
            ->rawColumns(['actions'])
        )->render();
    }

    public function create(Request $request) {
        $countries = Country::GetCountries()->GetSelects();
        return view('admin.bank.create', ['countries' => $countries]);
    }

    public function createPost(Request $request) {
        $rules = Bank::$rules;
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            //Check bank name availibility
            $name_cn_exists = Bank::where('name_cn', '=', $request->get('name_cn'))->first();
            if ($name_cn_exists) {
                return makeResponse(false, trans('common.bank_name_exists', ['name' => $request->get('name_cn')]));
            }

            $name_en_exists = Bank::where('name_en', '=', $request->get('name_en'))->first();
            if ($name_en_exists) {
                return makeResponse(false, trans('common.bank_name_exists', ['name' => $request->get('name_en')]));
            }

            $model = new Bank();
            $model->name_en = $request->get('name_en');
            $model->name_cn = $request->get('name_cn');
            $model->country_id = $request->get('country_id');
            $model->save();

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function edit($id) {
        $model = Bank::GetRecords()->where('id', '=', $id)->first();

        if (!$model) {
            addError(trans('common.record_not_found'));
            return redirect()->route('admin.bank');
        }

        $countries = Country::GetCountries()->GetSelects();
        return view('admin.bank.edit', ['model' => $model, 'countries' => $countries]);
    }

    public function editPost(Request $request, $id) {
        $rules = Bank::$rules;
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $model = Bank::GetRecords()->find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            //Check bank name availibility
            $name_cn_exists = Bank::where('name_cn', '=', $request->get('name_cn'))->where('id', '!=', $model->id)->first();
            if ($name_cn_exists) {
                return makeResponse(false, trans('common.bank_name_exists', ['name' => $request->get('name_cn')]));
            }

            $name_en_exists = Bank::where('name_en', '=', $request->get('name_en'))->where('id', '!=', $model->id)->first();
            if ($name_en_exists) {
                return makeResponse(false, trans('common.bank_name_exists', ['name' => $request->get('name_en')]));
            }

            $model->name_en = $request->get('name_en');
            $model->name_cn = $request->get('name_cn');
            $model->country_id = $request->get('country_id');

            $model->save();

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function delete($id) {
        try {
            \DB::beginTransaction();

            $model = Bank::GetRecords()->find($id);

            if ($model->delete()) {
                \DB::commit();
                return makeResponse(true, trans('common.operation_success'));
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }
}
