<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use App\Models\Branch;
use App\Models\Staff;
use Illuminate\Http\Request;
use DataTables;

class StaffController extends AdminBaseController
{
    public function index(Request $request)
    {
        return view('admin.staff.index');
    }

    public function indexDt(Request $request)
    {
        $model = Staff::with('branch');

        return DataTables::of($model)
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildLinkHtml([
                    'url' => route('admin.staff.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>',
                ]);
                if (\Auth::guard('admin')->user()->hasPermission('login_staff')) {
                    $actions .= buildLinkHtml([
                        'url' => route('admin.staff.login', ['id' => $model->id]),
                        'description' => '<i class="fa fa-key"></i>',
                        'color' => 'btn-info',
                    ]);
                }
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('admin.staff.delete', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                ]);
                return $actions;
            })
            ->make(true);
    }

    public function create(Request $request)
    {
        $branches = Branch::orderBy('name_en', 'asc')->pluck('name_en', 'id');
        return view('admin.staff.create', compact('branches'));
    }

    public function createPost(Request $request)
    {
        $rules = Staff::$rules;
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $name_exists = Staff::where('name', '=', $request->get('name'))->first();
            if ($name_exists) {
                return makeResponse(false, trans('common.staff_name_exists', ['name' => $request->get('name')]));
            }

            $model = new Staff($request->all());
            $model->branch_id = $request->get('branch_id');
            $model->username = $request->get('username');
            $model->password = bcrypt($request->get('password'));
            $model->save();

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function edit($id) {
        $model = Staff::where('id', '=', $id)->first();

        if (!$model) {
            addError(trans('common.record_not_found'));
            return redirect()->route('admin.staff');
        }

        $branches = Branch::orderBy('name_en', 'asc')->pluck('name_en', 'id');

        return view('admin.staff.edit', compact('model', 'branches'));
    }

    public function editPost(Request $request, $id) {
        $rules = Staff::$rules;
        $rules = array_merge($rules, [
            'username' => 'required|min:2|alpha_dash|unique:staff,username,'.$id,
            'password' => 'nullable|min:6|confirmed'
        ]);
        unset($rules['branch_id']);
        $this->validate($request, $rules);

        try {
            $model = Staff::find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $name_en_exists = Staff::where('name', '=', $request->get('name'))->where('id', '!=', $model->id)->first();
            if ($name_en_exists) {
                return makeResponse(false, trans('common.staff_name_exists', ['name' => $request->get('name')]));
            }

            $model->fill($request->all());
            $model->username = $request->input('username');
            if ($request->input('password')) {
                $model->password = bcrypt($request->input('password'));
            }
            $model->save();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }

    public function delete($id) {
        try {
            $model = Staff::find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            if ($model->delete()) {
                return makeResponse(true, trans('common.operation_success'));
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }

    public function loginStaff(Request $request, $id)
    {
        try {
            $model = Staff::find($id);

            if ($model) {
                \Auth::guard('staff')->login($model);

                return redirect()->route('staff.sale.index');
            } else {
                throw new \Exception(trans('common.staff_not_found'));
            }
        } catch (\Exception $e) {
            return makeResponse(false, $e->getMessage());
        }
    }
}
