<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\MenuItems;
use App\Models\Page;
use DataTables;

class MenuController extends AdminBaseController {
    public function index(Request $request) {
        return view('admin.menu.index');
    }

    public function indexDt(Request $request) {
        $dt = dt(Menu::GetRecords());

        return $dt->setup(
            $dt->getDt()
            ->addColumn('name', function ($m) {
                return $m->getName();
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($m) {
                $actions = '';
                $actions .= buildLinkHtml([
                    'url' => route('admin.menu.update', ['id' => $m->id]),
                    'description' => '<i class="fa fa-eye"></i>',
                ]);
                return $actions;
            })
            ->rawColumns(['actions'])
        )->render();
    }

    public function update(Request $request, $id) {
        $model = Menu::GetRecords()->find($id);

        if (!$model) {
            addError(trans('common.record_not_found'));
            return redirect()->route('admin.menu');
        }

        $field = 'page_description_' . app()->getLocale();

        $model->load('childrens');

        return view('admin.menu.update', ['model' => $model]);
    }

    public function subCreate(Request $request, $id) {
        $father = Menu::GetRecords()->find($id);

        return view('admin.menu.sub', ['father' => $father]);
    }

    public function subCreatePost(Request $request, $id) {
        $rules = MenuItems::$rules;
        if ($request->has('type')) {
            switch ($request->get('type')) {
                case 0: unset($rules['url'], $rules['page_id']); break;
                case 1: unset($rules['url']); break;
                case 2: unset($rules['page_id']); break;
            }
        }
        $this->validate($request, $rules);
        try {
            \DB::beginTransaction();
            $father = Menu::GetRecords()->find($id);

            if (!$father) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model = new MenuItems();
            $model->menu_id = $father->id;
            $model->text_en = $request->get('text_en');
            $model->text_cn = $request->get('text_cn');
            $model->open_in_new_tab = $request->get('open_in_new_tab');
            $model->type = $request->get('type');
            $model->sorting = MenuItems::where('menu_id', '=', $father->id)->count() + 1;

            $model = $this->fillInTheLink($request, $model);

            $model->save();

            \DB::commit();

            return makeResponse(true, trans('common.operation_success'), ['model' => $model, 'text' => $model->getTextWithLink()]);
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function subEdit(Request $request, $id, $id2) {
        $father = Menu::GetRecords()->find($id);
        $model = MenuItems::where('menu_id', '=', $id)->find($id2);

        return view('admin.menu.sub', ['father' => $father, 'model' => $model]);
    }

    public function subEditPost(Request $request, $id, $id2) {
        $rules = MenuItems::$rules;
        unset($rules['menu_id']);
        if ($request->has('type')) {
            switch ($request->get('type')) {
                case 0: unset($rules['url'], $rules['page_id']); break;
                case 1: unset($rules['url']); break;
                case 2: unset($rules['page_id']); break;
            }
        }
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();
            $father = Menu::GetRecords()->find($id);

            if (!$father) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model = MenuItems::where('menu_id', '=', $id)->find($id2);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model->text_en = $request->get('text_en');
            $model->text_cn = $request->get('text_cn');
            $model->open_in_new_tab = $request->get('open_in_new_tab');
            $model->type = $request->get('type');

            $model = $this->fillInTheLink($request, $model);
            $model->save();
            \DB::commit();

            return makeResponse(true, trans('common.operation_success'), ['model' => $model, 'text' => $model->getTextWithLink()]);
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function updateSorting(Request $request, $id) {
        try {
            \DB::beginTransaction();
            $father = Menu::GetRecords()->find($id);

            if (!$father) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $target = MenuItems::where('menu_id', '=', $father->id)->find($request->get('target_id'));
            if (!$target) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $target->parent_id = null;

            if ($request->has('parent_id') && $request->get('parent_id') != 0) {
                if ($request->get('parent_id') != $target->id) {
                    $parent = MenuItems::where('menu_id', '=', $father->id)->find($request->get('parent_id'));

                    if (!$parent) {
                        throw new \Exception(trans('common.record_not_found'));
                    }

                    $target->parent_id = $parent->id;
                }
            }
            $target->save();

            $i = 1;
            foreach ($request->get('ids') as $key => $var) {
                \DB::table('menu_items')->where('menu_id', '=', $father->id)->where('id', '=', $var)->update([
                    'sorting' => $i,
                ]);
                $i++;
            }
            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function subDeletePost(Request $request, $id, $id2) {
        try {
            \DB::beginTransaction();
            $model = MenuItems::where('menu_id', '=', $id)->find($id2);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model->delete();

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'), ['model_id' => $id2]);
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    protected function fillInTheLink($request, $model) {
        switch ($model->type) {
            case MenuItems::TYPE_PAGE:
                $model->page_id = $request->get('page_id');
                $model->url = null;
                break;
            case MenuItems::TYPE_LINK:
                $model->page_id = null;
                $model->url = $request->get('url');
                break;
            case MenuItems::TYPE_DISABLE:
                $model->page_id = null;
                $model->url = null;
                break;
            default: throw new \Exception(trans('common.unknown_error')); break;
        }

        return $model;
    }
}