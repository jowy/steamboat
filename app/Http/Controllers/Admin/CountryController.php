<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use Illuminate\Http\Request;
use App\Models\Country;
use DataTables;

class CountryController extends AdminBaseController {
    public function index(Request $request) {
        $countries = Country::GetRecords()->get();
        $status = Country::getStatusLists();
        return view('admin.country.index', ['countries' => $countries, 'status' => $status]);
    }

    public function indexDt(Request $request) {
        $dt = dt(Country::GetRecords());

        return $dt->setup(
            $dt->getDt()
            ->editColumn('country_name_en', function ($model) {
                return $model->getCountryName();
            })
            ->editColumn('currency_actual_rate', function ($model) {
                return fundFormat($model->currency_actual_rate);
            })
            ->editColumn('currency_in_rate', function ($model) {
                return fundFormat($model->currency_in_rate);
            })
            ->editColumn('currency_out_rate', function ($model) {
                return fundFormat($model->currency_out_rate);
            })
            ->editColumn('status', function ($model) {
                return $model->explainStatus();
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildRemoteLinkHtml([
                    'url' => route('admin.country.update', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>',
                ]);

                $actions .= buildLinkHtml([
                    'url' => route('admin.country.location.update', ['id' => $model->id]),
                    'description' => '<i class="fa fa-bars"></i>',
                    'color' => 'bg-deep-purple-a300 ',
                ]);

                return $actions;
            })
            ->rawColumns(['actions'])
        )->render();
    }

    public function update(Request $request, $id) {
        $model = Country::find($id);

        if (!$model) {
            return view('record_not_found');
        }

        $status = Country::getStatusLists();
        return view('admin.country.update', ['model' => $model, 'status' => $status]);
    }

    public function updatePost(Request $request, $id) {
        $rules = Country::$rules;
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();
            $status = Country::getStatusLists();

            $model = Country::find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model->currency_actual_rate = isFund($request->get('currency_actual_rate')) ? $request->get('currency_actual_rate') : 0.0000;
            $model->currency_in_rate = isFund($request->get('currency_in_rate')) ? $request->get('currency_in_rate') : 0.0000;
            $model->currency_out_rate = isFund($request->get('currency_out_rate')) ? $request->get('currency_out_rate') : 0.0000;
            $model->currency_prefix = $request->get('currency_prefix') ? $request->get('currency_prefix') : null;
            $model->currency_suffix = $request->get('currency_suffix') ? $request->get('currency_suffix') : null;
            $model->ext = $request->get('ext') ? $request->get('ext') : null;
            $model->status = array_key_exists($request->get('status'), $status) ? $request->get('status') : Country::STATUS_INACTIVE;

            $model->save();

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e->getMessage());
        }
    }
}