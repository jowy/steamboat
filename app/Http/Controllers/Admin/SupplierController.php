<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use App\Models\Branch;
use App\Models\Supplier;
use App\Models\SupplierAccount;
use Illuminate\Http\Request;
use DataTables;

class SupplierController extends AdminBaseController
{
    public function index(Request $request)
    {
        return view('admin.supplier.index');
    }

    public function indexDt(Request $request)
    {
        $model = Supplier::query();

        return DataTables::of($model)
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildLinkHtml([
                    'url' => route('admin.supplier.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>',
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('admin.supplier.delete', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                ]);
                return $actions;
            })
            ->make(true);
    }

    public function create(Request $request)
    {
        return view('admin.supplier.create');
    }

    public function createPost(Request $request) {
        $rules = Supplier::$rules;
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $name_exists = Supplier::where('name', '=', $request->get('name'))->first();
            if ($name_exists) {
                return makeResponse(false, trans('common.supplier_name_exists', ['name' => $request->get('name')]));
            }

            $model = new Supplier($request->all());
            $model->save();

            // Create a supplier account merge with branch
            $branches = Branch::all();
            if (count($branches) > 0) {
                foreach ($branches as $branch) {
                    $supplierAccount = new SupplierAccount();
                    $supplierAccount->supplier_id = $model->id;
                    $supplierAccount->branch_id = $branch->id;
                    $supplierAccount->account_credit = 0;
                    $supplierAccount->save();
                }
            }

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function edit($id) {
        $model = Supplier::where('id', '=', $id)->first();

        if (!$model) {
            addError(trans('common.record_not_found'));
            return redirect()->route('admin.supplier');
        }

        return view('admin.supplier.edit', ['model' => $model]);
    }

    public function editPost(Request $request, $id) {
        $rules = Supplier::$rules;

        $this->validate($request, $rules);

        try {
            $model = Supplier::find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            //Check bank name availibility
            /*$name_cn_exists = Branch::where('name_cn', '=', $request->get('name_cn'))->where('id', '!=', $model->id)->first();
            if ($name_cn_exists) {
                return makeResponse(false, trans('common.branch_name_exists', ['name' => $request->get('name_cn')]));
            }*/

            $name_en_exists = Supplier::where('name', '=', $request->get('name'))->where('id', '!=', $model->id)->first();
            if ($name_en_exists) {
                return makeResponse(false, trans('common.supplier_name_exists', ['name' => $request->get('name')]));
            }

            $model->fill($request->all());
            $model->save();

            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }

    public function delete($id) {
        try {
            $model = Supplier::find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            if ($model->delete()) {
                return makeResponse(true, trans('common.operation_success'));
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }
}
