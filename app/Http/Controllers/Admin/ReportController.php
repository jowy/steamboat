<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use App\Models\Branch;
use App\Models\Staff;
use Illuminate\Http\Request;

class ReportController extends AdminBaseController
{
    public function sale(Request $request)
    {
        if (isAjax()) {
            if ($request->get('branch_id')) {
                $model = Staff::join('branch', 'branch.id', '=', 'staff.branch_id')
                    ->join('sales', 'staff.id', '=', 'sales.staff_id')
                    ->where('staff.branch_id', '=', $request->get('branch_id'))
                    ->groupBy('staff.id')
                    ->select('staff.id', 'staff.name', 'branch.name_en', \DB::raw('COUNT(sales.id) as total_sales'), \DB::raw('SUM(sales.grand_total) as total_amount'));
            } else {
                $model = Branch::join('sales', 'branch.id', '=', 'sales.branch_id')->groupBy('branch.id')
                    ->select(\DB::raw('branch.id'), \DB::raw('branch.name_en'), \DB::raw('branch.name_cn'), \DB::raw('COUNT(sales.id) as total_sales'), \DB::raw('SUM(sales.grand_total) as total_amount'));

            }
            $dt = dt($model);

            return $dt->setup(
                $dt->getDt()
                    ->filter(function ($model) {
                        return $model->GetFilteredResults();
                    })
                    ->addColumn('actions', function ($model) {
                        $actions = buildLinkHtml([
                            'url' => route('admin.report.sale').'?branch_id='.$model->id,
                            'description' => '<i class="fa fa-eye"></i>',
                        ]);
                        return $actions;
                    })->rawColumns(['actions'])
            )->render();
        } else {
            return view('admin.report.sale');
        }
    }
}
