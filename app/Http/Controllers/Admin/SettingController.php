<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Input;
use DataTables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminBaseController;
use App\Models\Setting;

class SettingController extends AdminBaseController
{
    public function index(Request $request) {
        return view('admin.setting.index');
    }

    public function indexDt(Request $request) {
        $dt = dt(Setting::GetRecords());

        return $dt->setup(
            $dt->getDt()
            ->addColumn('description', function ($m) {
                return $m->getDescription();
            })
            ->editColumn('value', function ($m) {
                return $m->explainValue(true, true);
            })
            ->filter(function ($m) {
                return $m->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildLinkHtml([
                    'url' => route('admin.setting.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>',
                ]);
                return $actions;
            })
            ->rawColumns(['actions'])
        )->render();
    }

    public function edit($id) {
        $model = Setting::GetRecords()->where('id', '=', $id)->first();
        if (!$model) {
            addError(trans('common.record_not_found'));
            return redirect()->route('setting');
        }

        $params = $model->convertParams();

        return view("admin.setting.type_$model->type", ['model' => $model, 'params' => $params]);
    }

    public function editPost(Request $request, $id) {
        $rules = Setting::$rules;
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $model = Setting::GetRecords()->where('id', '=', $id)->first();
            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $params = $model->convertParams();
            $delete_image = array();

            switch ($model->type) {
                case Setting::TYPE_TEXT:
                    $model->value = strip_tags($request->get('value')) != null ? strip_tags($request->get('value')) : null;
                    break;
                case Setting::TYPE_TEXTAREA:
                    $model->value = strip_tags($request->get('value')) != null ? strip_tags($request->get('value')) : null;
                    break;
                case Setting::TYPE_EDITOR:
                    $model->value = $request->get('value') != null ? $request->get('value') : null;
                    break;
                case Setting::TYPE_SELECT:
                    $model->value = $request->get('value') != null ? $request->get('value') : null;
                    break;
                case Setting::TYPE_IMAGE:
                    if ($request->hasFile('value')) {
                        $file = $request->file('value');

                        if ($file instanceof \Illuminate\Http\UploadedFile) {
                            $img = \Image::make($file);
                            $mime = $img->mime();
                            $ext = convertMimeToExt($mime);

                            $path = 'uploads/setting/';
                            $filename = generateRandomUniqueName();

                            touchFolder($path);

                            if (isset($params['image_width']) && isset($params['image_height'])) {
                                $img = $img->fit($params['image_width'], $params['image_height'])->save($path . $filename . $ext, 90);
                            } else if (isset($params['image_width']) && !isset($params['image_height'])) {
                                $img = $img->fit($params['image_width'])->save($path . $filename . $ext, 90);
                            } else if (!isset($params['image_width']) && isset($params['image_height'])) {
                                $img = $img->fit(null, $params['image_height'])->save($path . $filename . $ext, 90);
                            }

                            //Delete old image if not default image
                            if ($model->value != null && $model->value != '' && $model->value != $model->default_value) {
                                $delete_image[] = public_path($model->value);
                            }

                            $model->value = '/' . $path . $filename . $ext;
                        } else {
                            throw new \Exception(trans('common.unknown_error'));
                        }
                    } else {
                        $model->value = $request->has('value') && $request->get('value') != '' && $request->get('value') != null ? $request->get('value') : null;
                    }
                    break;
            }

            $model->save();

            \DB::commit();
            if (isset($delete_image) && count($delete_image)) {
                foreach ($delete_image as $key => $var) {
                    @unlink($var);
                }
            }
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }
}
