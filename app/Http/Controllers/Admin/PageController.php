<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use Illuminate\Http\Request;
use DataTables;
use App\Models\Page;

class PageController extends AdminBaseController {
    public function index(Request $request) {
        return view('admin.page.index');
    }

    public function indexDt(Request $request) {
        $dt = dt(Page::GetRecords());

        return $dt->setup(
            $dt->getDt()
            ->editColumn('page_tag', function ($model) {
                return buildLinkHtml([
                    'url' => $model->getUrl(),
                    'description' => $model->getUrl(),
                    'target' => '_blank',
                    'is_button' => false,
                ]);
            })
            ->addColumn('page_description', function ($model) {
                return $model->getPageDescription();
            })
            ->addColumn('title', function ($model) {
                return $model->getTitle();
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';
                $actions .= buildLinkHtml([
                    'url' => route('admin.page.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>',
                ]);
                if ($model->is_system == 0) {
                    $actions .= buildConfirmationLinkHtml([
                        'url' => route('admin.page.delete.post', ['id' => $model->id]),
                        'description' => '<i class="fa fa-trash"></i>',
                    ]);
                }
                return $actions;
            })
            ->rawColumns(['actions', 'page_tag'])
        )->render();
    }

    public function create(Request $request) {
        return view('admin.page.create');
    }

    public function createPost(Request $request) {
        $rules = Page::$rules;

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();
            $model = new Page();

            $tag = slugify($request->get('page_tag'));
            if (Page::where('page_tag', '=', $tag)->exists()) {
                throw new \Exception(trans('common.duplicated_page'));
            }

            $model->page_tag = $tag;
            $model->page_description_en = $request->get('page_description_en');
            $model->page_description_cn = $request->get('page_description_cn');
            $model->title_en = $request->get('title_en');
            $model->title_cn = $request->get('title_cn');
            $model->content_en = $request->get('content_en');
            $model->content_cn = $request->get('content_cn');
            $model->mobile_content_en = $request->get('mobile_content_en');
            $model->mobile_content_cn = $request->get('mobile_content_cn');

            $model->save();

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function edit(Request $request, $id) {
        $model = Page::find($id);

        if (!$model) {
            addError(trans('common.record_not_found'));
            return redirect()->route('admin.page');
        }

        return view('admin.page.edit', ['page' => $model]);
    }

    public function editPost(Request $request, $id) {
        $rules = Page::$rules;
        $model = Page::find($id);

        if ($model && $model->is_system == 1) {
            unset($model['page_tag']);
        }

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            if ($model->is_system != 1) {
                if (!$request->has('page_tag') || $request->get('page_tag') == null || $request->get('page_tag') == '') {
                    throw new \Exception(trans('validation.required', ['attribute' => trans('common.url')]));
                }

                $tag = slugify($request->get('page_tag'));
                if (Page::where('page_tag', '=', $tag)->where('id', '!=', $id)->exists()) {
                    throw new \Exception(trans('common.duplicated_page'));
                }

                $model->page_tag = $tag;
            }

            $model->page_description_en = $request->get('page_description_en');
            $model->page_description_cn = $request->get('page_description_cn');
            $model->title_en = $request->get('title_en');
            $model->title_cn = $request->get('title_cn');
            $model->content_en = $request->get('content_en');
            $model->content_cn = $request->get('content_cn');
            $model->mobile_content_en = $request->get('mobile_content_en');
            $model->mobile_content_cn = $request->get('mobile_content_cn');

            if ($model->has_share == 1) {
                $model->sharing_title_en = $request->get('sharing_title_en');
                $model->sharing_title_cn = $request->get('sharing_title_cn');
                $model->sharing_content_en = $request->get('sharing_content_en');
                $model->sharing_content_cn = $request->get('sharing_content_cn');
                if ($request->hasFile('sharing_image_en')) {
                    $model->sharing_image_en = $request->file('sharing_image_en');
                }
                if ($request->hasFile('sharing_image_cn')) {
                    $model->sharing_image_cn = $request->file('sharing_image_cn');
                }
            }

            $model->save();
            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function deletePost(Request $request, $id) {
        try {
            \DB::beginTransaction();
            $model = Page::where('is_system', '=', 0)->find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model->delete();

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }
}