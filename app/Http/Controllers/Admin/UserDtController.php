<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use Illuminate\Http\Request;
use DataTables;
use App\Models\UserBonus;
use App\Models\UserTransaction;

class UserDtController extends AdminBaseController {
    public function userBonusDt(Request $request, $uuid) {
        $dt = dt(UserBonus::with(['user'])->where('related_key', '=', $uuid)->GetRecords());

        return $dt->setup(
            $dt->getDt()
            ->editColumn('user_id', function ($m) {
                return $m->user ? $m->user->username : '-';
            })
            ->editColumn('bonus_type', function ($m) {
                return $m->explainBonusType();
            })
            ->editColumn('amount', function ($m) {
                return fundFormat($m->amount);
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
        )->render();
    }

    public function userTransactionDt(Request $request, $uuid) {
        $dt = dt(UserTransaction::with(['user'])->where('related_key', '=', $uuid)->GetRecords());

        return $dt->setup(
            $dt->getDt()
            ->editColumn('user_id', function ($m) {
                return $m->user ? $m->user->username : '-';
            })
            ->editColumn('credit_type', function ($m) {
                return $m->explainCreditType();
            })
            ->editColumn('transaction_type', function ($m) {
                return $m->explainTransactionType();
            })
            ->editColumn('amount', function ($m) {
                return fundFormat($m->amount);
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
        )->render();
    }
}