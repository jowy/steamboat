<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Input;
use DataTables;
use Validator;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminBaseController;
use App\Models\PermissionGroup;
use App\Models\Admin;

class AdminController extends AdminBaseController
{
    public function index(Request $request) {
        return view('admin.admin.index');
    }

    public function indexDt(Request $request) {
        $dt = dt(Admin::with(['permissionGroup'])->GetAdmin());

        return $dt->setup(
            $dt->getDt()
            ->addColumn('group', function ($model) {
                if ($model->permissionGroup) {
                    return $model->permissionGroup->group_name;
                } else {
                    return '-';
                }
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildRemoteLinkHtml([
                    'url' => route('admin.admin.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>',
                    'modal' => '#remote-modal',
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('admin.admin.delete', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                ]);
                return $actions;
            })
            ->rawColumns(['actions'])
        )->render();
    }

    public function create(Request $request) {
        $permissions = PermissionGroup::pluck('group_name', 'id');
        return view('admin.admin.create', ['permissions' => $permissions]);
    }

    public function createPost(Request $request) {
        $rules = Admin::$rules;
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $model = new Admin();
            $model->username = $request->get('username');
            $model->email = $request->get('email');
            $model->name = $request->get('name');
            $model->password = $request->get('password');
            $model->account_type = Admin::defaultUserType();
            $model->permission_group_id = $request->get('permission_group_id');

            $model->save();

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e->getMessage());
        }
    }

    public function edit($id) {
        $model = Admin::with(['permissionGroup'])->getAdmin()->where('id', '=', $id)->first();

        if (!$model) {
            addError(trans('common.record_not_found'));
            return redirect()->route('admin.admin');
        }

        $permissions = PermissionGroup::pluck('group_name', 'id');

        return view('admin.admin.edit', ['model' => $model, 'permissions' => $permissions]);
    }

    public function editPost(Request $request, $id) {
        $admin = Admin::find($id);
        $rules = \App\Models\Admin::$rules;
        if ($admin) {
            $rules['username'] = $rules['username'] . ',' . $admin->id;
            $rules['email'] = $rules['email'] . ',' . $admin->id;
            $rules['password'] = str_replace('required|', '', $rules['password']);
        }
        $this->validate($request, $rules);
        try {
            \DB::beginTransaction();
            $model = Admin::with(['permissionGroup'])->getAdmin()->where('id', '=', $id)->first();

            //Check username availibility
            $username_exists = Admin::where('username', '=', $request->get('username'))->first();
            if ($username_exists && $username_exists->id != $model->id) {
                return makeResponse(false, trans('common.username_exists'));
            }

            //Check email availibility
            $email_exists = Admin::where('email', '=', $request->get('email'))->first();
            if ($email_exists && $email_exists->id != $model->id) {
                return makeResponse(false, trans('common.email_exists'));
            }

            //Deny edit for admin account
            if ($model->account_type != Admin::defaultUserType()) {
                return makeResponse(false, trans('common.superadmin_cannot_edit'));
            }

            $model->username = $request->get('username');
            $model->email = $request->get('email');
            $model->name = $request->get('name');
            if ($request->has('password')) {
                $model->password = $request->get('password');
            }
            $model->permission_group_id = $request->get('permission_group_id');

            $model->save();
            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e->getMessage());
        }
    }

    public function delete(Request $request, $id) {
        try {
            \DB::beginTransaction();

            $model = Admin::getAdmin()->where('id', '=', $id)->first();

            if ($model->delete()) {
                \DB::commit();
                return makeResponse(true, trans('common.operation_success'));
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e->getMessage());
        }
    }
}
