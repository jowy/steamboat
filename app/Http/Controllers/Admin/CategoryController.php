<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Page;
use DataTables;

class CategoryController extends AdminBaseController {
    public function update(Request $request) {
        $first_level = Category::with(['sons'])->whereNull('parent_id')->get();

        return view('admin.category.update', ['first_level' => $first_level]);
    }

    public function subCreate(Request $request) {
        return view('admin.category.sub');
    }

    public function subCreatePost(Request $request) {
        $rules = Category::$rules;
        $this->validate($request, $rules);
        try {
            \DB::beginTransaction();

            $model = new Category();
            $model->name_en = $request->get('name_en');
            $model->name_cn = $request->get('name_cn');
            $model->sorting = Category::count() + 1;
            $model->save();

            \DB::commit();

            return makeResponse(true, trans('common.operation_success'), ['model' => $model, 'text' => $model->getNames()]);
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function subEdit(Request $request, $id) {
        $model = Category::find($id);

        return view('admin.category.sub', ['model' => $model]);
    }

    public function subEditPost(Request $request, $id) {
        $rules = Category::$rules;
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();
            $model = Category::find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model->name_en = $request->get('name_en');
            $model->name_cn = $request->get('name_cn');

            $model->save();
            \DB::commit();

            return makeResponse(true, trans('common.operation_success'), ['model' => $model, 'text' => $model->getNames()]);
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function updateSorting(Request $request) {
        try {
            \DB::beginTransaction();

            $target = Category::find($request->get('target_id'));
            if (!$target) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $target->parent_id = null;

            if ($request->has('parent_id') && $request->get('parent_id') != 0) {
                if ($request->get('parent_id') != $target->id) {
                    $parent = Category::find($request->get('parent_id'));

                    if (!$parent) {
                        throw new \Exception(trans('common.record_not_found'));
                    }

                    $target->parent_id = $parent->id;
                }
            }

            $target->save();

            $i = 1;
            foreach ($request->get('ids') as $key => $var) {
                Category::where('id', '=', $var)->update([
                    'sorting' => $i,
                ]);
                $i++;
            }
            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function subDeletePost(Request $request, $id) {
        try {
            \DB::beginTransaction();
            $model = Category::find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model->delete();

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'), ['model_id' => $id]);
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }
}