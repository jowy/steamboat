<?php

namespace App\Http\Controllers\Admin;

use App\Models\BranchStock;
use App\Models\BranchStockTransaction;
use App\Models\Product;
use App\Models\Supplier;
use App\Models\SupplierAccount;
use Auth;
use Input;
use DataTables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminBaseController;
use App\Models\Branch;
use App\Models\Country;

class BranchController extends AdminBaseController
{
    public function index(Request $request) {
        return view('admin.branch.index');
    }

    public function indexDt(Request $request) {
        $model = Branch::GetRecords();

        return DataTables::of($model)
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildLinkHtml([
                    'url' => route('admin.branch.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>',
                ]);
                if (\Auth::guard('admin')->user()->hasPermission('login_branch')) {
                    $actions .= buildLinkHtml([
                        'url' => route('admin.branch.login', ['id' => $model->id]),
                        'description' => '<i class="fa fa-key"></i>',
                        'color' => 'btn-info',
                    ]);
                }
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('admin.branch.delete', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                ]);
                /*$actions .= buildLinkHtml([
                    'url' => route('admin.branch.stock', ['id' => $model->id]),
                    'description' => '<i class="fa fa-shopping-cart"></i>',
                    'color' => 'btn-info',
                ]);*/
                return $actions;
            })
            ->make(true);
    }

    public function create(Request $request) {
        $countries = Country::GetCountries()->GetSelects();
        return view('admin.branch.create', ['countries' => $countries]);
    }

    public function createPost(Request $request) {
        $rules = Branch::$rules;
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            //Check bank name availibility
//            $name_cn_exists = Branch::where('name_cn', '=', $request->get('name_cn'))->first();
//            if ($name_cn_exists) {
//                return makeResponse(false, trans('common.branch_name_exists', ['name' => $request->get('name_cn')]));
//            }

            $name_en_exists = Branch::where('name_en', '=', $request->get('name_en'))->first();
            if ($name_en_exists) {
                return makeResponse(false, trans('common.branch_name_exists', ['name' => $request->get('name_en')]));
            }

            $model = new Branch();
            $model->fill($request->all());
            $model->username = $request->get('username');
            $model->password = bcrypt($request->get('password'));
            $model->save();

            // Create a supplier account merge with branch
            $suppliers = Supplier::all();
            if (count($suppliers) > 0) {
                foreach ($suppliers as $supplier) {
                    $supplierAccount = new SupplierAccount();
                    $supplierAccount->supplier_id = $supplier->id;
                    $supplierAccount->branch_id = $model->id;
                    $supplierAccount->account_credit = 0;
                    $supplierAccount->save();
                }
            }

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function edit($id) {
        $model = Branch::GetRecords()->where('id', '=', $id)->first();

        if (!$model) {
            addError(trans('common.record_not_found'));
            return redirect()->route('admin.branch');
        }

        $countries = Country::GetCountries()->GetSelects();

        return view('admin.branch.edit', ['model' => $model, 'countries' => $countries]);
    }

    public function editPost(Request $request, $id) {
        $rules = Branch::$rules;
        $rules = array_merge($rules, [
            'username' => 'required|min:2|alpha_dash|unique:branch,username,'.$id,
            'password' => 'nullable|min:6|confirmed'
        ]);
        $this->validate($request, $rules);

        try {
            $model = Branch::GetRecords()->find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            //Check bank name availibility
            /*$name_cn_exists = Branch::where('name_cn', '=', $request->get('name_cn'))->where('id', '!=', $model->id)->first();
            if ($name_cn_exists) {
                return makeResponse(false, trans('common.branch_name_exists', ['name' => $request->get('name_cn')]));
            }*/

            $name_en_exists = Branch::where('name_en', '=', $request->get('name_en'))->where('id', '!=', $model->id)->first();
            if ($name_en_exists) {
                return makeResponse(false, trans('common.branch_name_exists', ['name' => $request->get('name_en')]));
            }

            $model->fill($request->all());
            $model->username = $request->input('username');
            if ($request->input('password')) {
                $model->password = bcrypt($request->input('password'));
            }
            $model->save();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }

    public function delete($id) {
        try {
            $model = Branch::GetRecords()->find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            if ($model->delete()) {
                // Delete staffs
                $model->staffs()->delete();

                return makeResponse(true, trans('common.operation_success'));
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }

    public function stock(Request $request, $id) {
        $model = Branch::GetRecords()->find($id);

        if (!$model) {
            addError(trans('common.record_not_found'));
            return redirect()->route('admin.branch');
        }

        $stocks = BranchStock::with(['product'])->where('branch_id', '=', $model->id)->get();

        $total_quantity = 0;
        $total_cost = 0.00;
        foreach ($stocks as $key => $stock) {
            $stock->cost = $stock->product ? $stock->product->product_cost : 0;
            $stock->total_cost = $stock->quantity * $stock->cost;

            $total_quantity += $stock->quantity;
            $total_cost += $stock->total_cost;
        }

        return view('admin.branch.stock', ['model' => $model, 'stocks' => $stocks, 'total_quantity' => $total_quantity, 'total_cost' => $total_cost]);
    }

    public function stockDt(Request $request, $id) {
        $model = BranchStockTransaction::with(['product'])->GetRecords()->where('branch_id', '=', $id);

        return DataTables::of($model)
            ->editColumn('product_id', function ($m) {
                return $m->product ? $m->product->getProductName() : $m->getName();
            })
            ->editColumn('quantity', function ($m) {
                return fundFormat($m->quantity, 0);
            })
            ->editColumn('price', function ($m) {
                return fundFormat($m->product ? $m->product->product_price : 0, 0);
            })
            ->editColumn('transaction_type', function ($m) {
                return $m->explainTransactionType();
            })
            ->editColumn('remark', function ($m) {
                return $m->remark != null ? $m->remark : '-';
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->make(true);
    }

    public function stockManage(Request $request, $id) {
        $field = 'product_name_' . app()->getLocale();
        $products = Product::pluck($field, 'id');

        $model = Branch::find($id);

        if (!$model) {
            return view('record_not_found');
        }

        return view('admin.branch.stock_manage', ['model' => $model, 'products' => $products]);
    }

    public function stockManagePost(Request $request, $id) {
        try {
            \DB::beginTransaction();

            $model = Branch::find($id);

            $model->manageStock($request->get('product'), $request->get('transaction_type'), $request->get('quantity'), [], $request->get('remark'));

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true,  trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function loginBranch(Request $request, $id)
    {
        try {
            $model = Branch::find($id);

            if ($model) {
                \Auth::guard('branch')->login($model);

                return redirect()->route('branch.dashboard');
            } else {
                throw new \Exception(trans('common.branch_not_found'));
            }
        } catch (\Exception $e) {
            return makeResponse(false, $e->getMessage());
        }
    }
}
