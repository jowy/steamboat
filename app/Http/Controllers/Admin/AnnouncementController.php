<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use Illuminate\Http\Request;
use App\Models\Announcement;
use DataTables;

class AnnouncementController extends AdminBaseController {
    public function index(Request $request) {
        $announcement = Announcement::first();
        return view('admin.announcement.index', ['announcement' => $announcement]);
    }

    public function indexDt(Request $request) {
        $dt = dt(Announcement::GetRecords());

        return $dt->setup(
            $dt->getDt()
            ->editColumn('title', function ($m) {
                return $m->getTitle();
            })

            ->addColumn('actions', function ($model) {
                $actions = '';

                $actions .= buildLinkHtml([
                    'url' => route('admin.announcement.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>'
                ]);

                $actions .= buildConfirmationLinkHtml([
                    'url' => route('admin.announcement.delete.post', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>'
                ]);

                return $actions;
            })
            ->rawColumns(['actions'])
        )->render();
    }

    public function create(Request $request) {
        return view('admin.announcement.create');
    }

    public function createPost(Request $request) {
        $rules = Announcement::$rules;
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();
            $model = new Announcement();
            $model->fill($request->all());
            $model->save();
            \DB::commit();

            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function edit(Request $request, $id) {
        $model = Announcement::find($id);

        if (!$model) {
            addError(trans('common.record_not_found'));
            return redirect()->route('admin.announcement');
        }

        return view('admin.announcement.edit', ['model' => $model]);
    }

    public function editPost(Request $request, $id) {
        $rules = Announcement::$rules;
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();
            $model = Announcement::find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }
            $model->fill($request->all());

            $model->save();
            \DB::commit();

            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e->getMessage());
        }
    }

    public function deletePost(Request $request, $id) {
        try {
            \DB::beginTransaction();
            $model = Announcement::find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model->delete();

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e->getMessage());
        }
    }
}