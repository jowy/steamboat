<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Input;
use DataTables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminBaseController;
use App\Models\Bank;
use App\Models\CompanyBank;
use App\Models\Country;

class CompanyBankController extends AdminBaseController
{
    public function index(Request $request) {
        $countries = Country::GetCountries()->GetSelects();
        return view('admin.company_bank.index', ['countries' => $countries]);
    }

    public function indexDt(Request $request) {
        $dt = dt(CompanyBank::GetRecords()->with(['country', 'bank']));

        return $dt->setup(
            $dt->getDt()
            ->editColumn('country_id', function ($m) {
                return $m->country ? $m->country->getCountryName() : '-';
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildRemoteLinkHtml([
                    'url' => route('admin.companybank.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>',
                    'modal' => '#remote-modal',
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('admin.companybank.delete', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                ]);
                return $actions;
            })
            ->rawColumns(['actions'])
        )->render();
    }

    public function create(Request $request) {
        $countries = Country::GetCountries()->GetSelects();
        return view('admin.company_bank.create', ['countries' => $countries]);
    }

    public function createPost(Request $request) {
        $rules = CompanyBank::$rules;
        $this->validate($request, $rules);
        try {
            \DB::beginTransaction();

            $bank = Bank::find($request->get('bank_id'));

            if (!$bank) {
                throw new \Exception(trans('common.bank_not_found'));
            }

            $model = new CompanyBank();
            $model->bank_id = $bank->id;
            $model->name_en = $bank->name_en;
            $model->name_cn = $bank->name_cn;
            $model->country_id = $bank->country_id;
            $model->account_name = $request->get('account_name');
            $model->account_number = $request->get('account_number');

            $model->save();

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function edit(Request $request, $id) {
        $model = CompanyBank::with(['bank', 'country'])->GetRecords()->where('id', '=', $id)->first();

        if (!$model) {
            addError(trans('common.record_not_found'));
            return redirect()->route('admin.companybank');
        }
        $countries = Country::GetCountries()->GetSelects();
        return view('admin.company_bank.edit', ['model' => $model, 'countries' => $countries]);
    }

    public function editPost(Request $request, $id) {
        $rules = CompanyBank::$rules;
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $model = CompanyBank::GetRecords()->find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $bank = Bank::GetRecords()->find($request->get('bank_id'));

            if (!$bank) {
                throw new \Exception(trans('common.bank_not_found'));
            }

            $model->bank_id = $bank->id;
            $model->name_en = $bank->name_en;
            $model->name_cn = $bank->name_cn;
            $model->country_id = $bank->country_id;
            $model->account_name = $request->get('account_name');
            $model->account_number = $request->get('account_number');

            $model->save();

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function delete($id) {
        try {
            \DB::beginTransaction();

            $model = CompanyBank::GetRecords()->find($id);

            if ($model->delete()) {
                \DB::commit();
                return makeResponse(true, trans('common.operation_success'));
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }
}
