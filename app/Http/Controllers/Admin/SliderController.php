<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Input;
use DataTables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminBaseController;
use App\Models\Slider;

class SliderController extends AdminBaseController
{
    public function index(Request $request) {
        return view('admin.slider.index');
    }

    public function indexDt(Request $request) {
        $dt = dt(Slider::GetRecords());

        return $dt->setup(
            $dt->getDt()
            ->addColumn('titles', function ($model) {
                return $model->getTitles();
            })
//            ->editColumn('blabla', function ($model) {
//
//            })
//            ->addColumn('blabla2', function ($model) {
//
//            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildLinkHtml([
                    'url' => route('admin.slider.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>',
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('admin.slider.delete.post', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                ]);
                return $actions;
            })
            ->rawColumns(['actions'])
        )->render();
    }

    public function create(Request $request) {
        return view('admin.slider.create');
    }

    public function createPost(Request $request) {
        $rules = Slider::$rules;

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $model = new Slider();
            $model->fill($request->all());

            $model->image_en = $request->file('image_en');
            $model->image_cn = $request->file('image_cn');

            $model->save();

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function edit($id) {
        $model = Slider::GetRecords()->find($id);
        return view('admin.slider.edit', ['model' => $model]);
    }

    public function editPost(Request $request, $id) {
        $rules = Slider::$rules;

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();
            $model = Slider::GetRecords()->find($id);

            $model->fill($request->all());

            if ($request->hasFile('image_en')) {
                $model->image_en = $request->file('image_en');
            }

            if ($request->hasFile('image_cn')) {
                $model->image_cn = $request->file('image_cn');
            }

            $model->save();

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function deletePost($id) {
        $model = Slider::GetRecords()->find($id);

        try {
            \DB::beginTransaction();

            if ($model->delete()) {
                \DB::commit();
                addSuccess(trans('common.operation_success'));
                return makeResponse(true, trans('common.operation_success'));
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }
}
