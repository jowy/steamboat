<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Input;
use DataTables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminBaseController;

class ImageController extends AdminBaseController
{
    public function __construct() {
        parent::__construct();
    }

    public function imageUploader(Request $request) {
        $id = generateRandomHtmlId();
        \Debugbar::disable();
        return view('image.image_uploader_dialog', ['id' => $id]);
    }

    public function imageUpload(Request $request) {
        $name = generateRandomUniqueName();
        $date = new \Carbon\Carbon();
        $path = 'uploads/' . $date->format('Y/m/d/');
        $filename = $name . '.jpg';
        $file = $request->file('imagefile');
        $file->move($path, $filename);
        $file_path = $path . $filename;

        return makeResponse(true, '/' . $file_path);
    }
}
