<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserWithdrawal;
use App\Models\UserTransaction;
use App\Models\Bank;
use DataTables;

class UserWithdrawalController extends AdminBaseController {

    public function index(Request $request) {
        return view('admin.user_withdrawal.index');
    }

    public function indexDt(Request $request) {
        $dt = dt(UserWithdrawal::with(['user', 'admin'])->GetRecords());

        return $dt->setup(
            $dt->getDt()
            ->editColumn('user_id', function ($m) {
                return $m->user ? $m->user->username : '-';
            })
            ->editColumn('admin_id', function ($m) {
                return $m->admin ? $m->admin->username : '-';
            })
            ->editColumn('bank_name_en', function ($m) {
                return $m->getBankName();
            })
            ->editColumn('bank_account_name', function ($m) {
                return $m->bank_account_name;
            })
            ->editColumn('bank_account_number', function ($m) {
                return $m->bank_account_number;
            })
            ->editColumn('amount', function ($m) {
                return fundFormat($m->amount);
            })
            ->editColumn('admin_fees', function ($m) {
                return fundFormat($m->admin_fees, 2);
            })
            ->editColumn('amount_after_admin_fees', function ($m) {
                return fundFormat($m->amount_after_admin_fees);
            })
            ->editColumn('local_currency_amount', function ($m) {
                return fundFormat($m->local_currency_amount, 2);
            })
            ->editColumn('credit_type', function ($m) {
                return $m->explainCreditType();
            })
            ->editColumn('status', function ($m) {
                return $m->explainStatus();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';

                $actions .= buildRemoteLinkHtml([
                    'url' => route('admin.user_withdrawal.details', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>'
                ]);

                return $actions;
            })
        )->render();
    }

    public function withdrawalHandle(Request $request, $id) {
        try {
            \DB::beginTransaction();
            $me = $this->_G['admin'];
            $model = UserWithdrawal::find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            if ($model->isDone()) {
                throw new \Exception(trans('common.unknown_error'));
            }

            switch ($request->get('status')) {
                case 1:
                    $model->admin_id = $me->id;
                    $model->admin_remark = $request->get('admin_remark');
                    $model->status = 1;

                    $model->save();
                    break;
                case 2:
                    $model->admin_id = $me->id;
                    $model->admin_remark = $request->get('admin_remark');
                    $model->status = 2;

                    $ut = new UserTransaction();
                    $ut->user_id = $model->user_id;
                    $ut->credit_type = $model->credit_type;
                    $ut->amount = $model->amount;
                    $ut->transaction_type = 402;
                    $ut->remark = $model->admin_remark;
                    $ut->save();

                    $model->save();

                    \DB::table('user')->where('id', '=', $model->user_id)->increment('credit_' . $model->credit_type, $model->amount);
                    break;
                default: throw new \Exception(trans('common.nothing_has_done')); break;
            }

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function details(Request $request, $id) {
        $me = $this->_G['admin'];

        $model = UserWithdrawal::with(['user', 'admin'])->find($id);

        if (!$model) {
            return view('record_not_found');
        }

        if (!$model->isDone()) {
            return view('admin.user_withdrawal.update', ['model' => $model]);
        } else {
            return view('admin.user_withdrawal.details', ['model' => $model]);
        }
    }
}