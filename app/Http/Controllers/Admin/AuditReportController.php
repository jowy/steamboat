<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Input;
use Datatables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminBaseController;
use App\Models\AuditRecords;

class AuditReportController extends AdminBaseController
{
    public function index(Request $request) {
        return view('admin.audit_log.index');
    }

    public function indexDt(Request $request) {
        $dt = dt(AuditRecords::GetRecords());

        return $dt->setup(
            $dt->getDt()
            ->editColumn('admin_id', function ($m) {
                return $m->admin ? $m->admin->username : '-';
            })
            ->editColumn('function_tag', function ($m) {
                return $m->explainFunctionTag();
            })
            ->editColumn('operation', function ($m) {
                return $m->explainOperation();
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                if ($model->operation != AuditRecords::OPERATION_LOGIN) {
                    $actions = buildRemoteLinkHtml([
                        'url' => route('admin.audit_log.details', ['id' => $model->id]),
                        'description' => '<i class="fa fa-eye"></i>',
                    ]);
                    return $actions;
                }
            })
            ->rawColumns(['actions'])
        )->render();
    }

    public function details(Request $request, $id) {
        $model = AuditRecords::with(['admin'])->GetRecords()->find($id);

        if (!$model) {
            return view('record_not_found');
        }

        return view('admin.audit_log.details', ['model' => $model]);
    }
}
