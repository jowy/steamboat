<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\ProductCategory;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminBaseController;
use App\Models\Product;
use App\Models\ProductImages;

class ProductController extends AdminBaseController
{
    public function index(Request $request) {
        return view('admin.product.index');
    }

    public function indexDt(Request $request) {
        $dt = dt(Product::query()->GetProducts());

        return $dt->setup(
            $dt->getDt()
            ->editColumn('product_name_en', function ($model) {
                return $model->getProductName();
            })
            ->editColumn('is_ala_carte', function ($model) {
                return ($model->is_ala_carte) ? trans('common.yes') : trans('common.no');
            })
            ->editColumn('ala_carte_price', function ($model) {
                if ($model->is_ala_carte) {
                    return fundFormat($model->ala_carte_price);
                } else {
                    return '-';
                }
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildLinkHtml([
                    'url' => route('admin.product.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>',
                ]);
                if ($model->canDelete()) {
                    $actions .= buildConfirmationLinkHtml([
                        'url' => route('admin.product.delete.post', ['id' => $model->id]),
                        'description' => '<i class="fa fa-trash"></i>',
                    ]);
                }
                return $actions;
            })
        )->render();
    }

    public function create(Request $request) {
        $model = new Product();
        $alaCarteProducts = Product::whereIsAlaCarte(true)
                                ->whereNull('subtract_product_id')
                                ->whereNull('subtract_quantity')
                                ->pluck('product_name_en', 'id');

        //$categories = Category::getCategories();
        return view('admin.product.form', ['model' => $model, 'ala_carte_products' => $alaCarteProducts]);
    }

    public function createPost(Request $request) {
        $rules = [
            'product_name_en' => 'required|string|min:1|max:64',
            //'product_name_cn' => 'required|string|min:1|max:64',
            //'product_image_en' => 'required|image',
            //'product_image_cn' => 'required|image',
            //'images.*' => 'nullable|image',
            //'sorting' => 'nullable|isNumber',
            //'product_code' => 'nullable|string|min:1|max:64',
            'is_ala_carte' => 'required|boolean',
            'ala_carte_price' => 'required_if:is_ala_carte,'.true.'|isFund',
            'subtract_product_id' => 'nullable|exists:product,id,is_ala_carte,'.true.',subtract_product_id,NULL,subtract_quantity,NULL',
            'subtract_quantity' => 'nullable|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
        ];

        $this->validate($request, $rules);

        if ($request->get('is_ala_carte') == true && $request->get('subtract_product_id')) {
            if (!$request->get('subtract_quantity') || empty($request->get('subtract_quantity')) || is_null($request->get('subtract_quantity'))) {
                return makeResponse(false, trans('common.subtract_quantity_required'));
            }
        }

        if ($request->get('subtract_product_id') && $request->get('subtract_quantity')) {
            if ($request->get('is_ala_carte') != true) {
                return makeResponse(false, trans('common.must_ala_carte_subtract'));
            }
        }

        try {
            \DB::beginTransaction();

            $model = new Product();
            $model->product_name_en = $request->get('product_name_en');
            $model->quantity_label = $request->get('quantity_label');
            $model->product_description_en = $request->get('product_description_en');
            $model->product_short_description_en = $request->get('product_short_description_en');
            if ($request->get('is_ala_carte')) {
                $model->is_ala_carte = $request->get('is_ala_carte');
                $model->ala_carte_price = $request->has('ala_carte_price') && $request->get('ala_carte_price') != '' ? $request->get('ala_carte_price') : 0;
            }

            # Is subtract quantity product (Eg: Bucket of beers)
            if ($request->get('is_ala_carte') == true && $request->get('subtract_product_id') && $request->get('subtract_quantity')) {
                $model->subtract_product_id = $request->get('subtract_product_id');
                $model->subtract_quantity = $request->get('subtract_quantity');
            }
            $model->save();
            /*if (!$request->hasFile('product_image_en')) {
                throw new \Exception(trans('common.please_upload_image'));
            }

            $model->product_image_en = $request->file('product_image_en');

            if (!$request->hasFile('product_image_cn')) {
                throw new \Exception(trans('common.please_upload_image'));
            }

            $model->product_image_cn = $request->file('product_image_cn');

            if ($request->hasFile('images')) {
                $files = $request->file('images');
                $images = array();
                foreach ($files as $key => $var) {
                    $image = new ProductImages();
                    $image->path = $var;

                    $images[] = $image;
                }
            }

            $categories = $request->get('categories');
            if (!$categories || count($categories) <= 0) {
                throw new \Exception(trans('common.please_select_category'));
            }*/

            /*foreach ($categories as $cid) {
                $category = Category::find($cid);
                if ($category) {
                    $pc = new ProductCategory();
                    $pc->product_id = $model->id;
                    $pc->category_id = $category->id;
                    $pc->save();
                }
            }

            if (isset($images) && count($images) > 0) {
                foreach ($images as $key => $var) {
                    $var->product_id = $model->id;
                    $var->save();
                }
            }*/

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function edit(Request $request, $id) {
        $model = Product::query()->GetProducts()->find($id);
        $alaCarteProducts = Product::whereIsAlaCarte(true)
            ->whereNull('subtract_product_id')
            ->whereNull('subtract_quantity')
            ->pluck('product_name_en', 'id');
        /*$categories = Category::getCategories();
        $selected = array();
        foreach ($model->categories as $c) {
            $selected[$c->category_id] = $c;
        }*/

        return view('admin.product.form', ['model' => $model, 'ala_carte_products' => $alaCarteProducts]);
    }

    public function editPost(Request $request, $id) {
        $rules = [
            'product_name_en' => 'required|string|min:1|max:64',
           /* 'product_name_cn' => 'required|string|min:1|max:64',
            'product_image_en' => 'nullable|image',
            'product_image_cn' => 'nullable|image',
            'images.*' => 'nullable|image',
            'sorting' => 'nullable|isNumber',
            'product_code' => 'nullable|string|min:1|max:64',*/
            'ala_carte_price' => 'required_if:is_ala_carte,'.true.'|isFund',
            'is_ala_carte' => 'required|boolean',
            'subtract_product_id' => 'nullable|exists:product,id,is_ala_carte,'.true.',subtract_product_id,NULL,subtract_quantity,NULL',
            'subtract_quantity' => 'nullable|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
        ];

        $this->validate($request, $rules);

        if ($request->get('is_ala_carte') == true && $request->get('subtract_product_id')) {
            if (!$request->get('subtract_quantity') || empty($request->get('subtract_quantity')) || is_null($request->get('subtract_quantity'))) {
                return makeResponse(false, trans('common.subtract_quantity_required'));
            }
        }

        if ($request->get('subtract_product_id') && $request->get('subtract_quantity')) {
            if ($request->get('is_ala_carte') != true) {
                return makeResponse(false, trans('common.must_ala_carte_subtract'));
            }
        }

        try {
            \DB::beginTransaction();
            $model = Product::GetProducts()->find($id);
            $model->product_name_en = $request->get('product_name_en');
            $model->quantity_label = $request->get('quantity_label');
            $model->product_description_en = $request->get('product_description_en');
            $model->product_short_description_en = $request->get('product_short_description_en');
            if ($request->get('is_ala_carte')) {
                $model->is_ala_carte = $request->get('is_ala_carte');
                $model->ala_carte_price = $request->has('ala_carte_price') && $request->get('ala_carte_price') != '' ? $request->get('ala_carte_price') : 0;
            }

            # Is subtract quantity product (Eg: Bucket of beers)
            if ($request->get('is_ala_carte') == true && $request->get('subtract_product_id') && $request->get('subtract_quantity')) {
                $model->subtract_product_id = $request->get('subtract_product_id');
                $model->subtract_quantity = $request->get('subtract_quantity');
            } else {
                $model->subtract_product_id = null;
                $model->subtract_quantity = null;
            }
            $model->save();

            /*$delete_imgs = array();

            if ($request->hasFile('product_image_en')) {
                if ($model->product_image_en != null) {
                    $delete_imgs[] = $model->product_image_en;
                }
                $model->product_image_en = $request->file('product_image_en');
            }

            if ($request->hasFile('product_image_cn')) {
                if ($model->product_image_cn != null) {
                    $delete_imgs[] = $model->product_image_cn;
                }
                $model->product_image_cn = $request->file('product_image_cn');
            }

            if ($request->hasFile('images')) {
                $files = $request->file('images');
                $images = array();
                foreach ($files as $key => $var) {
                    $image = new ProductImages();
                    $image->path = $var;

                    $images[] = $image;
                }
            }

            $names['product_name_en'] = $model->product_name_en;
            $names['product_name_cn'] = $model->product_name_cn;

            $categories = $request->get('categories');
            if (!$categories || count($categories) <= 0) {
                throw new \Exception(trans('common.please_select_category'));
            }

            $model->save();

            if (isset($images) && count($images) > 0) {
                foreach ($images as $key => $var) {
                    $var->product_id = $model->id;
                    $var->save();
                }
            }

            if ($request->has('delete_image')) {
                foreach ($request->get('delete_image') as $key => $var) {
                    $img = ProductImages::where('product_id', '=', $id)->where('id', '=', $var)->first();
                    if ($img) {
                        $delete_imgs[] = $img->path;
                        $img->delete();
                    }
                }
            }

            ProductCategory::where('product_id', '=', $model->id)->delete();

            foreach ($categories as $cid) {
                $category = Category::find($cid);
                if ($category) {
                    $pc = new ProductCategory();
                    $pc->product_id = $model->id;
                    $pc->category_id = $category->id;
                    $pc->save();
                }
            }*/

            \DB::commit();

            /*if (isset($delete_imgs) && count($delete_imgs) > 0) {
                foreach ($delete_imgs as $key => $var) {
                    if ($var != '' && $var != null) {
                        @unlink(public_path($var));
                    }
                }
            }*/
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function deletePost(Request $request, $id) {
        $model = Product::with(['images'])->GetProducts()->find($id);

        try {
            \DB::beginTransaction();
            if (!$model->canDelete()) {
                throw new \Exception(trans('common.unknown_error'));
            }

            $images = array();
            if ($model->product_image != null) {
                $images[] = $model->product_image;
            }
            if ($model->images) {
                foreach ($model->images as $key => $var) {
                    $images[] = $var->path;
                }
            }

            if ($model->delete()) {
                \DB::commit();
                //Soft deletes no need delete image
//                if (isset($images) && count($images) > 0) {
//                    foreach ($images as $key => $var) {
//                        if ($var != '' && $var != null) {
//                            @unlink(public_path($var));
//                        }
//                    }
//                }
                return makeResponse(true, trans('common.operation_success'));
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e->getMessage());
        }
    }
}
