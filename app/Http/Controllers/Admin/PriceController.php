<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use App\Models\Price;
use App\Models\Sale;
use Illuminate\Http\Request;
use DataTables;

class PriceController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (isAjax()) {
            $model = Price::query();

            return DataTables::of($model)
                /*->filter(function ($model) {
                    return $model->GetFilteredResults();
                })*/
                ->editColumn('per_pax_price', function ($model) {
                    return fundFormat($model->per_pax_price);
                })
                ->addColumn('actions', function ($model) {
                    $actions = buildRemoteLinkHtml([
                        'url' => route('admin.price.update', ['id' => $model->id]),
                        'description' => '<i class="fa fa-eye"></i>',
                    ]);
                    if (!$model->is_system) {
                        $actions .= buildConfirmationLinkHtml([
                            'url' => route('admin.price.delete', ['id' => $model->id]),
                            'description' => '<i class="fa fa-trash"></i>',
                        ]);
                    }
                    return $actions;
                })
                ->make(true);
        } else {
            return view('admin.price.index');
        }
    }

    public function create(Price $price)
    {
        $price = new Price();
        return view('admin.price.create', ['model' => $price]);
    }

    public function store(Request $request)
    {
        $rules = Price::$rules;
        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $price = new Price();
            $price->name_en = $request->get('name_en');
            $price->per_pax_price = $request->get('per_pax_price');
            $price->save();

            $price->type = $price->id;
            $price->save();

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Price $price
     * @return \Illuminate\Http\Response
     */
    public function edit(Price $price)
    {
        return view('admin.price.update', ['model' => $price]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Price $price
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Price $price)
    {
        $rules = Price::$rules;
        if ($price->is_system) {
            unset($rules['name_en']);
        }
        $this->validate($request, $rules);

        if ($price->type == 'free') {
            throw new \Exception(trans('common.free_type_cannot_changed'));
        }
        # Cannot have any sale table under this price (Check finish at)
        if(Sale::where('finish_at', '>=', \Carbon\Carbon::now())->whereHas('sale_details', function ($query) use ($price) {
            $query->where('sale_details.price_type', $price->type);
        })->exists()) {
            throw new \Exception(trans('common.sale_table_under_this_price'));
        }

        try {
            \DB::beginTransaction();

            if (!$price->is_system) {
                $price->name_en = $request->get('name_en');
            }
            $price->per_pax_price = $request->get('per_pax_price');
            $price->save();

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e->getMessage());
        }
    }

    public function destroy($id)
    {

        try {
            $model = Price::whereId($id)->whereIsSystem(false)->first();

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            # Cannot have any sale table under this price (Check finish at)
            if(Sale::where('finish_at', '>=', \Carbon\Carbon::now())->whereHas('sale_details', function ($query) use ($model) {
                $query->where('sale_details.price_type', $model->type);
            })->exists()) {
                throw new \Exception(trans('common.sale_table_under_this_price'));
            }

            if ($model->delete()) {
                return makeResponse(true, trans('common.operation_success'));
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }

    }
}
