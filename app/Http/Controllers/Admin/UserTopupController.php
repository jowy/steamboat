<?php

namespace App\Http\Controllers\Admin;

use App\Managers\UserBonusManager;
use Auth;
use Input;
use DataTables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminBaseController;
use App\Models\UserTransaction;
use App\Models\User;
use App\Models\UserTopup;
use App\Models\CompanyBank;

class UserTopupController extends AdminBaseController
{
    public function index(Request $request) {
        return view('admin.user_topup.index');
    }

    public function indexDt(Request $request) {
        $dt = dt(UserTopup::with(['user', 'admin', 'country', 'bank', 'companyBank'])->GetRecords());

        return $dt->setup(
            $dt->getDt()
            ->editColumn('user_id', function ($m) {
                return $m->user ? $m->user->username : '-';
            })
            ->editColumn('admin_id', function ($m) {
                return $m->admin ? $m->admin->username : '-';
            })
            ->editColumn('bank_name_en', function ($m) {
                return $m->getBankName();
            })
            ->editColumn('bank_account_name', function ($m) {
                return $m->bank_account_name;
            })
            ->editColumn('bank_account_number', function ($m) {
                return $m->bank_account_number;
            })
            ->editColumn('amount', function ($m) {
                return fundFormat($m->amount);
            })
            ->editColumn('admin_fees', function ($m) {
                return fundFormat($m->admin_fees, 2);
            })
            ->editColumn('amount_after_admin_fees', function ($m) {
                return fundFormat($m->amount_after_admin_fees);
            })
            ->editColumn('local_currency_amount', function ($m) {
                return $m->country ? $m->country->explainCurrency(fundFormat($m->local_currency_amount)) : fundFormat($m->local_currency_amount);
            })
            ->editColumn('credit_type', function ($m) {
                return $m->explainCreditType();
            })
            ->editColumn('status', function ($m) {
                return $m->explainStatus();
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';
                $actions .= buildRemoteLinkHtml([
                    'url' => route('admin.user_topup.details', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>'
                ]);

                return $actions;
            })
            ->rawColumns(['actions'])
        )->render();
    }

    public function updatePost(Request $request, $id) {
        try {
            \DB::beginTransaction();

            $me = $this->_G['admin'];

            $model = UserTopup::with(['user'])->find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            if ($model->isDone()) {
                throw new \Exception(trans('common.unknown_error'));
            }

            $model->admin_id = $me->id;
            $model->admin_remark = $request->get('admin_remark');
            $model->status = $request->get('status');

            $model->save();
            if ($model->status == 1) {
                if ($model->credit_type == 2) {
                    UserBonusManager::distributeTopupCredit2($model);
                }
            }
            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function details(Request $request, $id) {
        $model = UserTopup::with(['user', 'admin', 'country', 'bank', 'companyBank'])->find($id);

        if (!$model) {
            return view('record_not_found');
        }

        if (!$model->isDone()) {
            return view('admin.user_topup.update', ['model' => $model]);
        }

        return view('admin.user_topup.details', ['model' => $model]);
    }
}
