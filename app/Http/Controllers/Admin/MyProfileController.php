<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use Illuminate\Http\Request;
use Auth;

class MyProfileController extends AdminBaseController {
    public function profile(Request $request) {
        return view('admin.admin.my_profile');
    }

    public function profilePost(Request $request) {
        $rules = [
            'current_password' => 'required|isPassword',
            'new_password' => 'nullable|confirmed|isPassword'
        ];

        $this->validate($request, $rules);

        $me = $this->_G['admin'];

        if (!$me) {
            throw new \Exception('common.permission_denied');
        }

        if (!password_verify($request->get('current_password'), $me->password)) {
            throw new \Exception(trans('common.incorrect_current_password'));
        }

        if ($request->has('new_password') && $request->get('new_password') != '') {
            if ($request->get('new_password') == $request->get('new_password_confirmation')) {
                $me->password = $request->get('new_password');
            }
        }

        if (array_key_exists($request->get('lang'), $this->_G['langs'])) {
            $me->lang = $request->get('lang');
        } else {
            $me->lang = 'en';
        }

        session()->put('admin_lang', $me->lang);

        try {
            $me->save();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }
}