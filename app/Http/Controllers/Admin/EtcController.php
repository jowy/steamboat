<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use Illuminate\Http\Request;
use App\Constants;

class EtcController extends AdminBaseController {

    public function setLang(Request $request) {
        $me = \Auth::guard('admin')->user();
        $lang = $request->get('lang');

        if ($me) {
			if (array_key_exists(strtolower($lang), Constants::getLanguages())) {
				$me->lang = strtolower($lang);
			} else {
				$me->lang = 'en';
			}

            $me->save();
        }

        session()->put('admin_lang', $lang);
        app()->setLocale($lang);

        $redirect = $request->get('redirect');

        if (isset($redirect) && $redirect != '' && $redirect != null) {
            return redirect()->to($redirect);
        } else {
            return redirect()->route('admin.home');
        }
    }
}