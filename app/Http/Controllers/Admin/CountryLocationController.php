<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\CountryLocations;
use DataTables;

class CountryLocationController extends AdminBaseController {
    public function update(Request $request, $id) {
        $model = Country::with(['locations'])->find($id);

        if (!$model) {
            addError(trans('common.record_not_found'));
            return redirect()->route('admin.country');
        }

        $first_level = CountryLocations::with(['sons'])->where('country_id', '=', $model->id)->whereNull('parent_id')->get();

        return view('admin.country_locations.update', ['model' => $model, 'first_level' => $first_level]);
    }

    public function subCreate(Request $request, $id) {
        $father = Country::find($id);

        return view('admin.country_locations.sub', ['father' => $father]);
    }

    public function subCreatePost(Request $request, $id) {
        $rules = CountryLocations::$rules;
        $this->validate($request, $rules);
        try {
            \DB::beginTransaction();
            $father = Country::find($id);

            if (!$father) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model = new CountryLocations();
            $model->country_id = $father->id;
            $model->location_name_en = $request->get('location_name_en');
            $model->location_name_cn = $request->get('location_name_cn');
            $model->sorting = CountryLocations::where('country_id', '=', $father->id)->count() + 1;
            $model->save();

            \DB::commit();

            return makeResponse(true, trans('common.operation_success'), ['model' => $model, 'text' => $model->getLocationName()]);
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function subEdit(Request $request, $id, $id2) {
        $father = Country::find($id);
        $model = CountryLocations::where('country_id', '=', $id)->find($id2);

        return view('admin.country_locations.sub', ['father' => $father, 'model' => $model]);
    }

    public function subEditPost(Request $request, $id, $id2) {
        $rules = CountryLocations::$rules;
        unset($rules['menu_id']);
        $this->validate($request, $rules);
        try {
            \DB::beginTransaction();
            $father = Country::find($id);

            if (!$father) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model = CountryLocations::where('country_id', '=', $id)->find($id2);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model->location_name_en = $request->get('location_name_en');
            $model->location_name_cn = $request->get('location_name_cn');

            $model->save();
            \DB::commit();

            return makeResponse(true, trans('common.operation_success'), ['model' => $model, 'text' => $model->getLocationName()]);
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function updateSorting(Request $request, $id) {
        try {
            \DB::beginTransaction();
            $father = Country::GetRecords()->find($id);

            if (!$father) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $target = CountryLocations::where('country_id', '=', $father->id)->find($request->get('target_id'));
            if (!$target) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $target->parent_id = null;

            if ($request->has('parent_id') && $request->get('parent_id') != 0) {
                if ($request->get('parent_id') != $target->id) {
                    $parent = CountryLocations::where('country_id', '=', $father->id)->find($request->get('parent_id'));

                    if (!$parent) {
                        throw new \Exception(trans('common.record_not_found'));
                    }

                    $target->parent_id = $parent->id;
                }
            }

            $target->save();

            $i = 1;
            foreach ($request->get('ids') as $key => $var) {
                \DB::table('country_locations')->where('country_id', '=', $father->id)->where('id', '=', $var)->update([
                    'sorting' => $i,
                ]);
                $i++;
            }
            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function subDeletePost(Request $request, $id, $id2) {
        try {
            \DB::beginTransaction();
            $model = CountryLocations::where('country_id', '=', $id)->find($id2);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model->delete();
            \DB::commit();
            return makeResponse(true, trans('common.operation_success'), ['model_id' => $id2]);
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }
}