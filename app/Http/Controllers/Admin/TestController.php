<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use App\Models\User;
use App\Models\UserTransaction;
use App\Models\UserBonus;
use App\Constants;

class TestController extends AdminBaseController {
    public function reset(Request $request) {
        if (config('env.APP_DEBUG') == false || !$this->_G['admin']->isDev()) {
            return redirect()->route('backend.home');
        }
        try {
            \DB::unprepared('SET FOREIGN_KEY_CHECKS=0;');
            $tables = \DB::select('SHOW TABLES');
            foreach ($tables as $table) {
                foreach ($table as $key => $value) {
                    \DB::unprepared("DROP TABLE IF EXISTS `" . $value . "`");
                }
            }
            \DB::unprepared('SET FOREIGN_KEY_CHECKS=0;');
            Artisan::call('migrate');
            Artisan::call('db:seed');

            addSuccess('Successfully reset');
            return redirect()->route('admin.home');
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }

    public function getRandomInfo() {
        $post = "countries=GBR&format=json";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://randomprofile.com/api/api.php');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

        $json = json_decode($response, true);

        $arr = array();
        $arr['username'] = $json['profile']['username'];
        $arr['email'] = $json['profile']['email'];
        $arr['gender'] = $json['profile']['gender'] == 'Female' ? 1 : 0;
        $arr['name'] = $json['profile']['surname'] . ' ' . $json['profile']['firstName'];

        return $arr;
    }
}