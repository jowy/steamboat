<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Input;
use DataTables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminBaseController;
use App\Models\PermissionGroup;
use App\Models\PermissionGroupPermission;

class PermissionController extends AdminBaseController
{
    public function index(Request $request) {
        return view('admin.permission.index');
    }

    public function indexDt(Request $request) {
        $dt = dt(PermissionGroup::with(['permissions'])->GetGroups());

        return $dt->setup(
            $dt->getDt()
            ->filter(function ($m) {
                return $m->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildLinkHtml([
                    'url' => route('admin.permission.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-eye"></i>'
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('admin.permission.delete', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                ]);
                return $actions;
            })
            ->rawColumns(['actions'])
        )->render();
    }

    public function create(Request $request) {
        return view('admin.permission.create');
    }

    public function createPost(Request $request) {
        $rules = PermissionGroup::$rules;

        $this->validate($request, $rules);

        try {
            DB::beginTransaction();

            $model = new PermissionGroup();
            $model->group_name = $request->get('group_name');

            $permissions = array();
            foreach ($request->get('permissions') as $key => $var) {
                $p = new PermissionGroupPermission();
                $p->permission_tag = $var;

                $permissions[] = $p;
            }

            if (count($permissions) < 1) {
                throw new \Exception(trans('common.please_select_atleast_1_permission'));
            }

            $model->save();

            if (!$model->permissions()->saveMany($permissions)) {
                throw new \Exception(trans('common.unknown_error'));
            }

            DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function edit($id) {
        $model = PermissionGroup::with(['permissions'])->find($id);
        $permissions = array();
        foreach ($model->permissions as $key => $var) {
            $permissions[$var->permission_tag] = $var->permission_tag;
        }

        return view('admin.permission.edit', ['model' => $model, 'permissions' => $permissions]);
    }

    public function editPost(Request $request, $id) {
        $rules = PermissionGroup::$rules;
        $rules['group_name'] = $rules['group_name'] . ',' . $id;

        $this->validate($request, $rules);

        try {
            DB::beginTransaction();
            $model = PermissionGroup::with(['permissions'])->find($id);

            if (!$model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model->group_name = $request->get('group_name');

            $permissions = array();
            foreach ($request->get('permissions') as $key => $var) {
                $p = new PermissionGroupPermission();
                $p->permission_tag = $var;

                $permissions[] = $p;
            }

            if (count($permissions) < 1) {
                throw new \Exception(trans('common.please_select_atleast_1_permission'));
            }

            if (!$model->permissions()->delete()) {
                throw new \Exception(trans('common.unknown_error'));
            }

            foreach ($permissions as $key => $var) {
                $var->permission_group_id = $model->id;
                $var->save();
            }
			
            $model->save();

            DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function delete($id) {
        try {
            \DB::beginTransaction();

            $model = PermissionGroup::with(['permissions'])->find($id);

            if ($model->delete()) {
                \DB::commit();
                return makeResponse(true, trans('common.operation_success'));
            } else {
                throw new \Exception(trans('common.unknown_error'));
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }
}
