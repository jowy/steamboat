<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use Illuminate\Http\Request;
use App\Constants;
use App\Models\User;
use DataTables;
use App\Http\Requests\Admin\UserEditFormRequest;

class UserController extends AdminBaseController {
    public function index(Request $request) {
        $credit_1 = User::sum('credit_1');
        $credit_2 = User::sum('credit_2');
        $credit_3 = User::sum('credit_3');
        $credit_4 = User::sum('credit_4');
        return view('admin.user.index', [
            'credit_1' => $credit_1,
            'credit_2' => $credit_2,
            'credit_3' => $credit_3,
            'credit_4' => $credit_4,
        ]);
    }

    public function indexDt(Request $request) {
        $dt = dt(User::with(['country', 'contactCountry'])->GetUsers());
        return $dt->setup(
            $dt->getDt()
//                ->editColumn('package_id', function ($m) {
//                    return $m->package ? $m->package->getName() : '-';
//                })
                ->editColumn('name', function ($m) {
                    return $m->name != null && $m->name != '' ? $m->name : '-';
                })
                ->editColumn('country_id', function ($m) {
                    return $m->country ? $m->country->getName() : '-';
                })
                ->editColumn('contact_country_id', function ($m) {
                    return $m->buildContactNumber();
                })
                ->editColumn('freeze', function ($m) {
                    return $m->explainFreeze();
                })
                ->editColumn('gender', function ($m) {
                    return $m->explainGender();
                })
                ->editColumn('credit_1', function ($m) {
                    return fundFormat($m->credit_1, \App\Constants::$decimal_point);
                })
                ->editColumn('credit_2', function ($m) {
                    return fundFormat($m->credit_2, \App\Constants::$decimal_point);
                })
                ->editColumn('credit_3', function ($m) {
                    return fundFormat($m->credit_3, \App\Constants::$decimal_point);
                })
                ->editColumn('credit_4', function ($m) {
                    return fundFormat($m->credit_4, \App\Constants::$decimal_point);
                })
                ->filter(function ($model) {
                    return $model->GetFilteredResults();
                })
                ->addColumn('actions', function ($model) {
                    $actions = buildRemoteLinkHtml([
                        'url' => route('admin.user.view', ['id' => $model->id]),
                        'description' => '<i class="fa fa-eye"></i>',
                        'modal' => '#remote-modal-large',
                    ]);
                    if (\Auth::guard('admin')->user()->hasPermission('login_user')) {
                        $actions .= buildLinkHtml([
                            'url' => route('admin.user.login', ['id' => $model->id]),
                            'description' => '<i class="fa fa-key"></i>',
                            'color' => 'bg-purple-800',
                        ]);
                    }
                    if (\Auth::guard('admin')->user()->hasPermission('manage_account_status')) {
                        $options = array();
                        $options['url'] = route('admin.user.accountstatusmanage', ['id' => $model->id]);
                        if ($model->freeze == 1) {
                            $options['description'] = '<i class="fa fa-unlock"></i>';
                            $options['color'] = 'bg-teal-a400';
                        } else {
                            $options['description'] = '<i class="fa fa-lock"></i>';
                            $options['color'] = 'bg-red-500';
                        }
                        $actions .= buildConfirmationLinkHtml($options);
                    }
                    return $actions;
                })
                ->rawColumns(['actions'])
        )->render();
    }

    public function view(Request $request, $id) {
        $model = User::GetUsers()->find($id);
        return view('admin.user.view', ['model' => $model]);
    }

    public function update(Request $request, $id) {
        $rules = [
            'username' => 'required|isUsername|notEmail',
            'email' => 'required',
            'name' => 'required|min:1|max:64',
            'gender' => 'required|isGender',
            'lang' => 'required|isLanguage',
            'country_id' => 'required|exists:country,id,status,1',
            'new_password' => 'nullable|isPassword|confirmed',
            'new_password2' => 'nullable|isPassword|confirmed',
        ];

        $this->validate($request, $rules);

        try {
            $model = User::GetUsers()->find($id);

            if ($request->get('username') != '') {
                $exists = User::GetUsers()->where('username', '=', $request->get('username'))->where('id', '!=', $model->id)->exists();
                if ($exists) {
                    throw new \Exception(trans('common.username_exists'));
                }

                $model->username = $request->get('username');
            }

            $model->email = $request->get('email');
            $model->name = $request->get('name');
            $model->lang = $request->get('lang');
            $model->gender = $request->get('gender');

            if ($request->has('new_password') && $request->get('new_password') != '') {
                $model->password = $request->get('new_password');
            }

            if ($request->has('new_password2') && $request->get('new_password2') != '') {
                $model->password2 = $request->get('new_password2');
            }

            $model->save();

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }
	
	public function loginUser(Request $request, $id) {
        try {
            $model = User::find($id);

            if ($model) {
				$log = \App\Models\AuditRecords::addLog(
                    $this->_G['admin']->username . ' login user account: ' . $model->username,
                    'login_user',
                    \App\Models\AuditRecords::OPERATION_LOGIN,
                    $model
                );

                if ($log) {
                    $log->save();
                }
				
                \Auth::guard('user')->login($model);

                return redirect()->route('user.home');
            } else {
                throw new \Exception(trans('common.user_not_found'));
            }
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }
	
	public function accountStatusManage(Request $request, $id) {
        $model = User::find($id);

        if ($model) {
            if ($model->freeze == 1) {
                $model->freeze = 0;
            } else {
                $model->freeze = 1;
            }
            $model->save();
            return makeResponse(true, trans('common.operation_success'));
        } else {
            return makeResponse(false, trans('common.user_not_found'));
        }
    }
}