<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use Illuminate\Http\Request;

class HomeController extends AdminBaseController {
    public function index(Request $request) {
        return view('admin.home');
    }
}