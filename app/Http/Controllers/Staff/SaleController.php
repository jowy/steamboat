<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\UserBaseController;
use App\Models\BranchStock;
use App\Models\BranchStockTransaction;
use App\Models\BranchTable;
use App\Models\Price;
use App\Models\Product;
use App\Models\Promotion;
use App\Models\Sale;
use App\Models\SaleDetail;
use App\Models\SaleOrder;
use App\Models\SaleTable;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SaleController extends UserBaseController
{
    public function index()
    {
        $sales = Sale::where('branch_id', '=', \Auth::guard('staff')->user()->branch_id)
            ->notPaid()
            ->select('id', 'grand_total', 'finish_at', 'total_pax', 'is_ala_carte_sale', 'sale_ref_id')
//            ->where(function ($q) {
//                $q->orWhere('finish_at', '<', \Carbon\Carbon::now())
//                    ->orWhere('finish_at', '>=', \Carbon\Carbon::Now());
//            })
            ->orderBy('finish_at')
            ->get();

//        $notPaidAndFinished = Sale::whereBranchId(\Auth::guard('staff')->user()->branch_id)
//                                ->notPaid()
//                                ->where('finish_at', '<', \Carbon\Carbon::now())
//                                ->orderBy('finish_at')
//                                ->get();
//        $notPaidAndNotFinish =  Sale::whereBranchId(\Auth::guard('staff')->user()->branch_id)
//                                    ->notPaid();
//                                    ->where('finish_at', '>=', \Carbon\Carbon::now())
//                                    ->orderBy('finish_at')
//                                    ->get();
//
//        $sales = $notPaidAndNotFinish->merge($notPaidAndFinished);

        /*$sales = Sale::whereBranchId(\Auth::guard('staff')->user()->branch_id)
                    ->notPaid()
                    ->orWhere('finish_at', '>=', \Carbon\Carbon::now())
                    ->orderBy('finish_at')
                    ->get();*/

        return view('staff.sale.index', compact('sales'));
    }

    public function store(Request $request)
    {
        $rules = [
            'table_no' => 'required|array',
            'table_no.*' => 'required|exists:branch_tables,id,branch_id,'.\Auth::guard('staff')->user()->branch_id,
            'pax' => 'required|array',
            'pax.*' => 'nullable|integer|max:1000',
        ];
        $this->validate($request, $rules);

        $totalPax = 0;
        foreach ($request->get('pax') as $diffPaxType) {
            $totalPax += $diffPaxType;
        }

        # Total pax must be at least one to create a sale table
        if ($totalPax <= 0) {
            return makeResponse(false, trans('common.at_least_one_pax'));
        }

        try {
            \DB::beginTransaction();

            $sale = new Sale();
            $sale->branch_id = \Auth::guard('staff')->user()->branch_id;
            $sale->staff_id = \Auth::guard('staff')->id();
            $sale->total_pax = $totalPax;

            if ($this->_G['setting']['table_duration']) {
                $sale->table_duration = ($this->_G['setting']['table_duration'] * 60); # Convert to sec
            }

            if ($this->_G['setting']['tax_percentage'] > 0) {
                $sale->tax_name = $this->_G['setting']['tax_name'];
                $sale->tax_percentage = $this->_G['setting']['tax_percentage'];
            }

            $sale->save();

            # Create or update pax
            $createOrUpdatePax = $this->createOrUpdatePax($sale, $request->get('pax'));
            if (!$createOrUpdatePax['status']) {
                return makeResponse(false, $createOrUpdatePax['error']);
            }

            foreach ($request->get('table_no') as $tableNo) {
                $branchTable = BranchTable::whereId($tableNo)->whereBranchId(\Auth::guard('staff')->user()->branch_id)->first();

                # Table must finish time
                /*if (Sale::whereHas('sale_tables', function ($query) use ($branchTable) {
                    $query->where('branch_table_id', $branchTable->id);
                })->where('finish_at', '>=', \Carbon\Carbon::now())->exists()) {
                    return makeResponse(false, trans('common.table_still_serving', ['table' => $branchTable->table_no]));
                }*/

                # Table must be paid
                if (Sale::whereHas('sale_tables', function ($query) use ($branchTable) {
                    $query->where('branch_table_id', $branchTable->id);
                })->where('is_paid', false)->where('finish_at', '>=', \Carbon\Carbon::now())->exists()) {
                    return makeResponse(false, trans('common.table_not_yet_pay', ['table' => $branchTable->table_no]));
                }

                # Create sale table
                $saleTable = new SaleTable();
                $saleTable->sale_id = $sale->id;
                $saleTable->branch_table_id = $branchTable->id;
                $saleTable->table_no = $branchTable->table_no;
                $saleTable->save();
            }

            $sale->recalculateSale();
            $sale->save();

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'), ['redirectTo' => route('staff.sale.order-statement', $sale->id) ]);
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function storeAlaCarte(Request $request)
    {
        $rules = [
            'add_stock_details' => 'required',
            'add_stock_details.*' => 'required|array',
            'add_stock_details.*.product_id' => 'required|exists:product,id,is_ala_carte,'.true,
            'add_stock_details.*.quantity' => 'required|integer|min:1',
        ];
        $this->validate($request, $rules);

        $totalPax = 0;
        try {
            \DB::beginTransaction();

            $sale = new Sale();
            $sale->branch_id = \Auth::guard('staff')->user()->branch_id;
            $sale->staff_id = \Auth::guard('staff')->id();
            $sale->total_pax = $totalPax;

            if ($this->_G['setting']['table_duration']) {
                $sale->table_duration = ($this->_G['setting']['table_duration'] * 60); # Convert to sec
            }

            if ($this->_G['setting']['tax_percentage'] > 0) {
                $sale->tax_name = $this->_G['setting']['tax_name'];
                $sale->tax_percentage = $this->_G['setting']['tax_percentage'];
            }
            $sale->ala_carte_name = $request->get('ala_carte_name');
            $sale->ala_carte_remark = $request->get('ala_carte_remark');
            $sale->is_ala_carte_sale = true;
            $sale->save();

            SaleOrder::createOrUpdateOrder($sale, $request->get('add_stock_details'), $request);

            $sale->recalculateSale();
            $sale->save();

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'), ['redirectTo' => route('staff.sale.order-statement', $sale->id) ]);
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function getPax(Request $request)
    {
        $sale = Sale::find($request->get('sale_id'));

        return $sale->sale_details()->pluck('pax', 'price_type');
    }

    public function editPax(Request $request)
    {
        $rules = [
            'pax' => 'required|array',
            'pax.*' => 'nullable|integer|max:1000',
        ];
        $this->validate($request, $rules);

        $totalPax = 0;
        foreach ($request->get('pax') as $diffPaxType) {
            $totalPax += $diffPaxType;
        }

        # Total pax must be at least one to create a sale table
        if ($totalPax <= 0) {
            return makeResponse(false, trans('common.at_least_one_pax'));
        }

        $sale = Sale::whereId($request->get('sale_id'))->whereBranchId(\Auth::guard('staff')->user()->branch_id)->first();

        if ($sale && !$sale->is_ala_carte_sale) {

//            if ($sale->finish_at < \Carbon\Carbon::now()) {
//                return makeResponse(false, trans('common.table_sale_expired'));
//            }
            if ($sale->is_paid == true) {
                return makeResponse(false, trans('common.table_sale_is_paid'));
            }

            try {
                \DB::beginTransaction();

                # Create or update pax
                $createOrUpdatePax = $this->createOrUpdatePax($sale, $request->get('pax'));
                if ($createOrUpdatePax['status']) {
                    $sale->recalculateSale();
                    $sale->save();
                } else {
                    return makeResponse(false, $createOrUpdatePax['error']);
                }

                \DB::commit();
                addSuccess(trans('common.operation_success'));
                return makeResponse(true, trans('common.operation_success'));
            } catch (\Exception $e) {
                \DB::rollback();
                return makeResponse(false, $e);
            }
        }
        return makeResponse(false, trans('common.sale_not_exists'));
    }

    public function addPax(Request $request)
    {
        $rules = [
            'pax' => 'required|array',
            'pax.*' => 'nullable|integer|max:1000',
        ];
        $this->validate($request, $rules);

        $totalPax = 0;
        foreach ($request->get('pax') as $diffPaxType) {
            $totalPax += $diffPaxType;
        }

        # Total pax must be at least one to create a sale table
        if ($totalPax <= 0) {
            return makeResponse(false, trans('common.at_least_one_pax'));
        }

        $sale = Sale::whereId($request->get('sale_id'))->whereBranchId(\Auth::guard('staff')->user()->branch_id)->first();

        if ($sale && !$sale->is_ala_carte_sale) {

//            if ($sale->finish_at < \Carbon\Carbon::now()) {
//                return makeResponse(false, trans('common.table_sale_expired'));
//            }

            if ($sale->is_paid == true) {
                return makeResponse(false, trans('common.table_sale_is_paid'));
            }

            //$totalPaxPrice = 0;
            try {
                \DB::beginTransaction();

                # Create or update pax
                $createOrUpdatePax = $this->createOrUpdatePax($sale, $request->get('pax'));
                if ($createOrUpdatePax['status']) {
                    # Recalculate sale
                    $sale->recalculateSale();
                    $sale->save();
                } else {
                    return makeResponse(false, $createOrUpdatePax['error']);
                }

                \DB::commit();
                addSuccess(trans('common.operation_success'));
                return makeResponse(true, trans('common.operation_success'));
            } catch (\Exception $e) {
                \DB::rollback();
                return makeResponse(false, $e);
            }
        }
        return makeResponse(false, trans('common.sale_not_exists'));
    }

    public function detail(Request $request)
    {
        if ($sale = Sale::whereId($request->get('sale_id'))->whereBranchId(\Auth::guard('staff')->user()->branch_id)->first()) {
            return view('staff.sale.detail', compact('sale'));
        }
        return makeResponse(false, trans('common.sale_not_exists'));
    }

    public function addAlaCarte(Request $request)
    {
        $sale = Sale::whereId($request->get('sale_id'))->whereBranchId(\Auth::guard('staff')->user()->branch_id)->first();

        if ($sale) {
            $rules = [
                'add_stock_details' => 'required',
                'add_stock_details.*' => 'required|array',
                'add_stock_details.*.product_id' => 'required|exists:product,id,is_ala_carte,'.true,
                'add_stock_details.*.quantity' => 'required|integer|min:1',
            ];

            if (!$sale->is_ala_carte_sale) {
                unset($rules['add_stock_details']);
            }
            $this->validate($request, $rules);

            if ($sale->finish_at < \Carbon\Carbon::now() && !$sale->is_ala_carte_sale) {
                return makeResponse(false, trans('common.table_sale_expired'));
            }
            if ($sale->is_paid == true) {
                return makeResponse(false, trans('common.table_sale_is_paid'));
            }

            \DB::beginTransaction();
            try {
                // Revert the previous order
                $previousSaleOrders = SaleOrder::whereSaleId($sale->id)->get();
                foreach ($previousSaleOrders as $previousSaleOrder) {
                    $stockId = (is_null($previousSaleOrder->branch_stock_id)) ? $previousSaleOrder->subtract_branch_stock_id : $previousSaleOrder->branch_stock_id;

                    $previousBranchStockTransaction = BranchStockTransaction::whereSaleId($sale->id)
                                                        ->whereStockId($stockId)
                                                        ->whereTransactionType(BranchStockTransaction::TYPE_STAFF_ORDER)
                                                        ->first();

                    # Add back to branch stock
                    $previousBranchStock = BranchStock::whereId($previousBranchStockTransaction->stock_id)->whereBranchId(\Auth::guard('staff')->user()->branch_id)->first();
                    $previousBranchStock->quantity += (is_null($previousSaleOrder->subtract_total_quantity)) ? $previousSaleOrder->quantity : $previousSaleOrder->subtract_total_quantity ;
                    $previousBranchStock->save();

                    # Delete the branch stock transaction
                    $previousBranchStockTransaction->delete();

                    # Delete the previous sale order
                    $previousSaleOrder->delete();
                }

                if ($request->input('add_stock_details')) {
                    SaleOrder::createOrUpdateOrder($sale, $request->get('add_stock_details'), $request);
                }

                $sale->recalculateSale();
                $sale->save();

                \DB::commit();

                addSuccess(trans('common.order_stock_success'));
                return makeResponse(true, trans('common.order_stock_success'));
            } catch (\Exception $exception) {
                \DB::rollback();
                return makeResponse(false, $exception->getMessage());
            }
        }
        return makeResponse(false, trans('common.sale_not_exists'));
    }

    public function paid(Request $request)
    {
        $rules = [
            'payment_method' => 'required|integer|in:' . implode(',', array_keys(Sale::getPaymentMethodLists())),
            'discount_type' => 'required|in:fixed,percentage',
            'discount_total' => 'nullable|isFund',
            'paid' => 'required|numeric'
        ];

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $sale = Sale::whereId($request->get('sale_id'))->whereBranchId(\Auth::guard('staff')->user()->branch_id)->notPaid()->first();

            if (!$sale) {
                throw new \Exception(trans('common.sale_not_exists'));
            }

            $sale->payment_method = $request->get('payment_method');
            if ($request->get('discount_type') == 'fixed') {
                $sale->discount_total = $request->get('discount_total');
            } else if ($request->get('discount_type') == 'percentage') {
                $discountTotal = round(($sale->sub_total * ($request->get('discount_total')/100)), 2);
                $sale->discount_total = $discountTotal;
                $sale->discount_percentage = $request->get('discount_total');
            }

            # Make sure the total is correct
            $sale->recalculateSale();

            // Paid price must be higher/equal than grand total
            if ($request->input('paid') < (string) $sale->grand_total) {
                return makeResponse(false, trans('validation.paid_price_higher_than_subtotal'));
            }

            if ($request->get('discount_total') > $sale->sub_total) {
                throw new \Exception(trans('common.discount_too_much'));
            }
            /*if ($request->get('discount_total') > $sale->grand_total || $sale->grand_total < 0) {
                throw new \Exception(trans('common.discount_too_much'));
            }*/

            $sale->paid = $request->get('paid');
            $sale->remark = $request->get('remark');
            $sale->is_paid = true;
            $sale->save();

            \DB::commit();

            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'), ['redirectTo' => route('staff.sale.receipt', $sale->id) ]);
        } catch (\Exception $exception) {
            \DB::rollback();
            return makeResponse(false, $exception->getMessage());
        }
    }

    public function history(Request $request)
    {
        //return makeResponse(false, trans('common.permission_denied'));
        if ($request->ajax()) {
            $dt = dt(Sale::whereStaffId(\Auth::guard('staff')->id())->whereDate('created_at', Carbon::today()->toDateString()));

            return $dt->setup(
                $dt->getDt()
                    ->editColumn('grand_total', function ($m) {
                        return 'RM '.fundFormat($m->grand_total);
                    })
                    ->editColumn('paid', function ($m) {
                        return 'RM '.fundFormat($m->paid);
                    })
                    ->editColumn('tax_total', function ($m) {
                        return 'RM ' . fundFormat($m->tax_total);
                    })
                    ->editColumn('discount_total', function ($m) {
                        return 'RM ' . fundFormat($m->discount_total);
                    })
                    ->editColumn('payment_method', function ($m) {
                        return $m->explainPaymentMethod();
                    })
                    ->addColumn('change', function ($m) {
                        return 'RM '.fundFormat($m->calculateChange());
                    })
                    ->editColumn('created_at', function ($m) {
                        return $m->created_at->format('Y-m-d');
                    })
                    ->filter(function ($model) {
                        return $model->GetFilteredResults();
                    })
                    ->addColumn('actions', function ($model) {
                        $actions = buildLinkHtml([
                            'url' => route('staff.sale.history-detail', $model->id),
                            'description' => '<i class="fa fa-eye"></i>',
                        ]);
                        return $actions;
                    })->rawColumns(['actions'])
            )->render();
        } else {
            $totalEarn = Sale::whereStaffId(\Auth::guard('staff')->id())->where('finish_at', '<', \Carbon\Carbon::now())->isPaid()->sum('grand_total');
            $totalSales = Sale::whereStaffId(\Auth::guard('staff')->id())->where('finish_at', '<', \Carbon\Carbon::now())->isPaid()->count();

            return view('staff.sale.history', compact('totalEarn', 'totalSales'));
        }
    }

    public function indexWidget(Request $request)
    {
        if ($request->ajax()) {
            $sales = Sale::whereStaffId(\Auth::guard('staff')->id())
                ->whereDate('created_at', Carbon::today()->toDateString())
                ->GetFilteredResults()
                ->get();

            $totalSales = 0;
            $grandTotal = 0;
            $totalPax = 0;
            foreach ($sales as $sale) {
                $totalSales++;
                $grandTotal += $sale->grand_total;
                $totalPax += $sale->total_pax;
            }

            return response()->json([
                'total_sales' => $totalSales,
                'grand_total' => number_format($grandTotal, 2),
                'total_pax' => $totalPax,
            ]);
        }
    }

    public function historyDetail(Request $request, Sale $sale)
    {
        if ($sale->branch_id == \Auth::guard('staff')->user()->branch_id && date('Y-m-d') == date('Y-m-d', strtotime($sale->created_at))) {
            return view('staff.sale.history-detail', compact('sale'));
        }
        return redirect()->route('staff.sale.history');
    }

    public function postHistoryDetail(Request $request, Sale $sale)
    {
        $rules = [
            'payment_method' => 'required|integer|in:' . implode(',', array_keys(Sale::getPaymentMethodLists())),
            'remark' => 'nullable|max:1000'
        ];
        $this->validate($request, $rules);

        $sale->payment_method = $request->get('payment_method');
        $sale->remark = $request->get('remark');
        $sale->save();

        addSuccess(trans('common.operation_success'));
        return makeResponse(true, trans('common.operation_success'));
    }

    public function orderStatement(Sale $sale)
    {
        return view('sale_order_statement', compact('sale'));
    }

    public function receipt(Sale $sale)
    {
        return view('sale_receipt', compact('sale'));
    }

    public function getBranchTable()
    {
        $branchTables = BranchTable::whereBranchId(\Auth::guard('staff')->user()->branch_id)
                            ->whereDoesntHave('sale_tables.sale', function ($query) {
                                $query->where('sales.finish_at', '>=', \Carbon\Carbon::now())
                                    ->where('sales.is_paid', false);
                            })->get();

        return ['branch_tables' => $branchTables];
    }

    public function changeTable(Request $request)
    {
        $rules = [
            'table_no' => 'required|array',
            'table_no.*' => 'required|exists:branch_tables,id,branch_id,'.\Auth::guard('staff')->user()->branch_id,
        ];
        $this->validate($request, $rules);

        $sale = Sale::whereId($request->get('sale_id'))->whereBranchId(\Auth::guard('staff')->user()->branch_id)->first();

        if ($sale && !$sale->is_ala_carte_sale) {
//            if ($sale->finish_at < \Carbon\Carbon::now()) {
////                return makeResponse(false, trans('common.table_sale_expired'));
////            }

            if ($sale->is_paid == true) {
                return makeResponse(false, trans('common.table_sale_is_paid'));
            }

            try {
                \DB::beginTransaction();

                $sale->sale_tables()->delete();
                foreach ($request->get('table_no') as $tableNo) {
                    $branchTable = BranchTable::find($tableNo);

                    # Table must be paid, except own one
                    if (Sale::whereHas('sale_tables', function ($query) use ($tableNo) {
                        $query->where('branch_table_id', $tableNo);
                    })->where('is_paid', false)->where('id', '!=', $sale->id)->where('finish_at', '>=', \Carbon\Carbon::now())->exists()) {
                        return makeResponse(false, trans('common.table_still_serving', ['table' => $branchTable->table_no]));
                    }

                    # Create sale table
                    $saleTable = new SaleTable();
                    $saleTable->sale_id = $sale->id;
                    $saleTable->branch_table_id = $branchTable->id;
                    $saleTable->table_no = $branchTable->table_no;
                    $saleTable->save();
                }

                \DB::commit();
                addSuccess(trans('common.operation_success'));
                return makeResponse(true, trans('common.operation_success'));
            } catch (\Exception $e) {
                \DB::rollback();
                return makeResponse(false, $e);
            }
        }
        return makeResponse(false, trans('common.sale_not_exists'));
    }

    public function getChangeTable(Request $request)
    {
        $sale = Sale::where('branch_id', '=', \Auth::guard('staff')->user()->branch_id)->find($request->get('sale_id'));

        $allBranchTables = [];
        foreach ($sale->sale_tables as $sale_table) {
            $allBranchTables[$sale_table->branch_table_id] = $sale_table->table_no;
        }

        $branchTables = BranchTable::whereBranchId(\Auth::guard('staff')->user()->branch_id)
            ->whereDoesntHave('sale_tables.sale', function ($query) {
                $query->where('sales.is_paid', false)
                    ->where('sales.finish_at', '>=', \Carbon\Carbon::now());
            })->get();

        foreach ($branchTables as $branchTable) {
            $allBranchTables[$branchTable->id] = $branchTable->table_no;
        }

        return [
            'all_branche_tables' => $allBranchTables,
            'selected_tables' => $sale->sale_tables()->pluck('branch_table_id')
        ];
    }

    public function getAlaCarteStock()
    {
        /*$alaCarteStocks = BranchStock::with(['product'])->whereHas('product', function ($query) {
            $query->where('is_ala_carte', '=', true);
        })->whereBranchId(\Auth::guard('staff')->user()->branch_id)
            ->where('quantity', '>', 0)
            ->select('id', 'product_name_en', 'product_id')->get();*/

        $alaCarteStocks = Product::where('is_ala_carte', '=', true)
                            ->where(function($q) {
                                $q->where(function ($q) { # Single
                                    return $q->whereNull('subtract_product_id')
                                        ->whereNull('subtract_quantity')
                                        ->whereHas('branch_stocks', function ($iq) {
                                            return $iq->where('quantity', '>', 0)
                                                ->whereBranchId(\Auth::guard('staff')->user()->branch_id);
                                        });
                                })->orWhere(function ($q) { # Bucket
                                    $q->whereNotNull('subtract_product_id')
                                        ->whereNotNull('subtract_quantity')
                                        ->whereHas('subtract_product', function ($iq) {
                                            return $iq->whereHas('branch_stocks', function ($iiq) {
                                                return $iiq->where('quantity', '>', 0)
                                                    ->whereBranchId(\Auth::guard('staff')->user()->branch_id);
                                            });
                                        });
                                });
                            })
                            ->select('id', 'product_name_en')
                            ->get();

        return ['stocks' => $alaCarteStocks];
    }

    public function getSaleOrder(Request $request)
    {
        $saleOrders = SaleOrder::whereSaleId($request->get('sale_id'))
                        ->select('id', 'product_id', 'product_name', 'quantity')
                        ->get();

        return ['saleOrders' => $saleOrders];
    }

    /**
     * Create or update pax base on different type of price.
     *
     * @param $sale
     * @param $diffTypePax
     * @return array
     */
    private function createOrUpdatePax($sale, $diffTypePax)
    {
        foreach ($diffTypePax as $priceType => $typePax) {
            # Determine is adult type, calculate free pax promotion (Only for adult type)
            /*$totalFreePax = 0;
            $totalPayPax = (!is_null($typePax)) ? $typePax : 0;
            if ($priceType == Price::ADULT_TYPE) {
                $totalTypePax = $totalPayPax;
                if ($oriSaleDetail = $sale->sale_details()->wherePaxType(config('sale.pax_type.pay'))->wherePriceType(Price::ADULT_TYPE)->first()) {
                    $totalTypePax += $oriSaleDetail->pax;
                }
                if ($oriSaleDetail = $sale->sale_details()->wherePaxType(config('sale.pax_type.free'))->wherePriceType(Price::ADULT_TYPE)->first()) {
                    $totalTypePax += $oriSaleDetail->pax;
                }

                # Determine got promotion for free pax count
                if ($promotion = Promotion::whereBranchId(\Auth::guard('staff')->user()->branch_id)->whereNotNull('free_pax_count')->first()) {
                    $totalFreePax = Sale::calculateFreePax($totalTypePax, $promotion->free_pax_count);
                    $totalPayPax = (int) $totalTypePax - $totalFreePax;
                } else {
                    $totalPayPax = $totalTypePax;
                }
            } else {
                // Just calculate total pax
                if ($oriSaleDetail = $sale->sale_details()->wherePaxType(config('sale.pax_type.pay'))->wherePriceType($priceType)->first()) {
                    $totalPayPax += $oriSaleDetail->pax;
                }
            }*/

            $totalPax = (!is_null($typePax)) ? $typePax : 0;
            # Create or update to sale detail table (Pay pax)
            if ($price = Price::whereType($priceType)->first()) {
                if ($totalPax > 0) {
                    if ($saleDetail = $sale->sale_details()->wherePaxType(config('sale.pax_type.pay'))->wherePriceType($price->type)->first()) {
                        $saleDetail->price_type_name = $price->name_en;
                        $saleDetail->pax = $totalPax;
                        $saleDetail->per_pax_price = $price->per_pax_price;
                        $saleDetail->total_pax_price = Sale::calculateTotalPaxPrice($totalPax, $price->per_pax_price);
                        $saleDetail->save();
                    } else {
                        $saleDetail = new SaleDetail();
                        $saleDetail->sale_id = $sale->id;
                        $saleDetail->pax_type = config('sale.pax_type.pay');
                        $saleDetail->price_type = $price->type;
                        $saleDetail->price_type_name = $price->name_en;
                        $saleDetail->pax = $totalPax;
                        $saleDetail->per_pax_price = $price->per_pax_price;
                        $saleDetail->total_pax_price = Sale::calculateTotalPaxPrice($totalPax, $price->per_pax_price);
                        $saleDetail->save();
                    }
                } else {
                    if ($saleDetail = $sale->sale_details()->wherePaxType(config('sale.pax_type.pay'))->wherePriceType($price->type)->first()) {
                        $saleDetail->price_type_name = $price->name_en;
                        $saleDetail->pax = $totalPax;
                        $saleDetail->per_pax_price = $price->per_pax_price;
                        $saleDetail->total_pax_price = Sale::calculateTotalPaxPrice($totalPax, $price->per_pax_price);
                        $saleDetail->save();
                    }
                }

                # Create or update for free pax promotion
                /*if ($totalFreePax > 0) {
                    # Create or update if there is any free pax
                    if ($saleDetail = $sale->sale_details()->wherePaxType(config('sale.pax_type.free'))->wherePriceType($price->type)->first()) {
                        $saleDetail->price_type_name = $price->name_en;
                        $saleDetail->pax = $totalFreePax;
                        $saleDetail->save();
                    } else {
                        $saleDetail = new SaleDetail();
                        $saleDetail->sale_id = $sale->id;
                        $saleDetail->pax_type = config('sale.pax_type.free');
                        $saleDetail->price_type = $price->type;
                        $saleDetail->price_type_name = $price->name_en;
                        $saleDetail->pax = $totalFreePax;
                        $saleDetail->save();
                    }
                } else {
                    # Only update if there is any existing free pax data
                    if ($saleDetail = $sale->sale_details()->wherePaxType(config('sale.pax_type.free'))->wherePriceType($price->type)->first()) {
                        $saleDetail->price_type_name = $price->name_en;
                        $saleDetail->pax = $totalFreePax;
                        $saleDetail->save();
                    }
                }*/
            } else {
                return ['status' => 0, 'error' => trans('common.invalid_price_type', ['price_type' => $priceType])];
            }
        }
        return ['status' => 1];
    }

    public function cancelSale(Request $request) {
        try {
            \DB::beginTransaction();

            $sid = $request->get('sale_id');
            $model = Sale::where('branch_id', '=', \Auth::guard('staff')->user()->branch_id)->find($sid);

            if ($model->is_paid) {
                throw new \Exception(trans('common.permission_denied'));
            }

//            if ($model->created_at->diffInMinutes() >= 3) {
//                throw new \Exception(trans('common.the_order_over_x_minutes_cannot_cancel_please_cancel_at_branch', ['minute' => 3]));
//            }

            $model->delete();

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function joinBill()
    {
        $saleDatas = Sale::with(['sale_tables'])
            ->where('branch_id', '=', \Auth::guard('staff')->user()->branch_id)
            ->notPaid()
            ->notJoinSale()
            ->orderBy('finish_at')
            ->whereIsAlaCarteSale(false)
            ->select('id', 'sale_ref_id')
            ->get();

        $sales = [];
        foreach ($saleDatas as $saleData) {
            $sales[$saleData->id] = $saleData->sale_ref_id." - ".$saleData->all_table;
        }

        return view('staff.sale.join_bill', compact('sales'));
    }

    public function joinBillPost(Request $request)
    {
        $rules = [
            'sale_ids' => 'required|array|min:2|max:2',
            'sale_ids.*' => 'required|exists:sales,id,branch_id,'.\Auth::guard('staff')->user()->branch_id.',is_paid,'.false.',is_join_sale,'.false.',is_ala_carte_sale,'.false,
        ];
        $this->validate($request, $rules);

        try {
            $totalPax = 0;

            \DB::beginTransaction();
            # Re-create a new sale with the chosen sale
            $joinSale = new Sale();
            $joinSale->branch_id = \Auth::guard('staff')->user()->branch_id;
            $joinSale->staff_id = \Auth::guard('staff')->id();
            $joinSale->total_pax = $totalPax;
            if ($this->_G['setting']['tax_percentage'] > 0) {
                $joinSale->tax_name = $this->_G['setting']['tax_name'];
                $joinSale->tax_percentage = $this->_G['setting']['tax_percentage'];
            }
            $joinSale->is_join_sale = true;
            $joinSale->join_sale = implode(',', $request->get('sale_ids'));
            $joinSale->save();

            # Get the earliest start, finish and duration
            $sale = Sale::whereIn('id', $request->get('sale_ids'))->orderBy('finish_at', 'asc')->first();
            $joinSale->table_duration = $sale->table_duration;
            $joinSale->start_at = $sale->start_at;
            $joinSale->finish_at = $sale->finish_at;
            $joinSale->save();

            # Shift chosen bills to join bill
            foreach ($request->get('sale_ids') as $sale_id) {
                $sale = Sale::whereId($sale_id)->whereBranchId(\Auth::guard('staff')->user()->branch_id)->notPaid()->first();

                # Create join sale details
                foreach ($sale->sale_details as $sale_detail) {
                    $joinSaleDetail = new SaleDetail();
                    $joinSaleDetail->sale_id = $joinSale->id;
                    $joinSaleDetail->pax_type = config('sale.pax_type.pay');
                    $joinSaleDetail->price_type = $sale_detail->price_type;
                    $joinSaleDetail->price_type_name = $sale_detail->price_type_name;
                    $joinSaleDetail->pax = $sale_detail->pax;
                    $joinSaleDetail->per_pax_price = $sale_detail->per_pax_price;
                    $joinSaleDetail->total_pax_price = $sale_detail->total_pax_price;
                    $joinSaleDetail->save();

                    $totalPax += $sale_detail->pax;
                }

                # Create sale order
                foreach ($sale->sale_orders as $sale_order) {
                    $joinSaleOrder = new SaleOrder();
                    $joinSaleOrder->sale_id = $joinSale->id;
                    $joinSaleOrder->branch_stock_id = $sale_order->branch_stock_id;
                    $joinSaleOrder->product_name = $sale_order->product_name;
                    $joinSaleOrder->quantity = $sale_order->quantity;
                    $joinSaleOrder->ala_carte_price = $sale_order->ala_carte_price;
                    $joinSaleOrder->total_price = $sale_order->total_price;
                    $joinSaleOrder->save();
                }

                # Create sale table
                foreach ($sale->sale_tables as $sale_table) {
                    $saleTable = new SaleTable();
                    $saleTable->sale_id = $joinSale->id;
                    $saleTable->branch_table_id = $sale_table->branch_table_id;
                    $saleTable->table_no = $sale_table->table_no;
                    $saleTable->save();
                }
                # Soft delete the chosen sale
                $sale->delete();
            }
            $joinSale->total_pax = $totalPax;
            $joinSale->save();

            # Re-calculate the new join sale
            $joinSale->recalculateSale();
            $joinSale->save();

            \DB::commit();
            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'), ['sale_id' => $joinSale->id]);
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }
}
