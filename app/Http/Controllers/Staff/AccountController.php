<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\UserBaseController;
use App\Models\Sale;
use App\Models\Staff;
use App\Models\StaffAccount;
use Illuminate\Http\Request;

class AccountController extends UserBaseController
{
    public function submit() {
        return view('staff.account.submit');
    }

    public function submitPost(Request $request) {
        $this->validate($request, [
            'type' => 'required|in:' . implode(',', array_keys(StaffAccount::getTypeLists())),
            'amount' => 'required|isFund',
            'remark' => 'nullable',
        ]);

        try {
            \DB::beginTransaction();
            $me = \Auth::guard('staff')->user();

            if (!password_verify($request->get('current_password'), $me->password)) {
                throw new \Exception(trans('common.incorrect_current_password'));
            }

            $amount = $request->get('amount');

            $model = new StaffAccount();
            $model->branch_id = $me->branch_id;
            $model->staff_id = $me->id;
            $model->type = $request->get('type');
            if ($model->type == 2) {
                $model->amount = 0 - $amount;
            } else {
                $model->amount = $amount;
            }
            $model->remark = $request->get('remark');
            $model->save();

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function summary() {
        return view('staff.account.summary');
    }

    public function summaryPost(Request $request) {
        $this->validate($request, [
            'start_at' => 'required|date',
            'end_at' => 'required|date',
            'cash_collected' => 'required|isFund',
        ]);

        try {
            $me = \Auth::guard('staff')->user();

            if (!password_verify($request->get('current_password'), $me->password)) {
                throw new \Exception(trans('common.incorrect_current_password'));
            }

            $started = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request->get('start_at') . ':00');
            $ended = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request->get('end_at') . ':00');

            # For all payment method
            $sales = Sale::where('staff_id', '=', $me->id)
                ->where('created_at', '>=', $started)
                ->where('created_at', '<=', $ended);

            # For credit card
            $sales2 = Sale::where('staff_id', '=', $me->id)
                ->where('created_at', '>=', $started)
                ->where('created_at', '<=', $ended);

            # For cash
            $sales3 = Sale::where('staff_id', '=', $me->id)
                ->where('created_at', '>=', $started)
                ->where('created_at', '<=', $ended);

            $accounts = StaffAccount::where('staff_id', '=', \Auth::guard('staff')->user()->id)
                ->where('created_at', '>=', $started)
                ->where('created_at', '<=', $ended)
                ->orderBy('id', 'ASC')
                ->get();

            $balance = $sales->sum('grand_total') - $accounts->sum('amount');

            $staff = \Auth::guard('staff')->user();

            # (cash in - cashout + cash sales)
            $expectedDrawer = ($accounts->sum('amount') + $sales3->where('payment_method', '=', 1)->sum('grand_total'));

            return view('staff_account_receipt', [
                'started' => $started,
                'ended' => $ended,
                'sales' => $sales,
                'sales2' => $sales2,
                'sales3' => $sales3,
                'accounts' => $accounts,
                'balance' => $balance,
                'cash_collected' => $request->get('cash_collected', 0),
                'staff' => $staff,
                'expected_drawer' => $expectedDrawer
            ]);
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }

    public function index(Request $request) {
        return view('staff.account.index');
    }

    public function indexDt(Request $request) {
        $dt = dt(StaffAccount::with(['staff', 'branch'])->where('staff_id', '=', \Auth::guard('staff')->user()->id)->GetRecords());

        return $dt->setup(
            $dt->getDt()
                ->editColumn('staff_id', function ($m) {
                    return $m->staff ? $m->staff->username : '-';
                })
                ->editColumn('branch_id', function ($m) {
                    return $m->branch ? $m->branch->username : '-';
                })
                ->editColumn('type', function ($m) {
                    return $m->explainType();
                })
                ->filter(function ($model) {
                    return $model->GetFilteredResults();
                })
                ->addColumn('actions', function ($m) {
                    $actions = buildRemoteLinkHtml([
                        'url' => route('staff.account.edit', ['id' => $m->id]),
                        'description' => '<i class="fa fa-pencil"></i>',
                    ]);

                   /* $actions .= buildLinkHtml([
                        'url' => route('staff.account.receipt', ['id' => $m->id]),
                        'description' => '<i class="fa fa-file-o"></i>',
                        'target' => '_blank'
                    ]);*/

                    return $actions;

                })
        )->render();
    }

    public function edit(StaffAccount $staffAccount)
    {
        return view('staff.account.edit', compact('staffAccount'));
    }

    public function update(Request $request, StaffAccount $staffAccount)
    {
        $this->validate($request, [
            'type' => 'required|in:' . implode(',', array_keys(StaffAccount::getTypeLists())),
            'amount' => 'required|isFund',
            'remark' => 'nullable',
        ]);

        try {
            \DB::beginTransaction();
            $me = \Auth::guard('staff')->user();

            if (!password_verify($request->get('current_password'), $me->password)) {
                throw new \Exception(trans('common.incorrect_current_password'));
            }

            $amount = $request->get('amount');

            $model = $staffAccount;
            $model->type = $request->get('type');
            if ($model->type == 2) {
                $model->amount = 0 - $amount;
            } else {
                $model->amount = $amount;
            }
            $model->remark = $request->get('remark');
            $model->save();

            \DB::commit();
            return makeResponse(true, trans('common.operation_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(false, $e);
        }
    }
}
