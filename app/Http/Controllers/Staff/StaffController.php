<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\UserBaseController;
use App\Models\Staff;
use Illuminate\Http\Request;

class StaffController extends UserBaseController
{
    public function profile()
    {
        $profile = \Auth::guard('staff')->user();

        return view('staff.staff.profile', compact('profile'));
    }

    public function updateProfile(Request $request)
    {
        if ($request->ajax()) {
            $profile = \Auth::guard('staff')->user();
            if (!$profile) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $rules = Staff::$rules;
            unset($rules['branch_id']);
            $rules = array_merge($rules, [
                'username' => 'required|min:2|alpha_dash|unique:staff,username,'.$profile->id,
                'password' => 'nullable|min:6|confirmed'
            ]);
            $this->validate($request, $rules);

            $name_en_exists = Staff::where('name', '=', $request->get('name'))->where('id', '!=', $profile->id)->first();
            if ($name_en_exists) {
                return makeResponse(false, trans('common.staff_name_exists', ['name' => $request->get('name')]));
            }

            $profile->fill($request->all());
            $profile->username = $request->input('username');
            if ($request->input('password')) {
                $profile->password = bcrypt($request->input('password'));
            }
            $profile->save();

            addSuccess(trans('common.operation_success'));
            return makeResponse(true, trans('common.operation_success'));
        }
    }
}
