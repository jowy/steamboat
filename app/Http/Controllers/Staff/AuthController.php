<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\UserBaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class AuthController extends UserBaseController
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/staff';

    public function login()
    {
        return view('staff.auth.login');
    }

    public function authenticate(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);

        $login = \Auth::guard('staff')->attempt([
            'username' => $request->input('username'),
            'password' => $request->input('password')
        ], $request->has('remember'));

        if (!$login) {
            return $this->sendFailedLoginResponse($request);
        }

        return makeResponse(true, trans('common.login_success'));
    }

    public function logout()
    {
        \Auth::guard('staff')->logout();

        return redirect()->route('staff.login');
    }

    /**
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return makeResponse(false, trans('common.authentication_failed'));
    }
}
