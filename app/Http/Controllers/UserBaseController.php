<?php

namespace App\Http\Controllers;

class UserBaseController extends Controller
{
    protected $_G = null;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            app()->bind('global_variable', function ($app) {
                return \App\GlobalVariable::init(request());
            });

            $this->_G = app('global_variable');
            view()->share('_G', $this->_G);

            return $next($request);
        });
    }
}
