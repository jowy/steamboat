<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\UserBaseController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Bank;
use App\Constants;
use App\Http\Requests\User\FirstLoginFormRequest;

class EtcController extends UserBaseController {

    public function setLang(Request $request) {
        $lang = $request->get('lang');

        if (\Auth::guard('user')->check()) {
            $me = \Auth::guard('user')->user();

            if ($me) {
                if ($lang == 'cn') {
                    $me->lang = 'cn';
                } else {
                    $me->lang = 'en';
                }

                $me->save();
            }
        }

        session()->put('user_lang', $lang);
        app()->setLocale($lang);

        $redirect = $request->get('redirect');

        if (isset($redirect) && $redirect != '' && $redirect != null) {
            return redirect()->to($redirect);
        } else {
            return redirect()->to('/');
        }
    }
}