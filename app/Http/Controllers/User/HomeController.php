<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\UserBaseController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Slider;
use App\Models\Announcement;
use App\Models\AnnouncementStatus;

class HomeController extends UserBaseController {
    /*public function index(Request $request) {
        $me = $this->_G['user'];

		$sliders = null;
        if ($this->_G['setting']['user_homepage_slider'] == 1) {
            $sliders = Slider::GetAvailableSliders()->get();
        }

        $announcement = Announcement::orderBy('created_at', 'DESC')->first();

        $show_popup = false;

        if ($me != null) {
            if ($announcement && !$request->hasCookie('announcement') || $request->cookie('announcement') != $announcement->id) {
                $show_popup = true;
                \Cookie::queue('announcement', $announcement->id, 60 * 24);
                if ($announcement && !AnnouncementStatus::where('user_id', '=', $me->id)->where('announcement_id', '=', $announcement->id)->count()) {
                    $as = new AnnouncementStatus();
                    $as->user_id = $me->id;
                    $as->announcement_id = $announcement->id;
                    $as->save();
                }
            }
        }

        return view('user.home', [
			'sliders' => $sliders,
            'announcement' => $announcement,
            'show_popup' => $show_popup,
		]);
    }*/
    public function index()
    {
        return view('home');
    }
}
