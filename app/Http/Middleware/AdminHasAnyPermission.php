<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminHasAnyPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if (Auth::guard('admin')->check()) {
            $lists = explode('|', $permission);
            $has_permission = false;
            foreach ($lists as $key => $var) {
                if (Auth::guard('admin')->user()->hasPermission($var)) {
                    $has_permission = true;
                }
            }
            if ($has_permission === false) {
                if ($request->ajax() || $request->wantsJson()) {
                    return makeResponse(401, trans('common.permission_denied'));
                } else {
                    addError(trans('common.permission_denied'));
                    return redirect()->route('admin.home');
                }
            }
        } else {
            if ($request->ajax() || $request->wantsJson()) {
                return makeResponse(401, trans('common.permission_denied'));
            } else {
                return redirect()->guest(route('admin.login'));
            }
        }

        return $next($request);
    }
}
