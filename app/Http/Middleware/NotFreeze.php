<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class NotFreeze
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'user')
    {
        if (Auth::guard($guard)->check()) {
            if (Auth::guard($guard)->user()->freeze == 1) {
                if ($request->ajax() || $request->wantsJson()) {
                    return makeResponse(401, trans('common.account_banned'));
                } else {
                    Auth::guard($guard)->logout();
                    addError(trans('common.account_banned'));
                    return redirect()->route('user.login');
                }
            }
        }

        return $next($request);
    }
}
