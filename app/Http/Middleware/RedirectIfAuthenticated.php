<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if ($guard == 'admin') {
                return redirect()->route('admin.home');
            } else if ($guard == 'branch') {
                return redirect()->route('branch.dashboard');
            } else if ($guard == 'staff'){
                return redirect()->route('staff.sale.index');
            } else {
                return redirect()->route('user.home');
            }
        }

        return $next($request);
    }
}
