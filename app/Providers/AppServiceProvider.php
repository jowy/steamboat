<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Validator::extend('isUsername', function($attribute, $value, $parameters, $validator) {
            return preg_match('/^[a-zA-Z0-9_]{6,}$/', $value);
        });

        Validator::extend('isPassword', function($attribute, $value, $parameters, $validator) {
            return strlen($value) >= 6 ? true : false;
        });

        Validator::extend('isNumber', function($attribute, $value, $parameters, $validator) {
            return $value != '' && $value >= 0 && ctype_digit((string)$value) ? true : false;
        });

        Validator::extend('isFund', function($attribute, $value, $parameters, $validator) {
            return $value == 0 || preg_match_all('/[\d](.[\d]{1,' . \App\Constants::$decimal_point . '})?/im' ,$value) ? true : false;
        });

        Validator::extend('isPercentage', function($attribute, $value, $parameters) {
            if (is_numeric($value) && $value <= 100) {
                return true;
            } else {
                return false;
            }
        });

        Validator::extend('isPermissionTag', function($attribute, $value, $parameters, $validator) {
            return \App\Models\PermissionGroupPermission::isValidPermission($value);
        });

        Validator::extend('isGender', function($attribute, $value, $parameters, $validator) {
            return in_array($value, array_keys(\App\Constants::getGenders(false)));
        });

        Validator::extend('isLanguage', function($attribute, $value, $parameters, $validator) {
            return array_key_exists($value, \App\Constants::getLanguages());
        });

        Validator::extend('notEmail', function($attribute, $value, $parameters, $validator) {
            return !filter_var($value, FILTER_VALIDATE_EMAIL);
        });

        Validator::extend('isYesNo', function($attribute, $value, $parameters, $validator) {
            return array_key_exists($value, \App\Constants::getYesNoForSelect());
        });

        Validator::extend('isCreditManageType', function($attribute, $value, $parameters, $validator) {
            return array_key_exists($value, \App\Models\CreditManage::getActionTypeLists());
        });

        Validator::extend('isUserBonusType', function($attribute, $value, $parameters) {
            return array_key_exists($value, \App\Models\UserBonus::getBonusTypeLists());
        });

        Validator::extend('isUserTransactionType', function($attribute, $value, $parameters) {
            return array_key_exists($value, \App\Models\UserTransaction::getTransactionTypeLists());
        });

        Validator::extend('isUserCreditType', function($attribute, $value, $parameters) {
            return array_key_exists($value, \App\Models\User::getCreditTypes());
        });
		
		Validator::extend('isReceipt', function($attribute, $value, $parameters) {
            if ($value instanceof UploadedFile && ! $value->isValid()) {
                return false;
            }

            $extensions = ['jpeg','bmp','png','jpg','pdf','docx','xls','xlsx'];

            if ($parameters != null) {
                if (is_string($parameters)) {
                    $extensions = array_merge($extensions, explode(',', $parameters));
                } else if (is_array($parameters)) {
                    $extensions = array_merge($extensions, $parameters);
                }
            }

            return $value->getPath() !== '' && in_array($value->guessExtension(), $extensions);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
