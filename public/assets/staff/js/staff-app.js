var data = {"total":0,"rows":[]};
var totalCost = 0;
var roundUpTotalCost = 0;

// Listing branch stocks
window.branchStockListing = function (url)
{
    $.get(url, function(result) {
        var options = {
            valueNames: ['id', 'product_name_en', 'retail_price', 'show_retail_price', 'product_code', 'type'],
            item: '<li class="col-md-3">' +
            '<div class="product-border thumbnail product-info noselect">' +
            '<span class="m--hide id branch_stock_id"></span>' +
            '<span class="m--hide retail_price"></span>' +
            '<span class="m--hide product_code"></span>' +
            '<span class="m--hide type"></span>' +
            '<div class="caption text-center">' +
            '<h6 class="product_name_en m--font-primary ellipses"></h6> <p class="m--font-info show_retail_price"></p>' +
            '</div>' +
            '</div>' +
            '</li>',
            page: 10,
            pagination: true
        };

        var values = result.data;

        var hackerList = new List('hacker-list', options, values);

        // Search product
        $('#filter_name').bind("keyup change", function() {
            hackerList.search($(this).val(), ['product_name_en', 'product_code']); // Only search in the 'name' column
        });

        // All row same height
        $("ul.list").each(function(){
            var largest = 0;

            $(this).find(".caption").each(function(){
                var findHeight = $(this).height();
                if(findHeight > largest){
                    largest = findHeight;
                }
            })
            $(this).find(".caption").css({"height":largest+"px"});
        });
    })
        .fail(function() {
            //alert( "error" );
        });
}

// Add product to cart
window.addProduct = function (id,name,unitPrice,type,quantity=0)
{
    oriTotalPrice = null;
    function add(quantity){
        for(var i=0; i<data.total; i++) {
            var row = data.rows[i];
            if (row.id == id && row.type == type){
                oriTotalPrice = row.totalPrice;
                row.quantity = quantity;
                row.totalPrice = unitPrice * quantity;
                return;
            }
        }
        data.total += 1;
        data.rows.push({
            id: id,
            name: name,
            unitPrice: unitPrice,
            quantity: quantity,
            totalPrice: unitPrice * quantity,
            type: type,
            action: '<button type="button" class="btn btn-primary btn-sm btn-add-qty">+</button> <button type="button" class="btn btn-warning btn-sm btn-deduct-qty">-</button>',
            remove: '<a href="javascript:void(0)" class="remove-product btn btn-sm btn-xs btn-danger"> <i class="fa fa-trash-o"></i> </a>'
        });
    }
    add(parseInt(quantity) + 1);
    if(typeof(oriTotalPrice) != "undefined" && oriTotalPrice !== null) {
        totalCost -= oriTotalPrice;
    }
    totalCost += unitPrice * (parseInt(quantity) + 1);
    roundUpTotalCost = Math.round( totalCost * 10 ) / 10;
    // Empty table and append again
    $('#cartcontent tbody').empty();
    var $count = 1;
    $.each(data.rows, function (key, val) {
        //$('#cartcontent tbody').append("<tr><td field='name'>"+ val.name +"</td><td field='unit_price'>"+ addCommas(fundFormat(val.unitPrice)) +"</td><td field='quantity'>"+ val.quantity +"</td><td field='total_price'>"+ val.totalPrice.toFixed(2) +"</td><td align='center'>"+ val.action +"</td><td align='center'>"+ val.remove +"</td><input type='hidden' name='branch_stock_id' value='"+ val.id +"'><input type='hidden' name='type' value='"+ val.type +"'><input type='hidden' name='cart_product["+ $count +"][id]' value='"+ val.id +"'><input type='hidden' name='cart_product["+ $count +"][type]' value='"+ val.type +"'><input type='hidden' name='cart_product["+ $count +"][quantity]' value='"+ val.quantity +"'></tr>");
        $('#cartcontent tbody').append("<tr><td field='name'>"+ val.name +"</td><td field='unit_price'>"+ addCommas(fundFormat(val.unitPrice)) +"</td><td field='quantity'><input type='text' name='cart_product["+ $count +"][quantity]' value='"+ val.quantity +"' size='5' class='cart_quantity' onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td><td field='total_price'>"+ val.totalPrice.toFixed(2) +"</td><td align='center'>"+ val.action +"</td><td align='center'>"+ val.remove +"</td><input type='hidden' name='branch_stock_id' value='"+ val.id +"'><input type='hidden' name='type' value='"+ val.type +"'><input type='hidden' name='cart_product["+ $count +"][id]' value='"+ val.id +"'><input type='hidden' name='cart_product["+ $count +"][type]' value='"+ val.type +"'></tr>");
        $count++;
    });

    $('table.cart-total-table #grand_total_price').html('RM ' + roundUpTotalCost.toFixed(2));
    $('table.cart-total-table #items_count').html(data.total);
    if (data.total > 0) {
        $('#cartcontent > tbody  > tr').each(function() {
            if ($(this).find("td[field=quantity]").find("input").val().length <= 0) {
                $('#purchase-btn').attr('disabled', 'disabled');
                return false;
            }
            $('#purchase-btn').removeAttr('disabled');
        });
    }
}

// Deduct product to cart
window.deductProduct = function (id,name,unitPrice,type)
{
    function deduct(){
        for(var i=0; i<data.total; i++){
            var row = data.rows[i];
            if (row.id == id && row.type == type){
                row.quantity -= 1;
                row.totalPrice -= unitPrice;

                if (row.quantity <= 0) {
                    data.rows.splice(i, 1);
                    data.total--;
                }
                return;
            }
        }
        data.total -= 1;
        data.rows.push({
            id: id,
            name: name,
            unitPrice: unitPrice,
            quantity: 1,
            totalPrice: unitPrice, // For first time push will be unit price first
            type: type,
            action: '<button type="button" class="btn btn-primary btn-sm btn-add-qty">+</button> <button type="button" class="btn btn-warning btn-sm btn-deduct-qty">-</button>',
            remove: '<a href="javascript:void(0)" class="remove-product btn btn-sm btn-xs btn-danger"> <i class="fa fa-trash-o"></i> </a>'
        });
    }
    deduct();
    totalCost -= unitPrice;
    roundUpTotalCost = Math.round( totalCost * 10 ) / 10;
    // Empty table and append again
    $('#cartcontent tbody').empty();
    var $count = 1;
    $.each(data.rows, function (key, val) {
        //$('#cartcontent tbody').append("<tr><td field='name'>"+ val.name +"</td><td field='unit_price'>"+ addCommas(fundFormat(val.unitPrice)) +"</td><td field='quantity'>"+ val.quantity +"</td><td field='total_price'>"+ val.totalPrice.toFixed(2) +"</td><td align='center'>"+ val.action +"</td><td align='center'>"+ val.remove +"</td><input type='hidden' name='branch_stock_id' value='"+ val.id +"'><input type='hidden' name='type' value='"+ val.type +"'><input type='hidden' name='cart_product["+ $count +"][id]' value='"+ val.id +"'><input type='hidden' name='cart_product["+ $count +"][type]' value='"+ val.type +"'><input type='hidden' name='cart_product["+ $count +"][quantity]' value='"+ val.quantity +"'></tr>");
        $('#cartcontent tbody').append("<tr><td field='name'>"+ val.name +"</td><td field='unit_price'>"+ addCommas(fundFormat(val.unitPrice)) +"</td><td field='quantity'><input type='text' name='cart_product["+ $count +"][quantity]' value='"+ val.quantity +"' size='5' class='cart_quantity' onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td><td field='total_price'>"+ val.totalPrice.toFixed(2) +"</td><td align='center'>"+ val.action +"</td><td align='center'>"+ val.remove +"</td><input type='hidden' name='branch_stock_id' value='"+ val.id +"'><input type='hidden' name='type' value='"+ val.type +"'><input type='hidden' name='cart_product["+ $count +"][id]' value='"+ val.id +"'><input type='hidden' name='cart_product["+ $count +"][type]' value='"+ val.type +"'></tr>");
        $count++;
    });

    $('table.cart-total-table #grand_total_price').html('RM ' + roundUpTotalCost.toFixed(2));
    $('table.cart-total-table #items_count').html(data.total);
    if (data.total > 0) {
        $('#cartcontent > tbody  > tr').each(function() {
            if ($(this).find("td[field=quantity]").find("input").val().length <= 0) {
                $('#purchase-btn').attr('disabled', 'disabled');
                return false;
            }
            $('#purchase-btn').removeAttr('disabled');
        });
    }
}

// Direct edit product quantity
window.editProductQuantity = function (id,name,unitPrice,type,quantity)
{
    function edit() {
        for(var i=0; i<data.total; i++){
            var row = data.rows[i];
            if (row.id == id && row.type == type) {
                oriTotalPrice = row.totalPrice;
                row.quantity = quantity;
                row.totalPrice = unitPrice * quantity;
                return;
            }
        }
        data.total += 1;
        data.rows.push({
            id: id,
            name: name,
            unitPrice: unitPrice,
            quantity: quantity,
            totalPrice: unitPrice * quantity,
            type: type,
            action: '<button type="button" class="btn btn-primary btn-sm btn-add-qty">+</button> <button type="button" class="btn btn-warning btn-sm btn-deduct-qty">-</button>',
            remove: '<a href="javascript:void(0)" class="remove-product btn btn-sm btn-xs btn-danger"> <i class="fa fa-trash-o"></i> </a>'
        });
    }
    edit();
    totalCost -= oriTotalPrice;
    totalCost += unitPrice * quantity;
    roundUpTotalCost = Math.round( totalCost * 10 ) / 10;
    // Empty table and append again
    $('#cartcontent tbody').empty();
    var $count = 1;
    $.each(data.rows, function (key, val) {
        $('#cartcontent tbody').append("<tr edit='"+(val.id == id && val.type == type ? 'true' : '') +"'><td field='name'>"+ val.name +"</td><td field='unit_price'>"+ addCommas(fundFormat(val.unitPrice)) +"</td><td field='quantity'><input type='text' name='cart_product["+ $count +"][quantity]' value='"+ val.quantity +"' size='5' class='cart_quantity' onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td><td field='total_price'>"+ val.totalPrice.toFixed(2) +"</td><td align='center'>"+ val.action +"</td><td align='center'>"+ val.remove +"</td><input type='hidden' name='branch_stock_id' value='"+ val.id +"'><input type='hidden' name='type' value='"+ val.type +"'><input type='hidden' name='cart_product["+ $count +"][id]' value='"+ val.id +"'><input type='hidden' name='cart_product["+ $count +"][type]' value='"+ val.type +"'></tr>");
        $count++;
    });

    $('#cartcontent > tbody  > tr').each(function() {
        if($(this).attr('edit')) {
            $(this).find("td[field='quantity']").find("input").focusTextToEnd();
        }
    });

    $('table.cart-total-table #grand_total_price').html('RM ' + roundUpTotalCost.toFixed(2));
    $('table.cart-total-table #items_count').html(data.total);
    if (data.total > 0) {
        $('#cartcontent > tbody  > tr').each(function() {
            if ($(this).find("td[field=quantity]").find("input").val().length <= 0) {
                $('#purchase-btn').attr('disabled', 'disabled');
                return false;
            }
            $('#purchase-btn').removeAttr('disabled');
        });
    }
}

// Remove product from cart
window.removeProduct = function (el, event)
{
    var tr = $(el).closest('tr');
    var id = tr.find('input[name=branch_stock_id]').val();
    var unitPrice = tr.find('td[field=unit_price]').text();
    var quantity = tr.find('td[field=quantity]').find("input").val();
    var type = tr.find('input[name=type]').val();

    for(var i = 0; i < data.total; i++){
        var row = data.rows[i];
        if (row.id == id && row.type == type) {
            data.rows.splice(i, 1);
            data.total--;
            data.totalPrice -= unitPrice;
            break;
        }
    }
    totalCost -=  unitPrice * quantity;
    roundUpTotalCost = Math.round( totalCost * 10 ) / 10;
    // Empty table and append again
    $('#cartcontent tbody').empty();

    var $count = 1;
    $.each(data.rows, function (key, val) {
        //$('#cartcontent tbody').append("<tr><td field='name'>"+ val.name +"</td><td field='unit_price'>"+ val.unitPrice +"</td><td field='quantity'>"+ val.quantity +"</td><td field='total_price'>"+ val.totalPrice.toFixed(2) +"</td><td  align='center'>"+ val.action +"</td><td  align='center'>"+ val.remove +"</td><input type='hidden' name='branch_stock_id' value='"+ val.id +"'><input type='hidden' name='type' value='"+ val.type +"'><input type='hidden' name='cart_product["+ $count +"][id]' value='"+ val.id +"'><input type='hidden' name='cart_product["+ $count +"][type]' value='"+ val.type +"'><input type='hidden' name='cart_product["+ $count +"][quantity]' value='"+ val.quantity +"'></tr>");
        $('#cartcontent tbody').append("<tr><td field='name'>"+ val.name +"</td><td field='unit_price'>"+ val.unitPrice +"</td><td field='quantity'><input type='text' name='cart_product["+ $count +"][quantity]' value='"+ val.quantity +"' size='5' class='cart_quantity' onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td><td field='total_price'>"+ val.totalPrice.toFixed(2) +"</td><td  align='center'>"+ val.action +"</td><td  align='center'>"+ val.remove +"</td><input type='hidden' name='branch_stock_id' value='"+ val.id +"'><input type='hidden' name='type' value='"+ val.type +"'><input type='hidden' name='cart_product["+ $count +"][id]' value='"+ val.id +"'><input type='hidden' name='cart_product["+ $count +"][type]' value='"+ val.type +"'></tr>");
        $count++;
    });

    var grandTotal = roundUpTotalCost.toFixed(2);
    if(grandTotal == '-0.00'){
        grandTotal = '0.00';
    }
    $('table.cart-total-table #grand_total_price').html('RM ' + grandTotal);
    $('table.cart-total-table #items_count').html(data.total);

    if (data.total > 0) {
        $('#cartcontent > tbody  > tr').each(function() {
            if ($(this).find("td[field=quantity]").find("input").val().length <= 0) {console.log('xcvxcvcxv');
                $('#purchase-btn').attr('disabled', 'disabled');
                return false;
            }console.log('dhfg');
            $('#purchase-btn').removeAttr('disabled');
        });
    }
}

window.isNumberKey = function (evt)
{
    if ((evt.which != 46 || evt.value.indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) {
        //event it's fine

    }
    var input = evt.value;
    if ((input.indexOf('.') != -1) && (input.substring(input.indexOf('.')).length > 2)) {
        return false;
    }
}

$.fn.focusTextToEnd = function(){
    this.focus();
    var $thisVal = this.val();
    this.val('').val($thisVal);
    return this;
}