function blockBody(text) {
    if (typeof text === 'undefined') {
        text = 'Loading...';
    }

    App.blockUI({
        overlayColor: 'rgba(0,0,0,.6)',
        centerY: true,
        boxed: true,
        message: text,
    });
}

function unblockBody() {
    App.unblockUI();
}

function buildSweetAlertMessage(text) {
    var s = [];
    if (text instanceof Array) {
        text.forEach((value, index) => {
            if (value instanceof Array) {
                value.forEach((v, i) => {
                    var d = document.createElement('div');
                    s.push(v);
                });
            } else {
                s.push(value);
            }
        });
    } else {
        s.push(text);
    }

    return s;
}

function alertSuccess(text, clear, fn) {
    var d = document.createElement('div');
    var s = buildSweetAlertMessage(text);
    d.innerHTML = s.join('<br>');

    swal({
        icon: "success",
        content: d,
        cancel: "OK"
    }).then(function () {
        if (typeof fn === 'function') {
            fn();
        }
    });
}

function alertWarning(text, clear, fn) {
    var d = document.createElement('div');
    var s = buildSweetAlertMessage(text);
    d.innerHTML = s.join('<br>');

    swal({
        icon: "warning",
        content: d,
        cancel: "OK"
    }).then(function () {
        if (typeof fn === 'function') {
            fn();
        }
    });
}

function alertError(text, clear, fn) {
    var d = document.createElement('div');
    var s = buildSweetAlertMessage(text);
    d.innerHTML = s.join('<br>');

    swal({
        icon: "error",
        content: d,
        cancel: "OK"
    }).then(function () {
        if (typeof fn === 'function') {
            fn();
        }
    });
}

function clearAlert() {
    swal.close();
}

function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
}

function randomString(length_) {
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');
    if (typeof length_ !== "number") {
        length_ = Math.floor(Math.random() * chars.length_);
    }
    var str = '';
    for (var i = 0; i < length_; i++) {
        str += chars[Math.floor(Math.random() * chars.length)];
    }
    return str;
}

function fundConvert(num) {
    return num.toString().replace(/,/g, '');
}

function fundFormat(num, decimal) {
    var r = parseFloat(fundConvert(num));

    if (typeof decimal === 'undefined') {
        decimal = 2;
    }

    return r.toFixed(decimal);
}

function inputMirror(input, target, prepend, append, type, get_select_text) {
    if (typeof type === 'undefined') {
        type = 'html';
    }

    if (typeof get_select_text === 'undefined') {
        get_select_text = false;
    }

    var value = '';

    var buildString = function() {
        if (get_select_text === false) {
            value = $(input).val();
        } else {
            value = $(input).find('option:selected').text();
        }
        if (typeof prepend !== 'undefined' && prepend != null && prepend != false) {
            value = prepend + value;
        }
        if (typeof append !== 'undefined' && append != null && append != false) {
            value = value + append;
        }

        return value;
    };

    $(document).on('change keyup blur', input, function () {
        if (type == 'html') {
            $(target).html(buildString());
        } else if (type == 'val') {
            $(target).val(buildString());
        }
    });

    if (type == 'html') {
        $(target).html(buildString());
    } else if (type == 'val') {
        $(target).val(buildString());
    }
}

function isFund(value) {
    var pattern = /^\d+(\.\d{1,2})?$/;
    return pattern.test(value);
}

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function clearFormError(form) {
	$('.has-danger', form).removeClass('has-danger');
	$('.form-control-feedback', form).remove();
}

function handleError(response, form, fn) {
    if (response.status == 422 || response.status == 500) {
		var message = new Array;
		if (response.responseJSON.message !== 'undefined') {
			message.push(response.responseJSON.message);
		}
		
		if (response.responseJSON.errors) {
			$.each(response.responseJSON.errors, function (key, value) {
                if (form) {
                    var ele = null
                    if ($('input[name="' + key + '"]', form).length) {
                        ele = $('input[name="' + key + '"]', form);
                    } else if ($('select[name="' + key + '"]', form).length) {
                        ele = $('select[name="' + key + '"]', form);
                    } else if ($('textarea[name="' + key + '"]', form).length) {
                        ele = $('textarea[name="' + key + '"]', form);
                    }

                    if (ele != null) {
                        ele.closest('.form-group').append('<div class="form-control-feedback text-danger">' + value + '</div>');
                    } else {
                        message.push(value);
                    }
                } else {
                    message.push(value);
                }
			});
		}

		if (message.length) {
		    if (typeof fn === "function") {
                alertError(message, true, function () {
                    fn();
                });
            } else {
                alertError(message);
            }
        }
	} else if (response.status == 401) {
        if (typeof fn === "function") {
            alertError(response.message || 'Please refresh and login again', true, function () {
                fn();
            });
        } else {
            alertError(response.message || 'Please refresh and login again');
        }
	} else {
        if (typeof fn === "function") {
            alertError('Something went wrong', true, function () {
                fn();
            });
        } else {
            alertError('Something went wrong');
        }
	}
}

function removeCommas(fund) {
    if (typeof fund === 'undefined' || fund == '' || fund == null) return fund;
    return fund.replace(/,/g, '');
}

function xwwwfurlenc (srcjson, parent) {
    var u = encodeURIComponent;
    var urljson = "";
    var keys = Object.keys(srcjson);

    for (var i = 0; i < keys.length; i++) {
        var k = parent ? parent + "[" + keys[i] + "]" : keys[i];

        if (typeof srcjson[keys[i]] !== "object") {
            urljson += u(k) + "=" + u(srcjson[keys[i]]);
        } else {
            urljson += xwwwfurlenc(srcjson[keys[i]], k);
        }

        if (i < (keys.length - 1)) {
            urljson += "&";
        }
    }

    return urljson;
}