var App = function() {
    var assetsPath = '/';

    var globalImgPath = 'img/';

    var globalPluginsPath = 'plugins/';

    var globalCssPath = 'css/';

    var bodyAlertContainer = function() {
        return 'body';
    };

    var handleModal = function() {
        $('#remote-modal, #remote-modal-large, #remote-modal-full').on('show.bs.modal', function (e) {
            $(this).find('.modal-content').load(e.relatedTarget.href);
        });

        $('#remote-modal, #remote-modal-large, #remote-modal-full').on("hidden.bs.modal", function (e) {
            $(e.target).removeData("bs.modal").find(".modal-content").empty();
            e.preventDefault();
        });

        $('#remote-modal, #remote-modal-large, #remote-modal-full').on('shown.bs.modal', function(e) {
            handleDatetimePicker();
            handleInput();
            handleFileManager();
            handlePreviewer();
            handleSlug();
        });

        $('#confirm-modal').on('show.bs.modal', function(e) {
            var redirect = $(e.relatedTarget).data('redirect');
            $(this).find('.modal-header > h4').html($(e.relatedTarget).data('header'));
            $(this).find('.modal-body').html($(e.relatedTarget).data('body'));

            var successFunction = $(e.relatedTarget).data('successcall');

            if (typeof successFunction === 'undefined') {
                successFunction = 'default';
            }

            if (redirect === 'yes') {
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            } else {
                //make ajax post
                $('.btn-ok').unbind('click');
                $('.btn-ok', this).on('click', function() {
                    $.ajax({
                        url: $(e.relatedTarget).data('href'),
                        method: 'post',
                        dataType: 'json',
                        beforeSend: function() {
                            blockBody();
                        },
                        success: function(response) {
                            alertSuccess(response.message, true);
                            if (successFunction != 'default') {
                                window[successFunction](response);
                            }
                            $('.modal').modal('hide');
                            unblockBody();

                            $('.modal-backdrop').remove();
                        },
                        error: function(response) {
                            handleError(response);
                            unblockBody();
                        }
                    });
                });
            }
        });
        $('#confirm-modal').on('hidden.bs.modal', function(e) {
            $(this).find('.modal-header > h4').empty();
            $(this).find('.modal-body').empty();
            $(this).find('.btn-ok').removeAttr('href');
        });
    };

    var handleDatetimePicker = function() {
		if ($.fn.datepicker) {
			$('.date-picker').datepicker({
				format: 'yyyy-mm-dd',
                autoclose: true,
			});
		}
		if ($.fn.datetimepicker) {
			$('.datetime-picker').datetimepicker();
		}
    };

    var handleInput = function() {
        if ($('.number-input').length) {
            if (typeof Cleave != 'undefined') {
                $('.number-input').each(function (index, field) {
                    new Cleave(field, {
                        numeral: true,
                        numeralThousandsGroupStyle: 'thousand',
                        numericOnly: true,
                        numeralDecimalScale: 0
                    });
                });
            } else if ($.fn.numberInput) {
                $('.number-input').numberInput();
            }
        }

        if ($('.number-input-no-separator').length) {
            if ($.fn.numberInput) {
                $('.number-input-no-separator').numberInput();
            }
        }

        if ($('.fund-input').length) {
            if (typeof Cleave != 'undefined') {
                $('.fund-input').each(function (index, field) {
                    var dc = $(this).data('decimal');
                    dc = typeof dc !== 'undefined' ? dc : 5;

                    new Cleave(field, {
                        numeral: true,
                        numeralThousandsGroupStyle: 'thousand',
                        numeralDecimalScale: dc
                    });
                });
            } else if ($.fn.fundInput) {
                $('.fund-input').fundInput();
            }
        }

        if ($.fn.percentageInput) {
            $('.percentage-input').percentageInput();
        }
    };

    var handleFileManager = function () {
        if ($.fn.imageSelector) {
            $('.image-selector').imageSelector();
        }
    };

    var handlePreviewer = function () {
        $(document).on('click', '.previewer', function () {
            if (typeof $(this).data('target') !== 'undefined') {
                if ($(this).data('target') != '') {
                    $('#img-previewer').prop('src', $(this).data('target'));
                    $('#img-previewer-modal').modal('toggle');
                }
            }
        });
    };

    var handleSlug = function () {
        if ($.fn.slugify) {
            $('.slug-input').slugify();
        }
    };

    return {
        init: function () {
            handleModal();
            handleDatetimePicker();
            handleInput();
            handleFileManager();
            handlePreviewer();
            handleSlug();
        },

        modalInit: function() {
            handleModal();
        },

        datetimeInit: function() {
            handleDatetimePicker();
        },

        getAssetsPath: function() {
            return assetsPath;
        },

        setAssetsPath: function(path) {
            assetsPath = path;
        },

        setGlobalImgPath: function(path) {
            globalImgPath = path;
        },

        getGlobalImgPath: function() {
            return assetsPath + globalImgPath;
        },

        setGlobalPluginsPath: function(path) {
            globalPluginsPath = path;
        },

        getGlobalPluginsPath: function() {
            return assetsPath + globalPluginsPath;
        },

        getGlobalCssPath: function() {
            return assetsPath + globalCssPath;
        },

        // wrApper function to scroll(focus) to an element
        scrollTo: function(el, offeset) {
            var pos = (el && el.length > 0) ? el.offset().top : 0;

            if (el) {
                pos = pos + (offeset ? offeset : -1 * el.height());
            }

            $('html,body').animate({
                scrollTop: pos
            }, 'slow');
        },

        // function to scroll to the top
        scrollTop: function() {
            App.scrollTo();
        },

        // wrApper function to  block element(indicate loading)
        blockUI: function(options) {
            options = $.extend(true, {}, options);

            if (typeof options.overlayColor === 'undefined') {
                options.overlayColor = 'none';
            }

            if (typeof options.centerY === 'undefined') {
                options.centerY = true;
            }

            if (typeof options.boxed === 'undefined') {
                options.boxed = true;
            }

            var html = '';

            if (options.animate) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '">' + '<div class="block-spinner-bar"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>' + '</div>';
            } else if (options.iconOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img src="' + this.getGlobalImgPath() + 'loading.svg" align="center"></div>';
            } else if (options.textOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><span>&nbsp;&nbsp;' + (options.message ? options.message : 'LOADING...') + '</span></div>';
            } else {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img src="' + this.getGlobalImgPath() + 'loading.svg" align="center"><span class="loading" data-text="' + options.message + '">' + options.message + '</span></div>';
            }

            if (options.target) { // element blocking
                var el = $(options.target);
                if (el.height() <= ($(window).height())) {
                    options.centerY = true;
                }
                el.block({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    centerY: options.centerY !== undefined ? options.centerY : true,
                    css: {
                        top: '10%',
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                        opacity: options.boxed ? 1 : 0,
                        cursor: 'wait'
                    }
                });
            } else { // page blocking
                $.blockUI({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    css: {
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                        opacity: options.boxed ? 1: 0,
                        cursor: 'wait'
                    }
                });
            }
        },

        // wrApper function to  un-block element(finish loading)
        unblockUI: function(target) {
            if (target) {
                $(target).unblock({
                    onUnblock: function() {
                        $(target).css('position', '');
                        $(target).css('zoom', '');
                    }
                });
            } else {
                $.unblockUI();
            }
        },

        getUniqueID: function(prefix) {
            return 'prefix_' + Math.floor(Math.random() * (new Date()).getTime());
        },

        //public function to get a paremeter by name from URL
        getURLParameter: function(paramName) {
            var searchString = window.location.search.substring(1),
                i, val, params = searchString.split("&");

            for (i = 0; i < params.length; i++) {
                val = params[i].split("=");
                if (val[0] == paramName) {
                    return unescape(val[1]);
                }
            }
            return null;
        }
    };
}();

jQuery(document).ready(function() {
    App.init();
});