function getMapSetupTemplate() {
    var template =
        '<div class="portlet light gmap-container">' +
            '<div class="portlet-title">' +
                '<div class="caption">Maps</div>' +
                '<div class="actions">' +
                    '<a href="javascript:;" class="btn btn-md btn-delete-map red"><i class="fa fa-trash"></i></a>' +
                '</div>' +
            '</div>' +
            '<div class="portlet-body form">' +
                '<div class="form-body" style="display: table; width:100%;">' +
                    '<div class="form-group google-map-setup">' +
                        '<div class="col-sm-12 col-md-7">' +
                            '<h3>Search and drag the <img src="/img/gmapmarker.png" style="width: 21px;"> to arrange your location</h3>' +
                            '<div class="searchmap-map-container" style="height: 350px;"></div>' +
                        '</div>' +
                        '<div class="col-sm-12 col-md-5">' +
                            '<h3>Map Title:</h3>' +
                            '<input type="text" name="maps[@@index@@][title]" value="" class="googlemap-title-input form-control" placeholder="Map Title" />' +
                            '<h3>Map Description</h3>' +
                            '<textarea class="form-control" name="maps[@@index@@][description]" placeholder="Description"></textarea>' +
                            '<h3>Search Address:</h3>' +
                            '<a href="javascript:;" class="clear-map">Clear Map Data</a>' +
                            '<div class="row">' +
                                '<div class="col-md-12">' +
                                    '<input type="text" name="maps[@@index@@][address]" value="" class="googlemap-address-input form-control" placeholder="Address" />' +
                                '</div>' +
                            '</div>' +
                            '<div class="row">' +
                                '<div class="col-md-6">' +
                                    '<input type="text" name="maps[@@index@@][latitude]" value="" class="googlemap-latitude-input form-control" placeholder="Latitude" readonly />' +
                                '</div>' +
                                '<div class="col-md-6">' +
                                    '<input type="text" name="maps[@@index@@][longitude]" value="" class="googlemap-longitude-input form-control" placeholder="Longitude" readonly />' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>';

    return template;
}

(function($) {
    var MakeGmapContainer = function ($el, options) {
        this._geocoder = new google.maps.Geocoder();
        this._map;
        this._marker;

        var $this = this;

        var placeSearch, autocomplete;

        function geocodePosition(pos) {
            $this._geocoder.geocode({
                latLng: pos
            }, function (responses) {
                if (responses && responses.length > 0) {
                    updateMarkerAddress(responses[0].formatted_address);
                }
            });
        }

        function updateMarkerPosition(latLng) {
            jQuery('.googlemap-latitude-input', $el).val(latLng.lat());
            jQuery('.googlemap-longitude-input', $el).val(latLng.lng());
        }

        function updateMarkerAddress(str) {
            jQuery('.googlemap-address-input', $el).val(str);
        }

        function getCurrentValue(name) {
            if (name == 'address') {
                return jQuery('.googlemap-address-input', $el).val();
            } else if (name == 'longitude') {
                return jQuery('.googlemap-longitude-input', $el).val();
            } else if (name == 'latitude') {
                return jQuery('.googlemap-latitude-input', $el).val();
            } else {
                return null;
            }
        }

        function initialize() {
            var lat = 3.1588472;
            var lng = 101.71383700000001;
            var zoom = 2;

            if (getCurrentValue('latitude') != '' && getCurrentValue('latitude') != null && getCurrentValue('longitude') != '' && getCurrentValue('longitude') != null) {
                var lat = getCurrentValue('latitude');
                var lng = getCurrentValue('longitude');
                var zoom = 16;
            }

            var latlng = new google.maps.LatLng(lat,lng);
            var mapOptions = {
                zoom: zoom,
                center: latlng
            }

            $this._map = new google.maps.Map(jQuery('.searchmap-map-container', $el)[0], mapOptions);

            $this._geocoder = new google.maps.Geocoder();

            $this._marker = new google.maps.Marker({
                map: $this._map,
                draggable: true,
                icon: '/img/gmapmarker.png',
                position: latlng,
            });

            // Add dragging event listeners.
            google.maps.event.addListener($this._marker, 'dragstart', function () {
                updateMarkerAddress('Dragging...');
            });

            google.maps.event.addListener($this._marker, 'drag', function () {
                updateMarkerPosition($this._marker.getPosition());
            });

            google.maps.event.addListener($this._marker, 'dragend', function () {
                geocodePosition($this._marker.getPosition());
            });
        }

        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            var addr = place.formatted_address;

            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();

            jQuery('.googlemap-address-input', $el).val(addr);
            jQuery('.googlemap-latitude-input', $el).val(lat);
            jQuery('.googlemap-longitude-input', $el).val(lng);

            var location = new google.maps.LatLng(lat, lng);

            $this._marker.setPosition(location);
            $this._map.setZoom(16);
            $this._map.setCenter(location);
        }

        autocomplete = new google.maps.places.Autocomplete(
            /** @type {HTMLInputElement} */(jQuery('.googlemap-address-input', $el).get(0)),
            { types: ['geocode'] });
        autocomplete.setComponentRestrictions(
            {'country': ['my', 'sg', 'th']});

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            fillInAddress();
        });

        initialize();
        google.maps.event.addDomListener(window, 'load', initialize);

        //Add listener to marker for reverse geocoding
        google.maps.event.addListener($this._marker, 'drag', function () {
            $this._geocoder.geocode({'latLng': $this._marker.getPosition()}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        jQuery('.googlemap-address-input', $el).val(results[0].formatted_address);
                        jQuery('.googlemap-latitude-input', $el).val($this._marker.getPosition().lat());
                        jQuery('.googlemap-longitude-input', $el).val($this._marker.getPosition().lng());
                    }
                }
            });
        });

        $('.clear-map', $el).on('click', function() {
            jQuery('.googlemap-address-input', $el).val('');
            jQuery('.googlemap-latitude-input', $el).val('');
            jQuery('.googlemap-longitude-input', $el).val('');

            var location = new google.maps.LatLng(3.1588472, 101.71383700000001);

            $this._marker.setPosition(location);
            $this._map.setZoom(2);
            $this._map.setCenter(location);
        });

        $('.btn-delete-map', $el).on('click', function () {
            $el.remove();
        });
    };

    $.fn.makeGmapContainer = function (methodOrOptions) {
        var method = (typeof methodOrOptions === 'string') ? methodOrOptions : undefined;

        if (method) {
            function getMakeGmapContainer() {
                var $el               = $(this);
                var gmapContainer     = $el.data('MakeGmapContainer');

                gmapContainer.push(MakeGmapContainer);
            }

            this.each(getMakeGmapContainer);

            var args    = (arguments.length > 1) ? Array.prototype.slice.call(arguments, 1) : undefined;
            var results = [];

            function applyMethod(index) {
                var makeGmapContainer = makeGmapContainer[index];

                if (!makeGmapContainer) {
                    console.warn('$.makeGmapContainer not instantiated yet');
                    console.info(this);
                    results.push(undefined);
                    return;
                }

                if (typeof makeGmapContainer[method] === 'function') {
                    var result = makeGmapContainer[method].apply(makeGmapContainer, args);
                    results.push(result);
                } else {
                    console.warn('Method \'' + method + '\' not defined in $.makeGmapContainer');
                }
            }

            this.each(applyMethod);

            return (results.length > 1) ? results : results[0];
        } else {
            var options = (typeof methodOrOptions === 'object') ? methodOrOptions : undefined;

            function init() {
                var $el          = $(this);
                var makeGmapContainer = new MakeGmapContainer($el, options);

                $el.data('makeGmapContainer', makeGmapContainer);
            }

            return this.each(init);
        }
    };
}(jQuery));