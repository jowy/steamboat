;(function ( $, document, undefined ) { //Protect the $ Alias and Adding Scope
    //Make ajaxform extensions - START
    var MakeAjaxForm = function ($el, options) {
        this._defaults =  {
            loadingText: 'Loading...',
            submitBtn: 'default',
            successFunction: 'default',
            afterSuccessFunction: 'default',
            errorFunction: 'default',
            beforeFunction: 'default',
            dataType: 'json',
            url: $el.prop('action'),
            type: $el.prop('method'),
            closeModal: true,
            clearForm: false,
            container: 'default',
            inModal: false,
            successRefresh: false,
            redirectTo: false,
        };
        this._options = $.extend(true, {}, this._defaults, options);
        this.options = function(options) {
            return (options) ?
                $.extend(true, this._options, options) :
                this._options;
        };

        if (this._options.container === 'default') {
            if (this._options.inModal === true) {
                this._options.container = $el;
            } else {
                this._options.container = $el;
            }
        }

        if (this._options.submitBtn === 'default') {

        } else {
            $(this._options.submitBtn).on('click', function() {
                $el.submit();
            });
        }

        var $this = this;

        $el.ajaxForm({
            url: $this._options.url,
            type: $this._options.type,
            dataType: $this._options.dataType,
            resetForm: $this._options.refreshForm,
            beforeSubmit: function (submitdata, $form, options) {
                clearFormError($el[0]);

                $.each(submitdata, function (i, v) {
                    if($('input[name="' + v.name + '"]').length && ($('input[name="' + v.name + '"]').hasClass('fund-input') || $('input[name="' + v.name + '"]').hasClass('number-input'))) {
                        submitdata[i].value = removeCommas(v.value);
                    }
                });

                if ($this._options.beforeFunction === 'default') {
                    var $next = true;
                } else {
                    var $next = $this._options.beforeFunction($el);
                }

                if ($($this._options.submitBtn).length) {
                    $($this._options.submitBtn).prop('disabled', 'disabled');
                }

                if ($next === true) {
                    blockBody($this._options.loadingText);

                    return true;
                } else {
                    return false;
                }
            },
            success: function (response, statusText, xhr, formElm) {
				unblockBody();
				
                if ($this._options.successFunction === 'default') {
                    alertSuccess(response.message, true, function () {
                        if ($this._options.successRefresh === true) {
                            location.reload();
                        }

                        if ($this._options.redirectTo != false) {
                            location.href = $this._options.redirectTo;
                        }

                        if ($this._options.closeModal === true) {
                            $('.modal').modal('hide');
                        }

                        if ($this._options.clearForm === true) {
                            $el[0].reset();
                        }
                    });

                    if ($this._options.afterSuccessFunction === 'default') {

                    } else {
                        $this._options.afterSuccessFunction(response, $el, $this);
                    }
                } else {
                    $this._options.successFunction(response, $el, $this);
                }

                if ($($this._options.submitBtn).length) {
                    $($this._options.submitBtn).removeAttr('disabled');
                }
            },
            error: function (response, statusText, xhr, formElm) {
				unblockBody();
				
                if ($this._options.errorFunction === 'default') {
                    handleError(response, $el[0]);
                } else {
                    opts.errorFunction(response, $el, $this);
                }

                if ($($this._options.submitBtn).length) {
                    $($this._options.submitBtn).removeAttr('disabled');
                }
            }
        });

        // $el.on('submit', function () {

            // var submitdata = new FormData(this);
            //
            // try {
            //     //Remove thousand separator for number and fund input
            //     for (var [key, value] of submitdata.entries()) {
            //         if($('input[name="' + key + '"]').length && ($('input[name="' + key + '"]').hasClass('fund-input') || $('input[name="' + key + '"]').hasClass('number-input'))) {
            //             submitdata.set(key, value.replace(/,/g, ''));
            //         }
            //     }
            // }
            // catch(err) {
            //     alert(err.message);
            // }

            // for (var pair of submitdata.entries()) {
            //     if($('input[name="' + pair[0] + '"]').length && ($('input[name="' + pair[0] + '"]').hasClass('fund-input') || $('input[name="' + pair[0] + '"]').hasClass('number-input'))) {
            //         submitdata.set(pair[0], pair[1].replace(/,/g, ''));
            //     }
            // }
    //     });
    };

    $.fn.makeAjaxForm = function (methodOrOptions) {
        var method = (typeof methodOrOptions === 'string') ? methodOrOptions : undefined;

        if (method) {

            function getMakeAjaxForm() {
                var $el          = $(this);
                var ajaxForm     = $el.data('makeAjaxForm');

                ajaxForm.push(makeAjaxForm);
            }

            this.each(getMakeAjaxForm);

            var args    = (arguments.length > 1) ? Array.prototype.slice.call(arguments, 1) : undefined;
            var results = [];

            function applyMethod(index) {
                var makeAjaxForm = makeAjaxForm[index];

                if (!makeAjaxForm) {
                    console.warn('$.makeAjaxForm not instantiated yet');
                    console.info(this);
                    results.push(undefined);
                    return;
                }

                if (typeof makeAjaxForm[method] === 'function') {
                    var result = makeAjaxForm[method].apply(makeAjaxForm, args);
                    results.push(result);
                } else {
                    console.warn('Method \'' + method + '\' not defined in $.makeAjaxForm');
                }
            }

            this.each(applyMethod);

            return (results.length > 1) ? results : results[0];
        } else {
            var options = (typeof methodOrOptions === 'object') ? methodOrOptions : undefined;

            function init() {
                var $el          = $(this);
                var makeAjaxForm = new MakeAjaxForm($el, options);

                $el.data('makeAjaxForm', makeAjaxForm);
            }

            return this.each(init);
        }
    };
    //Make ajaxform extensions - END

    //Slugify -- START

    var Slugify = function($el, options) {
        this._defaults =  {
            preSlug: null,
            postSlug: null,
            lang: 'en',
            slugFunc: function(input, opts) {
                opts['custom'] = {
                    '_': '-'
                };

                return window.location.protocol + '//' + window.location.host + '/p/' + window.getSlug(input, opts);
            }
        };

        this._options = $.extend(true, {}, this._defaults, options);

        this.options = this._options;
        options = this._options;

        var $this = this;

        var $target = $($el.data('target')),
            $source = $el;

        var slugify = function (sourceString, options) {
            // Guess language specifics from html.lang attribute
            // when options.lang is not defined
            options.lang = options.lang || $('html').prop('lang');

            // Apply preSlug function - if exists
            if (typeof options.preSlug === 'function') {
                sourceString = options.preSlug(sourceString);
            }

            sourceString = options.slugFunc(sourceString, options);

            // Apply postSlug function - if exists
            if (typeof options.postSlug === 'function') {
                sourceString = options.postSlug(sourceString);
            }

            return sourceString;
        };

        $target.on('keyup change', function() {
            if ($target.val() !== '' && $target.val() !== undefined) {
                $target.data('locked', true);
            } else {
                $target.data('locked', false);
            }
        });

        $source.on('keyup change', function() {
            if (true === $target.data('locked')) {
                return;
            }
            if ($target.is('input') || $target.is('textarea')) {
                $target.val(slugify($source.val(), options));
            } else {
                $target.text(slugify($source.val(), options));
            }
        });
    };

    $.fn.slugify = function (methodOrOptions) {
        var method = (typeof methodOrOptions === 'string') ? methodOrOptions : undefined;

        if (typeof window.getSlug !== 'function') {
            // console.warn('window.Slug not found');
            return;
        }

        if (method) {

            function getSlugify() {
                var $el          = $(this);
                var slugify     = $el.data('slugify');

                slugify.push(slugify);
            }

            this.each(getSlugify);

            var args    = (arguments.length > 1) ? Array.prototype.slice.call(arguments, 1) : undefined;
            var results = [];

            function applyMethod(index) {
                var slugify = slugify[index];

                if (!slugify) {
                    console.warn('$.slugify not instantiated yet');
                    console.info(this);
                    results.push(undefined);
                    return;
                }

                if (typeof slugify[method] === 'function') {
                    var result = slugify[method].apply(slugify, args);
                    results.push(result);
                } else {
                    console.warn('Method \'' + method + '\' not defined in $.slugify');
                }
            }

            this.each(applyMethod);

            return (results.length > 1) ? results : results[0];
        } else {
            var options = (typeof methodOrOptions === 'object') ? methodOrOptions : undefined;

            function init() {
                var $el          = $(this);
                var slugify = new Slugify($el, options);

                $el.data('slugify', slugify);
            }

            return this.each(init);
        }
    };
    //Slugify -- END

    //Percentage input extensions
    //Input must be valid percentage(100% maximum)
    $.fn.percentageInput = function() {
        this.on('cut copy paste', function(evt) {
            evt.preventDefault();
        });
        this.on('keypress', function(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            var value = this.value;

            var estimatedValue = value + String.fromCharCode(charCode);

            if (estimatedValue > 100) {
                evt.preventDefault();
            }

            if ((evt.which != 46 || value.indexOf('.') != -1) &&
                ((evt.which < 48 || evt.which > 57) &&
                (evt.which != 0 && evt.which != 8))) {
                evt.preventDefault();
            }

            if ((value.indexOf('.') != -1) &&
                (value.substring(value.indexOf('.')).length > 2) &&
                (value.which != 0 && value.which != 8) &&
                (this.selectionStart >= value.length - 2)) {
                evt.preventDefault();
            }
        });
    };

    $.fn.centerMe = function () {
        $(this).each (function() {
            $(this).css('left', $(window).width()/2 - $(this).width()/2);
        });

    };

    //Input number only
    $.fn.numberInput = function() {
        this.on('cut copy paste', function(evt) {
            evt.preventDefault();
        });
        this.on('keypress', function(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                evt.preventDefault();
            }
        });
    };

    //Input must be valid fund
    $.fn.fundInput = function() {
        this.on('cut copy paste', function(evt) {
            evt.preventDefault();
        });
        this.on('keypress', function(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            var value = this.value;

            if ((evt.which != 46 || value.indexOf('.') != -1) &&
                ((evt.which < 48 || evt.which > 57) &&
                (evt.which != 0 && evt.which != 8))) {
                evt.preventDefault();
            }

            if ((value.indexOf('.') != -1) &&
                (value.substring(value.indexOf('.')).length > 5) &&
                (value.which != 0 && value.which != 8) &&
                (this.selectionStart >= value.length - 5)) {
                evt.preventDefault();
            }
        });
    };

    var lfm = function(options, cb) {

        var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';

        window.open(route_prefix + '?type=Images', 'FileManager', 'width=900,height=600');
        window.SetUrl = cb;
    };

    //Image selector
    var ImageSelector = function ($el, options) {
        var $this = $el;
        var id = randomString(10);

        $this.find('input[type="text"]').prop('id', id + '-input').prop('readonly', 'readonly');
        $this.find('.previewer').prop('id', id + '-previewer');
        $this.find('.btn-select-image').prop('id', id + '-btn').prop('data-selector-id', id);

        if ($this.find('input[type="text"]').val() != '') {
            $this.find('.previewer').prop('data-target', $this.find('input[type="text"]').val());
        }

        $this.find('.btn-select-image').on('click', function () {
            lfm({type: 'image', prefix: 'prefix'}, function(url, path) {
                //set the value of the desired input to image url
                var target_input = $('#' + localStorage.getItem('target_input'));
                target_input.val(url);

                //set or change the preview image src
                var target_preview = $('#' + localStorage.getItem('target_preview'));
                target_preview.data('target',url);
            });
            localStorage.setItem('target_input', id + '-input');
            localStorage.setItem('target_preview', id + '-previewer');
            window.open('/laravel-filemanager?type=Images', 'FileManager', 'width=900,height=600');
            return false;
        });
    };

    $.fn.imageSelector = function () {
        var method = (typeof methodOrOptions === 'string') ? methodOrOptions : undefined;

        if (method) {

            function getImageSelector() {
                var $el          = $(this);
                var selector     = $el.data('imageSelector');

                selector.push(imageSelector);
            }

            this.each(getImageSelector);

            var args    = (arguments.length > 1) ? Array.prototype.slice.call(arguments, 1) : undefined;
            var results = [];

            function applyMethod(index) {
                var imageSelector = imageSelector[index];

                if (!imageSelector) {
                    console.warn('$.imageSelector not instantiated yet');
                    console.info(this);
                    results.push(undefined);
                    return;
                }

                if (typeof imageSelector[method] === 'function') {
                    var result = imageSelector[method].apply(imageSelector, args);
                    results.push(result);
                } else {
                    console.warn('Method \'' + method + '\' not defined in $.imageSelector');
                }
            }

            this.each(applyMethod);

            return (results.length > 1) ? results : results[0];
        } else {
            var options = (typeof methodOrOptions === 'object') ? methodOrOptions : undefined;

            function init() {
                var $el          = $(this);
                var imageSelector = new ImageSelector($el, options);

                $el.data('imageSelector', imageSelector);
            }

            return this.each(init);
        }
    };
}( jQuery ));