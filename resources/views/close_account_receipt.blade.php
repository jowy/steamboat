<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        html, body, div, span, applet, object, iframe,
        h1, h2, h3, h4, h5, h6, p, blockquote, pre,
        a, abbr, acronym, address, big, cite, code,
        del, dfn, em, img, ins, kbd, q, s, samp,
        small, strike, strong, sub, sup, tt, var,
        b, u, i, center,
        dl, dt, dd, ol, ul, li,
        fieldset, form, label, legend,
        table, caption, tbody, tfoot, thead, tr, th, td,
        article, aside, canvas, details, embed,
        figure, figcaption, footer, header, hgroup,
        menu, nav, output, ruby, section, summary,
        time, mark, audio, video {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            font-family: "Arial Black", line-height;
            vertical-align: baseline;
        }
        /* HTML5 display-role reset for older browsers */
        article, aside, details, figcaption, figure,
        footer, header, hgroup, menu, nav, section {
            display: block;
        }
        body {
            line-height: 1;
        }
        ol, ul {
            list-style: none;
        }
        blockquote, q {
            quotes: none;
        }
        blockquote:before, blockquote:after,
        q:before, q:after {
            content: '';
            content: none;
        }
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        @page { margin: 0; }
        @media print {
            @page { margin: 0; }
            body { margin: 0; }
        }
        body{
            font-size: 12px;
        }
        .block{
            padding: 3px 10px;
        }
        .left{
            text-align: left;
            display: flex;
        }
        .right{
            text-align: right;
        }
        .flex-1{
            flex-grow: 1;
        }
        .flex-2{
            flex-grow: 3;
        }
        p{
            padding: 1px 0;
            font-weight: 500;
            font-family: 'Roboto', monospace;
        }
        .food-info{

        }
        .max-width-50{
            max-width: 50px;
        }
        .max-width-60{
            max-width: 60px;
        }
        .max-width-80{
            max-width: 80px;
        }
        .horizontal-line{
            padding: 2px 0;
            border-bottom: solid 2px rgba(0,0,0);
        }
    </style>
    <script src="{{ mix('js/staff.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            /*var utcStartTime = moment.utc($('#start_time').text()).toDate();
            var utcFinishTime = moment.utc($('#finish_time').text()).toDate();

            var localStartTime = moment(utcStartTime).local().format('YYYY/MM/DD hh:mm:ss a');
            var localFinishTime = moment(utcFinishTime).local().format('YYYY/MM/DD hh:mm:ss a');*/

            var startTime = moment($('#start_time').text()).toDate();
            var finishTime = moment($('#finish_time').text()).toDate();

            var localStartTime = moment(startTime).format('YYYY/MM/DD hh:mm:ss a');
            var localFinishTime = moment(finishTime).format('YYYY/MM/DD hh:mm:ss a');

            $('#start_time').text("Start Time: " + localStartTime);
            $('#finish_time').text("Finish Time: " + localFinishTime);
        });
        function printContent(el){
            var restorepage = document.body.innerHTML;
            var printcontent = document.getElementById(el).innerHTML;
            document.body.innerHTML = printcontent;
            window.print();
            document.body.innerHTML = restorepage;
        }

        window.onload =function(){
            printContent('content');
        }
    </script>
</head>
<body>
    <div style="max-width: 300px;text-align: center;" id="content">
        <img src="{{ asset('logo_black.jpg') }}" style="max-width: 100px;padding: 10px;">
        <div class="block header">
            {{--<p style="font-size:16px">{{ $_G['setting']['site_name_' . $_G['lang']] }}</p>--}}
            @if ($acc->staff)
                <p>Staff ID: {{ $acc->staff->username }}</p>
            @endif
            <p id="start_time">Start Time: {{ $acc->start_at->toDateTimeString() }}</p>
            <p id="finish_time">Finish Time: {{ $acc->end_at->toDateTimeString() }}</p>
        </div>
        <div class="horizontal-line"></div>
        <div class="block delivery-info">
            <p class="left"><span class="flex-2">Order: {{ $acc->total_transaction }}</span></p>
            <p class="left"><span class="flex-2">Total: {{ $acc->total_sales }}</span></p>
            <p class="left"><span class="flex-2">Cash Collected: {{ $acc->cash_collected }}</span></p>
        </div>
        <div class="horizontal-line"></div>
        <div class="block footer" style="padding: 10px 0 0 0">
            <p>{{ $acc->created_at->toDateTimeString() }}</p>
        </div>
    </div>
</body>
</html>
