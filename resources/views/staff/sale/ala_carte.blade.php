<div class="m-portlet__body">
    <div class="m-form__section m-form__section--first">
        <div class="form-group m-form__group row">
            <label class="col-lg-2 col-form-label">
                {{ trans('common.product') }}
            </label>
            <div class="col-lg-6">
                {!! Form::select('select_ala_carte', $alaCarteStocks, '', ['class' => 'form-control m-input', 'id' => 'select-ala-carte']) !!}
            </div>
        </div>

        <div class="form-group m-form__group row">
            <label class="col-lg-2 col-form-label">
                {{ trans('common.quantity') }}
            </label>
            <div class="col-lg-6">
                {!! Form::text('quantity', null, ['class' => 'form-control m-input number-input', 'placeholder' => 'Enter quantity', 'id' => 'select-quantity']) !!}
            </div>
        </div>
    </div>
</div>
<div class="m-portlet__foot m-portlet__foot--fit">
    <div class="m-form__actions m-form__actions">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-6">
                <button type="button" class="btn btn-primary add-stock">
                    <i class="fa fa-plus"></i> {{ trans('common.add') }}
                </button>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="m-portlet__body">
    <!--begin::Section-->
    <div class="m-section">
        <div class="m-section__content">
            <table class="table m-table m-table--head-bg-success" id="order-stock-table">
                <thead>
                <tr>
                    <th>Select</th>
                    <th>{{ trans('common.product') }}</th>
                    <th>{{ trans('common.quantity') }}</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <button type="button" class="btn btn-danger blue text-white delete-stock"><i class="fa fa-minus"></i> {{ trans('common.remove_stock') }}</button>

            <hr>
            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">
                    {{ trans('common.total_order_cost') }}
                </label>
                <div class="col-lg-6">
                    {!! Form::text('total_order_cost', null, ['class' => 'form-control m-input fund-input', 'placeholder' => '0.00']) !!}
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">
                    {{ trans('common.paid') }}
                </label>
                <div class="col-lg-6">
                    {!! Form::text('paid', null, ['class' => 'form-control m-input fund-input', 'placeholder' => '0.00']) !!}
                </div>
            </div>
        </div>
    </div>
    <!--end::Section-->
</div>
