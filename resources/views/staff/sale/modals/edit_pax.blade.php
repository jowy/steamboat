<!--begin::Modal-->
<div class="modal fade" id="edit-pax-modal" role="dialog" aria-labelledby="editPaxModal" aria-hidden="true">
    {!! Form::open(['route' => ['staff.sale.edit-pax'], 'method' => 'post', 'role' => 'form', 'id' => 'edit-pax-form', 'class' => 'm-form m-form--label-align-right']) !!}
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editPaxModal">
                    {{ trans('common.edit_pax') }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="edit-pax-loader"></div>
                    <div class="col-md-12" id="edit-pax-body">
                        @foreach (\App\Models\Price::NotSystem()->get() as $price)
                            <div class="form-group m-form__group">
                                <label for="price-{{ $price->type }}">{{ $price->name_en }} ({{ $price->per_pax_price }})</label>
                                <input type="text" id="price-{{ $price->type }}" class="form-control m-input number-input" placeholder="{{ $price->name_en }} pax" name="pax[{{ $price->type }}]" autocomplete="off">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-block" id="btn-edit-pax">
                    <i class="fa fa-edit"></i> {{ trans('common.edit_pax') }}
                </button>
            </div>
        </div>
    </div>
    <input type="hidden" id="hidden-sale-id" name="sale_id" value="">
    {!! Form::close() !!}
</div>
<!--end::Modal-->
