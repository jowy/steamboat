<!--begin::Modal-->
<div class="modal fade" id="add-pax-modal" role="dialog" aria-labelledby="addPaxModal" aria-hidden="true">
    {!! Form::open(['route' => ['staff.sale.add-pax'], 'method' => 'post', 'role' => 'form', 'id' => 'add-pax-form', 'class' => 'm-form m-form--label-align-right']) !!}
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addPaxModal">
                    {{ trans('common.add_pax') }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        @foreach (\App\Models\Price::all() as $price)
                            <div class="form-group m-form__group">
                                <label for="{{ $price->name_en }}">{{ $price->name_en }} ({{ $price->per_pax_price }})</label>
                                <input type="text" id="{{ $price->name_en }}" class="form-control m-input number-input" placeholder="{{ $price->name_en }} pax" name="pax[{{ $price->type }}]" autocomplete="off">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-block" id="btn-add-pax">
                    <i class="fa fa-user-plus"></i> {{ trans('common.add_pax') }}
                </button>
            </div>
        </div>
    </div>
    <input type="hidden" id="hidden-sale-id" name="sale_id" value="">
    {!! Form::close() !!}
</div>
<!--end::Modal-->
