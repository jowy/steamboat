<!--begin::Modal-->
<div class="modal fade" id="change-table" role="dialog" aria-labelledby="changeTableModal" aria-hidden="true">
    {!! Form::open(['route' => ['staff.sale.change-table'], 'method' => 'post', 'role' => 'form', 'id' => 'change-table-form', 'class' => 'm-form m-form--label-align-right']) !!}
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changeTableModal">
                    {{ trans('common.change_table') }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="change-table-loader"></div>
                        <div class="form-group m-form__group" id="change-table-select-body">
                            <label for="table-select">{{ trans('common.table_no') }}</label>
                            {!! Form::select('table_no[]', [], '', ['class' => 'form-control m-input', 'id' => 'change-table-select',  'multiple' => "multiple"]) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-block" id="btn-change-table">
                    <i class="fa fa-edit"></i> {{ trans('common.change_table') }}
                </button>
            </div>
        </div>
    </div>
    <input type="hidden" id="hidden-sale-id" name="sale_id" value="">
    {!! Form::close() !!}
</div>
<!--end::Modal-->
