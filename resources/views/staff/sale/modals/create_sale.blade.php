<!--begin::Modal-->
<div class="modal fade" id="sale-modal" role="dialog" aria-labelledby="saleModal" aria-hidden="true">
    {!! Form::open(['route' => ['staff.sale.store'], 'method' => 'post', 'role' => 'form', 'id' => 'add-sale-form', 'class' => 'm-form m-form--label-align-right']) !!}
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="saleModal">
                    {{ trans('common.add_sale') }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="add-sale-loader"></div>
                        <div class="form-group m-form__group" id="table-select-body">
                            <label for="table-select">{{ trans('common.table_no') }}</label>
                            {!! Form::select('table_no[]', [], '', ['class' => 'form-control m-input', 'id' => 'table-select',  'multiple' => "multiple"]) !!}
                        </div>
                        @foreach (\App\Models\Price::NotSystem()->get() as $price)
                            <div class="form-group m-form__group">
                                <label for="{{ $price->name_en }}">{{ $price->name_en }} ({{ $price->per_pax_price }})</label>
                                <input type="text" id="{{ $price->name_en }}" class="form-control m-input number-input" placeholder="{{ $price->name_en }} pax" name="pax[{{ $price->type }}]" autocomplete="off">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-block" id="btn-add-sale">
                    <i class="fa fa-plus"></i> {{ trans('common.add_sale') }}
                </button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
<!--end::Modal-->
