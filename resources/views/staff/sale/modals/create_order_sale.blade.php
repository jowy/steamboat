<!--begin::Modal-->
<div class="modal fade" id="order-sale-modal" role="dialog" aria-labelledby="orderSaleModal" aria-hidden="true">
    {!! Form::open(['route' => ['staff.sale.store-ala-carte'], 'method' => 'post', 'role' => 'form', 'id' => 'add-order-sale-form', 'class' => 'm-form m-form--label-align-right']) !!}
    <div class="modal-dialog modal-lg" role="document" style="max-width:1000px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="orderSaleModal">
                    {{ trans('common.add_ala_carte_sale') }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-portlet__body">
                    <div class="m-form__section m-form__section--first">
                        <div id="add-order-sale-loader"></div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label" id="add-order-ala-carte-body">
                                {{ trans('common.product') }}
                            </label>
                            <div class="col-lg-6">
                                {!! Form::select('select_ala_carte', [], '', ['class' => 'form-control m-input', 'id' => 'select-order-ala-carte']) !!}
                            </div>
                        </div>

                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">
                                {{ trans('common.quantity') }}
                            </label>
                            <div class="col-lg-6">
                                {!! Form::text('quantity', null, ['class' => 'form-control m-input number-input', 'placeholder' => 'Enter quantity', 'id' => 'select-order-quantity']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-6">
                                <button type="button" class="btn btn-primary add-order-stock-btn">
                                    <i class="fa fa-plus"></i> {{ trans('common.add') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="m-portlet__body">
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="m-section__content">
                            <table class="table m-table m-table--head-bg-success" id="add-order-stock-table">
                                <thead>
                                <tr>
                                    <th>Select</th>
                                    <th>{{ trans('common.product') }}</th>
                                    <th>{{ trans('common.quantity') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <button type="button" class="btn btn-danger blue text-white delete-order-stock"><i class="fa fa-minus"></i> {{ trans('common.remove_stock') }}</button>
                        </div>
                    </div>
                    <!--end::Section-->

                    <div class="form-group m-form__group">
                        <label>{{ trans('common.name') }}</label>
                        {!! Form::text('ala_carte_name', null, ['class' => 'form-control m-input']) !!}
                    </div>

                    <div class="form-group m-form__group">
                        <label>{{ trans('common.ala_carte_remark') }}</label>
                        {!! Form::textarea('ala_carte_remark', null, ['class' => 'form-control m-input', 'rows' => 5]) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-block" id="btn-add-order-sale">
                    <i class="fa fa-plus"></i> {{ trans('common.add_ala_carte_sale') }}
                </button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
<!--end::Modal-->
