<!--begin::Modal-->
<div class="modal fade" id="table-detail-modal" role="dialog" aria-labelledby="tableDetailModal" aria-hidden="true">
    {!! Form::open(['route' => ['staff.sale.paid'], 'method' => 'post', 'role' => 'form', 'id' => 'paid-form']) !!}
    <div class="modal-dialog modal-lg" role="document" style="max-width:1200px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tableDetailModal">
                    {{ trans('common.table_detail') }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body" id="table-sale-detail-content">
                <!-- Loader -->
                <div id="modal-loader" style="display:none; text-align: center;" class="m-loader m-loader--primary"></div>

                <!-- content will be load here -->
                <div id="modal-content"></div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hidden-sale-id" name="sale_id" value="">
    {!! Form::close() !!}
</div>
<!--end::Modal-->
