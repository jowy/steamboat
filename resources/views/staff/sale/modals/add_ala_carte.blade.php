<!--begin::Modal-->
<div class="modal fade" id="add-ala-carte-modal" role="dialog" aria-labelledby="addAlaCarteModal" aria-hidden="true">
    {!! Form::open(['route' => ['staff.sale.add-ala-carte'], 'method' => 'post', 'role' => 'form', 'id' => 'add-stock-form', 'class' => 'm-form m-form--label-align-right']) !!}
    <div class="modal-dialog modal-lg" role="document" style="max-width:1000px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addAlaCarteModal">
                    {{ trans('common.add_ala_carte') }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-portlet__body">
                    <div class="m-form__section m-form__section--first">
                        <div id="add-sale-loader"></div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label" id="add-ala-carte-body">
                                {{ trans('common.product') }}
                            </label>
                            <div class="col-lg-6">
                                {!! Form::select('select_ala_carte', [], '', ['class' => 'form-control m-input', 'id' => 'select-ala-carte']) !!}
                            </div>
                        </div>

                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">
                                {{ trans('common.quantity') }}
                            </label>
                            <div class="col-lg-6">
                                {!! Form::text('quantity', null, ['class' => 'form-control m-input number-input', 'placeholder' => 'Enter quantity', 'id' => 'select-quantity']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-6">
                                <button type="button" class="btn btn-primary add-stock-btn">
                                    <i class="fa fa-plus"></i> {{ trans('common.add') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="m-portlet__body">
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="m-section__content">
                            <table class="table m-table m-table--head-bg-success" id="add-stock-table">
                                <thead>
                                <tr>
                                    <th>Select</th>
                                    <th>{{ trans('common.product') }}</th>
                                    <th>{{ trans('common.quantity') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <button type="button" class="btn btn-danger blue text-white delete-stock"><i class="fa fa-minus"></i> {{ trans('common.remove_stock') }}</button>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="submit-add-stock-btn" class="btn btn-success">
                    {{ trans('common.update') }}
                </button>
            </div>
        </div>
    </div>
    <input type="hidden" id="hidden-sale-id" name="sale_id" value="">
    {!! Form::close() !!}
</div>
<!--end::Modal-->
