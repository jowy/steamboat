<!--begin::Modal-->
<div class="modal fade" id="table-action-modal" role="dialog" aria-labelledby="tableActionModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tableActionModal">
                    {{ trans('common.actions') }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-section__content" style="margin-top: 30px;">
                    <div class="m-demo m-demo--last" data-code-preview="true" data-code-html="true" data-code-js="false">
                        <div class="m-demo__preview">
                            <a href="javascript:void(0)" class="btn btn-info btn-lg btn-block btn-action" data-sale-id="" data-url="{{ route('staff.sale.detail') }}" onclick="dynamicModal(this, '#table-detail-modal', true)">
                                <i class="fa fa-info-circle"></i>
                                {{ trans('common.table_detail') }}
                            </a>
                            <a href="javascript:void(0)" class="btn btn-primary btn-lg btn-block btn-action btn-change-table" id="change-table-modal" data-sale-id="" onclick="dynamicModal(this, '#change-table')">
                                <i class="fa fa-table"></i>
                                {{ trans('common.change_table') }}
                            </a>
                            {{--<a href="javascript:void(0)" class="btn btn-success btn-lg btn-block btn-action" data-sale-id="" onclick="dynamicModal(this, '#add-pax-modal')">
                                <i class="fa fa-user-plus"></i>
                                {{ trans('common.add_pax') }}
                            </a>--}}
                            <a href="javascript:void(0)" class="btn btn-warning btn-lg btn-block btn-action btn-edit-pax" data-sale-id="" onclick="dynamicModal(this, '#edit-pax-modal')">
                                <i class="fa fa-user-plus"></i>
                                {{ trans('common.edit_pax') }}
                            </a>
                            <a href="javascript:void(0)" class="btn btn-primary btn-lg btn-block btn-action btn-add-ala-carte" data-sale-id="" onclick="dynamicModal(this, '#add-ala-carte-modal')">
                                <i class="fa fa-plus"></i>
                                {{ trans('common.add_edit_ala_carte') }}
                            </a>
                            <a href="javascript:void(0)" class="btn btn-danger btn-lg btn-block btn-action cancel-bill" data-sale-id="">
                                <i class="fa fa-close"></i>
                                {{ trans('common.cancel_bill') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <h4 class="text-center" id="footer-table-txt"></h4>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->
