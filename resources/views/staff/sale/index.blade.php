@extends('staff.layout')

@section('title')
    {{ trans('common.sales') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header')
    {{--<link rel="stylesheet" href="/custom/numpad/jquery.numpad.css" media="all">--}}
    {{--<link rel="stylesheet" href="/custom/mottie-keyboard/css/keyboard-dark.css" media="all">--}}
    {{--<link rel="stylesheet" href="/css/keyboard.css" media="all">--}}
@endpush

@section('action') @endsection

@section('content')
    <div class="m-portlet">
        <!--begin::Form-->
        <div class="m-portlet__body">
            <div class="m-form__actions m-form__actions row">
                <div id="hacker-list" class="col-md-12">
                    <ul class="list row table">
                        @if(count($sales) > 0)
                            @foreach($sales as $sale)
                                <li class="col-md-3" id="sale-{{ $sale->id }}-ct">
                                    <div class="sale-table-body" onclick="openActionModal('{{ $sale->id }}', '{{ $sale->all_table }}', '{{ ($sale->is_ala_carte_sale) ? true : false }}')">
                                        <div class="sale-table-top text-center">
                                            <img src="{{ asset('img/icon-table.png') }}" class="sale-table-icon" alt="sale-table">
                                            @if($sale->is_ala_carte_sale)
                                                <h4 class="ellipses">{{ trans('common.sale_ref_id') }}: </h4>
                                                <div class="countdown-body">
                                                    <span>{{ $sale->sale_ref_id }}</span>
                                                </div>
                                            @else
                                                <h4 class="ellipses">Table: {{ ($sale->all_table) ? $sale->all_table : '-' }}</h4>
                                                <div class="countdown-body">
                                                    <span class="countdown-timer"></span>
                                                    <span class="countdown-text">time left</span>
                                                </div>
                                            @endif
                                            <h5 class="ellipses mt-3 text-danger">{{ trans('common.total') }}: {{ fundFormat($sale->grand_total) }}</h5>
                                        </div>
                                        <div class="sale-table-bottom text-center">
                                            <div class="row">
                                                @if($sale->is_ala_carte_sale)
                                                    <div class="col-md-12 text-center">
                                                        <h5>Ala Carte Sale</h5>

                                                    </div>
                                                @else
                                                    <div class="col-md-5">
                                                        <h5>{{ $sale->total_pax }}</h5>
                                                        <span>pax</span>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <h5 class="table_finish_table" data-finish-time="{{ $sale->finish_at }}"></h5>
                                                        <span>finish time</span>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <h1>No Table Yet</h1>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <!--end::Form-->
    </div>

    @include('staff.sale.modals.action')
    @include('staff.sale.modals.table_detail')
    {{--@include('staff.sale.modals.add_pax')--}}
    @include('staff.sale.modals.add_ala_carte')
    @include('staff.sale.modals.change_table')
    @include('staff.sale.modals.edit_pax')
@endsection

@push('footer')
    <script type="text/javascript" src="/custom/jQuery-countdown/jquery.countdown.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var $cancelFormSubmitted = false;

            $(document).on('click', '.cancel-bill', function () {
                if ($cancelFormSubmitted === false) {
                    if (confirm('Confirm? this action cannot be revert')) {
                        var btnCancel = $(this);
                        btnCancel.attr('disabled', 'disabled');
                        $cancelFormSubmitted = true;
                        var sid = $(this).data('sale-id');
                        $.ajax({
                            type: 'POST',
                            url: '{{ route('staff.sale.cancel') }}',
                            data: {'sale_id': sid},
                            beforeSend: function() {
                                blockBody();
                            },
                            success: function(data) {
                                unblockBody();
                                location.reload();
                            },
                            error: function(xhr, status, error) {
                                unblockBody();
                                handleError(xhr);
                                $cancelFormSubmitted = false;
                                setTimeout(function(){
                                    btn.removeAttr('disabled', 'disabled');
                                }, 500);
                            },
                            dataType: 'json'
                        });
                    }
                }
            });

            $(".table_finish_table").each(function() {
                /*var utcFinishTime = moment.utc($(this).attr('data-finish-time')).toDate();

                var localFinishTime = moment(utcFinishTime).local().format('hh:mm:ss a');
                var countdownFinishTime = moment(utcFinishTime).local().format('MM/DD/YYYY HH:mm:ss');*/

                var finishTime = moment($(this).attr('data-finish-time')).toDate();

                var localFinishTime = moment(finishTime).format('hh:mm:ss a');
                var countdownFinishTime = moment(finishTime).format('MM/DD/YYYY HH:mm:ss');

                // Append Finish Time
                $(this).text(localFinishTime);

                // Append countdown
                $(this).closest('.sale-table-body').find('.countdown-body').countdown(countdownFinishTime, function(event) {
                    $(this).children('.countdown-timer').html(event.strftime('%H:%M:%S'));
                }).on('finish.countdown', function(event) {
                    $(this).empty();
                    $(this).html("<span class='finish-countdown-txt text-danger'>FINISHED</span>");
                });
            });

            $('#add-pax-form').makeAjaxForm({
                submitBtn: '#btn-add-pax',
                redirectTo: '{{ route('staff.sale.index') }}',
            });

            $('#edit-pax-form').makeAjaxForm({
                submitBtn: '#btn-edit-pax',
                redirectTo: '{{ route('staff.sale.index') }}',
            });

            $('#add-stock-form').makeAjaxForm({
                submitBtn: '#submit-add-stock-btn',
                redirectTo: '{{ route('staff.sale.index') }}',
            });

            $('#change-table-form').makeAjaxForm({
                submitBtn: '#btn-change-table',
                redirectTo: '{{ route('staff.sale.index') }}',
            });

            var $paidFormSubmited = false;
            $('#paid-form').on('click', '#btn-paid', function() {
                var btn = $(this);
                btn.attr('disabled', 'disabled');
                $paidFormSubmited = true;
                $.ajax({
                    type: 'POST',
                    url: '{{ route('staff.sale.paid') }}',
                    data: $('#paid-form').serializeArray(),
                    beforeSend: function() {
                        blockBody();
                    },
                    success: function(data) {
                        unblockBody();
                        alertSuccess(data.message, true, function () {
                            //window.open(data.redirectTo, '_blank');
                            const a = document.createElement("a");
                            a.href = data.redirectTo;
                            a.target = "_blank";
                            a.rel = "noreferrer";
                            a.click();
                            location.reload();
                        });

                        var mid = parseInt($('#hidden-sale-id').val());
                        if (mid > 0) {
                            $('#sale-' + mid + '-dt').remove();
                        }
                    },
                    error: function(xhr, status, error) {
                        unblockBody();
                        handleError(xhr);
                        $paidFormSubmited = false;
                        setTimeout(function(){
                            btn.removeAttr('disabled', 'disabled');
                        }, 500);
                    },
                    dataType: 'json'
                });
            });

            $('#select-ala-carte').select2({ width: '100%' });

            $(".add-stock-btn").click(function() {
                var stock_id = $("#select-ala-carte").val();
                var stock_name = $("#select-ala-carte option:selected").text();
                var quantity = $("#select-quantity").val();

                if (stock_id == '' || stock_name == '') {
                    alert('Some error happened, please try again');
                    return false;
                }
                if (quantity == '' || !$.isNumeric(quantity)) {
                    alert('Please key in quantity or quantity must be integer');
                    return false;
                }
                var $unique_stock_id = $('#add-stock-table').find('input.hidden_stock_id:hidden[value=' + stock_id + ']');

                if ($unique_stock_id.length > 0) {
                    // If there is stock id then remove and insert again
                    $unique_stock_id.parents("tr").remove();
                }
                var markup = "<tr><td><input type='checkbox' name='record'></td><td>"+ stock_name +"</td><td><input type='text' class='form-control' name='add_stock_details["+ stock_id +"][quantity]' value='"+ quantity +"'></td><input type='hidden' name='add_stock_details["+ stock_id +"][product_id]' class='hidden_stock_id' value='"+ stock_id +"'></tr>";
                $("table#add-stock-table tbody").append(markup);
                $('#submit-add-stock-btn').prop('disabled', false);
            });

            // Find and remove selected table rows
            $(".delete-stock").click(function() {
                $("table#add-stock-table tbody").find('input[name="record"]').each(function(){
                    if($(this).is(":checked")){
                        $(this).parents("tr").remove();
                    }
                });
                /*if ($("#add-stock-table > tbody > tr").length == null || $("#add-stock-table > tbody > tr").length == 0){
                    $('#submit-add-stock-btn').prop('disabled', true);
                }*/
            });

            $('#table-detail-modal').on('keypress', '#sale-paid', function(event) {
                if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });

            $('#table-detail-modal').on('keypress', '#sale-discount', function(event) {
                if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });

            $('#table-detail-modal').on("keypress keyup blur", '#sale-discount-percentage', function (event) {
                $(this).val($(this).val().replace(/[^\d].+/, ""));
                if ((event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });

            $('#table-detail-modal').on('change', '#discount-type', function(event) {
                if ($('#sale-discount').val() != '') {
                    countTotal();
                }
            });

            var countTotal = function () {
                var paid = parseFloat($('#sale-paid').val()) || 0;
                var discount = parseFloat($('#sale-discount').val()) || 0;
                var tax = parseFloat($('#modal-tax-price').text()) || 0;
                if ($('#discount-type').val() == 'fixed') {
                    // Take grand total
                    var grandTotal = parseFloat($('#hidden-grandtotal-price').val()) || 0;
                    var grandTotalValue = customRound(grandTotal - discount);
                    var subtotal = parseFloat($('#hidden-subtotal-price').val()) || 0;

                    // Change grand total value
                    $('#modal-grandtotal-price').text(grandTotalValue);
                    $('#modal-subtotal-price').text(subtotal);
                } else {
                    // Take subtotal (cause need before + tax)
                    var subtotal = parseFloat($('#hidden-subtotal-price').val()) || 0;
                    //var grandTotal = parseFloat($('#hidden-grandtotal-price').val()) || 0;
                    var discountTotal = (subtotal * (discount/100)).toFixed(2);
                    var subTotalValue = subtotal - discountTotal;
                    var grandTotalValue = customRound(subtotal - discountTotal + tax); // Round to 1 decimal

                    // Change subtotal and grand total value
                    $('#modal-subtotal-price').text(subTotalValue.toFixed(2));
                    $('#modal-grandtotal-price').text(grandTotalValue);
                }

                if (paid <= 0) {
                    $("#modal-sale-change").text("0.00");
                } else {
                    var c = paid - grandTotalValue;
                    // console.log(c);
                    $("#modal-sale-change").text(c.toFixed(2));
                }
            }

            function customRound(value)
            {
                var $rangeDefinitions = [
                    [[0.00, 0.02], 0.00],
                    [[0.03, 0.07], 0.05],
                    [[0.08, 0.09], 0.10],
                ];

                var $no = value.toFixed(2).split('.');
                var $integer = Math.floor($no[0]);
                var $firstDecimal = $no[1].substr(0, 1) / 10;
                var $secondDecimal = $no[1].substr(-1, 1) / 100;

                var $result = value;
                $.each($rangeDefinitions, function( index, value ) {
                    if ($secondDecimal >= value[0][0] && $secondDecimal <= value[0][1]) {
                        $result = parseFloat($integer + $firstDecimal + value[1]).toFixed(2);
                    }
                });
                return $result;
            }

            /*function round(value, precision) {
                var aPrecision = Math.pow(10, precision);
                return Math.round(value * aPrecision) / aPrecision;
            }*/

            $('#table-detail-modal').on('keyup', '#sale-paid', function(event) {
                countTotal();
            });

            $('#table-detail-modal').on('keyup', '#sale-discount', function(event) {
                countTotal();
            });

            $('#table-detail-modal').on("keyup", '#sale-discount-percentage', function (event) {
                countTotal();
            });
        });

        function openActionModal(table_id, table_no, isAlaCarte = false) {
            $('#table-action-modal').modal('show');
            $('#table-action-modal .btn-action').attr('data-sale-id', table_id);

            if (isAlaCarte) {
                $('#table-action-modal .btn-change-table').hide();
                $('#table-action-modal .btn-edit-pax').hide();
                //$('#table-action-modal .btn-add-ala-carte').hide();
                $('#table-action-modal #footer-table-txt').hide();
            } else {
                $('#table-action-modal .btn-change-table').show();
                $('#table-action-modal .btn-edit-pax').show();
                //$('#table-action-modal .btn-add-ala-carte').show();
                $('#table-action-modal #footer-table-txt').show();
                $('#table-action-modal #footer-table-txt').text('Table: ' + table_no);
            }
        }

        function dynamicModal(elem, modal_id, isAjaxLoad = false) {
            var sale_id = $(elem).attr("data-sale-id");

            $('#table-action-modal').one('hidden.bs.modal', function() {
                $(modal_id).modal('show');

                if (isAjaxLoad) {
                    var url = $(elem).attr("data-url") + '?sale_id=' + sale_id;

                    $(modal_id + ' #modal-content').html(''); // leave it blank before ajax call
                    $(modal_id + ' #modal-loader').show();      // load ajax loader

                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'html'
                    })
                    .done(function(data) {
                        $(modal_id + ' #modal-content').html('');
                        $(modal_id + ' #modal-content').html(data); // load response
                        $(modal_id + ' #modal-loader').hide(); // hide ajax loader
                        if ($(modal_id + ' #sale-paid').length > 0) {
                            $(modal_id + ' #sale-paid').focus(); // Focus paid input
                        }

                        // Convert start at and finish at
                        var utcStartTime = moment($(modal_id + ' #modal-content').find('#start-at').val()).toDate();
                        var utcFinishTime = moment($(modal_id + ' #modal-content').find('#finish-at').val()).toDate();

                        var localStartTime = moment(utcStartTime).format('hh:mm:ss a');
                        var localFinishTime = moment(utcFinishTime).format('hh:mm:ss a');

                        $(modal_id + ' #modal-content').find('#start-at').val(localStartTime);
                        $(modal_id + ' #modal-content').find('#finish-at').val(localFinishTime);

                        $(modal_id + ' #hidden-sale-id').val(sale_id);
                    })
                    .fail(function(){
                        $(modal_id + ' #modal-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
                        $(modal_id + '#modal-loader').hide();
                    });
                } else {
                    $(modal_id + ' #hidden-sale-id').val(sale_id);

                    if (modal_id == '#edit-pax-modal') {
                        $.ajax({
                            type: 'GET',
                            url: '{{ route('staff.sale.get-pax') }}?sale_id=' + sale_id,
                            beforeSend: function() {
                                $('#edit-pax-body').hide();
                                $('#edit-pax-loader').html('<div class="m-loader m-loader--primary"></div>');
                            },
                            success: function(data) {
                                $('#edit-pax-body').show();
                                $.each(data, function(index, item) {
                                    $('#edit-pax-modal #price-'+index).val(item);
                                });
                            },
                            error: function(xhr, status, error) {
                                alert(error + '. Please refresh the page and try again');
                            },
                            complete: function () {
                                $('#edit-pax-loader').empty();
                            },
                            dataType: 'json'
                        });
                    }

                    if (modal_id == '#change-table') {
                        $.ajax({
                            type: 'GET',
                            url: '{{ route('staff.sale.get-change-table') }}?sale_id=' + sale_id,
                            beforeSend: function() {
                                $('#change-table-select-body').hide();
                                $('#change-table-select')
                                    .find('option')
                                    .remove();
                                $('#change-table-loader').html('<div class="m-loader m-loader--primary"></div>');
                            },
                            success: function(data) {
                                $('#change-table-select-body').show();
                                $.each(data.all_branche_tables, function(index, item) {
                                    $('#change-table-select').append('<option value="'+ index +'">'+ item +'</option>');
                                });
                                $('#change-table-select').val(data.selected_tables).trigger('change');
                            },
                            error: function(xhr, status, error) {
                                alert(error + '. Please refresh the page and try again');
                            },
                            complete: function () {
                                $('#change-table-loader').empty();
                            },
                            dataType: 'json'
                        });
                    }

                    if (modal_id == '#add-ala-carte-modal') {
                        $("table#add-stock-table tbody").empty();
                        // Get ala carte product
                        $.ajax({
                            type: 'GET',
                            url: '{{ route('staff.sale.get-ala-carte-stock') }}',
                            beforeSend: function() {
                                $('#add-ala-carte-body').hide();
                                $('#select-ala-carte')
                                    .find('option')
                                    .remove();
                                $('#add-sale-loader').html('<div class="m-loader m-loader--primary"></div>');
                            },
                            success: function(data) {
                                $('#add-ala-carte-body').show();
                                $.each(data.stocks, function(index, item) {
                                    $('#select-ala-carte').append('<option value="'+ item.id +'">'+ item.product_name_en +'</option>');
                                });
                            },
                            error: function(xhr, status, error) {
                                alert(error + '. Please refresh the page and try againn');
                            },
                            complete: function () {
                                $('#add-sale-loader').empty();
                            },
                            dataType: 'json'
                        });

                        // Get current order ala carte and append
                        $.get('{{ route('staff.sale.get-sale-order') }}?sale_id=' + sale_id, function(datas) {
                            $.each(datas.saleOrders, function( index, value ) {
                                var markup = "<tr><td><input type='checkbox' name='record'></td><td>"+ value.product_name +"</td><td><input type='text' class='form-control' name='add_stock_details["+ value.product_id +"][quantity]' value='"+ value.quantity +"'></td><input type='hidden' name='add_stock_details["+ value.product_id +"][product_id]' class='hidden_stock_id' value='"+ value.product_id +"'></tr>";
                                $("table#add-stock-table tbody").append(markup);
                            });
                        }).fail(function(xhr, status, error) {
                            alert(error + '. Please refresh the page and try againn');
                        }).always(function() {
                            //alert( "finished" );
                        });
                    }
                }
            }).modal('hide');
        }
    </script>
@endpush
