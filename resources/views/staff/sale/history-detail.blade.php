@extends('staff.layout')

@section('title')
    {{ trans('common.sale_histories') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@section('content')
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.sale_detail') }}
                    </h3>
                </div>
            </div>
        </div>

        <div class="m-form m-form--label-align-right" id="filter-container">
            <div class="m-portlet__body">
                <div class="m-section">
                    <div class="m-section__content">
                        <div class="form-group m-form__group row">
                            <div class="col-md-6">
                                <label class="col-form-label">{{ trans('common.table_no') }}</label>
                                {!! Form::text('table_no', $sale->all_table, ['class' => 'form-control m-input', 'disabled']) !!}
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">{{ trans('common.sale_ref_id') }}</label>
                                {!! Form::text('sale_ref_id', $sale->sale_ref_id, ['class' => 'form-control m-input', 'disabled']) !!}
                            </div>
                        </div>
                        @foreach($sale->sale_details()->wherePaxType(config('sale.pax_type.pay'))->get() as $sale_detail)
                            <div class="form-group m-form__group row">
                                <div class="col-md-6">
                                    <label class="col-form-label">{{ $sale_detail->price_type_name }}</label>
                                    {!! Form::text('adult_pay_pax', $sale_detail->pax, ['class' => 'form-control m-input', 'disabled']) !!}
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label">{{ trans('common.total_pax_price') }}</label>
                                    {!! Form::text('grand_total', fundFormat($sale_detail->total_pax_price), ['class' => 'form-control m-input', 'disabled']) !!}
                                </div>
                            </div>
                        @endforeach
                        @if($freeSaleDetail = $sale->sale_details()->wherePaxType(config('sale.pax_type.free'))->wherePriceType(\App\Models\Price::ADULT_TYPE)->first())
                            <div class="form-group m-form__group row">
                                <div class="col-md-6">
                                    <label class="col-form-label">{{ $freeSaleDetail->price_type_name }} (Free Pax)</label>
                                    {!! Form::text('adult_pay_pax', $freeSaleDetail->pax, ['class' => 'form-control m-input', 'disabled']) !!}
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label">{{ trans('common.total_pax_price') }}</label>
                                    {!! Form::text('grand_total',  $freeSaleDetail->total_pax_price , ['class' => 'form-control m-input', 'disabled']) !!}
                                </div>
                            </div>
                        @endif
                        @if($sale->is_paid)
                            <div class="form-group m-form__group row">
                                <div class="col-md-6">
                                    <label class="col-form-label">{{ trans('common.total_pax') }}</label>
                                    {!! Form::text('total_pax', $sale->total_pax, ['class' => 'form-control m-input', 'disabled']) !!}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-md-6">
                                    <label class="col-form-label">{{ trans('common.grand_total') }}</label>
                                    {!! Form::text('per_pax_price', $sale->grand_total, ['class' => 'form-control m-input', 'disabled']) !!}
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label">{{ trans('common.paid') }}</label>
                                    {!! Form::text('per_pax_price', $sale->paid, ['class' => 'form-control m-input', 'disabled']) !!}
                                </div>
                            </div>
                        @else
                            <div class="form-group m-form__group row">
                                <div class="col-md-6">
                                    <label class="col-form-label">{{ trans('common.total_pax') }}</label>
                                    {!! Form::text('total_pax', $sale->total_pax, ['class' => 'form-control m-input', 'disabled']) !!}
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label">{{ trans('common.grand_total') }}</label>
                                    {!! Form::text('per_pax_price', $sale->grand_total, ['class' => 'form-control m-input', 'disabled']) !!}
                                </div>
                            </div>
                        @endif
                        @if(!$sale->is_ala_carte_sale)
                        <div class="form-group m-form__group row">
                            <div class="col-md-6">
                                <label class="col-form-label">{{ trans('common.start_at') }}</label>
                                {!! Form::text('start_at', $sale->start_at, ['class' => 'form-control m-input', 'id' => 'start-at', 'disabled']) !!}
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">{{ trans('common.finish_at') }}</label>
                                {!! Form::text('finish_at', $sale->finish_at, ['class' => 'form-control m-input', 'id' => 'finish-at', 'disabled']) !!}
                            </div>
                        </div>
                        @else
                            <div class="form-group m-form__group row">
                                <div class="col-md-6">
                                    <label class="col-form-label">{{ trans('common.name') }}</label>
                                    {!! Form::text('ala_carte_name', $sale->ala_carte_name, ['class' => 'form-control m-input', 'disabled']) !!}
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label">{{ trans('common.ala_carte_remark') }}</label>
                                    {!! Form::text('ala_carte_remark', $sale->ala_carte_remark, ['class' => 'form-control m-input', 'disabled']) !!}
                                </div>
                            </div>
                        @endif

                        <div class="m--margin-10">
                            <table class="table m-table m-table--head-bg-brand" id="modal-order-table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th width="300">{{ trans('common.product_name') }}</th>
                                    <th>{{ trans('common.quantity') }}</th>
                                    <th>{{ trans('common.ala_carte_price') }}</th>
                                    <th>{{ trans('common.total_price') }}</th>
                                </tr>
                                </thead>
                                <?php $count = 1; ?>
                                <tbody>
                                @if(count($sale->sale_orders) > 0)
                                    @foreach($sale->sale_orders as $sale_order)
                                        <tr>
                                            <td>{{ $count }}</td>
                                            <td>{{ $sale_order->product_name }}</td>
                                            <td>{{ $sale_order->quantity }}</td>
                                            <td>{{ $sale_order->ala_carte_price }}</td>
                                            <td>{{ $sale_order->total_price }}</td>
                                        </tr>
                                        <?php $count++; ?>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="text-center">
                                            <h5>No Order Ala Carte</h5>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>

                            <hr>

                            <div class="row">
                                <div class="col-md-10 text-right">
                                    <h5>{{ trans('common.total_pax') }}</h5>
                                </div>
                                <div class="col-md-2 text-right">
                                    <h5 class="m--font-info" >
                                        {{ $sale->totalPaxPriceWithoutFreePax() }}
                                    </h5>
                                </div>
                            </div>

                            @if($sale->sale_details()->wherePaxType(config('sale.pax_type.free'))->exists())
                                <div class="row">
                                    <div class="col-md-10 text-right">
                                        <h5>{{ trans('common.free_pax') }} ({{ $sale->sale_details()->wherePaxType(config('sale.pax_type.free'))->first()->pax }})</h5>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <h5 class="m--font-info">
                                            {{ -1 * abs($sale->totalFreePaxPrice()) }}
                                        </h5>
                                    </div>
                                </div>
                            @endif

                            @if ($sale->ala_carte_total > 0)
                                <div class="row">
                                    <div class="col-md-10 text-right">
                                        <h5>{{ trans('common.total_ala_carte_order') }}</h5>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <h5 class="m--font-info" id="modal-grandtotal-price" >
                                            {{ $sale->ala_carte_total }}
                                        </h5>
                                    </div>
                                </div>
                            @endif

                            @if ($sale->tax_total > 0)
                                <div class="row">
                                    <div class="col-md-10 text-right">
                                        <h5>{{ $sale->tax_name }}</h5>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <h5 class="m--font-info" id="modal-grandtotal-price" >
                                            {{ $sale->tax_total }}
                                        </h5>
                                    </div>
                                </div>
                            @endif

                            @if ($sale->sub_total > 0)
                                <div class="row">
                                    <div class="col-md-10 text-right">
                                        <h5>{{ trans('common.sub_total') }}</h5>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <h5 class="m--font-info" id="modal-grandtotal-price" >
                                            {{ $sale->sub_total }}
                                        </h5>
                                    </div>
                                </div>
                            @endif

                            @if ($sale->discount_total > 0)
                                <div class="row">
                                    <div class="col-md-10 text-right">
                                        <h5>{{ trans('common.discount') }}</h5>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <h5 class="m--font-info" id="modal-grandtotal-price" >
                                            {{ $sale->discount_total }}
                                        </h5>
                                    </div>
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-md-10 text-right">
                                    <h5>{{ trans('common.grand_total') }}</h5>
                                </div>
                                <div class="col-md-2 text-right">
                                    <h5 class="m--font-info" id="modal-grandtotal-price" >
                                        {{ $sale->grand_total }}
                                    </h5>
                                </div>
                            </div>

                            @if($sale->is_paid)
                                <div class="row">
                                    <div class="col-md-10 text-right">
                                        <h5>{{ trans('common.paid') }}</h5>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <h5 class="m--font-info">{{ $sale->paid }}</h5>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-10 text-right">
                                        <h5>{{ trans('common.change') }}</h5>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <h5 class="m--font-info">{{ fundFormat($sale->calculateChange()) }}</h5>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions">
                    <div class="row">
                        <div class="col-lg-12 text-right">
                            <a href="{{ route('staff.sale.receipt', $sale->id) }}" target="_blank" class="btn btn-info" id="print-receipt-btn">
                                <i class="fa fa-print"></i> {{ trans('common.print_receipt') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--begin::Form-->
    </div>

    <div class="m-portlet m-portlet--full-height m-portlet--tabs">
        <div class="tab-content">
            <div class="tab-pane active" id="m_user_profile_tab_1">
                {!! Form::open(['route' => ['staff.sale.history-detail.post', $sale->id], 'method' => 'post', 'role' => 'form', 'id' => 'history-detail-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                {{ trans('common.edit_payment_method') }}
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-2 col-form-label">
                            {{ trans('common.sales') }}
                        </label>
                        <div class="col-7">
                            {!! Form::select('payment_method', \App\Models\Sale::getPaymentMethodLists(), $sale->payment_method, ['class' => 'form-control m-input']) !!}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-2 col-form-label">
                            {{ trans('common.remark') }}
                        </label>
                        <div class="col-7">
                            {!! Form::textarea('remark', $sale->remark, ['class' => 'form-control m-input', 'rows' => 5]) !!}
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                <button type="button" class="btn btn-accent m-btn m-btn--air m-btn--custom" id="btn-submit">
                                    {{ trans('common.update') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {{--</form>--}}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#history-detail-form').makeAjaxForm({
                    submitBtn: '#btn-submit',
                    redirectTo: '{{ route('staff.sale.history-detail', $sale->id) }}',
                });
            });
        }(jQuery);
    </script>
@endpush
