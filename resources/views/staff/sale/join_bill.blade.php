@extends('staff.layout')

@section('title')
    {{ trans('common.sales') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@section('content')
    <div class="m-portlet m-portlet--full-height m-portlet--tabs">
        <div class="tab-content">
            <div class="tab-pane active" id="m_user_profile_tab_1">
                {!! Form::open(['route' => 'staff.sale.join-bill.post', 'method' => 'post', 'role' => 'form', 'id' => 'profile-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
                {{--<form class="m-form m-form--fit m-form--label-align-right">--}}
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-2 col-form-label">
                            {{ trans('common.sales') }}
                        </label>
                        <div class="col-7">
                            {!! Form::select('table_no[]', $sales, '', ['class' => 'form-control m-input', 'id' => 'join-sale-select',  'multiple' => "multiple"]) !!}
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                <button type="button" class="btn btn-accent m-btn m-btn--air m-btn--custom" id="btn-submit">
                                    {{ trans('common.join_bill') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {{--</form>--}}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @include('staff.sale.modals.table_detail')
@endsection

@push('footer')
    <script>
        $('#btn-submit').on('click', function () {
            //if ($cancelFormSubmitted === false) {
                if (confirm('Confirm? this action cannot be revert')) {
                    var btnSubmit = $(this);
                    btnSubmit.attr('disabled', 'disabled');
                    var sale_ids = $('#join-sale-select').val();
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('staff.sale.join-bill.post') }}',
                        data: {'sale_ids': sale_ids},
                        beforeSend: function() {
                            blockBody();
                        },
                        success: function(data) {
                            unblockBody();
                            console.log(data.sale_id);
                            openSaleDetail(data.sale_id);
                        },
                        error: function(xhr, status, error) {
                            unblockBody();
                            handleError(xhr);
                            setTimeout(function(){
                                btnSubmit.removeAttr('disabled', 'disabled');
                            }, 500);
                        },
                        dataType: 'json'
                    });
                }
            //}
        });

        $('#join-sale-select').select2({
            width: '100%'
        });

        var $paidFormSubmited = false;
        $('#paid-form').on('click', '#btn-paid', function() {
            var btn = $(this);
            btn.attr('disabled', 'disabled');
            $paidFormSubmited = true;
            $.ajax({
                type: 'POST',
                url: '{{ route('staff.sale.paid') }}',
                data: $('#paid-form').serializeArray(),
                beforeSend: function() {
                    blockBody();
                },
                success: function(data) {
                    unblockBody();
                    alertSuccess(data.message, true, function () {
                        //window.open(data.redirectTo, '_blank');
                        const a = document.createElement("a");
                        a.href = data.redirectTo;
                        a.target = "_blank";
                        a.rel = "noreferrer";
                        a.click();
                        location.reload();
                    });

                    var mid = parseInt($('#hidden-sale-id').val());
                    if (mid > 0) {
                        $('#sale-' + mid + '-dt').remove();
                    }
                },
                error: function(xhr, status, error) {
                    unblockBody();
                    handleError(xhr);
                    $paidFormSubmited = false;
                    setTimeout(function(){
                        btn.removeAttr('disabled', 'disabled');
                    }, 500);
                },
                dataType: 'json'
            });
        });

        $("#table-detail-modal").on("hidden.bs.modal", function () {
            location.reload();
        });

        var countTotal = function () {
            var p = parseFloat($('#sale-paid').val()) || 0;
            if (p <= 0) {
                $("#modal-sale-change").text("0.00");
            } else {
                var d = parseFloat($('#sale-discount').val()) || 0;
                var t = parseFloat($('#modal-grandtotal-price').text()) || 0;
                var c = p - (t - d);
                console.log(d);
                console.log(t);
                // console.log(c);
                $("#modal-sale-change").text(c.toFixed(2));
            }
        }

        $('#table-detail-modal').on('keyup', '#sale-paid', function(event) {
            countTotal();
        });

        $('#table-detail-modal').on('keyup', '#sale-discount', function(event) {
            countTotal();
        });

        function openSaleDetail(sale_id) {
            var modal_id = '#table-detail-modal';
            $(modal_id).modal('show');

            var url = '{{ route("staff.sale.detail") }}?sale_id=' + sale_id;

            $(modal_id + ' #modal-content').html(''); // leave it blank before ajax call
            $(modal_id + ' #modal-loader').show(); // load ajax loader

            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'html'
            })
            .done(function(data) {
                $(modal_id + ' #modal-content').html('');
                $(modal_id + ' #modal-content').html(data); // load response
                $(modal_id + ' #modal-loader').hide(); // hide ajax loader
                if ($(modal_id + ' #sale-paid').length > 0) {
                    $(modal_id + ' #sale-paid').focus(); // Focus paid input
                }

                // Convert start at and finish at
                var utcStartTime = moment($(modal_id + ' #modal-content').find('#start-at').val()).toDate();
                var utcFinishTime = moment($(modal_id + ' #modal-content').find('#finish-at').val()).toDate();

                var localStartTime = moment(utcStartTime).format('hh:mm:ss a');
                var localFinishTime = moment(utcFinishTime).format('hh:mm:ss a');

                $(modal_id + ' #modal-content').find('#start-at').val(localStartTime);
                $(modal_id + ' #modal-content').find('#finish-at').val(localFinishTime);

                $(modal_id + ' #hidden-sale-id').val(sale_id);
            })
            .fail(function(){
                $(modal_id + ' #modal-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
                $(modal_id + '#modal-loader').hide();
            });
        }
    </script>
@endpush
