<div class="form-group m-form__group row">
    <div class="col-md-6">
        <label class="col-form-label">{{ trans('common.table_no') }}</label>
        {!! Form::text('table_no', $sale->all_table, ['class' => 'form-control m-input', 'disabled']) !!}
    </div>
    <div class="col-md-6">
        <label class="col-form-label">{{ trans('common.sale_ref_id') }}</label>
        {!! Form::text('sale_ref_id', $sale->sale_ref_id, ['class' => 'form-control m-input', 'disabled']) !!}
    </div>
</div>
@foreach($sale->sale_details()->wherePaxType(config('sale.pax_type.pay'))->get() as $sale_detail)
    <div class="form-group m-form__group row">
        <div class="col-md-6">
            <label class="col-form-label">{{ $sale_detail->price_type_name }} ({{ $sale_detail->per_pax_price }})</label>
            {!! Form::text('adult_pay_pax', $sale_detail->pax, ['class' => 'form-control m-input', 'disabled']) !!}
        </div>
        <div class="col-md-6">
            <label class="col-form-label">{{ trans('common.total_pax_price') }}</label>
            {!! Form::text('grand_total', fundFormat($sale_detail->total_pax_price), ['class' => 'form-control m-input', 'disabled']) !!}
        </div>
    </div>
@endforeach
@if($freeSaleDetail = $sale->sale_details()->wherePaxType(config('sale.pax_type.free'))->wherePriceType(\App\Models\Price::ADULT_TYPE)->first())
    <div class="form-group m-form__group row">
        <div class="col-md-6">
            <label class="col-form-label">{{ $freeSaleDetail->price_type_name }} (Free Pax)</label>
            {!! Form::text('adult_pay_pax', $freeSaleDetail->pax, ['class' => 'form-control m-input', 'disabled']) !!}
        </div>
        <div class="col-md-6">
            <label class="col-form-label">{{ trans('common.total_pax_price') }}</label>
            {!! Form::text('grand_total',  $freeSaleDetail->total_pax_price , ['class' => 'form-control m-input', 'disabled']) !!}
        </div>
    </div>
@endif
<div class="form-group m-form__group row">
    <div class="col-md-6">
        <label class="col-form-label">{{ trans('common.total_pax') }}</label>
        {!! Form::text('total_pax', $sale->total_pax, ['class' => 'form-control m-input', 'disabled']) !!}
    </div>
    <div class="col-md-6">
        <label class="col-form-label">{{ trans('common.grand_total') }}</label>
        {!! Form::text('grand_total', fundFormat($sale->grand_total), ['class' => 'form-control m-input', 'disabled']) !!}
    </div>
</div>
<div class="form-group m-form__group row">
    <div class="col-md-6">
        <label class="col-form-label">{{ trans('common.start_at') }}</label>
        {!! Form::text('start_at', $sale->start_at, ['class' => 'form-control m-input', 'id' => 'start-at', 'disabled']) !!}
    </div>
    <div class="col-md-6">
        <label class="col-form-label">{{ trans('common.finish_at') }}</label>
        {!! Form::text('finish_at', $sale->finish_at, ['class' => 'form-control m-input', 'id' => 'finish-at', 'disabled']) !!}
    </div>
</div>

<table class="table m-table m-table--head-bg-brand" id="modal-order-table">
    <thead>
    <tr>
        <th>#</th>
        <th width="300">{{ trans('common.product_name') }}</th>
        <th>{{ trans('common.quantity') }}</th>
        <th>{{ trans('common.ala_carte_price') }}</th>
        <th>{{ trans('common.total_price') }}</th>
    </tr>
    </thead>
    <?php $count = 1; ?>
    <tbody>
    @if(count($sale->sale_orders) > 0)
        @foreach($sale->sale_orders as $sale_order)
            <tr>
                <td>{{ $count }}</td>
                <td>{{ $sale_order->product_name }}</td>
                <td>{{ $sale_order->quantity }}</td>
                <td>{{ $sale_order->ala_carte_price }}</td>
                <td>{{ $sale_order->total_price }}</td>
            </tr>
            <?php $count++; ?>
        @endforeach
    @else
        <tr>
            <td colspan="5" class="text-center">
                <h5>No Order Ala Carte</h5>
            </td>
        </tr>
    @endif
    </tbody>
</table>
<hr>

<div class="row">
    <div class="col-md-10 text-right">
        <h5>{{ trans('common.total_pax') }}</h5>
    </div>
    <div class="col-md-2 text-right">
        <h5 class="m--font-info" >
            {{ fundFormat($sale->totalPaxPriceWithoutFreePax()) }}
        </h5>
    </div>
</div>

@if($sale->sale_details()->wherePaxType(config('sale.pax_type.free'))->wherePriceType(\App\Models\Price::ADULT_TYPE)->exists())
    <div class="row">
        <div class="col-md-10 text-right">
            <h5>{{ trans('common.free_pax') }} ({{ $sale->sale_details()->wherePaxType(config('sale.pax_type.free'))->wherePriceType(\App\Models\Price::ADULT_TYPE)->first()->pax }})</h5>
        </div>
        <div class="col-md-2 text-right">
            <h5 class="m--font-info">
                {{fundFormat( -1 * abs($sale->totalFreePaxPrice())) }}
            </h5>
        </div>
    </div>
@endif

@if($sale->ala_carte_total > 0)
<div class="row">
    <div class="col-md-10 text-right">
        <h5>{{ trans('common.total_ala_carte_order') }}</h5>
    </div>
    <div class="col-md-2 text-right">
        <h5 class="m--font-info" >
            {{ $sale->ala_carte_total }}
        </h5>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-10 text-right">
        <h5>{{ trans('common.discount_type') }}</h5>
    </div>
    <div class="col-md-2 text-right">
        <h5 class="m--font-info">
            @if($sale->is_paid)
                {{ $sale->explainDiscountType() }}
            @else
                {!! Form::select('discount_type', ['fixed' => 'Fixed', 'percentage' => 'Percentage'], 'fixed', ['class' => 'form-control m-input', 'id' => 'discount-type']) !!}
            @endif
        </h5>
    </div>
</div>

<div class="row">
    <div class="col-md-10 text-right">
        <h5>{{ trans('common.discount') }}</h5>
    </div>
    <div class="col-md-2 text-right">
        <h5 class="m--font-info">
            @if($sale->is_paid)
                {{ $sale->discount }}
            @else
                {!! Form::text('discount_total', null, ['class' => 'form-control m-input fund-input', 'id' => 'sale-discount']) !!}
            @endif
        </h5>
    </div>
</div>

@if($sale->sub_total > 0)
    <div class="row">
        <div class="col-md-10 text-right">
            <h5>{{ trans('common.sub_total') }}</h5>
        </div>
        <div class="col-md-2 text-right">
            <h5 class="m--font-info" id="modal-subtotal-price">
                {{ $sale->sub_total }}
            </h5>
            <input type="hidden" id="hidden-subtotal-price" value="{{ $sale->sub_total }}">
        </div>
    </div>
@endif

@if ($sale->tax_total > 0)
    <div class="row">
        <div class="col-md-10 text-right">
            <h5>{{ $sale->tax_name }}</h5>
        </div>
        <div class="col-md-2 text-right">
            <h5 class="m--font-info" id="modal-tax-price">
                {{ $sale->tax_total }}
            </h5>
        </div>
    </div>
@endif

<div class="row">
    <div class="col-md-10 text-right">
        <h5>{{ trans('common.grand_total') }}</h5>
    </div>
    <div class="col-md-2 text-right">
        <h5 class="m--font-info" id="modal-grandtotal-price">
            {{ $sale->grand_total }}
        </h5>
        <input type="hidden" id="hidden-grandtotal-price" value="{{ $sale->grand_total }}">
    </div>
</div>

<div class="row">
    <div class="col-md-10 text-right">
        <h5>{{ trans('common.payment_method') }}</h5>
    </div>
    <div class="col-md-2 text-right">
        <h5 class="m--font-info">
            @if($sale->is_paid)
                {{ $sale->explainPaymentMethod() }}
            @else
                {!! Form::select('payment_method', \App\Models\Sale::getPaymentMethodLists(), 1, ['class' => 'form-control m-input']) !!}
            @endif
        </h5>
    </div>
</div>

<div class="row">
    <div class="col-md-10 text-right">
        <h5>{{ trans('common.paid') }}</h5>
    </div>
    <div class="col-md-2 text-right">
        <h5 class="m--font-info">
            @if($sale->is_paid)
                {{ $sale->paid }}
            @else
                {!! Form::text('paid', null, ['class' => 'form-control m-input fund-input', 'id' => 'sale-paid']) !!}
            @endif
        </h5>
    </div>
</div>

<div class="row">
    <div class="col-md-10 text-right">
        <h5>{{ trans('common.change') }}</h5>
    </div>
    <div class="col-md-2 text-right">
        <h5 class="m--font-info">
            @if($sale->is_paid)
                {{ $sale->calculateChange() }}
            @else
                <h5 class="m--font-info" id="modal-sale-change">0.00</h5>
            @endif
        </h5>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <h5>{{ trans('common.remark') }}</h5>
        {!! Form::textarea('remark', null, ['class' => 'form-control', 'rows' => 4]) !!}
    </div>
</div>

@if(!$sale->is_paid)
    <hr>
    <button type="button" class="btn btn-success btn-block" id="btn-paid">
        <i class="fa fa-shopping-cart"></i> {{ trans('common.paid') }}
    </button>
    <a href="{{ route('staff.sale.order-statement', $sale->id) }}" target="_blank" rel="noreferrer" class="btn btn-info btn-block" id="print-statement-btn">
        <i class="fa fa-print"></i> {{ trans('common.print_order_statement') }}
    </a>
@else
    <a href="{{ route('staff.sale.receipt', $sale->id) }}" target="_blank" rel="noreferrer" class="btn btn-info btn-block" id="print-receipt-btn">
        <i class="fa fa-print"></i> {{ trans('common.print_receipt') }}
    </a>
@endif
