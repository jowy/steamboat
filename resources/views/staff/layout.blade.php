<!DOCTYPE html>
<html lang="{{ $_G['lang'] }}">
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
        @if ($_G['setting']['site_name_' . $_G['lang']])
            {{ $_G['setting']['site_name_' . $_G['lang']] }}
        @endif
        @yield('title')
        @if ($_G['setting']['slogan_' . $_G['lang']])
            {{ $_G['setting']['slogan_' . $_G['lang']] }}
        @endif
    </title>
    <meta name="description" content="@yield('description')">
    <meta content="@yield('author')" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @stack('metatag')
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700","Asap+Condensed:500"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    @include('staff.include.header_script')
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{ $_G['setting']['user_navigation_logo_' . $_G['lang']] }}"  />
    @stack('header')
</head>
<!-- end::Head -->
<!-- end::Body -->
<body  class="m-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default"  >
<!-- begin::Page loader -->
<div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
				<span>
					Please wait...
				</span>
        <span>
					<div class="m-loader m-loader--brand"></div>
				</span>
    </div>
</div>
<!-- end::Page Loader -->
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- begin::Header -->
    <header id="m_header" class="m-grid__item m-header "  m-minimize="minimize" m-minimize-mobile="minimize" m-minimize-offset="10" m-minimize-mobile-offset="10" >
        <div class="m-header__top">
            <div class="m-container m-container--fluid m-container--full-height m-page__container">
                <div class="m-stack m-stack--ver m-stack--desktop">
                    @include('staff.include.header')
                </div>
            </div>
        </div>
        <div class="m-header__bottom">
            <div class="m-container m-container--fluid m-container--full-height m-page__container">
                <div class="m-stack m-stack--ver m-stack--desktop">
                    <!-- begin::Horizontal Menu -->
                    <div class="m-stack__item m-stack__item--fluid m-header-menu-wrapper">
                        @include('staff.include.sidebar')
                    </div>
                    <!-- end::Horizontal Menu -->
                </div>
            </div>
        </div>
    </header>
    <!-- end::Header -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop m-page__container m-body">
        <div class="m-grid__item m-grid__item--fluid">
            <div class="m-content">
                @include('staff.include.flash')
                @yield('content')
            </div>
        </div>
    </div>
    <!-- end::Body -->
    @include('staff.sale.modals.create_sale')
    @include('staff.sale.modals.create_order_sale')
    @include('staff.include.footer')
</div>
<!-- end:: Page -->
<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>
<!-- begin::Quick Nav -->
<!--begin::Base Scripts -->
@include('staff.include.footer_script')
@stack('footer')
<script>
    $(window).on('load', function() {
        $('body').removeClass('m-page--loading');
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#table-select, #table-order-select, #change-table-select').select2({
        width: '100%'
    });

    $('#add-sale-modal').on('click', function() {
        $.ajax({
            type: 'GET',
            url: '{{ route('staff.sale.get-branch-table') }}',
            beforeSend: function() {
                $('#table-select-body').hide();
                $('#table-select')
                    .find('option')
                    .remove();
                $('#add-sale-loader').html('<div class="m-loader m-loader--primary"></div>');
            },
            success: function(data) {
                $('#sale-modal #table-select-body').show();
                $.each(data.branch_tables, function(index, item) {
                    $('#sale-modal #table-select').append('<option value="'+ item.id +'">'+ item.table_no +'</option>');
                });
            },
            error: function(xhr, status, error) {
                alert(error + '. Please refresh the page and try again');
            },
            complete: function () {
                $('#add-sale-loader').empty();
            },
            dataType: 'json'
        });
    });


    var $addSaleSubmited = false;
    $('#add-sale-form').on('click', '#btn-add-sale', function() {
        var btn = $(this);
        btn.attr('disabled', 'disabled');
        $addSaleSubmited = true;
        $.ajax({
            type: 'POST',
            url: '{{ route('staff.sale.store') }}',
            data: $('#add-sale-form').serializeArray(),
            beforeSend: function() {
                blockBody();
            },
            success: function(data) {
                unblockBody();
                alertSuccess(data.message, true, function () {
                    //window.open(data.redirectTo, '_blank');
                    const a = document.createElement("a");
                    a.href = data.redirectTo;
                    a.target = "_blank";
                    a.rel = "noreferrer";
                    a.click();
                    window.location.replace("{{ route('staff.sale.index') }}");
                });
            },
            error: function(xhr, status, error) {
                unblockBody();
                handleError(xhr);
                $paidFormSubmited = false;
                setTimeout(function(){
                    btn.removeAttr('disabled', 'disabled');
                }, 500);
            },
            dataType: 'json'
        });
    });

    $('#add-order-sale-modal').on('click', function() {
        $.ajax({
            type: 'GET',
            url: '{{ route('staff.sale.get-ala-carte-stock') }}',
            beforeSend: function() {
                $('#add-order-ala-carte-body').hide();
                $('#select-order-ala-carte')
                    .find('option')
                    .remove();
                $('#add-order-sale-loader').html('<div class="m-loader m-loader--primary"></div>');
            },
            success: function(data) {
                $('#add-order-ala-carte-body').show();
                $.each(data.stocks, function(index, item) {
                    $('#select-order-ala-carte').append('<option value="'+ item.id +'">'+ item.product_name_en +'</option>');
                });
            },
            error: function(xhr, status, error) {
                alert(error + '. Please refresh the page and try againn');
            },
            complete: function () {
                $('#add-order-sale-loader').empty();
            },
            dataType: 'json'
        });
    });

    $(".add-order-stock-btn").click(function () {
        var stock_id = $("#select-order-ala-carte").val();
        var stock_name = $("#select-order-ala-carte option:selected").text();
        var quantity = $("#select-order-quantity").val();

        if (stock_id == '' || stock_name == '') {
            alert('Some error happened, please try again');
            return false;
        }
        if (quantity == '' || !$.isNumeric(quantity)) {
            alert('Please key in quantity or quantity must be integer');
            return false;
        }
        var $unique_stock_id = $('#add-order-stock-table').find('input.hidden_stock_id:hidden[value=' + stock_id + ']');

        if ($unique_stock_id.length > 0) {
            // If there is stock id then remove and insert again
            $unique_stock_id.parents("tr").remove();
        }
        var markup = "<tr><td><input type='checkbox' name='record'></td><td>"+ stock_name +"</td><td><input type='text' name='add_stock_details["+ stock_id +"][quantity]' value='"+ quantity +"' class='form-control'></td><input type='hidden' name='add_stock_details["+ stock_id +"][product_id]' class='hidden_stock_id' value='"+ stock_id +"'></tr>";
        $("table#add-order-stock-table tbody").append(markup);
        $('#submit-add-stock-btn').prop('disabled', false);
    });

    // Find and remove selected table rows
    $(".delete-order-stock").click(function(){
        $("table#add-order-stock-table tbody").find('input[name="record"]').each(function(){
            if($(this).is(":checked")){
                $(this).parents("tr").remove();
            }
        });
        if ($("#add-stock-table > tbody > tr").length == null || $("#add-stock-table > tbody > tr").length == 0){
            $('#submit-add-stock-btn').prop('disabled', true);
        }
    });

    var $addOrderSaleSubmited = false;
    $('#add-order-sale-form').on('click', '#btn-add-order-sale', function() {
        var btn = $(this);
        btn.attr('disabled', 'disabled');
        $addOrderSaleSubmited = true;
        $.ajax({
            type: 'POST',
            url: '{{ route('staff.sale.store-ala-carte') }}',
            data: $('#add-order-sale-form').serializeArray(),
            beforeSend: function() {
                blockBody();
            },
            success: function(data) {
                unblockBody();
                alertSuccess(data.message, true, function () {
                    //window.open(data.redirectTo, '_blank');
                    const a = document.createElement("a");
                    a.href = data.redirectTo;
                    a.target = "_blank";
                    a.rel = "noreferrer";
                    a.click();
                    window.location.replace("{{ route('staff.sale.index') }}");
                });
            },
            error: function(xhr, status, error) {
                unblockBody();
                handleError(xhr);
                $addOrderSaleSubmited = false;
                setTimeout(function(){
                    btn.removeAttr('disabled', 'disabled');
                }, 500);
            },
            dataType: 'json'
        });
    });
</script>
<!-- end::Page Loader -->
<div class="modal fade modal-primary" id="remote-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">

        </div>
    </div>
</div>
<div class="modal fade modal-primary" id="remote-modal-large" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-simple modal-lg">
        <div class="modal-content">

        </div>
    </div>
</div>
<div class="modal fade modal-primary" id="remote-modal-full" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-simple modal-lg">
        <div class="modal-content">

        </div>
    </div>
</div>
</body>
<!-- end::Body -->
</html>
