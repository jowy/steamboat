<!-- begin::Brand -->
<div class="m-stack__item m-brand m-stack__item--left">
    <div class="m-stack m-stack--ver m-stack--general m-stack--inline">
        <div class="m-stack__item m-stack__item--middle m-brand__logo">
            <a href="{{ route('staff.sale.index') }}" class="m-brand__logo-wrapper">
                <img alt="" src="{{ $_G['setting']['user_navigation_logo_' . $_G['lang']] }}" class="m-brand__logo-desktop" width="60"/>
                <img alt="" src="{{ $_G['setting']['user_navigation_logo_' . $_G['lang']] }}" class="m-brand__logo-mobile" width="60"/>
            </a>
        </div>
        <div class="m-stack__item m-stack__item--middle m-brand__tools">
            <!-- begin::Responsive Header Menu Toggler-->
            <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                <span></span>
            </a>
            <!-- end::Responsive Header Menu Toggler-->
            <!-- begin::Topbar Toggler-->
            <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                <i class="flaticon-more"></i>
            </a>
            <!--end::Topbar Toggler-->
        </div>
    </div>
</div>

<!-- begin::Topbar -->
<div class="m-stack__item m-stack__item--right m-header-head" id="m_header_nav">
    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
        <div class="m-stack__item m-topbar__nav-wrapper">
            <ul class="m-topbar__nav m-nav m-nav--inline">
                <li class="m-nav__item">
                    <a href="#" class="m-nav__link m-dropdown__toggle" style="padding: 20px; font-size: 25px;">
                        {{ now()->format('Y-m-d') }}
                    </a>
                </li>
                <li class="m-nav__item m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                    <a href="#" class="m-nav__link m-dropdown__toggle">
                        <span class="m-nav__link-icon">
                            <span class="m-nav__link-icon-wrapper">
                                <i class="la la-user"></i>
                            </span>
                        </span>
                    </a>
                    <div class="m-dropdown__wrapper">
                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                        <div class="m-dropdown__inner">
                            <div class="m-dropdown__header m--align-center">
                                <div class="m-card-user m-card-user--skin-light">
                                    <div class="m-card-user__details">
                                        <span class="m-card-user__name m--font-weight-500">
                                            {{ \Auth::guard('staff')->user()->name }}
                                        </span>
                                        <a href="" class="m-card-user__email m--font-weight-300 m-link">
                                            {{ \Auth::guard('staff')->user()->email }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="m-dropdown__body">
                                <div class="m-dropdown__content">
                                    <ul class="m-nav m-nav--skin-light">
                                        <li class="m-nav__section m--hide">
                                            <span class="m-nav__section-text">
                                                Section
                                            </span>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="{{ route('staff.profile') }}" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                <span class="m-nav__link-title">
                                                    <span class="m-nav__link-wrap">
                                                        <span class="m-nav__link-text">
                                                            My Profile
                                                        </span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li class="m-nav__separator m-nav__separator--fit"></li>
                                        <li class="m-nav__item">
                                            <a href="{{ route('staff.logout') }}" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
                                                Logout
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- end::Topbar -->
