<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light " id="m_aside_header_menu_mobile_close_btn">
    <i class="la la-close"></i>
</button>
<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light "  >
    <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
        <li class="m-menu__item  {{ in_array($_G['route'], ['staff.sale.create', 'staff.sale.index', 'staff.sale.join-bill', 'staff.close_account', 'staff.account']) ? ' m-menu__item--active  m-menu__item--active-tab' : '' }}  m-menu__item--submenu m-menu__item--tabs"  m-menu-submenu-toggle="tab" aria-haspopup="true">
            <a  href="javascript:;" class="m-menu__link m-menu__toggle">
                <span class="m-menu__link-text">
                    {{ trans('common.sales') }}
                </span>
                <i class="m-menu__hor-arrow la la-angle-down"></i>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left m-menu__submenu--tabs">
                <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item {{ $_G['route'] == 'staff.sale.create' ? ' m-menu__item--active' : '' }}"  m-menu-link-redirect="1" aria-haspopup="true">
                        <a href="javascript:void(0)" id="add-sale-modal" class="m-menu__link" data-toggle="modal" data-target="#sale-modal">
                            <i class="m-menu__link-icon fa fa-plus"></i>
                            <span class="m-menu__link-text">
                                {{ trans('common.add_sale') }}
                            </span>
                        </a>
                    </li>

                    <li class="m-menu__item {{ $_G['route'] == 'staff.sale.create' ? ' m-menu__item--active' : '' }}"  m-menu-link-redirect="1" aria-haspopup="true">
                        <a href="javascript:void(0)" id="add-order-sale-modal" class="m-menu__link" data-toggle="modal" data-target="#order-sale-modal">
                            <i class="m-menu__link-icon fa fa-plus-square"></i>
                            <span class="m-menu__link-text">
                                {{ trans('common.add_ala_carte_sale') }}
                            </span>
                        </a>
                    </li>

                    <li class="m-menu__item {{ in_array($_G['route'], ['staff.sale.index']) ? ' m-menu__item--active' : '' }}"  m-menu-link-redirect="1" aria-haspopup="true">
                        <a  href="{{ route('staff.sale.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-icon fa fa-list"></i>
                            <span class="m-menu__link-text">
                                {{ trans('common.sale_listing') }}
                            </span>
                        </a>
                    </li>

                    <li class="m-menu__item{{ $_G['route'] == 'staff.sale.join-bill' ? ' m-menu__item--active' : '' }}"  m-menu-link-redirect="1" aria-haspopup="true">
                        <a href="{{ route('staff.sale.join-bill') }}" class="m-menu__link">
                            <i class="m-menu__link-icon fa fa-link"></i>
                            <span class="m-menu__link-text">
                                {{ trans('common.join_bill') }}
                            </span>
                        </a>
                    </li>

                    <li class="m-menu__item{{ $_G['route'] == 'staff.account' ? ' m-menu__item--active' : '' }}"  m-menu-link-redirect="1" aria-haspopup="true">
                        <a href="{{ route('staff.account') }}" class="m-menu__link">
                            <i class="m-menu__link-icon fa fa-history"></i>
                            <span class="m-menu__link-text">
                                {{ trans('common.account_history') }}
                            </span>
                        </a>
                    </li>

                    <li class="m-menu__item"  m-menu-link-redirect="0" aria-haspopup="true">
                        <a href="{{ route('staff.account.submit') }}" class="m-menu__link" data-toggle="modal" data-target="#remote-modal">
                            <i class="m-menu__link-icon fa fa-calculator"></i>
                            <span class="m-menu__link-text">
                                {{ trans('common.account_adjustment') }}
                            </span>
                        </a>
                    </li>

                    <li class="m-menu__item"  m-menu-link-redirect="0" aria-haspopup="true">
                        <a href="{{ route('staff.account.summary') }}" class="m-menu__link" data-toggle="modal" data-target="#remote-modal">
                            <i class="m-menu__link-icon fa fa-close"></i>
                            <span class="m-menu__link-text">
                                {{ trans('common.close_account') }}
                            </span>
                        </a>
                    </li>

{{--                    <li class="m-menu__item {{ in_array($_G['route'], ['staff.sale.history', 'staff.sale.history-detail']) ? ' m-menu__item--active' : '' }}"  m-menu-link-redirect="1" aria-haspopup="true">
                        <a  href="{{ route('staff.sale.history') }}" class="m-menu__link ">
                            <i class="m-menu__link-icon fa fa-history"></i>
                            <span class="m-menu__link-text">
                                {{ trans('common.sale_histories') }}
                            </span>
                        </a>
                    </li>--}}
                </ul>
            </div>
        </li>

        <li class="m-menu__item  {{ in_array($_G['route'], ['staff.sale.history', 'staff.sale.history-detail']) ? ' m-menu__item--active  m-menu__item--active-tab' : '' }}  m-menu__item--submenu m-menu__item--tabs"  m-menu-submenu-toggle="tab" aria-haspopup="true">
            <a  href="javascript:;" class="m-menu__link m-menu__toggle">
                <span class="m-menu__link-text">
                    {{ trans('common.listing') }}
                </span>
                <i class="m-menu__hor-arrow la la-angle-down"></i>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left m-menu__submenu--tabs">
                <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item {{ in_array($_G['route'], ['staff.sale.history', 'staff.sale.history-detail']) ? ' m-menu__item--active' : '' }}"  m-menu-link-redirect="1" aria-haspopup="true">
                        <a  href="{{ route('staff.sale.history') }}" class="m-menu__link ">
                            <i class="m-menu__link-icon fa fa-history"></i>
                            <span class="m-menu__link-text">
                                {{ trans('common.sale_histories') }}
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="m-menu__item  {{ in_array($_G['route'], ['staff.profile']) ? ' m-menu__item--active  m-menu__item--active-tab' : '' }}  m-menu__item--submenu m-menu__item--tabs"  m-menu-submenu-toggle="tab" aria-haspopup="true">
            <a  href="javascript:;" class="m-menu__link m-menu__toggle">
                <span class="m-menu__link-text">
                    {{ trans('common.setting') }}
                </span>
                <i class="m-menu__hor-arrow la la-angle-down"></i>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left m-menu__submenu--tabs">
                <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item {{ $_G['route'] == 'staff.profile' ? ' m-menu__item--active' : '' }}"  m-menu-link-redirect="1" aria-haspopup="true">
                        <a  href="{{ route('staff.profile') }}" class="m-menu__link ">
                            <i class="m-menu__link-icon fa fa-user"></i>
                            <span class="m-menu__link-text">
                                {{ trans('common.my_profile') }}
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</div>
