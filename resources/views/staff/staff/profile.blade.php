@extends('staff.layout')

@section('title')
    {{ trans('common.profile') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header') @endpush

@section('action') @endsection

@section('content')

        <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                <i class="flaticon-share m--hide"></i>
                                {{ __('common.profile') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="m_user_profile_tab_1">
                    {!! Form::model($profile, ['method' => 'post', 'role' => 'form', 'id' => 'profile-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
                    {{--<form class="m-form m-form--fit m-form--label-align-right">--}}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    {{ trans('common.username') }}
                                </label>
                                <div class="col-7">
                                    {!! Form::text('username', null, ['class' => 'form-control m-input']) !!}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    {{ trans('common.name') }}
                                </label>
                                <div class="col-7">
                                    {!! Form::text('name', null, ['class' => 'form-control m-input']) !!}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    {{ trans('common.contact_number') }}
                                </label>
                                <div class="col-7">
                                    {!! Form::text('contact_number', null, ['class' => 'form-control m-input']) !!}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    {{ trans('common.email') }}
                                </label>
                                <div class="col-7">
                                    {!! Form::text('email', null, ['class' => 'form-control m-input']) !!}
                                </div>
                            </div>
                            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                            <div class="form-group m-form__group row">
                                <div class="col-10 ml-auto">
                                    <h3 class="m-form__section">
                                        {{ __('common.set_new_password') }}
                                    </h3>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    {{ trans('common.password') }}
                                </label>
                                <div class="col-7">
                                    {!! Form::password('password', ['class' => 'form-control m-input']) !!}
                                    <span class="m-form__help">{{ trans('common.leave_blank_not_change') }}</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    {{ trans('common.password_confirmation') }}
                                </label>
                                <div class="col-7">
                                    {!! Form::password('password_confirmation', ['class' => 'form-control m-input']) !!}
                                    <span class="m-form__help">{{ trans('common.leave_blank_not_change') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-7">
                                        <button type="button" class="btn btn-accent m-btn m-btn--air m-btn--custom" id="btn-submit">
                                            {{ trans('common.update') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {{--</form>--}}
                    {!! Form::close() !!}
                </div>

            </div>
        </div>

@endsection

@push('footer')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#profile-form').makeAjaxForm({
                submitBtn: '#btn-submit',
                redirectTo: '{{ route('staff.profile') }}',
            });
        })
    </script>
@endpush
