@extends('ajaxmodal')

@section('title')
    {{ trans('common.close_account') }}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                // $('#data-form').makeAjaxForm({
                //     inModal: true,
                //     closeModal: true,
                //     submitBtn: '#btn-submit',
                //     afterSuccessFunction: function(response, $el, $this) {
                //         $('.modal-backdrop').remove();
                //
                //         if (typeof response.redirectTo !== 'undefined') {
                //             window.open(response.redirectTo, '_blank');
                //             location.reload();
                //         }
                //
                //         location.reload();
                //         return true;
                //     }
                // });

                $('#btn-submit').on('click', function () {
                    $('#data-form').submit();
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit" class="btn btn-primary blue">{{ trans('common.submit') }}</button>
@endsection

@section('content')
    {!! Form::open(['route' => 'staff.account.summary.post', 'method' => 'post', 'role' => 'form', 'id' => 'data-form', 'class' => 'm-form m-form--fit m-form--label-align-right', 'target' => '_blank']) !!}
        <div class="form-group m-form__group">
            <label>{{ trans('common.start_datetime') }}</label>
            {!! Form::text('start_at', now()->startOfDay()->format('Y-m-d H:i'), ['class' => 'form-control m-input date-picker']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.end_datetime') }}</label>
            {!! Form::text('end_at', now()->format('Y-m-d H:i'), ['class' => 'form-control m-input date-picker']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.cash_received') }}</label>
            {!! Form::text('cash_collected', '', ['class' => 'form-control m-input fund-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.current_password') }}</label>
            {!! Form::password('current_password', ['class' => 'form-control m-input']) !!}
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection
