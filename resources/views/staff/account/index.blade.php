@extends('staff.layout')

@section('title')
    {{ trans('common.account_history') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@section('content')
    <div class="m-section m-wrapper">
        <div class="m-section__content">
            <table class="table table-hover table-bordered table-striped" id="data-dt">
                <thead>
                <tr role="row" class="heading">
                    <th width="80">#</th>
{{--                    <th>{{ trans('common.action') }}</th>--}}
                    <th>{{ trans('common.type') }}</th>
                    <th>{{ trans('common.amount') }}</th>
                    <th>{{ trans('common.remark') }}</th>
                    <th>{{ trans('common.created_at') }}</th>
                    <th>{{ trans('common.action') }}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#data-dt'),
                    showIndex: true,
                    filterContainer: '#filter-container',
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('staff.account.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            // {data: 'actions', name: 'actions', orderable: false, searchable: false},
                            {data: 'type', name: 'type'},
                            {data: 'amount', name: 'amount'},
                            {data: 'remark', name: 'remark'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false},
                        ],
                        dom: 'lrtip',
                        "processing": true
                    }
                });
            });
        }(jQuery);
    </script>
@endpush
