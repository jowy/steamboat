@extends('ajaxmodal')

@section('title')
    {{ trans('common.account_adjustment') }}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#data-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit',
                    afterSuccessFunction: function(response, $el, $this) {
                        $('.modal-backdrop').remove();

                        if (typeof response.redirectTo !== 'undefined') {
                            window.open(response.redirectTo, '_blank');
                            location.reload();
                        }

                        location.reload();
                        return true;
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit" class="btn btn-primary blue">{{ trans('common.submit') }}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['staff.account.update', $staffAccount->id], 'method' => 'put', 'role' => 'form', 'id' => 'data-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
    <div class="form-group m-form__group">
        <label>{{ trans('common.type') }}</label>
        {!! Form::select('type', \App\Models\StaffAccount::getTypeLists(), $staffAccount->type, ['class' => 'form-control m-input']) !!}
    </div>
    <div class="form-group m-form__group">
        <label>{{ trans('common.amount') }}</label>
        {!! Form::text('amount', fundFormat($staffAccount->amount), ['class' => 'form-control m-input fund-input']) !!}
    </div>
    <div class="form-group m-form__group">
        <label>{{ trans('common.remark') }}</label>
        {!! Form::textarea('remark', $staffAccount->remark, ['class' => 'form-control m-input', 'rows' => 4]) !!}
    </div>
    <div class="form-group m-form__group">
        <label>{{ trans('common.current_password') }}</label>
        {!! Form::password('current_password', ['class' => 'form-control m-input']) !!}
    </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection
