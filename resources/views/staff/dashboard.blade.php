@extends('staff.layout')

@section('title')
    {{ trans('common.dashboard') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header')
    <style>
        ul {
            list-style: none !important;
            padding: 0;
        }
        .ellipses {
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
        }
        .product-border {
            padding: 25px 5px;
            margin-bottom: 22px;
            /*line-height: 1.6;*/
            background-color: #f5f8fa;
            border: 1px solid #dadada;
            border-radius: 4px;
            cursor: pointer;
        }
        .cart-table {
            background-color: #e5f4ff;
            padding: 10px;
            height: 500px;
            overflow-x: auto;
        }
        .cart-total {
            border-radius:10px;
            border: 1px solid #c3c3c3;
            overflow: hidden;
        }
        #filter_name {
            margin-bottom: 20px;
        }
        .pagination li {
            display:inline-block;
            padding:5px;
        }
        .cart-total > div{ background-color: #e5f4ff; height: 200px; }
    </style>
@endpush

@section('action') @endsection

@section('content')
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.list_products') }}
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <div class="m-portlet__body">
            <div class="m-form__actions m-form__actions row">
                <div id="hacker-list" class="col-md-7">
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::text('filter_name', null, ['class' => 'form-control m-input', 'placeholder' => 'Search for any product name/product code', 'id' => 'filter_name']) !!}
                        </div>
                    </div>
                    <ul class="list row table" id="product-list"></ul>
                    <ul class="pagination"></ul>
                </div>
                <div class="col-md-5">
                    <div class="m-section cart-table">
                        <div class="m-section__content">
                            <form id="cart-content">
                                <table class="table m-table m-table--head-separator-primary" id="cartcontent">
                                    <thead>
                                    <tr>
                                        <th field="name"> {{ trans('common.product_name') }}</th>
                                        <th field="quantity"> {{ trans('common.quantity') }}</th>
                                        <th field="price"> {{ trans('common.price') }}</th>
                                        <th field="remove" style="text-align: center;" width="30"> {{ trans('common.remove') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </form>
                        </div>
                    </div>

                    <div class="cart-total">
                        <div class="m-section__content">
                            <table class="table m-table m-table--head-separator-primary cart-total-table">
                                <tbody>
                                <tr>
                                    <td>{{ trans('common.grand_total') }}</td>
                                    <td id="grand_total_price">RM 0</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('common.items') }}</td>
                                    <td id="items_count">0</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#checkout-modal" id="purchase-btn">
                                            <i class="fa fa-shopping-cart"></i> {{ trans('common.purchase') }}
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--begin::Modal-->
                    <div class="modal fade" id="checkout-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        {!! Former::vertical_open()->action(route('staff.sale.checkout'))->id('cart-form') !!}
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">
                                        {{ trans('common.checkout') }}
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">
                                        &times;
                                    </span>
                                    </button>
                                </div>
                                <div class="modal-body" id="checkout-modal-body">
                                    <div id="checkout-modal-spinner"></div>
                                    <table class="table m-table m-table--head-bg-brand" id="modal-checkout-table">
                                        <thead>
                                        <tr>
                                            <th> {{ trans('common.product_name') }}</th>
                                            <th> {{ trans('common.quantity') }}</th>
                                            <th> {{ trans('common.price') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-9 text-right">
                                            <h5>{{ trans('common.grand_total') }}</h5>
                                        </div>
                                        <div class="col-md-3 text-right">
                                            <h5 class="m--font-info" id="modal-subtotal-price">0.00</h5>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            <h5>{{ trans('common.paid') }}</h5>
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                RM
                                            </span>
                                            </div>
                                            <input type="text" id="paid-price" onkeypress="return isNumberKey(this)" class="form-control m-input" placeholder="10" aria-describedby="basic-addon1" pattern="^[0-9]*\.[0-9]{2}$">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 text-right">
                                            <h5>{{ trans('common.change') }}</h5>
                                        </div>
                                        <div class="col-md-3 text-right">
                                            <h5 class="m--font-info" id="modal-change-cost">0.00</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success btn-block" id="btn-checkout">
                                        <i class="fa fa-shopping-cart"></i> {{ trans('common.checkout') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Former::close() !!}
                    </div>
                    <!--end::Modal-->
                </div>
            </div>
        </div>

        <!--end::Form-->
    </div>
@endsection

@push('footer')
    <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            productListing();

            $('#cartcontent').on('click', '.remove-product', function () {
                removeProduct(this, event);
            });

            $("ul#product-list").delegate('.product-info', 'click', function() {
                var $id = $(this).children('.product_id').text();
                var $name = $(this).children('.caption').children('.product_name_en').text();
                var $price = parseFloat($(this).children('.retail_price').text());

                addProduct($id,$name,$price);
            });

            $('#purchase-btn').on('click', function() {
                $.ajax({
                    type: 'GET',
                    url: '{{ route('staff.sale.confirmation') }}',
                    data: $('#cart-content').serializeArray(),
                    beforeSend: function() {
                        $('#modal-checkout-table tbody').empty();
                        $('#modal-checkout-table tbody').hide();
                        $('#checkout-modal-spinner').html('<div class="m-loader m-loader--primary"></div>');
                    },
                    success: function(data) {
                        $('#checkout-modal-spinner').empty();
                        $('#modal-checkout-table tbody').show();
                        if (data.status == 1) {
                            $.each(data.checkoutList, function(key, value) {
                                $('#modal-checkout-table tbody').append("<tr><td>" + value.product_name + "</td><td>" + value.retail_price + "</td><td>" + value.quantity + "</td><input type='hidden' name='cart_product["+ value.product_id +"][id]' value='"+ value.product_id +"'><input type='hidden' name='cart_product["+ value.product_id +"][quantity]' value='"+ value.quantity +"'></tr>");
                            });
                            $('#modal-subtotal-price').html(data.subTotal);

                        } else {
                            alert(data.msg);
                        }
                    },
                    error: function(xhr) {
                        // if error occured
                        /*alert("Error occured.please try again");
                        $(placeholder).append(xhr.statusText + xhr.responseText);
                        $(placeholder).removeClass('loading');*/
                    },
                    dataType: 'json'
                });
            });

            $('#paid-price').on('change keyup', function(event) {
                if ($(this).val()) {
                    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                        event.preventDefault();
                    }
                    var input = $(this).val();
                    if ((input.indexOf('.') != -1) && (input.substring(input.indexOf('.')).length > 2)) {
                        event.preventDefault();
                    }
                    var $subtotal = parseFloat($("#modal-subtotal-price").text());
                    var $paid_price = parseFloat($(this).val());
                    var total = $paid_price - $subtotal;
                    $("#modal-change-cost").text(total.toFixed(2));
                } else {
                    $("#modal-change-cost").text("0.00");
                }
            });

            $('#cart-form').makeAjaxForm({
                beforeFunction: function ($el, e) {
                    var isConfirm = confirm('Confirm to order?');
                    if (isConfirm) {
                        return true;
                    } else {
                        setTimeout(function(){ $el.find("#btn-checkout").removeAttr('disabled'); }, 500);
                        return false;
                    }
                },
                submitBtn: '#btn-checkout',
                redirectTo: '{{ route('staff.sale.index') }}',
            });
        });

        var data = {"total":0,"rows":[]};
        var totalCost = 0;

        // Listing product
        function productListing(filterName)
        {
            $.get("{{ route('staff.sale.product') }}"/*, { filter_name: filterName }*/, function(result) {
                var options = {
                    valueNames: ['product_id', 'product_name_en', 'retail_price', 'product_code'],
                    item: '<li class="col-md-3">' +
                    '<div class="product-border thumbnail product-info">' +
                    '<span class="m--hide product_id"></span>' +
                    '<span class="m--hide retail_price"></span>' +
                    '<span class="m--hide product_code"></span>' +
                    '<div class="caption text-center">' +
                    '<h6 class="product_name_en m--font-primary ellipses"></h6> <!--<p class="retail_price"></p>-->' +
                    '</div>' +
                    '</div>' +
                    '</li>',
                    page: 10,
                    pagination: true
                };

                var values = result.data;

                var hackerList = new List('hacker-list', options, values);

                // Search product
                $('#filter_name').bind("keyup change", function() {
                    hackerList.search($(this).val(), ['product_name_en', 'product_code']); // Only search in the 'name' column
                });

                // All row same height
                $("ul.list").each(function(){
                    var largest = 0;

                    $(this).find(".caption").each(function(){
                        var findHeight = $(this).height();
                        if(findHeight > largest){
                            largest = findHeight;
                        }
                    })
                    $(this).find(".caption").css({"height":largest+"px"});
                });
            })
                .fail(function() {
                    //alert( "error" );
                });
        }

        // Add product to cart
        function addProduct(id,name,price)
        {
            function add(){
                for(var i=0; i<data.total; i++){
                    var row = data.rows[i];
                    if (row.id == id){
                        row.quantity += 1;
                        return;
                    }
                }
                data.total += 1;
                data.rows.push({
                    id: id,
                    name: name,
                    quantity: 1,
                    price: price,
                    remove: '<a href="javascript:void(0)" class="remove-product"> <i class="fa fa-trash-o"></i> </a>'
                });
            }
            add();
            totalCost += price;
            // Empty table and append again
            $('#cartcontent tbody').empty();
            $.each(data.rows, function (key, val) {
                $('#cartcontent tbody').append("<tr><td field='name'>"+ val.name +"</td><td field='quantity'>"+ val.quantity +"</td><td field='price'>"+ val.price +"</td><td align='center'>"+ val.remove +"</td><input type='hidden' name='product_id' value='"+ val.id +"'><input type='hidden' name='cart_product["+ val.id +"][id]' value='"+ val.id +"'><input type='hidden' name='cart_product["+ val.id +"][quantity]' value='"+ val.quantity +"'></tr>");
            });

            $('table.cart-total-table #grand_total_price').html('RM ' + totalCost.toFixed(2));
            $('table.cart-total-table #items_count').html(data.total);
        }

        // Remove product from cart
        function removeProduct(el, event)
        {
            var tr = $(el).closest('tr');
            var id = tr.find('input[name=product_id]').val();
            var price = tr.find('td[field=price]').text();
            var quantity = tr.find('td[field=quantity]').text();
            for(var i = 0; i < data.total; i++){
                var row = data.rows[i];
                if (row.id == id) {
                    data.rows.splice(i, 1);
                    data.total--;
                    break;
                }
            }
            totalCost -=  price * quantity;
            // Empty table and append again
            $('#cartcontent tbody').empty();

            $.each(data.rows, function (key, val) {
                $('#cartcontent tbody').append("<tr><td field='name'>"+ val.name +"</td><td field='quantity'>"+ val.quantity +"</td><td field='price'>"+ val.price +"</td><td  align='center'>"+ val.remove +"</td><input type='hidden' name='product_id' value='"+ val.id +"'><input type='hidden' name='cart_product["+ val.id +"][id]' value='"+ val.id +"'><input type='hidden' name='cart_product["+ val.id +"][quantity]' value='"+ val.quantity +"'></tr>");
            });

            var subTotal = totalCost.toFixed(2);
            if(subTotal == '-0.00'){
                subTotal = '0.00';
            }
            $('table.cart-total-table #grand_total_price').html('RM ' + subTotal);
            $('table.cart-total-table #items_count').html(data.total);
        }

        function isNumberKey(evt)
        {
            console.log(evt.value);
            if ((evt.which != 46 || evt.value.indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) {
                //event it's fine

            }
            var input = evt.value;
            if ((input.indexOf('.') != -1) && (input.substring(input.indexOf('.')).length > 2)) {
                return false;
            }
        }
    </script>
@endpush
