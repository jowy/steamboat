<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        html, body, div, span, applet, object, iframe,
        h1, h2, h3, h4, h5, h6, p, blockquote, pre,
        a, abbr, acronym, address, big, cite, code,
        del, dfn, em, img, ins, kbd, q, s, samp,
        small, strike, strong, sub, sup, tt, var,
        b, u, i, center,
        dl, dt, dd, ol, ul, li,
        fieldset, form, label, legend,
        table, caption, tbody, tfoot, thead, tr, th, td,
        article, aside, canvas, details, embed,
        figure, figcaption, footer, header, hgroup,
        menu, nav, output, ruby, section, summary,
        time, mark, audio, video {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            font-family: "Arial Black", line-height;
            vertical-align: baseline;
        }
        /* HTML5 display-role reset for older browsers */
        article, aside, details, figcaption, figure,
        footer, header, hgroup, menu, nav, section {
            display: block;
        }
        body {
            line-height: 1;
        }
        ol, ul {
            list-style: none;
        }
        blockquote, q {
            quotes: none;
        }
        blockquote:before, blockquote:after,
        q:before, q:after {
            content: '';
            content: none;
        }
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        @page { margin: 0; }
        @media print {
            @page { margin: 0; }
            body { margin: 0; }
        }
        body{
            font-size: 12px;
        }
        .block{
            padding: 3px 10px;
        }
        .left{
            text-align: left;
            display: flex;
        }
        .right{
            text-align: right;
        }
        .flex-1{
            flex-grow: 1;
        }
        .flex-2{
            flex-grow: 3;
        }
        p{
            padding: 1px 0;
            font-weight: 500;
            font-family: 'Roboto', monospace;
        }
        .food-info{

        }
        .max-width-50{
            max-width: 50px;
        }
        .max-width-60{
            max-width: 60px;
        }
        .max-width-80{
            max-width: 80px;
        }
        .horizontal-line{
            padding: 2px 0;
            border-bottom: solid 2px rgba(0,0,0);
        }
    </style>
    <script src="{{ mix('js/staff.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            var startTime = moment($('#start_time').text()).toDate();
            var finishTime = moment($('#finish_time').text()).toDate();

            var localStartTime = moment(startTime).format('YYYY/MM/DD hh:mm:ss a');
            var localFinishTime = moment(finishTime).format('YYYY/MM/DD hh:mm:ss a');

            $('#start_time').text("Start Time: " + localStartTime);
            $('#finish_time').text("Finish Time: " + localFinishTime);
        });
        function printContent(el){
            var restorepage = document.body.innerHTML;
            var printcontent = document.getElementById(el).innerHTML;
            document.body.innerHTML = printcontent;
            window.print();
            document.body.innerHTML = restorepage;
        }

        window.onload =function(){
            printContent('content');
        }
    </script>
</head>
<body>
<div style="max-width: 300px;text-align: center;" id="content">
    <img src="{{ asset('logo_black.jpg') }}" style="max-width: 100px;padding: 10px;">
    <div class="block header">
        {{--<p style="font-size:16px">{{ $_G['setting']['site_name_' . $_G['lang']] }}</p>--}}
        <p>Sale Ref ID: {{ $sale->sale_ref_id }}</p>
        <p id="start_time">Start Time: {{ $sale->start_at->toDateTimeString() }}</p>
        <p id="finish_time">Finish Time: {{ $sale->finish_at->toDateTimeString() }}</p>
        {{--<p>Tel: 019-778 2881</p>--}}
    </div>
    <div class="horizontal-line"></div>
    <div class="block delivery-info">
        <p class="left">{{ $sale->created_at->toDateTimeString() }}</p>
        {{--<p class="left">{{ $booking->slot_type }} {{ \Carbon\Carbon::parse($booking->delivery_time_start)->format('h:i A') }} - {{ \Carbon\Carbon::parse($booking->delivery_time_end)->format('h:i A') }}</p>--}}
        <p class="left"><span class="flex-2">Name: {{ $sale->staff->name }}</span><span class="flex-1 right">{{ trans('common.table_no') }}: {{ ($sale->all_table) ? $sale->all_table : '-' }}</span></p>
        <p class="left">
            {!! nl2br($sale->branch->receipt_address) !!}
            {{--Lot 10 Ground Floor, <br>
            Block 1 Plaza Sentosa, <br />
            Jalan Sutera Taman Sentosa<br />
            80150 Johor Bahru--}}
        </p>
    </div>

    <div class="horizontal-line"></div>
    @if(count($sale->sale_orders) > 0)
        @foreach($sale->sale_orders as $saleOrder)
            <div class="block">
                 <p class="left">{{ $saleOrder->product_name }}</p>
                <p class="left">
                    <span class="flex-1 right">{{ $saleOrder->ala_carte_price }} X {{ $saleOrder->quantity }}</span>
                    <span class="flex-1 right max-width-60">{{ fundFormat($saleOrder->calculateTotalPrice($saleOrder->ala_carte_price, $saleOrder->quantity)) }}</span>
                </p>
            </div>
        @endforeach
    @else
        <div class="block">No ala carte</div>
    @endif

    <div class="horizontal-line"></div>
    @foreach($sale->sale_details()->wherePaxType(config('sale.pax_type.pay'))->get() as $sale_detail)
        <div class="block">
            <span class="left">TYPE: {{ $sale_detail->price_type_name }}</span>
            <p class="left">
                <span class="flex-1 right">{{ $sale_detail->per_pax_price }} X {{ $sale_detail->pax }}</span>
                <span class="flex-1 right max-width-80">{{ fundFormat($sale_detail->total_pax_price) }}</span>
            </p>
        </div>
    @endforeach

    <div class="block">
        <p class="left">
            <span class="flex-2">TOTAL PAX: {{ $sale->total_pax }}</span>
{{--            <span class="flex-1 right">Total: </span>--}}
{{--            <span class="flex-1 right max-width-50">{{ fundFormat($sale->totalPaxPriceWithoutFreePax()) }}</span>--}}
        </p>
    </div>
    @if($sale->sale_details()->wherePaxType(config('sale.pax_type.free'))->exists())
        <div class="block">
            <p class="left">
                <span class="flex-2">FREE PAX: {{ $sale->sale_details()->wherePaxType(config('sale.pax_type.free'))->first()->pax }}</span>
{{--                <span class="flex-1 right">Total: </span>--}}
{{--                <span class="flex-1 right max-width-50">{{ fundFormat(-1 * abs($sale->totalFreePaxPrice())) }}</span>--}}
            </p>
        </div>
    @endif

    @if($sale->ala_carte_total > 0)
        <div class="block">
            <p class="left">
                <span class="flex-3"></span>
                <span class="flex-1 right">Ala Carte Order: </span>
                <span class="flex-1 right max-width-50">{{ fundFormat($sale->ala_carte_total) }}</span>
            </p>
        </div>
    @endif

    @if($sale->sub_total > 0)
        <div class="block">
            <p class="left">
                <span class="flex-3"></span>
                <span class="flex-1 right">Sub total: </span>
                <span class="flex-1 right max-width-50">{{ fundFormat($sale->sub_total) }}</span>
            </p>
        </div>
    @endif

    @if($sale->tax_total > 0)
        <div class="block">
            <p class="left">
                <span class="flex-3"></span>
                <span class="flex-1 right">{{ $sale->tax_name }}: </span>
                <span class="flex-1 right max-width-50">{{ fundFormat($sale->tax_total) }}</span>
            </p>
        </div>
    @endif

    <div class="block">
        <p class="left">
            <span class="flex-3"></span>
            <span class="flex-1 right">Rounding adj: </span>
            <span class="flex-1 right max-width-50">{{ fundFormat($sale->round_adjustment) }}</span>
        </p>
    </div>

    @if($sale->grand_total > 0)
        <div class="block">
            <p class="left">
                <span class="flex-3"></span>
                <span class="flex-1 right">Grand Total: </span>
                <span class="flex-1 right max-width-50">{{ fundFormat($sale->grand_total) }}</span>
            </p>
        </div>
    @endif

    <div class="horizontal-line"></div>
    <div class="block footer" style="padding: 10px 0 0 0">
        <p style="padding: 1px 0 10px 0;">
            @if(!is_null($sale->ala_carte_remark))
                Remark: {!! $sale->ala_carte_remark !!}
            @endif
        </p>
        <h4>Thank You, Please Check Your Order!</h4>
    </div>
    {{--<img src="{{ asset('storage/images/qrcode.png') }}" style="max-width: 80px;;padding: 5px 0 0 0;">
    <div class="block footer">
        <p>FB: thaipinto1212</p>
        <p>Cotact: 019-778 2881</p>
    </div>--}}
</div>
</body>
</html>
