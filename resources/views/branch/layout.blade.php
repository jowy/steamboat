<!DOCTYPE html>
<html lang="{{ $_G['lang'] }}" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
        @if ($_G['setting']['site_name_' . $_G['lang']])
            {{ $_G['setting']['site_name_' . $_G['lang']] }}
        @endif
        @yield('title')
        @if ($_G['setting']['slogan_' . $_G['lang']])
            {{ $_G['setting']['slogan_' . $_G['lang']] }}
        @endif
    </title>
    <meta name="description" content="@yield('description')">
    <meta content="@yield('author')" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @stack('metatag')
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Montserrat:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    @include('branch.include.header_script')
    <!--begin::Base Styles -->
    <link rel="shortcut icon" href="{{ $_G['setting']['user_navigation_logo_' . $_G['lang']] }}" />
    @stack('header')
</head>
<!-- end::Head -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- BEGIN: Header -->
    <header id="m_header" class="m-grid__item m-header " minimize-offset="200" minimize-mobile-offset="200">
        <div class="m-container m-container--fluid m-container--full-height">
            <div class="m-stack m-stack--ver m-stack--desktop">
                @include('branch.include.header')
            </div>
        </div>
    </header>
    <!-- END: Header -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        @include('branch.include.sidebar')

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            {{--@include('user.include.flash')--}}
            @yield('content')
        </div>
    </div>
    <!-- end:: Body -->
    <!-- begin::Footer -->
   {{-- @include('user.include.footer')--}}
    @include('branch.include.modal')
    @stack('modal')
</div>
<!-- end:: Page -->
<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>
<!-- end::Scroll Top -->
@include('branch.include.footer_script')
@stack('footer')

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<!-- begin::Quick Nav -->
</body>
<!-- end::Body -->
