@extends('branch.layout')

@section('title')
    {{ trans('common.staff') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@section('content')
    <div class="m-content">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            {{ trans('common.staff') }}
                        </h3>
                    </div>
                </div>
            </div>
            <!--begin::Form-->
            @if(isset($mode) == 'edit')
                {!! Form::model($staff, ['route' => ['branch.staff.update', $staff->id], 'method' => 'put', 'role' => 'form', 'id' => 'staff-form', 'class' => 'm-form m-form--label-align-right']) !!}
            @else
                {!! Form::model($staff, ['route' => ['branch.staff.store'], 'method' => 'post', 'role' => 'form', 'id' => 'staff-form', 'class' => 'm-form m-form--label-align-right']) !!}
            @endif
            <div class="m-portlet__body">
                <div class="m-form__section m-form__section--first">
                    <div class="m-form__heading">
                        <h3 class="m-form__heading-title">
                            {{ trans('common.staff') }}
                        </h3>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            {{ trans('common.name') }}
                        </label>
                        <div class="col-lg-6">
                            {!! Form::text('name', null, ['class' => 'form-control m-input', 'placeholder' => 'Enter staff name']) !!}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            {{ trans('common.username') }}
                        </label>
                        <div class="col-lg-6">
                            {!! Form::text('username', null, ['class' => 'form-control m-input', 'placeholder' => 'Enter username']) !!}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            {{ trans('common.password') }}
                        </label>
                        <div class="col-lg-6">
                            {!! Form::password('password', ['class' => 'form-control m-input', 'placeholder' => 'Enter password']) !!}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            {{ trans('common.password_confirmation') }}
                        </label>
                        <div class="col-lg-6">
                            {!! Form::password('password_confirmation', ['class' => 'form-control m-input']) !!}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            {{ trans('common.contact_number') }}
                        </label>
                        <div class="col-lg-6">
                            {!! Form::text('contact_number', null, ['class' => 'form-control m-input', 'placeholder' => 'Enter contact number']) !!}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            {{ trans('common.email') }}
                        </label>
                        <div class="col-lg-6">
                            {!! Form::text('email', null, ['class' => 'form-control m-input', 'placeholder' => 'Enter email']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-6">
                            <button type="button" id="btn-submit" class="btn btn-primary">
                                @if(isset($mode) == 'edit')
                                    {{ trans('common.edit') }}
                                @else
                                    {{ trans('common.create') }}
                                @endif
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        <!--end::Form-->
        </div>
    </div>
@endsection

@push('footer')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#staff-form').makeAjaxForm({
                submitBtn: '#btn-submit',
                redirectTo: '{{ route('branch.staff.index') }}',
            });
        })
    </script>
@endpush
