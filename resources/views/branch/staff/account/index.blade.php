@extends('branch.layout')

@section('title')
    {{ trans('common.account_history') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                {{ trans('common.account_history') }}
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="{{ route('branch.staff.account.summary') }}" class="btn btn-primary" data-toggle="modal" data-target="#remote-modal"><i class="md-plus"></i> {{ trans('common.close_account') }}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <table class="table table-hover table-bordered table-striped" id="data-dt">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="80">#</th>
                                {{--                    <th>{{ trans('common.action') }}</th>--}}
                                <th>{{ trans('common.staff') }}</th>
                                <th>{{ trans('common.type') }}</th>
                                <th>{{ trans('common.amount') }}</th>
                                <th>{{ trans('common.remark') }}</th>
                                <th>{{ trans('common.created_at') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#data-dt'),
                    showIndex: true,
                    filterContainer: '#filter-container',
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('branch.staff.account.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            // {data: 'actions', name: 'actions', orderable: false, searchable: false},
                            {data: 'staff_id', name: 'staff_id'},
                            {data: 'type', name: 'type'},
                            {data: 'amount', name: 'amount'},
                            {data: 'remark', name: 'remark'},
                            {data: 'created_at', name: 'created_at'},
                        ],
                        dom: 'lrtip',
                        "processing": true
                    }
                });
            });
        }(jQuery);
    </script>
@endpush
