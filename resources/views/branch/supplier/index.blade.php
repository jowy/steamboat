@extends('branch.layout')

@section('title')
    {{ trans('common.suppliers') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header') @endpush

@section('action') @endsection

@section('content')
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">
                    {{ __('common.suppliers') }}
                </h3>
            </div>
        </div>
    </div>

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                {{ __('common.suppliers') }}
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <!--begin: Datatable -->
                    <table class="table table-hover table-bordered table-striped" id="suppliers-dt">
                        <thead>
                        <tr role="row" class="heading">
                            <th width="80">#</th>
                            <th>{{ trans('common.supplier') }}</th>
                            <th>{{ trans('common.contact_number') }}</th>
                            <th>{{ trans('common.account_credit') }}</th>
                            <th>{{ trans('common.action') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#suppliers-dt'),
                    showIndex: true,
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('branch.supplier.index') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'name', name: 'name'},
                            {data: 'contact_number', name: 'contact_number'},
                            {data: 'supplier_branch_account.account_credit', name: 'account_credit'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ],
                        "columnDefs": [
                            {"className": "dt-center", "targets": "_all"}
                        ],
                        dom: 'lrtip',
                        "processing": true
                    }
                });
            });
        }(jQuery);
    </script>
@endpush
