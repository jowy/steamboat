@extends('branch.layout')

@section('title')
    {{ trans('common.supplier') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@section('content')
    <div class="m-content">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            {{ trans('common.supplier') }}
                        </h3>
                    </div>
                </div>
            </div>
            <!--begin::Form-->

            {!! Form::model($supplier, ['route' => ['branch.supplier.update', $supplier->id], 'method' => 'put', 'role' => 'form', 'id' => 'supplier-form', 'class' => 'm-form m-form--label-align-right']) !!}
            <div class="m-portlet__body">
                <div class="m-section">
                    <div class="m-section__content">
                        <table class="table table-bordered m-table">
                            <tbody>
                            <tr>
                                <th>{{ trans('common.supplier_name') }}</th>
                                <td>{{ $supplier->name }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('common.contact_number') }}</th>
                                <td>{{ $supplier->contact_number }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('common.account_credit') }}</th>
                                <td>{{ $supplier->supplier_branch_account->account_credit }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('common.payment_method') }}</th>
                                <td>{!! Form::select('payment_method', \App\Models\SupplierTransaction::getPaymentMethodLists(), 1, ['class' => 'form-control m-input']) !!}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('common.account_adjustment') }}</th>
                                <td>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <select name="in_out_type" class="form-control m-input">
                                                <option value="add">Owe</option>
                                                <option value="deduct">Pay</option>
                                            </select>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control m-input col-md-8 fund-input" placeholder="Adjustment" name="adjustment_credit" type="text">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>{{ trans('common.remark') }}</th>
                                <td><input type="text" class="form-control m-input" placeholder="Enter comment" name="remark"></td>
                            </tr>
                            <tr>
                                <td>
                                    <button type="button" id="btn-submit" class="btn btn-primary">
                                        <i class="fa fa-edit"></i> {{ trans('common.edit') }}
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="m-section">
                    <div class="m-section__content table-responsive">
                        <!--begin: Datatable -->
                        <table class="table table-hover table-bordered table-striped" id="account-log-dt">
                            <thead>
                            <tr role="row" class="heading">
                                <th width="80">#</th>
                                <th>{{ trans('common.created_at') }}</th>
                                <th>{{ trans('common.type') }}</th>
                                <th>{{ trans('common.payment_method') }}</th>
                                <th>{{ trans('common.in_out_amount') }}</th>
                                <th>{{ trans('common.remark') }}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        <!--end::Form-->
        </div>
    </div>
@endsection

@push('footer')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#supplier-form').makeAjaxForm({
                submitBtn: '#btn-submit',
                redirectTo: '{{ route('branch.supplier.edit', $supplier->id) }}',
            });
        });

        var dTable = new Datatable();
        dTable.init({
            src: $('#account-log-dt'),
            showIndex: true,
            dataTable: {
                @include('custom.datatable.common')
                "ajax": {
                    "url": "{{ route('branch.supplier.edit', $supplier->id) }}",
                    "type": "GET"
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'transaction_type', name: 'transaction_type'},
                    {data: 'payment_method', name: 'payment_method'},
                    {data: 'amount', name: 'amount'},
                    {data: 'remark', name: 'remark'},
                ],
                dom: 'lrtip',
                "processing": true
            }
        });
    </script>
@endpush

