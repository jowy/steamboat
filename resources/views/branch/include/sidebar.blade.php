<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close m-aside-left-close--skin-dark" id="m_aside_left_close_btn">
    <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div
            id="m_ver_menu"
            class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark m-aside-menu--dropdown "
            data-menu-vertical="true"
            m-menu-dropdown="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500"
    >
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <!-- Dashboard -->
            <li class="m-menu__item {{ in_array($_G['route'], ['branch.dashboard']) ? ' m-menu__item--active' : '' }}" aria-haspopup="true" >
                <a  href="{{ route('branch.dashboard') }}" class="m-menu__link ">
                    <span class="m-menu__item-here"></span>
                    <i class="m-menu__link-icon flaticon-statistics"></i>
                    <span class="m-menu__link-text">
                        {{ trans('common.dashboard') }}
                    </span>
                </a>
            </li><!-- End Dashboard -->

            <li class="m-menu__item {{ in_array($_G['route'], ['branch.order.index', 'branch.order.create', 'branch.order.show']) ? ' m-menu__item--active' : '' }}" aria-haspopup="true" >
                <a  href="{{ route('branch.order.index') }}" class="m-menu__link ">
                    <span class="m-menu__item-here"></span>
                    <i class="m-menu__link-icon flaticon-cart"></i>
                    <span class="m-menu__link-text">
                        {{ trans('common.order') }}
                    </span>
                </a>
            </li>

            <!-- Stock -->
            <li class="m-menu__item {{ in_array($_G['route'], ['branch.stock.index', 'branch.stock.edit']) ? ' m-menu__item--active' : '' }}" aria-haspopup="true" >
                <a  href="{{ route('branch.stock.index') }}" class="m-menu__link ">
                    <span class="m-menu__item-here"></span>
                    <i class="m-menu__link-icon la la-cubes"></i>
                    <span class="m-menu__link-text">
                        {{ trans('common.stocks') }}
                    </span>
                </a>
            </li><!-- End Stock -->

            <!-- Supplier -->
            <li class="m-menu__item {{ in_array($_G['route'], ['branch.supplier.index', 'branch.supplier.edit']) ? ' m-menu__item--active' : '' }}" aria-haspopup="true" >
                <a  href="{{ route('branch.supplier.index') }}" class="m-menu__link ">
                    <span class="m-menu__item-here"></span>
                    <i class="m-menu__link-icon la la-cubes"></i>
                    <span class="m-menu__link-text">
                        {{ trans('common.suppliers') }}
                    </span>
                </a>
            </li><!-- End Supplier -->

            <li class="m-menu__item  m-menu__item--submenu {{ \Str::startsWith($_G['route'], ['branch.staff', 'branch.table', 'branch.promotion', 'branch.stock-manage', 'branch.staff.account']) ? ' m-menu__item--active': '' }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a  href="javascript:;" class="m-menu__link m-menu__toggle">
                    <span class="m-menu__item-here"></span>
                    <i class="m-menu__link-icon flaticon-close"></i>
                    <span class="m-menu__link-text">
                        {{ trans('common.management') }}
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                            <span class="m-menu__link">
                                <span class="m-menu__item-here"></span>
                                <span class="m-menu__link-text">
                                    management
                                </span>
                            </span>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('branch.staff.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-icon la la-users">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    {{ trans('common.staff') }}
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('branch.stock-manage.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-icon la la-cubes">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    {{ trans('common.stock_manage') }}
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('branch.staff.account') }}" class="m-menu__link ">
                                <i class="m-menu__link-icon la la-money">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    {{ trans('common.account_history') }}
                                </span>
                            </a>
                        </li>

                        <li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
                            <a href="{{ route('branch.branch-table.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-icon la la-table">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                     {{ trans('common.tables') }}
                                </span>
                            </a>
                        </li>
                        {{--<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
                            <a href="{{ route('branch.promotion.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-icon la la-tag">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    {{ trans('common.promotion') }}
                                </span>
                            </a>
                        </li>--}}
                    </ul>
                </div>
            </li>

            <!-- Sale -->
            <li class="m-menu__item {{ in_array($_G['route'], ['branch.sale.index', 'branch.sale.show']) ? ' m-menu__item--active' : '' }}" aria-haspopup="true" >
                <a  href="{{ route('branch.sale.index') }}" class="m-menu__link ">
                    <span class="m-menu__item-here"></span>
                    <i class="m-menu__link-icon la la-money"></i>
                    <span class="m-menu__link-text">
                        {{ trans('common.sales') }}
                    </span>
                </a>
            </li><!-- End Sale -->

            <!-- Wallet -->
            <li class="m-menu__item {{ in_array($_G['route'], ['branch.wallet.index']) ? ' m-menu__item--active' : '' }}" aria-haspopup="true" >
                <a  href="{{ route('branch.wallet.index') }}" class="m-menu__link ">
                    <span class="m-menu__item-here"></span>
                    <i class="m-menu__link-icon la la-dollar"></i>
                    <span class="m-menu__link-text">
                        {{ trans('common.wallet') }}
                    </span>
                </a>
            </li><!-- End Wallet -->
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>
<!-- END: Left Aside -->
