<div id="body-alert-container">
    @if (Session::has('flash_success'))
        @foreach (Session::get('flash_success') as $msg)
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" aria-label="{{ trans('common.close') }}" data-dismiss="alert">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa-lg fa fa-fa fa-check"></i>
                {{ $msg }}
            </div>
        @endforeach
    @endif
    @if (Session::has('flash_info'))
        @foreach (Session::get('flash_info') as $msg)
            <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" aria-label="{{ trans('common.close') }}" data-dismiss="alert">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa-lg fa fa-fa fa-exclamation"></i>
                {{ $msg }}
            </div>
        @endforeach
    @endif
    @if (Session::has('flash_warning'))
        @foreach (Session::get('flash_warning') as $msg)
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" aria-label="{{ trans('common.close') }}" data-dismiss="alert">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa-lg fa fa-fa fa-times"></i>
                {{ $msg }}
            </div>
        @endforeach
    @endif
    @if (Session::has('flash_error'))
        @foreach (Session::get('flash_error') as $msg)
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" aria-label="{{ trans('common.close') }}" data-dismiss="alert">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa-lg fa fa-fa fa-times"></i>
                {{ $msg }}
            </div>
        @endforeach
    @endif
</div>