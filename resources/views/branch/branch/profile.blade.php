@extends('branch.layout')

@section('title')
    {{ trans('common.profile') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header') @endpush

@section('action') @endsection

@section('content')
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">
                    {{ __('common.profile') }}
                </h3>
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                <i class="flaticon-share m--hide"></i>
                                {{ __('common.profile') }}
                            </a>
                        </li>
                        {{--<li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
                                Messages
                            </a>
                        </li>--}}
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="m_user_profile_tab_1">
                    {!! Form::model($profile, ['method' => 'post', 'role' => 'form', 'id' => 'profile-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
                    {{--<form class="m-form m-form--fit m-form--label-align-right">--}}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <div class="col-10 ml-auto">
                                    <h3 class="m-form__section">
                                        {{ __('common.details') }}
                                    </h3>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    {{ trans('common.branch_name') }}
                                </label>
                                <div class="col-7">
                                    {!! Form::text('name_en', null, ['class' => 'form-control m-input']) !!}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    {{ trans('common.contact_number') }}
                                </label>
                                <div class="col-7">
                                    {!! Form::text('contact_number', null, ['class' => 'form-control m-input']) !!}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    {{ trans('common.email') }}
                                </label>
                                <div class="col-7">
                                    {!! Form::text('email', null, ['class' => 'form-control m-input']) !!}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    {{ trans('common.country') }}
                                </label>
                                <div class="col-7">
                                    {!! Form::select('country_id', $countries, null, ['class' => 'form-control m-input', 'placeholder' => trans('common.please_select')]) !!}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    {{ trans('common.receipt_address') }}
                                </label>
                                <div class="col-7">
                                    {!! Form::textarea('receipt_address', null, ['class' => 'form-control m-input']) !!}
                                </div>
                            </div>
                            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                            <div class="form-group m-form__group row">
                                <div class="col-10 ml-auto">
                                    <h3 class="m-form__section">
                                        {{ __('common.set_new_password') }}
                                    </h3>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    {{ trans('common.password') }}
                                </label>
                                <div class="col-7">
                                    {!! Form::password('password', ['class' => 'form-control m-input']) !!}
                                    <span class="m-form__help">{{ trans('common.leave_blank_not_change') }}</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    {{ trans('common.password_confirmation') }}
                                </label>
                                <div class="col-7">
                                    {!! Form::password('password_confirmation', ['class' => 'form-control m-input']) !!}
                                    <span class="m-form__help">{{ trans('common.leave_blank_not_change') }}</span>
                                </div>
                            </div>

                            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                            <div class="form-group m-form__group row">
                                <div class="col-10 ml-auto">
                                    <h3 class="m-form__section">
                                        {{ __('common.map') }}
                                    </h3>
                                </div>
                            </div>

                            <div class="m-portlet__body">
                                <div id="maps-container" class="form-group m-form__group row">
                                    <div class="gmap-container">
                                        <div class="form-group google-map-setup">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                                                    <h3>{{ trans('common.search_map_prefix') }}<img src="{{ asset('img/gmapmarker.png') }}" style="width: 21px;"> to arrange your location</h3>
                                                    <div class="searchmap-map-container" style="height: 350px;"></div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
                                                    <h3>{{ trans('common.search_address') }}:</h3>
                                                    <a href="javascript:;" class="clear-map">{{ trans('common.clear_map_data') }}</a>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            {!! Form::text('address', null, ['class' => 'googlemap-address-input form-control', 'placeholder' => trans('common.address')]) !!}
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            {!! Form::text('latitude', null, ['class' => 'googlemap-latitude-input form-control', 'placeholder' => trans('common.latitude'), 'readonly' => 'readonly']) !!}
                                                        </div>
                                                        <div class="col-md-6">
                                                            {!! Form::text('longitude', null, ['class' => 'googlemap-longitude-input form-control', 'placeholder' => trans('common.longitude'), 'readonly' => 'readonly']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-7">
                                        <button type="button" class="btn btn-accent m-btn m-btn--air m-btn--custom" id="btn-submit">
                                            {{ trans('common.update') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {{--</form>--}}
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>
@endsection

@push('footer')
    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?sensor=true&key={{ config('env.GOOGLE_MAP_API_KEY') }}&libraries=places"></script>
    <script type="text/javascript" src="/custom/gmap.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#profile-form').makeAjaxForm({
                submitBtn: '#btn-submit',
                redirectTo: '{{ route('branch.profile') }}',
            });

            //Google Maps
            $('.gmap-container').makeGmapContainer();
            $('#add-map').on('click', function() {
                var mapCount = $('.gmap-container').length;
                var index = parseInt(mapCount) + parseInt(1);
                var template = getMapSetupTemplate();
                template = template.replace(/@@index@@/g, index);

                $('#maps-container').append(template);
                $('.gmap-container').makeGmapContainer();
            });
        })
    </script>
@endpush
