@extends('branch.layout')

@section('title')
    {{ trans('common.sales') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@section('content')
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">
                    {{ trans('common.sales') }}
                </h3>
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-12 col-xl-4">
                        <!--begin:: Widgets/Stats2-1 -->
                        <div class="m-widget1">
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            {{ trans('common.total_sales') }}
                                        </h3>
                                    </div>
                                    <div class="col m--align-right">
                                        <span class="m-widget1__number m--font-brand" id="total_sales_span"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end:: Widgets/Stats2-1 -->
                    </div>
                    <div class="col-md-12 col-lg-12 col-xl-4">
                        <!--begin:: Widgets/Stats2-2 -->
                        <div class="m-widget1">
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            {{ trans('common.grand_total') }}
                                        </h3>
                                    </div>
                                    <div class="col m--align-right">
                                        <span class="m-widget1__number m--font-accent" id="grand_total_span"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin:: Widgets/Stats2-2 -->
                    </div>
                    <div class="col-md-12 col-lg-12 col-xl-4">
                        <!--begin:: Widgets/Stats2-2 -->
                        <div class="m-widget1">
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            {{ trans('common.total_pax') }}
                                        </h3>
                                    </div>
                                    <div class="col m--align-right">
                                        <span class="m-widget1__number m--font-danger" id="total_pax_span"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin:: Widgets/Stats2-2 -->
                    </div>
                    {{--<div class="col-md-12 col-lg-12 col-xl-4">
                        <!--begin:: Widgets/Stats2-3 -->
                        <div class="m-widget1">
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            {{ __('common.total_earned') }}
                                        </h3>
                                    </div>
                                    <div class="col m--align-right">
                                        <span class="m-widget1__number m--font-success" id="total_earned_span">
                                            +15%
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin:: Widgets/Stats2-3 -->
                    </div>--}}
                </div>
            </div>
        </div>

        <div class="m-portlet">
            <!--begin::Form-->
            <div class="m-form m-form--label-align-right" id="filter-container">
                <div class="m-portlet__body">
                    <div class="m-form__section m-form__section--first">
                        <div class="m-form__heading">
                            <h3 class="m-form__heading-title">
                                {{ trans('common.search') }}:
                            </h3>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-2 col-form-label">
                                {{ trans('common.staff') }}
                            </label>
                            <div class="col-7">
                                {!! Form::select('staff_id', $staffs, null, ['class' => 'form-control m-input form-filter', 'placeholder' => trans('common.please_select')]) !!}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-2 col-form-label">
                                {{ trans('common.sale_ref_id') }}
                            </label>
                            <div class="col-7">
                                {!! Form::text('sale_ref_id', null, ['class' => 'form-control m-input form-filter']) !!}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-2 col-form-label">
                                {{ trans('common.credit_card') }}
                            </label>
                            <div class="col-7">
                                {!! Form::text('filter_credit_card', null, ['class' => 'form-control m-input form-filter']) !!}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">
                                {{ trans('common.payment_method') }}
                            </label>
                            <div class="col-lg-6">
                                {!! Form::select('filter_payment_method', \App\Models\Sale::getPaymentMethodLists(), null, ['class' => 'form-control m-input form-filter', 'placeholder' => trans('common.all')]) !!}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="form-control-label col-2 col-form-label">{{ trans('common.date') }}</label>
                            <div class="col-sm-4">
                                {!! Form::text('filter_created_after_date', '', ['class' => 'form-control form-filter input-sm', 'placeholder' => trans('common.from'), 'data-field' => 'date', 'id' => 'm_datepicker_2']) !!}
                            </div>
                            <div class="col-sm-4">
                                {!! Form::text('filter_created_before_date', '', ['class' => 'form-control form-filter input-sm', 'placeholder' => trans('common.to'), 'data-field' => 'date', 'id' => 'm_datepicker_3']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-6">
                                <button class="btn btn-primary filter-submit">
                                    <i class="fa fa-search"></i> {{ trans('common.filter') }}
                                </button>
                                <button class="btn btn-secondary filter-cancel">
                                    <i class="fa fa-times"></i> {{ trans('common.reset') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body table-responsive">
                        <!--begin: Datatable -->
                        <table class="table table-hover table-bordered table-striped" id="sale-dt">
                            <thead>
                            <tr role="row" class="heading">
                                <th width="80">#</th>
                                <th>{{ trans('common.action') }}</th>
                                <th>{{ trans('common.staff') }}</th>
                                <th>{{ trans('common.sale_date') }}</th>
                                <th>{{ trans('common.sale_ref_id') }}</th>
                                <th>{{ trans('common.tax') }}</th>
                                <th>{{ trans('common.discount') }}</th>
                                <th>{{ trans('common.grand_total') }}</th>
                                <th>{{ trans('common.paid') }}</th>
                                <th>{{ trans('common.change') }}</th>
                                <th>{{ trans('common.payment_method') }}</th>
                                <th>{{ trans('common.is_cancelled') }}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!--end::Form-->
        </div>
    </div>
@endsection

@push('footer')
    <script>
        +function() {
            $("#m_datepicker_2, #m_datepicker_3").datepicker({
                todayHighlight:!0,
                orientation:"bottom left",
                format: 'yyyy-mm-dd',
                templates:{
                    leftArrow:'<i class="la la-angle-left"></i>',
                    rightArrow:'<i class="la la-angle-right"></i>'
                }
            });

            $(document).ready(function() {
                getWidget();
                var dTable = new Datatable();
                dTable.init({
                    src: $('#sale-dt'),
                    showIndex: true,
                    "order": [[ 1, "desc" ]],
                    filterContainer: '#filter-container',
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('branch.sale.index') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false},
                            {data: 'staff.name', name: 'staff.name'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'sale_ref_id', name: 'sale_ref_id'},
                            {data: 'tax_total', name: 'tax_total'},
                            {data: 'discount_total', name: 'discount_total'},
                            {data: 'grand_total', name: 'grand_total'},
                            {data: 'paid', name: 'paid'},
                            {data: 'change', name: 'change'},
                            {data: 'payment_method', name: 'payment_method'},
                            {data: 'is_cancelled', name: 'deleted_at'},
                        ],
                        dom: 'lrtip',
                        "processing": true,
                        "columnDefs": [
                            { className: "text-center", "targets": [ 1 ] }
                        ]
                    }
                });

                function getWidget()
                {
                    var $staffId = $("select[name=staff_id]").val();
                    var $saleRefId = $("input[name=sale_ref_id]").val();
                    var $creditCard = $("input[name=filter_credit_card]").val();
                    var $paymentMethod = $("select[name=filter_payment_method]").val();
                    var $createdAfterDate = $("input[name=filter_created_after_date]").val();
                    var $createdBeforeDate = $("input[name=filter_created_before_date]").val();

                    $.ajax({
                        type: 'GET',
                        url: '{{ route('branch.sale.widget') }}',
                        data: {
                            "staff_id": $staffId,
                            "sale_ref_id": $saleRefId,
                            "filter_credit_card": $creditCard,
                            "filter_payment_method": $paymentMethod,
                            "filter_created_after_date": $createdAfterDate,
                            "filter_created_before_date": $createdBeforeDate
                        },
                        success: function(data) {
                            $('#total_sales_span').text(data.total_sales);
                            $('#grand_total_span').text(data.grand_total);
                            $('#total_earned_span').text(data.total_earned);
                            $('#total_pax_span').text(data.total_pax);
                        },
                        error: function(xhr, status, error) {
                            alert("Please refresh page and try again");
                        },
                        dataType: 'json'
                    });
                }
                $(".filter-submit").on('click', function () {
                    getWidget();
                });
            });
        }(jQuery);
    </script>
@endpush
