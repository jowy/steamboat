@extends('branch.layout')

@section('title')
    {{ trans('common.details') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header') @endpush

@section('action') @endsection

@section('content')
    <div class="m-content">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ trans('common.details') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-form m-form--fit m-form--label-align-right">
                <div class="m-portlet__body">
                    <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                            <div class="col-6">
                                <label>
                                    {{ trans('common.staff') }}
                                </label>
                                <input type="text" class="form-control m-input" disabled="disabled" value="{{ $sale->staff()->withTrashed()->first()->name }}">
                            </div>
                            <div class="col-6">
                                <label>
                                    {{ trans('common.sale_ref_id') }}
                                </label>
                                <input type="text" class="form-control m-input" disabled="disabled" value="{{ $sale->sale_ref_id }}">
                            </div>
                        </div>

                        <div class="form-group m-form__group row">
                            <div class="col-6">
                                <label>
                                    {{ trans('common.table_no') }}
                                </label>
                                <input type="text" class="form-control m-input" disabled="disabled" value="{{ $sale->all_table }}">
                            </div>
                            <div class="col-6">
                                <label>
                                    {{ trans('common.total_pax') }}
                                </label>
                                <input type="text" class="form-control m-input" disabled="disabled" value="{{ $sale->total_pax }}">
                            </div>
                        </div>

                        @foreach($sale->sale_details()->wherePaxType(config('sale.pax_type.pay'))->get() as $sale_detail)
                            <div class="form-group m-form__group row">
                                <div class="col-md-6">
                                    <label class="col-form-label">{{ $sale_detail->price_type_name }} ({{ $sale_detail->per_pax_price }})</label>
                                    {!! Form::text('adult_pay_pax', $sale_detail->pax, ['class' => 'form-control m-input', 'disabled']) !!}
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label">{{ trans('common.total_pax_price') }}</label>
                                    {!! Form::text('grand_total', fundFormat($sale_detail->total_pax_price), ['class' => 'form-control m-input', 'disabled']) !!}
                                </div>
                            </div>
                        @endforeach

                        <div class="form-group m-form__group row">
                            <div class="col-6">
                                <label>
                                    {{ trans('common.grand_total') }}
                                </label>
                                <input type="text" class="form-control m-input" disabled="disabled" value="{{ fundFormat($sale->grand_total) }}">
                            </div>
                            @if($sale->is_paid)
                            <div class="col-6">
                                <label>
                                    {{ trans('common.paid') }}
                                </label>
                                <input type="text" class="form-control m-input" disabled="disabled" value="{{ fundFormat($sale->paid) }}">
                            </div>
                            @endif
                            {{--@if($saleDetail = $sale->sale_details()->wherePaxType(config('sale.pax_type.free'))->wherePriceType(\App\Models\Price::ADULT_TYPE)->first())
                                <div class="col-6">
                                    <label>
                                        {{ trans('common.free_pax') }}
                                    </label>
                                    <input type="text" class="form-control m-input" disabled="disabled" value={{ $saleDetail->pax }}>
                                </div>
                            @endif--}}
                        </div>

                        @if ($sale->remark)
                            <div class="form-group m-form__group row">
                                <div class="col-12">
                                    <label>
                                        {{ trans('common.remark') }}
                                    </label>
                                    {!! Form::textarea('', $sale->remark, ['class' => 'form-control', 'rows' => 4, 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
                                </div>
                            </div>
                        @endif
                        @if(!$sale->is_ala_carte_sale)
                            <div class="form-group m-form__group row">
                                <div class="col-6">
                                    <label>
                                        {{ trans('common.start_at') }}
                                    </label>
                                    <input type="text" class="form-control m-input" disabled="disabled" value="{{ $sale->start_at }}" id="start_at">
                                </div>
                                <div class="col-6">
                                    <label>
                                        {{ trans('common.finish_at') }}
                                    </label>
                                    <input type="text" class="form-control m-input" disabled="disabled" value="{{ $sale->finish_at }}" id="finish_at">
                                </div>
                            </div>
                        @else
                            <div class="form-group m-form__group row">
                                <div class="col-6">
                                    <label>
                                        {{ trans('common.name') }}
                                    </label>
                                    <input type="text" class="form-control m-input" disabled="disabled" value="{{ $sale->name }}">
                                </div>
                                <div class="col-6">
                                    <label>
                                        {{ trans('common.ala_carte_remark') }}
                                    </label>
                                    <input type="text" class="form-control m-input" disabled="disabled" value="{{ $sale->ala_carte_remark }}">
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <!--begin::Section-->
                <div class="m-section">
                    <div class="m-section__content">
                        <table class="table m-table m-table--head-bg-success">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ trans('common.product_name') }}</th>
                                <th>{{ trans('common.ala_carte_price') }}</th>
                                <th>{{ trans('common.quantity') }}</th>
                                <th>{{ trans('common.total_price') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(count($sale->sale_orders) > 0)
                                    <?php $count = 1; ?>
                                    @foreach($sale->sale_orders as $sale_order)
                                        <tr>
                                            <td>{{ $count }}</td>
                                            <td> {{ $sale_order->product_name }}</td>
                                            <td>RM {{ fundFormat($sale_order->ala_carte_price) }}</td>
                                            <td>{{ $sale_order->quantity }}</td>
                                            <td>RM {{ fundFormat($sale_order->total_price) }}</td>
                                        </tr>
                                        <?php $count++; ?>
                                    @endforeach
                                @else
                                    <tr class="text-center">
                                        <td colspan="5">
                                            <h4>No Ala Carte</h4>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--end::Section-->

                <div class="text-right">
                    <a href="{{ route('branch.sale.receipt', $sale->id)  }}" class="btn btn-success m-btn m-btn--air m-btn--custom" target="_blank">
                        <i class="fa fa-print"></i> {{ trans('common.print_receipt') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer')
    <script>
        $(document).ready(function() {
            var startTime = moment($('#start_at').val()).toDate();
            var finishTime = moment($('#finish_at').val()).toDate();

            var localStartTime = moment(startTime).format('YYYY/MM/DD hh:mm:ss a');
            var localFinishTime = moment(finishTime).format('YYYY/MM/DD hh:mm:ss a');

            $('#start_at').val(localStartTime);
            $('#finish_at').val(localFinishTime);
        });
    </script>
@endpush
