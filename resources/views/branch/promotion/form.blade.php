@extends('branch.layout')

@section('title')
    {{ trans('common.promotion') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@section('content')
    <div class="m-content">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            {{ trans('common.promotion') }}
                        </h3>
                    </div>
                </div>
            </div>
            <!--begin::Form-->
            @if(isset($mode) == 'edit')
                {!! Form::model($promotion, ['route' => ['branch.promotion.update', $promotion->id], 'method' => 'put', 'role' => 'form', 'id' => 'promotion-form', 'class' => 'm-form m-form--label-align-right number-input']) !!}
            @else
                {!! Form::model($promotion, ['route' => ['branch.promotion.store'], 'method' => 'post', 'role' => 'form', 'id' => 'promotion-form', 'class' => 'm-form m-form--label-align-right number-input']) !!}
            @endif
            <div class="m-portlet__body">
                <div class="m-form__section m-form__section--first">
                    <div class="m-form__heading">
                        <h3 class="m-form__heading-title">
                            {{ trans('common.promotion') }}
                        </h3>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            {{ trans('common.free_pax_count') }}
                        </label>
                        <div class="col-lg-6">
                            {!! Form::text('free_pax_count', null, ['class' => 'form-control m-input', 'placeholder' => 'Enter free pax']) !!}
                            <span class="help-block">Leave blank or key in 0 if not going to have free pax</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-6">
                            <button type="button" id="btn-submit" class="btn btn-primary">
                                @if(isset($mode) == 'edit')
                                    {{ trans('common.edit') }}
                                @else
                                    {{ trans('common.create') }}
                                @endif
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        <!--end::Form-->
        </div>
    </div>
@endsection

@push('footer')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#promotion-form').makeAjaxForm({
                submitBtn: '#btn-submit',
                redirectTo: '{{ route('branch.promotion.index') }}',
            });
        })
    </script>
@endpush
