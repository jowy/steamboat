@extends('branch.layout')

@section('title')
    {{ trans('common.promotion') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header') @endpush

@section('action') @endsection

@section('content')
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">
                    {{ __('common.promotion') }}
                </h3>
            </div>
        </div>
    </div>

    @if(!\App\Models\Promotion::whereBranchId(\Auth::guard('branch')->id())->exists())
    <div class="m-section__content">
        <div class="m-demo m-demo--last" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <a href="{{ route('branch.promotion.create') }}" class="btn btn-primary btn-block">
                    <i class="fa fa-plus"></i> {{ trans('common.create_promotion') }}
                </a>
            </div>
        </div>
    </div>
    @endif

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                {{ __('common.promotion') }}
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <!--begin: Datatable -->
                    <table class="table table-hover table-bordered table-striped" id="promotion-dt">
                        <thead>
                        <tr role="row" class="heading">
                            <th width="80">#</th>
                            <th>{{ trans('common.free_pax_count') }}</th>
                            <th>{{ trans('common.created_at') }}</th>
                            <th>{{ trans('common.updated_at') }}</th>
                            <th>{{ trans('common.action') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#promotion-dt'),
                    showIndex: true,
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('branch.promotion.index') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'free_pax_count', name: 'free_pax_count'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'updated_at', name: 'updated_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false},
                        ],
                        "columnDefs": [
                            {"className": "dt-center", "targets": "_all"}
                        ],
                        dom: 'lrtip',
                        "processing": true
                    }
                });
            });
        }(jQuery);
    </script>
@endpush
