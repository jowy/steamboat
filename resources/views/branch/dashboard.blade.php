@extends('branch.layout')

@section('title')
    {{ trans('common.dashboard') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header') @endpush

@section('action') @endsection

@section('content')
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">{{ trans('common.dashboard') }}</h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                {{ trans('common.welcome') }}, {{ \Auth::guard('branch')->user()->name_en }}
            </div>
        </div>

        {{--<div class="m-portlet">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-12 col-xl-4">
                        <!--begin:: Widgets/Stats2-1 -->
                        <div class="m-widget1">
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            {{ trans('common.orders') }}
                                        </h3>
                                        <span class="m-widget1__desc">
                                            {{ trans('common.in_total') }}
                                        </span>
                                    </div>
                                    <div class="col m--align-right">
                                        <span class="m-widget1__number m--font-brand">

                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            {{ trans('common.orders') }}
                                        </h3>
                                        <span class="m-widget1__desc">
                                            {{ trans('common.in_pending_status') }}
                                        </span>
                                    </div>
                                    <div class="col m--align-right">
                                        <span class="m-widget1__number m--font-danger">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end:: Widgets/Stats2-1 -->
                    </div>
                    <div class="col-md-12 col-lg-12 col-xl-4">
                        <!--begin:: Widgets/Stats2-2 -->
                        <div class="m-widget1">
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            {{ trans('common.sales') }}
                                        </h3>
                                        <span class="m-widget1__desc">
                                            {{ trans('common.today') }}
                                        </span>
                                    </div>
                                    <div class="col m--align-right">
                                        <span class="m-widget1__number m--font-accent">
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            {{ trans('common.sales') }}
                                        </h3>
                                        <span class="m-widget1__desc">
                                             {{ trans('common.week') }}
                                        </span>
                                    </div>
                                    <div class="col m--align-right">
                                        <span class="m-widget1__number m--font-info">
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            {{ trans('common.sales') }}
                                        </h3>
                                        <span class="m-widget1__desc">
                                            {{ trans('common.month') }}
                                        </span>
                                    </div>
                                    <div class="col m--align-right">
                                        <span class="m-widget1__number m--font-warning">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin:: Widgets/Stats2-2 -->
                    </div>
                    <div class="col-md-12 col-lg-12 col-xl-4">
                        <!--begin:: Widgets/Stats2-3 -->
                        <div class="m-widget1">
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            {{ trans('common.staffs') }}
                                        </h3>
                                        <span class="m-widget1__desc">
                                            {{ trans('common.in_total') }}
                                        </span>
                                    </div>
                                    <div class="col m--align-right">
                                        <span class="m-widget1__number m--font-success">
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            {{ trans('common.products') }}
                                        </h3>
                                        <span class="m-widget1__desc">
                                            {{ trans('common.in_total') }}
                                        </span>
                                    </div>
                                    <div class="col m--align-right">
                                        <span class="m-widget1__number m--font-danger">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin:: Widgets/Stats2-3 -->
                    </div>
                </div>
            </div>
        </div>--}}
    </div>
@endsection

@push('footer')

@endpush
