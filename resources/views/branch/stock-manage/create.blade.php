@extends('branch.layout')

@section('title')
    {{ trans('common.stock_manage') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@section('content')
    <div class="m-content">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            {{ trans('common.stock_manage') }}
                        </h3>
                    </div>
                </div>
            </div>
            {!! Form::model($stockManage, ['route' => ['branch.stock-manage.store'], 'method' => 'post', 'role' => 'form', 'id' => 'stock-manage-form', 'class' => 'm-form m-form--label-align-right']) !!}

            <div class="m-portlet__body">
                <div class="m-form__section m-form__section--first">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            {{ trans('common.action') }}
                        </label>
                        <div class="col-lg-6">
                            {!! Form::select('action_id', \App\Models\StockManage::getActionLists(), '', ['class' => 'form-control m-input']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            {{ trans('common.place') }}
                        </label>
                        <div class="col-lg-6">
                            {!! Form::select('place', \App\Models\StockManage::getPlaceLists(), '', ['class' => 'form-control m-input']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            {{ trans('common.stock') }}
                        </label>
                        <div class="col-lg-6">
                            {!! Form::select('select_stock', $stocks, '', ['class' => 'form-control m-input', 'id' => 'select-stock']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            {{ trans('common.quantity') }}
                        </label>
                        <div class="col-lg-6">
                            {!! Form::text('quantity', null, ['class' => 'form-control m-input fund-input', 'placeholder' => 'Enter quantity', 'id' => 'select-quantity']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-6">
                            <button type="button" class="btn btn-primary add-stock">
                                <i class="fa fa-plus"></i> {{ trans('common.add') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="m-portlet__body">
                <!--begin::Section-->
                <div class="m-section">
                    <div class="m-section__content">
                        <table class="table m-table m-table--head-bg-success" id="stock-manage-table">
                            <thead>
                            <tr>
                                <th>Select</th>
                                <th>{{ trans('common.stock') }}</th>
                                <th>{{ trans('common.quantity') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-danger blue text-white delete-stock"><i class="fa fa-minus"></i> {{ trans('common.remove_stock') }}</button>

                        <hr>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">
                                {{ trans('common.remark') }}
                            </label>
                            <div class="col-lg-6">
                                {!! Form::textarea('remark', null, ['class' => 'form-control m-input', 'rows' => 5]) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Section-->
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions">
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="button" id="submit-btn" class="btn btn-success" disabled>
                                {{ trans('common.create') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
@endsection

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#stock-manage-form').makeAjaxForm({
                    submitBtn: '#submit-btn',
                    redirectTo: '{{ route('branch.stock-manage.index') }}',
                });

                $('#select-stock').select2();

                $(".add-stock").click(function() {
                    var select_stock_id = $("#select-stock").val();
                    var select_stock_name = $("#select-stock option:selected").text();
                    var select_quantity = $("#select-quantity").val();

                    if (select_stock_id == '' || select_stock_name == '') {
                        alert('Some error happened, please try again');
                        return false;
                    }
                    if (select_quantity == '' || !$.isNumeric(select_quantity)) {
                        alert('Please key in quantity or quantity must be integer');
                        return false;
                    }
                    var $unique_stock_id = $('#stock-manage-table').find('input.hidden_stock_id:hidden[value=' + select_stock_id + ']');

                    if ($unique_stock_id.length > 0) {
                        alert('You can only add unique stock');
                        return false;
                    }
                    var markup = "<tr><td><input type='checkbox' name='record'></td><td>"+ select_stock_name +"</td><td>" + select_quantity + "</td><input type='hidden' name='stock_manage_detail["+ select_stock_id +"][stock_id]' class='hidden_stock_id' value='"+ select_stock_id +"'><input type='hidden' class='hidden_quantity' name='stock_manage_detail["+ select_stock_id +"][quantity]' value='"+ select_quantity +"'></tr>";
                    $("table tbody").append(markup);
                    $('#submit-btn').prop('disabled', false);
                });

                // Find and remove selected table rows
                $(".delete-stock").click(function() {
                    $("#stock-manage-table tbody").find('input[name="record"]').each(function(){
                        if($(this).is(":checked")){
                            $(this).parents("tr").remove();
                        }
                    });
                    if ($("#stock-manage-table > tbody > tr").length == null || $("#stock-manage-table > tbody > tr").length == 0){
                        $('#submit-btn').prop('disabled', true);
                    }
                });
            });
        }(jQuery);
    </script>
@endpush
