@extends('branch.layout')

@section('title')
    {{ trans('common.stock_manage') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header') @endpush

@section('action') @endsection

@section('content')
    <div class="m-content">
        <div class="m-portlet m-portlet--full-height m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#stock-manage" role="tab" aria-selected="true">
                                <i class="fa fa-info-circle" aria-hidden="true"></i> {{ trans('common.stock_manage') }}
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#stock-manage-detail" role="tab" aria-selected="false">
                                <i class="fa fa-list" aria-hidden="true"></i> {{ trans('common.stock_manage_detail') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane active show" id="stock-manage">
                    {!! Form::model($stockManage, ['class' => 'm-form m-form--fit m-form--label-align-right']) !!}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <div class="col-10 ml-auto">
                                <h3 class="m-form__section">
                                    {{ trans('common.stock_manage') }}
                                </h3>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                                {{ trans('common.action') }}
                            </label>
                            <div class="col-7">
                                {!! Form::text('action_id', $stockManage->explainAction(), ['class' => 'form-control', 'disabled']) !!}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                                {{ trans('common.place') }}
                            </label>
                            <div class="col-7">
                                {!! Form::text('place', $stockManage->explainPlace(), ['class' => 'form-control', 'disabled']) !!}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                                {{ trans('common.remark') }}
                            </label>
                            <div class="col-7">
                                {!! Form::textarea('remark', $stockManage->remark, ['class' => 'form-control fund-input', 'disabled']) !!}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                                {{ trans('common.created_at') }}
                            </label>
                            <div class="col-7">
                                {!! Form::text('created_at', $stockManage->created_at, ['class' => 'form-control', 'disabled']) !!}
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

                <!-- Stock Manage Detail -->
                <div class="tab-pane" id="stock-manage-detail">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    {{ trans('common.stock_manage_detail') }}
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <div class="m-section">
                            <div class="m-section__content">
                                <table class="table m-table m-table--head-bg-success">
                                    <thead class="table-active">
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">{{ trans('common.stock') }}</th>
                                        <th class="text-center">{{ trans('common.quantity') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $count = 1; ?>
                                    @foreach($stockManage->stock_manage_details as $stock_manage_detail)
                                        <tr>
                                            <td class="text-center">{{ $count }}</td>
                                            <td class="text-center">{{ $stock_manage_detail->branch_stock->product_name_en }}</td>
                                            <td class="text-center">{{ $stock_manage_detail->quantity }}</td>
                                        </tr>
                                        <?php $count++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer')
    <script type="text/javascript">

    </script>
@endpush
