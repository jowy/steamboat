@extends('branch.layout')

@section('title')
    {{ trans('common.order_stock') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@section('content')
    <div class="m-content">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            {{ trans('common.order_stock') }}
                        </h3>
                    </div>
                </div>
            </div>
            {!! Form::model($order, ['route' => ['branch.order.store'], 'method' => 'post', 'role' => 'form', 'id' => 'order-form', 'class' => 'm-form m-form--label-align-right']) !!}
            <div class="m-portlet__body">
                <div class="m-form__section m-form__section--first">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            {{ trans('common.supplier') }}
                        </label>
                        <div class="col-lg-6">
                            {!! Form::select('select_supplier', $suppliers, '', ['class' => 'form-control m-input', 'id' => 'select-supplier']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            {{ trans('common.product') }}
                        </label>
                        <div class="col-lg-6">
                            {!! Form::select('select_product', $products, '', ['class' => 'form-control m-input', 'id' => 'select-product']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            {{ trans('common.quantity') }}
                        </label>
                        <div class="col-lg-6">
                            {!! Form::text('quantity', null, ['class' => 'form-control m-input fund-input', 'placeholder' => 'Enter quantity', 'id' => 'select-quantity']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-6">
                            <button type="button" class="btn btn-primary add-stock">
                                <i class="fa fa-plus"></i> {{ trans('common.add') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="m-portlet__body">
                <!--begin::Section-->
                <div class="m-section">
                    <div class="m-section__content">
                        <table class="table m-table m-table--head-bg-success" id="order-stock-table">
                            <thead>
                            <tr>
                                <th>Select</th>
                                <th>{{ trans('common.product') }}</th>
                                <th>{{ trans('common.quantity') }}</th>
                                <th width="500">{{ trans('common.total_price') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-danger blue text-white delete-stock"><i class="fa fa-minus"></i> {{ trans('common.remove_stock') }}</button>

                        <hr>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">
                                {{ trans('common.total_order_cost') }}
                            </label>
                            <div class="col-lg-6">
                                {!! Form::text('total_order_cost', 0.00, ['class' => 'form-control m-input fund-input', 'disabled']) !!}
                            </div>
                        </div>

                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">
                                {{ trans('common.payment_method') }}
                            </label>
                            <div class="col-lg-6">
                                {!! Form::select('payment_method', \App\Models\SupplierTransaction::getPaymentMethodLists(), 1, ['class' => 'form-control m-input']) !!}
                            </div>
                        </div>

                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">
                                {{ trans('common.paid') }}
                            </label>
                            <div class="col-lg-6">
                                {!! Form::text('paid', null, ['class' => 'form-control m-input fund-input', 'placeholder' => '0.00']) !!}
                            </div>
                        </div>

                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">
                                {{ trans('common.remark') }}
                            </label>
                            <div class="col-lg-6">
                                {!! Form::textarea('remark', null, ['class' => 'form-control m-input', 'rows' => 5]) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Section-->
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions">
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="button" id="submit-btn" class="btn btn-success" disabled>
                                {{ trans('common.create') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
@endsection

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#order-form').makeAjaxForm({
                    submitBtn: '#submit-btn',
                    redirectTo: '{{ route('branch.order.index') }}',
                });

                $('#select-product').select2();

                $(".add-stock").click(function() {
                    var select_product_id = $("#select-product").val();
                    var select_product_name = $("#select-product option:selected").text();
                    var select_quantity = $("#select-quantity").val();

                    if (select_product_id == '' || select_product_name == '') {
                        alert('Some error happened, please try again');
                        return false;
                    }
                    if (select_quantity == '' || !$.isNumeric(select_quantity)) {
                        alert('Please key in quantity or quantity must be integer');
                        return false;
                    }
                    var $unique_product_id = $('#order-stock-table').find('input.hidden_product_id:hidden[value=' + select_product_id + ']');

                    if ($unique_product_id.length > 0) {
                        alert('You can only add unique product');
                        return false;
                    }
                    var markup = "<tr><td><input type='checkbox' name='record'></td><td>"+ select_product_name +"</td><td>" + select_quantity + "</td><td><input type='text' class='form-control m-input product_total_price' name='order_stock_detail["+ select_product_id +"][total_price]'></td><input type='hidden' name='order_stock_detail["+ select_product_id +"][product_id]' class='hidden_product_id' value='"+ select_product_id +"'><input type='hidden' class='hidden_quantity' name='order_stock_detail["+ select_product_id +"][quantity]' value='"+ select_quantity +"'></tr>";
                    $("table tbody").append(markup);
                    $('#submit-btn').prop('disabled', false);
                });

                // Find and remove selected table rows
                $(".delete-stock").click(function() {
                    $("#order-stock-table tbody").find('input[name="record"]').each(function(){
                        if($(this).is(":checked")){
                            $(this).parents("tr").remove();
                        }
                    });
                    if ($("#order-stock-table > tbody > tr").length == null || $("#order-stock-table > tbody > tr").length == 0){
                        $('#submit-btn').prop('disabled', true);
                    }

                    sumGrandTotal();
                });

                // Add total price on product
                $("table#order-stock-table").on('keyup', '.product_total_price', function (event) {
                    return isPrice(event, this);
                });
            });

            function isPrice(e, element) {
                // Check 2 decimal and numeric
                var val = element.value;
                var re = /^([0-9]+[\.]?[0-9]?[0-9]?|[0-9]+)$/g;
                var re1 = /^([0-9]+[\.]?[0-9]?[0-9]?|[0-9]+)/g;
                if (re.test(val)) {
                    sumGrandTotal();
                } else {
                    val = re1.exec(val);
                    if (val) {
                        element.value = val[0];
                    } else {
                        element.value = "";
                    }
                }
            }

            function sumGrandTotal() {
                var grandTotal = 0;
                // Sum all the grand total
                var $dataRows = $('table#order-stock-table tbody > tr');

                $dataRows.each(function(i, obj) {
                    $(this).find('.product_total_price').each(function(i) {
                        if(typeof($(this).val()) != "undefined" && $(this).val() !== null && $(this).val() !== "") {
                            grandTotal += parseFloat($(this).val());
                        }
                    });
                });

                $("input[name='total_order_cost']").val(grandTotal.toFixed(2));
            }
        }(jQuery);
    </script>
@endpush
