@extends('branch.layout')

@section('title')
    {{ trans('common.order_stock') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header') @endpush

@section('action') @endsection

@section('content')
    <div class="m-content">
        <div class="m-portlet m-portlet--full-height m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#order" role="tab" aria-selected="true">
                                <i class="fa fa-info-circle" aria-hidden="true"></i> {{ trans('common.order') }}
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#order-detail" role="tab" aria-selected="false">
                                <i class="fa fa-list" aria-hidden="true"></i> {{ trans('common.order_detail') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane active show" id="order">
                    {!! Form::model($order, ['class' => 'm-form m-form--fit m-form--label-align-right']) !!}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <div class="col-10 ml-auto">
                                <h3 class="m-form__section">
                                    {{ trans('common.order') }}
                                </h3>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                                {{ trans('common.supplier') }}
                            </label>
                            <div class="col-7">
                                {!! Form::text('supplier', $order->supplier->name, ['class' => 'form-control', 'disabled']) !!}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                                {{ trans('common.order_ref_id') }}
                            </label>
                            <div class="col-7">
                                {!! Form::text('order_ref_id', $order->order_ref_id, ['class' => 'form-control', 'disabled']) !!}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                                {{ trans('common.total_order_cost') }}
                            </label>
                            <div class="col-7">
                                {!! Form::text('total_order_cost', $order->total_order_cost, ['class' => 'form-control fund-input', 'disabled']) !!}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                                {{ trans('common.total_products') }}
                            </label>
                            <div class="col-7">
                                {!! Form::text('total_products', $order->order_details->count(), ['class' => 'form-control', 'disabled']) !!}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                                {{ trans('common.remark') }}
                            </label>
                            <div class="col-7">
                                {!! Form::textarea('remark', $order->remark, ['class' => 'form-control', 'disabled', 'rows' => 5]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <div class="row">
                                <div class="col-2"></div>
                                <div class="col-7">
                                    <a href="{{ route('branch.order.print-invoice', $order->id) }}" class="btn btn-info m-btn m-btn--air m-btn--custom" target="_blank">
                                        <i class="fa fa-print"></i> {{ trans('common.print_invoice') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

                <!-- Order Stock -->
                <div class="tab-pane" id="order-detail">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    {{ trans('common.order_detail') }}
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <div class="m-section">
                            <div class="m-section__content">
                                <table class="table m-table m-table--head-bg-success">
                                    <thead class="table-active">
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">{{ trans('common.product') }}</th>
                                        <th class="text-center">{{ trans('common.quantity') }}</th>
                                        <th class="text-center">{{ trans('common.total_price') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $count = 1; ?>
                                    @foreach($order->order_details as $orderDetail)
                                        <tr>
                                            <td class="text-center">{{ $count }}</td>
                                            <td class="text-center">{{ $orderDetail->product_name }}</td>
                                            <td class="text-center">{{ $orderDetail->quantity }}</td>
                                            <td class="text-center">{{ fundFormat($orderDetail->total_price) }}</td>
                                        </tr>
                                        <?php $count++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer')
    <script type="text/javascript">

    </script>
@endpush
