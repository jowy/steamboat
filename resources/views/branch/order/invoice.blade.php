<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{ trans('common.order_invoice') }}</title>

    {{--<link href="{{ mix('css/branch.css') }}" rel="stylesheet" type="text/css" />--}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 15px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading {
            background: #5b51fc;
            font-weight: bold;
            color: white
        }

        .invoice-box table tr.heading td { }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }

        table {
            margin-bottom: 15px;
        }

        table.product-info {
            border-collapse: collapse;
            font-size: 13px;
        }

        table.product-info td, table.product-info th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        table.product-info tr:nth-child(even){background-color: #f2f2f2;}

        table.product-info tr:hover {background-color: #ddd;}

        table.product-info th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: center;
            background-color: #bebebe;
            color: black;
        }

        .text-center {
            text-align: center !important;
        }
    </style>
</head>

<body onload="window.print()">
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="4">
                <table>
                    <tr>
                        <td class="title"><img src="{{ $_G['setting']['admin_login_logo_' . $_G['lang']] }}" style="width:100%; max-width:200px;"></td>

                        <td># {{ ucwords(trans('common.order_stock')) }}<br>
                            {{ ucwords(trans('common.order_date')) }}: {{  date('F d, Y', strtotime( $order->created_at)) }}<br>
                            {{ ucwords(trans('common.order_ref_id')) }}: {{ $order->order_ref_id }}<br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="4">
                <table style="width: 30%;">
                    <tr>
                        <td>
                            <b>{{ $order->branch->name_en }}</b><br>
                            {{ $order->branch->address }}<br>
                            {{ $order->branch->contact_number }}<br>
                            {{ $order->branch->email_address }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="heading">
            <td class="text-center" width="15%"># {{ ucwords(trans('common.id')) }}</td>
            <td class="text-center" width="25%">{{ ucwords(trans('common.order_date')) }}</td>
            <td class="text-center">{{ ucwords(trans('common.branch_name')) }}</td>
            <td class="text-center">{{ ucwords(trans('common.total_products')) }}</td>
        </tr>

        <tr class="details">
            <td class="text-center">{{ $order->id }}</td>
            <td class="text-center">{{ $order->created_at }}</td>
            <td class="text-center">{{ $order->branch->name_en }}</td>
            <td class="text-center">{{ $order->order_details->count() }}</td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="product-info">
        <tr class="heading">
            <th>#</th>
            <th>{{ ucwords(trans('common.product_name')) }}</th>
            <th>{{ ucwords(trans('common.quantity')) }}</th>
            <th>{{ ucwords(trans('common.total_price')) }}</th>
        </tr>

        @if($order->order_details->count() > 0)
            <?php $count = 1; ?>
            @foreach($order->order_details as $orderDetail)
                <tr class="text-center">
                    <td>{{ $count }}</td>
                    <td class="text-center">{{ $orderDetail->product_name }}</td>
                    <td>{{ $orderDetail->quantity }}</td>
                    <td>{{ fundFormat($orderDetail->total_price) }}</td>
                </tr>
                <?php $count++; ?>
            @endforeach
        @else
            <tr colspan="3">
                <td>{{ trans('common.record_not_found') }}</td>
            </tr>
        @endif
    </table>

    <div class="row">
        <div class="col-lg-4 offset-lg-8">
            <div class="text-right clearfix">
                <div class="float-right">
                    <h4 class="page-invoice-amount">{{ trans('common.total_order_cost') }}:
                        <span>{{ fundFormat($order->total_order_cost) }}</span>
                    </h4>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
