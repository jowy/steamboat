@extends('branch.layout')

@section('title')
    {{ trans('common.wallet') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@section('content')
    <div class="m-content">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            {{ trans('common.wallet') }}
                        </h3>
                    </div>
                </div>
            </div>
            <!--begin::Form-->

            {!! Form::open(['route' => ['branch.wallet.update'], 'method' => 'put', 'role' => 'form', 'id' => 'wallet-form', 'class' => 'm-form m-form--label-align-right']) !!}
            <div class="m-portlet__body">
                <div class="m-section">
                    <div class="m-section__content">
                        <table class="table table-bordered m-table">
                            <tbody>
                            <tr>
                                <th>{{ trans('common.wallet') }}</th>
                                <td>
                                    {{ $user->credit_1 }}
                                </td>
                            </tr>
                            <tr>
                                <th>{{ trans('common.wallet_to_add_subtract') }}</th>
                                <td>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <select name="in_out_type" class="form-control m-input">
                                                <option value="add">+</option>
                                                <option value="deduct">-</option>
                                            </select>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control m-input col-md-8 fund-input" placeholder="0.00" name="in_out_wallet" type="text">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>{{ trans('common.remark') }}</th>
                                <td>
                                    <input type="text" class="form-control m-input" placeholder="Enter comment" name="remark">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button type="button" id="btn-submit" class="btn btn-primary">
                                        <i class="fa fa-edit"></i> {{ trans('common.edit') }}
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="m-section">
                    <div class="m-section__content table-responsive">
                        <!--begin: Datatable -->
                        <table class="table table-hover table-bordered table-striped" id="wallet-log-dt">
                            <thead>
                            <tr role="row" class="heading">
                                <th width="80">#</th>
                                <th>{{ trans('common.created_at') }}</th>
                                <th>{{ trans('common.type') }}</th>
                                <th>{{ trans('common.in_out_wallet') }}</th>
                                <th>{{ trans('common.remark') }}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        <!--end::Form-->
        </div>
    </div>
@endsection

@push('footer')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#wallet-form').makeAjaxForm({
                submitBtn: '#btn-submit',
                redirectTo: '{{ route('branch.wallet.index') }}',
            });
        });

        var dTable = new Datatable();
        dTable.init({
            src: $('#wallet-log-dt'),
            showIndex: true,
            dataTable: {
                @include('custom.datatable.common')
                "ajax": {
                    "url": "{{ route('branch.wallet.index') }}",
                    "type": "GET"
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'transaction_type', name: 'transaction_type'},
                    {data: 'amount', name: 'amount'},
                    {data: 'remark', name: 'remark'},
                ],
                dom: 'lrtip',
                "processing": true
            }
        });
    </script>
@endpush

