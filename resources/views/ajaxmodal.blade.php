<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="{{ trans('common.close') }}">
        <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">@section('title')@show</h4>
</div>
<div class="modal-body">
    @section('content')@show
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">{{ trans('common.close') }}</button>
    @section('footer')@show
</div>
@section('script')@show