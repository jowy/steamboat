<div class="image-selector">
    <div class="input-group">
        <span class="input-group-btn">
            <button class="btn btn-primary waves-effect waves-classic previewer" type="button" data-target="{{ $default_value ?? '' }}">
                <i class="fa fa-eye"></i> {{ trans('common.preview') }}
            </button>
        </span>

        <input class="form-control" type="text" name="{{ $field_name ?? '' }}" value="{{ $default_value ?? '' }}" />

        <span class="input-group-btn">
            <button class="btn btn-primary btn-red-500 waves-effect waves-classic btn-select-image" type="button"  data-input="thumbnail" data-preview="img-previewer">
                <i class="fa fa-picture-o"></i> {{ trans('common.choose') }}
            </button>
        </span>
    </div>
</div>