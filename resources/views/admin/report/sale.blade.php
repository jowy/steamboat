@extends('admin.layout')

@section('title')
    {{ trans('permission.sale_report') }}
@endsection

@section('description')
    {{ trans('permission.sale_report') }}
@endsection

@push('header')

@endpush

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#sale-report-dt'),
                    showIndex: true,
                    filterContainer: '#filter-container',
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('admin.report.sale') }}?branch_id={{ Request::get('branch_id') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                                @if(Request::get('branch_id'))
                            {data: 'name_en', name: 'name_en'},
                            {data: 'name', name: 'name'},
                                @else
                            {data: 'name_en', name: 'name_en'},
                                @endif
                            {data: 'total_sales', name: 'total_sales', orderable: false, searchable: false},
                            {data: 'total_amount', name: 'total_amount', orderable: false, searchable: false},
                                @if(!Request::get('branch_id'))
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                            @endif
                        ]
                    }
                });
            });
        }(jQuery);
    </script>
@endpush

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.lists') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-hover table-bordered table-striped" id="sale-report-dt">
                <thead>
                <tr role="row" class="heading">
                    <th width="80">#</th>
                    @if(Request::get('branch_id'))
                        <th>{{ trans('common.branch') }}</th>
                        <th>{{ trans('common.name') }}</th>
                    @else
                        <th>{{ trans('common.branch') }}</th>
                    @endif
                    <th>{{ trans('common.total_sales') }}</th>
                    <th>{{ trans('common.total_amount') }}</th>
                    @if(!Request::get('branch_id'))
                        <th>{{ trans('common.action') }}</th>
                    @endif
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('modal')

@endpush
