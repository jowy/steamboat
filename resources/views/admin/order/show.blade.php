@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_order') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header')
@endpush

@section('action') @endsection

@section('content')
    {!! Form::model($order, ['role' => 'form', 'method' => 'post', 'id' => 'order-form']) !!}
    <div class="m-portlet m-portlet--mobile m-portlet--tabs">
        <div class="m-portlet__head">
            <div class="m-portlet__head-tools">
                <ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
                    <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link active" data-toggle="tab" href="#tab-order" role="tab">
                            <i class="fa fa-info-circle" aria-hidden="true"></i> {{ trans('common.order') }}
                        </a>
                    </li>
                    <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-order-detail" role="tab">
                            <i class="fa fa-list" aria-hidden="true"></i> {{ trans('common.order_detail') }}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="form-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-order" role="tabpanel" aria-expanded="true">
                        <div class="form-group">
                            <label class="control-label">{{ trans('common.branch_name') }}</label>
                            {!! Form::text('branch', $order->branch->name_en, ['class' => 'form-control', 'disabled']) !!}
                        </div>
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.supplier') }}</label>
                            {!! Form::text('supplier', $order->supplier->name, ['class' => 'form-control', 'disabled']) !!}
                        </div>
                        <div class="form-group">
                            <label class="control-label">{{ trans('common.order_ref_id') }}</label>
                            {!! Form::text('order_ref_id', $order->order_ref_id, ['class' => 'form-control', 'disabled']) !!}
                        </div>
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.total_order_cost') }}</label>
                            {!! Form::text('total_order_cost', $order->total_order_cost, ['class' => 'form-control fund-input', 'disabled']) !!}
                        </div>
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.total_products') }}</label>
                            {!! Form::text('total_products', $order->order_details->count(), ['class' => 'form-control fund-input', 'disabled']) !!}
                        </div>
                    </div>

                    <div class="tab-pane" id="tab-order-detail" role="tabpanel" aria-expanded="false">
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ trans('common.product') }}</th>
                                <th scope="col">{{ trans('common.quantity') }}</th>
                                <th scope="col">{{ trans('common.total_price') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 1; ?>
                            @foreach($order->order_details as $order_detail)
                                <tr>
                                    <th scope="row">{{ $count }}</th>
                                    <td>{{ $order_detail->product_name }}</td>
                                    <td>{{ $order_detail->quantity }} {{ $order_detail->quantity_label }}</td>
                                    <td>{{ $order_detail->total_price }}</td>
                                </tr>
                                <?php $count++ ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <a href="{{ route('admin.order.invoice', $order->id) }}" class="btn btn-success btn-block blue" target="_blank">
                <i class="fa fa-print"></i> {{ trans('common.print_invoice') }}
            </a>
            {{--<button type="button" id="submit-btn" class="btn btn-primary btn-block blue"><i class="fa fa-edit"></i> {{ trans('common.edit') }}</button>--}}
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('footer')
    <script>
        +function ($) {
            /*$(document).ready(function () {
                $('#order-form').makeAjaxForm({
                    submitBtn: '#submit-btn',
                    redirectTo: ''
                });

                if ($('#is_ala_carte').val() == true) {
                    $('#ala_carte_price').prop('disabled', false);
                } else {
                    $('#ala_carte_price').prop('disabled', true);
                }

                $('#is_ala_carte').change(function() {
                    if ($(this).val() == true) {
                        $('#ala_carte_price').prop('disabled', false);
                    } else {
                        $('#ala_carte_price').prop('disabled', true);
                    }
                });
            });*/
        }(jQuery);
    </script>
@endpush

@push('modal') @endpush


