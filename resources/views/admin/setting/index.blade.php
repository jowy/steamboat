@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_setting') }}
@endsection

@section('description')
    {{ trans('permission.manage_setting') }}
@endsection

@push('header')

@endpush

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#data-dt'),
                    showIndex: true,
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('admin.setting.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id', sortable: true},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false},
                            {data: 'description', name: 'description', sortable: false},
                            {data: 'value', name: 'value'},
                            {data: 'updated_at', name: 'updated_at'},
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endpush

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.lists') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-hover table-bordered table-striped" id="data-dt">
                <thead>
                    <tr role="row" class="heading">
                        <th width="80">#</th>
                        <th width="150">{{ trans('common.actions') }}</th>
                        <th>{{ trans('common.description') }}</th>
                        <th width="250">{{ trans('common.value') }}</th>
                        <th width="185">{{ trans('common.updated_at') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('modal')

@endpush