@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_setting') }}
@endsection

@section('description')
    {{ trans('permission.manage_setting') }}
@endsection

@section('author') @endsection

@section('metatag') @endsection

@push('header') @endpush

@section('content')
    {!! Form::open(['route' => ['admin.setting.edit.post', 'id' => $model->id], 'id' => 'setting-form', 'role' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right', 'files' => true]) !!}
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ $model->getDescription() }}
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="form-group m-form__group">
                    @if (isset($params['classes']))
                        {!! Form::text('value', $model->getValue(), ['class' => 'form-control m-input ' . $params['classes']]) !!}
                    @else
                        {!! Form::text('value', $model->getValue(), ['class' => 'form-control m-input']) !!}
                    @endif
                </div>
                <div class="form-group m-form__group">
                    <button type="button" id="submit-btn" class="btn btn-primary ">{{ trans('common.submit') }}</button>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@push('footer')
    <script>
        +function ($) {
            $(document).ready(function () {
                $('#setting-form').makeAjaxForm({
                    submitBtn: '#submit-btn',
                    redirectTo: '{{ route('admin.setting') }}',
                });
            });
        }(jQuery);
    </script>
@endpush

@push('modal') @endpush


