@extends('ajaxmodal')

@section('title')
    {{ trans('common.withdrawal') }}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {

            });
        }(jQuery);
    </script>
@endsection

@section('footer') @endsection

@section('content')
    <div class="form-body">
        <div class="form-group m-form__group">
            <label>{{ trans('common.user') }}</label>
            {!! Form::text('', $model->user ? $model->user->username : '-', ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.admin') }}</label>
            {!! Form::text('', $model->admin ? $model->admin->username : '-', ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.bank') }}</label>
            {!! Form::text('', $model->getBankName(), ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.bank_account_name') }}</label>
            {!! Form::text('', $model->bank_account_name, ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.bank_account_number') }}</label>
            {!! Form::text('', $model->bank_account_number, ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.credit_' . $model->credit_type) }}</label>
            {!! Form::text('', fundFormat($model->amount), ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.admin_fees') }}</label>
            {!! Form::text('', fundFormat($model->admin_fees, 2), ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.currency') }}</label>
            {!! Form::text('', fundFormat($model->local_currency_amount, 2), ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.status') }}</label>
            {!! Form::text('', $model->explainStatus(), ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        @if ($model->admin_response_date != null)
            <div class="form-group m-form__group">
                <label>{{ trans('common.handle_date') }}</label>
                {!! Form::text('', $model->admin_response_date, ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
            </div>
        @endif
        @if ($model->user_remark != null)
            <div class="form-group m-form__group">
                <label>{{ trans('common.remark') }}</label>
                {!! Form::textarea('', $model->user_remark, ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled', 'rows' => 4]) !!}
            </div>
        @endif
        @if ($model->admin_remark != null)
            <div class="form-group m-form__group">
                <label>{{ trans('common.admin_remark') }}</label>
                {!! Form::textarea('', $model->admin_remark, ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled', 'rows' => 4]) !!}
            </div>
        @endif
    </div>
@endsection

@section('modal')

@endsection