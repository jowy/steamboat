@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_permission_group') }}
@endsection

@section('description')
    {{ trans('permission.manage_permission_group') }}
@endsection

@push('header') @endpush

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#permission-form').makeAjaxForm({
                    redirectTo: '{{ route('admin.permission') }}',
                    submitBtn: '#submit-btn'
                });

                $("[data-switch=true]").bootstrapSwitch();
            });
        }(jQuery);
    </script>
@endpush

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__body">
            {!! Form::open(['method' => 'post', 'role' => 'form', 'id' => 'permission-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
                <div class="form-group m-form__group">
                    <label>{{ trans('common.group_name') }}</label>
                    {!! Form::text('group_name', $model->group_name, ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <div class="row mb-5">
                        @foreach (\App\Models\PermissionGroupPermission::getPermissionsLists() as $permission => $explain)
                            <label class="col-form-label col-lg-3 col-sm-12">{{ $explain }}</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <input data-switch="true" type="checkbox" name="permissions[]" value="{{ $permission }}"{{ isset($permissions[$permission]) ? ' checked' : '' }}>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="form-group m-form__group">
                    <button type="button" class="btn btn-primary btn-block" id="submit-btn">{{ trans('common.edit') }}</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@push('modal')

@endpush