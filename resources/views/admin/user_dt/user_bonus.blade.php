<script>
    +function() {
        $(document).ready(function() {
            var udtUserBonus = new Datatable();
            udtUserBonus.init({
                src: $('#udt-user-bonus-dt'),
                showIndex: true,
                filterContainer: '#filter-container',
                dataTable: {
                    @include('custom.datatable.common')
                    "ajax": {
                        "url": "{{ route('admin.userdt.bonus', ['uuid' => $uuid]) }}",
                        "type": "GET"
                    },
                    columns: [
                        {data: 'id', name: 'id', sortable: true, orderable: true},
                        {data: 'user_id', name: 'user_id'},
                        {data: 'bonus_type', name: 'bonus_type'},
                        {data: 'amount', name: 'amount'},
                        {data: 'created_at', name: 'created_at'},
                    ],
                }
            });
        });
    }(jQuery);
</script>

<table class="table table-striped table-bordered table-hover" id="udt-user-bonus-dt">
    <thead>
    <tr role="row" class="heading">
        <th width="80">#</th>
        <th width="200">{{ trans('common.username') }}</th>
        <th>{{ trans('common.description') }}</th>
        <th width="180">{{ trans('common.amount') }}</th>
        <th width="180">{{ trans('common.created_at') }}</th>
    </tr>
    </thead>
</table>
