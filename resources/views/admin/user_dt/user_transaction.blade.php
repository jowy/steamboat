<script>
    +function() {
        $(document).ready(function() {
            var udtUserTransaction = new Datatable();
            udtUserTransaction.init({
                src: $('#udt-user-transaction-dt'),
                showIndex: true,
                filterContainer: '#filter-container',
                dataTable: {
                    @include('custom.datatable.common')
                    "ajax": {
                        "url": "{{ route('admin.userdt.transaction', ['uuid' => $uuid]) }}",
                        "type": "GET"
                    },
                    columns: [
                        {data: 'id', name: 'id', sortable: true, orderable: true},
                        {data: 'user_id', name: 'user_id'},
                        {data: 'transaction_type', name: 'transaction_type'},
                        {data: 'credit_type', name: 'credit_type'},
                        {data: 'amount', name: 'amount'},
                        {data: 'created_at', name: 'created_at'},
                    ],
                }
            });
        });
    }(jQuery);
</script>

<table class="table table-striped table-bordered table-hover" id="udt-user-transaction-dt">
    <thead>
    <tr role="row" class="heading">
        <th width="80">#</th>
        <th width="200">{{ trans('common.username') }}</th>
        <th>{{ trans('common.description') }}</th>
        <th width="180">{{ trans('common.credit_type') }}</th>
        <th width="180">{{ trans('common.amount') }}</th>
        <th width="180">{{ trans('common.created_at') }}</th>
    </tr>
    </thead>
</table>
