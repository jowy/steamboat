@extends('ajaxmodal')

@section('title')
    {{ trans('common.price') }}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                @if ($model)
                $('#create-form').makeAjaxForm({
                    submitBtn: '#btn-submit',
                    inModal: true,
                    closeModal: true,
                });
                @endif
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    @if ($model)
        <button type="button" id="btn-submit" class="btn btn-primary blue">{{ trans('common.confirm') }}</button>
    @endif
@endsection

@section('content')
    @if ($model)
        {!! Form::open(['route' => ['admin.price.store', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'create-form', 'class' => 'm-form m-form--fit m-form--label-align-right', 'files' => true]) !!}
        <div class="form-group m-form__group">
            <label>{{ trans('common.price_name') }}</label>
            {!! Form::text('name_en', '', ['class' => 'form-control m-input', 'placeholder' => 'Special Price']) !!}
            <span class="help-block">For remark purpose only.</span>
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.price_per_pax') }}</label>
            {!! Form::text('per_pax_price', '', ['class' => 'form-control m-input fund-input', 'placeholder' => '29.90']) !!}
        </div>
        {!! Form::close() !!}
    @else
        <h4>{{ trans('common.record_not_found') }}</h4>
    @endif
@endsection

@section('modal')

@endsection
