@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_price') }}
@endsection

@section('description')
    {{ trans('permission.manage_price') }}
@endsection

@section('author') @endsection

@section('metatag') @endsection

@push('header') @endpush

@push('action') @endpush

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.lists') }}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin.price.create') }}" class="btn btn-primary" data-target="#remote-modal" data-toggle="modal"><i class="md-plus"></i> {{ trans('common.create') }}</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-hover table-bordered table-striped" id="data-dt">
                <thead>
                    <tr role="row" class="heading">
                        <th width="80">#</th>
                        <th width="180">{{ trans('common.action') }}</th>
                        <th width="180">{{ trans('common.price_name') }}</th>
                        <th width="200">{{ trans('common.price_per_pax') }}</th>
                        <th width="200">{{ trans('common.created_at') }}</th>
                        <th width="200">{{ trans('common.updated_at') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('footer')
    <script type="text/javascript">
        (function ($) {
            $(document).ready(function () {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#data-dt'),
                    showIndex: true,
                    filterContainer: '#filter-container',
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('admin.price') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id', sortable: true},
                            {data: 'actions', name: 'actions', orderable: false, sortable: false},
                            {data: 'name_en', name: 'name_en'},
                            {data: 'per_pax_price', name: 'per_pax_price'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'updated_at', name: 'updated_at'},
                        ]
                    }
                });
            });
        }(jQuery));
    </script>
@endpush

@push('modal') @endpush
