@extends('ajaxmodal')

@section('title')
    {{ trans('common.price') }}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                @if ($model)
                $('#update-form').makeAjaxForm({
                    submitBtn: '#btn-submit',
                    inModal: true,
                    closeModal: true,
                });
                @endif
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    @if ($model)
        @if($model->type != 'free')
            <button type="button" id="btn-submit" class="btn btn-primary blue">{{ trans('common.confirm') }}</button>
        @endif
    @endif
@endsection

@section('content')
    @if ($model)
        {!! Form::open(['route' => ['admin.price.update.post', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'update-form', 'class' => 'm-form m-form--fit m-form--label-align-right', 'files' => true]) !!}
        <div class="form-group m-form__group">
            <label>{{ trans('common.price_name') }}</label>
            @if($model->is_system)
                {!! Form::text('', ucfirst($model->name_en), ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
            @else
                {!! Form::text('name_en', ucfirst($model->name_en), ['class' => 'form-control m-input']) !!}
            @endif
        </div>
        <div class="form-group m-form__group">
            @if($model->type == 'free')
                <label>{{ trans('common.price_per_pax') }}</label>
                {!! Form::text('per_pax_price', fundFormat($model->per_pax_price), ['class' => 'form-control m-input fund-input', 'disabled' => 'disabled']) !!}
            @else
                <label>{{ trans('common.price_per_pax') }}</label>
                {!! Form::text('per_pax_price', fundFormat($model->per_pax_price), ['class' => 'form-control m-input fund-input']) !!}
            @endif
        </div>
        {!! Form::close() !!}
    @else
        <h4>{{ trans('common.record_not_found') }}</h4>
    @endif
@endsection

@section('modal')

@endsection
