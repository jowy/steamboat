@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_category') }}
@endsection

@section('description')
    {{ trans('permission.manage_category') }}
@endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header')
    <link href="/custom/nestable/nestable.css" rel="stylesheet">
    <style>

    </style>
@endpush

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.lists') }}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin.category.sub.create') }}" class="btn btn-primary" data-target="#remote-modal" data-toggle="modal"><i class="md-plus"></i> {{ trans('common.create') }}</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            {!! Form::open(['route' => ['admin.category.update.post'], 'id' => 'menu-form', 'role' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
                <div class="dd" id="nestable">
                    <ol class="dd-list">
                        @foreach ($first_level as $k1 => $l1)
                            <li class="dd-item" data-id="{{ $l1->id }}">
                                <a data-href="{{ route('admin.category.sub.delete.post', ['id' => $l1->id]) }}" class="btn btn-danger btn-sm" data-redirect="no" data-toggle="modal" data-target="#confirm-modal" data-header="{{ trans('common.confirm') }}?" data-body="{{ trans('common.are_your_sure') }}?" data-successcall="reloadMenu"><i class="fa fa-trash"></i></a>
                                <a href="{{ route('admin.category.sub.edit', ['id' => $l1->id]) }}" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#remote-modal"><i class="fa fa-eye"></i></a>
                                <div class="dd-handle">{{ $l1->getNames() }}</div>
                                <ol class="dd-list">
                                    @foreach ($l1->sons as $k2 => $l2)
                                        <li class="dd-item" data-id="{{ $l2->id }}">
                                            <a data-href="{{ route('admin.category.sub.delete.post', ['id' => $l2->id]) }}" class="btn btn-danger btn-sm" data-redirect="no" data-toggle="modal" data-target="#confirm-modal" data-header="{{ trans('common.confirm') }}?" data-body="{{ trans('common.are_your_sure') }}?" data-successcall="reloadMenu"><i class="fa fa-trash"></i></a>
                                            <a href="{{ route('admin.category.sub.edit', ['id' => $l2->id]) }}" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#remote-modal"><i class="fa fa-eye"></i></a>
                                            <div class="dd-handle">{{ $l2->getNames() }}</div>
                                        </li>
                                    @endforeach
                                </ol>
                            </li>
                        @endforeach
                    </ol>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@push('footer')
    <script type="text/javascript" src="/custom/nestable/jquery.nestable.js"></script>
    <script>
        window['reloadMenu'] = function (resp) {
            if (typeof resp.model_id !== 'undefined') {
                $('.dd-item[data-id="' + resp.model_id + '"]').remove();
                alertSuccess('{{ trans('common.operation_success') }}');
            }
        };

        +function ($) {
            $(document).ready(function () {

                $(document).on('change update blur', '#type-selector', function () {
                    var value = $(this).val();

                    $('.type-related').hide();
                    $('#type-related-' + value).show();
                });

                $('#nestable').nestable({
                    group: 1,
                    maxDepth: 1,
                });

                //Reorder
                $(document).on('change', '.dd-item', function(e) {
                    e.stopPropagation();

                    var id = $(this).data('id'),
                        parentId = $(this).parents('.dd-item').data('id');

                    if (typeof parentId === 'undefined') {
                        parentId = 0;
                    }

                    var brothers = $(this).closest('.dd-list').find('.dd-item');

                    var ids = [];
                    $(brothers).each(function () {
                        ids.push($(this).data('id'));
                    });

                    var data = {
                        parent_id: parentId,
                        target_id: id,
                        ids: ids
                    };

                    $.ajax({
                        url: "{{ route('admin.category.sub.updatesorting') }}",
                        type: "post",
                        dataType: 'json',
                        data: data,
                        beforeSend: function () {
                            blockBody();
                        },
                        success: function (resp) {
                            if (typeof resp.msg !== 'undefined') {
                                alertSuccess(resp.message);
                            }
                        },
                        error: function (resp) {
                            handleError(resp);
                        },
                        complete: function () {
                            unblockBody();
                        }
                    });
                });
            });
        }(jQuery);
    </script>
@endpush

@push('modal') @endpush