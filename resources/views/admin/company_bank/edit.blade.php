@extends('ajaxmodal')

@section('title')
    {{ trans('permission.manage_company_bank') }}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#data-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit" class="btn btn-primary blue">{{ trans('common.submit') }}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['admin.companybank.edit.post', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'data-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
        <div class="form-group m-form__group">
            <label>{{ trans('common.bank') }}</label>
            {!! Form::select('bank_id', \App\Models\Bank::GetSelects(), $model->bank_id, ['class' => 'form-control m-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.bank_account_name') }}</label>
            {!! Form::text('account_name', $model->account_name, ['class' => 'form-control m-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.bank_account_number') }}</label>
            {!! Form::text('account_number', $model->account_number, ['class' => 'form-control m-input']) !!}
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection