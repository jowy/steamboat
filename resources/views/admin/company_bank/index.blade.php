@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_company_bank') }}
@endsection

@section('description')
    {{ trans('permission.manage_company_bank') }}
@endsection

@push('header')

@endpush

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#data-dt'),
                    showIndex: true,
                    filterContainer: '#filter-container',
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('admin.companybank.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id', sortable: false},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false},
                            {data: 'country_id', name: 'country_id'},
                            {data: 'name_en', name: 'name_en'},
                            {data: 'name_cn', name: 'name_cn'},
                            {data: 'account_name', name: 'account_name'},
                            {data: 'account_number', name: 'account_number'},
                            {data: 'updated_at', name: 'updated_at'},
                            {data: 'created_at', name: 'created_at'},
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endpush

@section('content')
    <div class="m-portlet m-portlet--mobile" id="filter-container">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.filter') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m-form m-form--fit m-form--label-align-right">
                <div class="form-group m-form__group">
                    <label>{{ trans('common.date') }}</label>
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            {!! Form::text('filter_created_after_date', '', ['class' => 'form-control m-input form-filter input-sm date-picker', 'placeholder' => trans('common.from')]) !!}
                        </div>
                        <div class="col-12 col-lg-6">
                            {!! Form::text('filter_created_before_date', '', ['class' => 'form-control m-input form-filter input-sm date-picker', 'placeholder' => trans('common.to')]) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.bank_name') }}</label>
                    <input type="text" class="form-control m-input form-filter input-sm" name="filter_name" placeholder="{{ trans('common.bank_name') }}">
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.bank_account_name') }}</label>
                    <input type="text" class="form-control m-input form-filter input-sm" name="filter_account_name" placeholder="{{ trans('common.bank_account_name') }}">
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.bank_account_number') }}</label>
                    <input type="text" class="form-control m-input form-filter input-sm" name="filter_account_number" placeholder="{{ trans('common.bank_account_number') }}">
                </div>
                <div class="form-group m-form__group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <button class="btn btn-danger btn-block  filter-submit">
                                <i class="fa fa-search"></i> {{ trans('common.filter') }}
                            </button>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <button class="btn btn-info btn-block  filter-cancel">
                                <i class="fa fa-times"></i> {{ trans('common.reset') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.lists') }}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin.companybank.create') }}" class="btn btn-primary" data-target="#remote-modal" data-toggle="modal"><i class="md-plus"></i> {{ trans('common.create') }}</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-hover table-bordered table-striped" id="data-dt">
                <thead>
                    <tr role="row" class="heading">
                        <th width="80">#</th>
                        <th>{{ trans('common.actions') }}</th>
                        <th>{{ trans('common.country') }}</th>
                        <th>{{ trans('common.name_in_english') }}</th>
                        <th>{{ trans('common.name_in_chinese') }}</th>
                        <th>{{ trans('common.bank_account_name') }}</th>
                        <th>{{ trans('common.bank_account_number') }}</th>
                        <th>{{ trans('common.updated_at') }}</th>
                        <th>{{ trans('common.created_at') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('modal')

@endpush