@extends('ajaxmodal')

@section('title')
    {{ trans('permission.manage_user') }}
    @if ($model->is_blank_account == 1)
        <span class="font-red">({{ trans('common.blank_account') }})</span>
    @endif
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                @if ($_G['admin']->hasPermission('manage_user_advance'))
                    $('#user-form').makeAjaxForm({
                        inModal: true,
                        closeModal: true,
                        submitBtn: '#btn-submit-user'
                    });
                @endif
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    @if ($_G['admin']->hasPermission('manage_user_advance'))
    <button type="button" id="btn-submit-user" class="btn btn-primary text-white">{{ trans('common.edit') }}</button>
    @endif
@endsection

@section('content')
    @if ($_G['admin']->hasPermission('manage_user_advance'))
    {!! Form::open(['route' => ['admin.user.update', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'user-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
    @else
    <div class="form-horizontal">
    @endif
    <div class="form-body">
        <div class="form-group m-form__group">
            <label>{{ trans('common.username') }}</label>
            {!! Form::text('username', $model->username, ['class' => 'form-control m-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.email') }}</label>
            {!! Form::text('email', $model->email, ['class' => 'form-control m-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.name') }}</label>
            {!! Form::text('name', $model->name, ['class' => 'form-control m-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.gender') }}</label>
            {!! Form::select('gender', \App\Constants::getGenders(), $model->gender, ['class' => 'form-control m-input', 'placeholder' => trans('common.gender')]) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.language') }}</label>
            {!! Form::select('lang', \App\Constants::getLanguages(), $model->lang, ['class' => 'form-control m-input', 'placeholder' => trans('common.please_select')]) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.country') }}</label>
            {!! Form::select('country_id', \App\Constants::getCountryForSelect(), $model->country_id, ['class' => 'form-control m-input']) !!}
        </div>
        @if ($_G['admin']->hasPermission('manage_user_advance'))
            <div class="form-group m-form__group">
                <label>{{ trans('common.new_password') }}</label>
                {!! Form::password('new_password', ['class' => 'form-control m-input']) !!}
                <span class="help-block">{{ trans('common.leave_blank_not_change') }}</span>
            </div>
            <div class="form-group m-form__group">
                <label>{{ trans('common.new_password_confirmation') }}</label>
                {!! Form::password('new_password_confirmation', ['class' => 'form-control m-input']) !!}
                <span class="help-block">{{ trans('common.leave_blank_not_change') }}</span>
            </div>
            <div class="form-group m-form__group">
                <label>{{ trans('common.new_password2') }}</label>
                {!! Form::password('new_password2', ['class' => 'form-control m-input']) !!}
                <span class="help-block">{{ trans('common.leave_blank_not_change') }}</span>
            </div>
            <div class="form-group m-form__group">
                <label>{{ trans('common.new_password2_confirmation') }}</label>
                {!! Form::password('new_password2_confirmation', ['class' => 'form-control m-input']) !!}
                <span class="help-block">{{ trans('common.leave_blank_not_change') }}</span>
            </div>
        @endif
    </div>
    @if ($_G['admin']->hasPermission('manage_user_advance'))
    {!! Form::close() !!}
    @else
    </div>
    @endif
@endsection

@section('modal')

@endsection