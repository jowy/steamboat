@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_user') }}
@endsection

@section('description')
    {{ trans('permission.manage_user') }}
@endsection

@push('header')

@endpush

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#data-dt'),
					showIndex: true,
					filterContainer: '#filter-container',
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('admin.user.dt') }}",
                            "type": "GET"
                        },
                        columns: [
							{data: 'id', name: 'id', sortable: true, orderable: true},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false},
                            {data: 'username', name: 'username'},
                            {data: 'name', name: 'name'},
                            {data: 'email', name: 'email'},
                            {data: 'country_id', name: 'country_id'},
                            {data: 'contact_country_id', name: 'contact_country_id'},
                            {data: 'credit_1', name: 'credit_1'},
                            {data: 'credit_2', name: 'credit_2'},
                            {data: 'freeze', name: 'freeze'},
                            {data: 'created_at', name: 'created_at'},
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endpush

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="dashboard-stat info">
                <div class="visual">
                    <i class="fa fa-money"></i>
                </div>
                <div class="details">
                    <div class="number">{{ fundFormat($credit_1, \App\Constants::$decimal_point) }}</div>
                    <div class="desc"> {{ trans('common.credit_1') }} </div>
                </div>
                <a class="more" href="javascript:;"> &nbsp;
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="dashboard-stat info">
                <div class="visual">
                    <i class="fa fa-money"></i>
                </div>
                <div class="details">
                    <div class="number">{{ fundFormat($credit_2, \App\Constants::$decimal_point) }}</div>
                    <div class="desc"> {{ trans('common.credit_2') }} </div>
                </div>
                <a class="more" href="javascript:;"> &nbsp;
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="dashboard-stat info">
                <div class="visual">
                    <i class="fa fa-money"></i>
                </div>
                <div class="details">
                    <div class="number">{{ fundFormat($credit_3, \App\Constants::$decimal_point) }}</div>
                    <div class="desc"> {{ trans('common.credit_3') }} </div>
                </div>
                <a class="more" href="javascript:;"> &nbsp;
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="dashboard-stat info">
                <div class="visual">
                    <i class="fa fa-money"></i>
                </div>
                <div class="details">
                    <div class="number">{{ fundFormat($credit_4, \App\Constants::$decimal_point) }}</div>
                    <div class="desc"> {{ trans('common.credit_4') }} </div>
                </div>
                <a class="more" href="javascript:;"> &nbsp;
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile" id="filter-container">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.filter') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m-form m-form--fit m-form--label-align-right">
                <div class="form-group m-form__group">
                    <label>{{ trans('common.date') }}</label>
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            {!! Form::text('filter_created_after_date', '', ['class' => 'form-control m-input form-filter input-sm date-picker', 'placeholder' => trans('common.from')]) !!}
                        </div>
                        <div class="col-12 col-lg-6">
                            {!! Form::text('filter_created_before_date', '', ['class' => 'form-control m-input form-filter input-sm date-picker', 'placeholder' => trans('common.to')]) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.username') }}</label>
                    {!! Form::text('filter_username', '', ['class' => 'form-control m-input form-filter input-sm', 'placeholder' => trans('common.username')]) !!}
                </div>
                <div class="form-group m-form__group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <button class="btn btn-danger btn-block  filter-submit">
                                <i class="fa fa-search"></i> {{ trans('common.filter') }}
                            </button>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <button class="btn btn-info btn-block  filter-cancel">
                                <i class="fa fa-times"></i> {{ trans('common.reset') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.lists') }}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-hover table-bordered table-striped" id="data-dt">
                <thead>
                    <tr role="row" class="heading">
                        <th width="80">#</th>
                        <th>{{ trans('common.actions') }}</th>
                        <th>{{ trans('common.username') }}</th>
                        <th>{{ trans('common.name') }}</th>
                        <th>{{ trans('common.email') }}</th>
                        <th>{{ trans('common.country') }}</th>
                        <th>{{ trans('common.contact_number') }}</th>
                        <th>{{ trans('common.credit_1') }}</th>
                        <th>{{ trans('common.credit_2') }}</th>
                        <th>{{ trans('common.status') }}</th>
                        <th>{{ trans('common.created_at') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('modal')

@endpush