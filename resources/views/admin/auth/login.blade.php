<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <title>
            @if ($_G['setting']['site_name_' . $_G['lang']])
                {{ $_G['setting']['site_name_' . $_G['lang']] }}
            @endif
            {{ trans('common.login') }}
            @if ($_G['setting']['slogan_' . $_G['lang']])
                {{ $_G['setting']['slogan_' . $_G['lang']] }}
            @endif
        </title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
            WebFont.load({
                google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                active: function() {
                    sessionStorage.fonts = true;
                }
            });
        </script>
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link rel="apple-touch-icon" href="{{ $_G['setting']['favicon'] }}">
        <link rel="shortcut icon" href="{{ $_G['setting']['favicon'] }}">
        <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,300italic">
        @if (config('env.APP_DEBUG') == true)
            <link rel="stylesheet" href="{{ asset('assets/admin/vendors/base/vendors.bundle.css?id=' . time()) }}">
            <link rel="stylesheet" href="{{ asset('assets/admin/assets/demo/default/base/style.bundle.css?id=' . time()) }}">
            <link rel="stylesheet" href="{{ asset('custom/flag-icon-css/flag-icon.min.css?id=' . time()) }}">
            <link rel="stylesheet" href="{{ asset('custom/datatables/datatables.min.css?id=' . time()) }}">
            <link rel="stylesheet" href="{{ asset('custom/global.css?id=' . time()) }}">
            <link rel="stylesheet" href="{{ asset('custom/admin.css?id=' . time()) }}">
        @else
            <link rel="stylesheet" href="{{ mix('css/admin_core.css') }}">
        @endif
    </head>
    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-3" id="m_login" style="background-image: url('/assets/admin/assets/app/media/img//bg/bg-2.jpg');">
                <div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
                    <div class="m-login__container">
                        <div class="m-login__logo">
                            <a href="#">
                                <img src="{{ $_G['setting']['admin_login_logo_' . $_G['lang']] }}" style="max-width: 200px;">
                            </a>
                        </div>
                        <div class="m-login__signin">
                            <div class="m-login__head">
                                <h3 class="m-login__title">{{ trans('common.login') }}</h3>
                            </div>
                            {!! Form::open(['route' => 'admin.login.post', 'role' => 'form', 'method' => 'post', 'autocomplete' => 'off', 'id' => 'login-form', 'class' => 'm-login__form m-form']) !!}
                                <div class="form-group m-form__group">
                                    @include('admin.include.flash')
                                </div>
                                <div class="form-group m-form__group">
                                    {!! Form::text('username', '', ['class' => 'form-control m-input', 'placeholder' => trans('common.username')]) !!}
                                </div>
                                <div class="form-group m-form__group">
                                    {!! Form::password('password', ['class' => 'form-control m-input', 'placeholder' => trans('common.password')]) !!}
                                </div>
                                <div class="row m-login__form-sub">
                                    <div class="col m--align-left m-login__form-left">
                                        <label class="m-checkbox  m-checkbox--light">
                                            <input type="checkbox" name="remember" value="1"> {{ trans('common.remember_me') }}
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="m-login__form-action">
                                    <button id="submit-login-btn" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">{{ trans('common.login_now') }}</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (config('env.APP_DEBUG') == true)
            <script src="{{ asset('custom/jquery.min.js?id=' . time()) }}"></script>
            <script src="{{ asset('assets/admin/vendors/base/vendors.bundle.js?id=' . time()) }}"></script>
            <script src="{{ asset('assets/admin/assets/demo/default/base/scripts.bundle.js?id=' . time()) }}"></script>
            <script src="{{ asset('custom/datatables/datatables.min.js?id=' . time()) }}"></script>
            <script src="{{ asset('custom/sweetalert.min.js?id=' . time()) }}"></script>
            <script src="{{ asset('custom/jquery.form.min.js?id=' . time()) }}"></script>
            <script src="{{ asset('custom/jquery.blockUI.js?id=' . time()) }}"></script>
            <script src="{{ asset('custom/functions.js?id=' . time()) }}"></script>
            <script src="{{ asset('custom/extension.js?id=' . time()) }}"></script>
            <script src="{{ asset('custom/datatable.js?id=' . time()) }}"></script>
            <script src="{{ asset('custom/app.js?id=' . time()) }}"></script>
            <script src="{{ asset('custom/global.js?id=' . time()) }}"></script>
            <script src="{{ asset('custom/admin.js?id=' . time()) }}"></script>
        @else
            <script src="{{ mix('/js/admin_core.js') }}" type="text/javascript"></script>
        @endif
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
        <script>
            +function ($) {
                $(document).ready(function () {
                    $('#login-form').makeAjaxForm({
                        submitBtn: '#submit-login-btn',
                        redirectTo: '{{ route('admin.home') }}'
                    });
                });
            }(jQuery);
        </script>
    </body>
</html>