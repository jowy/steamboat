@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_country') }}
@endsection

@section('description')
    {{ trans('permission.manage_country') }}
@endsection

@section('author') @endsection

@section('metatag') @endsection

@push('header') @endpush

@push('action') @endpush

@section('content')
    <div class="m-portlet m-portlet--mobile" id="filter-container">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.filter') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m-form m-form--fit m-form--label-align-right">
                <div class="form-group m-form__group">
                    <label>{{ trans('common.country_name') }}</label>
                    {!! Form::text('filter_country_name', '', ['class' => 'form-control m-input form-filter input-sm', 'placeholder' => trans('common.country_name')]) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.status') }}</label>
                    {!! Form::select('filter_status', $status, '', ['class' => 'form-control m-input form-filter input-sm', 'placeholder' => trans('common.all')]) !!}
                </div>
                <div class="form-group m-form__group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <button class="btn btn-danger btn-block filter-submit">
                                <i class="fa fa-search"></i> {{ trans('common.filter') }}
                            </button>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <button class="btn btn-info btn-block filter-cancel">
                                <i class="fa fa-times"></i> {{ trans('common.reset') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.lists') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-hover table-bordered table-striped" id="data-dt">
                <thead>
                    <tr role="row" class="heading">
                        <th width="80">#</th>
                        <th width="180">{{ trans('common.actions') }}</th>
                        <th width="200">{{ trans('common.country_name') }}</th>
                        <th width="200">{{ trans('common.actual_exchange_rate') }}</th>
                        <th width="200">{{ trans('common.buy_in') }}</th>
                        <th width="200">{{ trans('common.sell_out') }}</th>
                        <th width="200">{{ trans('common.status') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('footer')
    <script type="text/javascript">
        (function ($) {
            $(document).ready(function () {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#data-dt'),
                    showIndex: true,
                    filterContainer: '#filter-container',
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('admin.country.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id', sortable: true},
                            {data: 'actions', name: 'actions', orderable: false, sortable: false},
                            {data: 'country_name_en', name: 'country_name_en'},
                            {data: 'currency_actual_rate', name: 'currency_actual_rate'},
                            {data: 'currency_in_rate', name: 'currency_in_rate'},
                            {data: 'currency_out_rate', name: 'currency_out_rate'},
                            {data: 'status', name: 'status'},
                        ]
                    }
                });
            });
        }(jQuery));
    </script>
@endpush

@push('modal') @endpush