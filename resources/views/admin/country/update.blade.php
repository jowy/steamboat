@extends('ajaxmodal')

@section('title')
    {{ trans('common.country') }}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                @if ($model)
                $('#update-form').makeAjaxForm({
                    submitBtn: '#btn-submit',
                    inModal: true,
                    closeModal: true,
                });
                @endif
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    @if ($model)
    <button type="button" id="btn-submit" class="btn btn-primary blue">{{ trans('common.confirm') }}</button>
    @endif
@endsection

@section('content')
    @if ($model)
        {!! Form::open(['route' => ['admin.country.update.post', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'update-form', 'class' => 'm-form m-form--fit m-form--label-align-right', 'files' => true]) !!}
            <div class="form-group m-form__group">
                <label>{{ trans('common.country_name') }}</label>
                {!! Form::text('', $model->getCountryName(), ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>{{ trans('common.actual_exchange_rate') }}</label>
                {!! Form::text('currency_actual_rate', $model->currency_actual_rate, ['class' => 'form-control m-input fund-input']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>{{ trans('common.buy_in') }}</label>
                {!! Form::text('currency_in_rate', $model->currency_in_rate, ['class' => 'form-control m-input fund-input']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>{{ trans('common.sell_out') }}</label>
                {!! Form::text('currency_out_rate', $model->currency_out_rate, ['class' => 'form-control m-input fund-input']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>{{ trans('common.currency_prefix') }}</label>
                {!! Form::text('currency_prefix', $model->currency_prefix, ['class' => 'form-control m-input']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>{{ trans('common.currency_suffix') }}</label>
                {!! Form::text('currency_suffix', $model->currency_suffix, ['class' => 'form-control m-input']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>{{ trans('common.phone_extension') }}</label>
                {!! Form::text('ext', $model->ext, ['class' => 'form-control m-input']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>{{ trans('common.status') }}</label>
                {!! Form::select('status', $status, $model->status, ['class' => 'form-control m-input']) !!}
            </div>
        {!! Form::close() !!}
    @else
        <h4>{{ trans('common.record_not_found') }}</h4>
    @endif
@endsection

@section('modal')

@endsection
