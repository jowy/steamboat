@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_slider') }}
@endsection

@section('description')
    {{ trans('permission.manage_slider') }}
@endsection

@push('header') @endpush

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#slider-form').makeAjaxForm({
                    redirectTo: '{{ route('admin.slider') }}',
                    submitBtn: '#submit-btn'
                });
            });
        }(jQuery);
    </script>
@endpush

@push('action') @endpush

@section('content')
    {!! Form::open(['route' => 'admin.slider.create.post', 'method' => 'post', 'role' => 'form', 'id' => 'slider-form', 'files' => true, 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
        <div class="m-portlet m-portlet--mobile m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#tab-en" role="tab">
                                <i class="flag-icon {{ \App\Constants::getFlagClassByLanguage('en') }}" aria-hidden="true"></i> {{ trans('common.english') }}
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-cn" role="tab">
                                <i class="flag-icon {{ \App\Constants::getFlagClassByLanguage('cn') }}" aria-hidden="true"></i> {{ trans('common.chinese') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="form-group m-form__group">
                    <label class="control-label">{{ trans('common.start_datetime') }}</label>
                    {!! Form::text('start_at', '', ['class' => 'form-control m-input datetime-picker']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label class="control-label">{{ trans('common.end_datetime') }}</label>
                    {!! Form::text('end_at', '', ['class' => 'form-control m-input datetime-picker']) !!}
                </div>
                <hr />
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-en" role="tabpanel" aria-expanded="true">
                        <div class="form-body">
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.title') }}</label>
                                {!! Form::text('title_en', '', ['class' => 'form-control m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.description') }}</label>
                                {!! Form::text('description_en', '', ['class' => 'form-control m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.open_in_new_tab') }}</label>
                                {!! Form::select('new_tab_en', \App\Constants::getYesNoForSelect(), '', ['class' => 'form-control m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.url') }}</label>
                                {!! Form::text('url_en', '', ['class' => 'form-control m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.url_text') }}</label>
                                {!! Form::text('url_text_en', '', ['class' => 'form-control m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.image') }}</label>
                                {!! Form::file('image_en', ['class' => 'form-control m-input', 'accept' => 'image/*']) !!}
                                <span class="text-help">{{ trans('common.recommend_image_size', ['width' => \App\Models\Slider::IMAGE_WIDTH, 'height' => \App\Models\Slider::IMAGE_HEIGHT]) }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-cn" role="tabpanel" aria-expanded="false">
                        <div class="form-body">
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.title') }}</label>
                                {!! Form::text('title_cn', '', ['class' => 'form-control m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.description') }}</label>
                                {!! Form::text('description_cn', '', ['class' => 'form-control m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.open_in_new_tab') }}</label>
                                {!! Form::select('new_tab_cn', \App\Constants::getYesNoForSelect(), '', ['class' => 'form-control m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.url') }}</label>
                                {!! Form::text('url_cn', '', ['class' => 'form-control m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.url_text') }}</label>
                                {!! Form::text('url_text_cn', '', ['class' => 'form-control m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.image') }}</label>
                                {!! Form::file('image_cn', ['class' => 'form-control m-input', 'accept' => 'image/*']) !!}
                                <span class="text-help">{{ trans('common.recommend_image_size', ['width' => \App\Models\Slider::IMAGE_WIDTH, 'height' => \App\Models\Slider::IMAGE_HEIGHT]) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="form-group m-form__group">
                    <button type="button" class="btn btn-primary btn-block " id="submit-btn"><i class="fa fa-check"></i> {{ trans('common.submit') }}</button>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@push('modal')

@endpush