@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_supplier') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@section('metatag') @endsection

@push('header') @endpush

@push('action')
@endpush

@section('content')
    {!! Form::open(['route' => ['admin.supplier.create.post'], 'method' => 'post', 'role' => 'form', 'id' => 'supplier-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="form-group m-form__group">
                    <label>{{ trans('common.supplier_name') }} </label>
                    {!! Form::text('name', '', ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.contact_number') }}</label>
                    {!! Form::text('contact_number', '', ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.remark') }}</label>
                    {!! Form::textarea('remark', '', ['class' => 'form-control m-input']) !!}
                </div>
               {{-- <hr />--}}
                {{--<div class="form-group m-form__group">
                    <div class="panel panel-bordered" id="maps-container">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('common.map') }}</h3>
                        </div>
                        <div class="panel-body gmap-container">
                            <div class="form-body" style="display: table; width: 100%;">
                                <div class="form-group google-map-setup row">
                                    <div class="col-sm-12 col-md-7">
                                        <h3>{{ trans('common.search_map_prefix') }}<img src="{{ asset('img/gmapmarker.png') }}" style="width: 21px;"> to arrange your location</h3>
                                        <div class="searchmap-map-container" style="height: 350px;"></div>
                                    </div>
                                    <div class="col-sm-12 col-md-5">
                                        <h3>{{ trans('common.search_address') }}:</h3>
                                        <a href="javascript:;" class="clear-map">{{ trans('common.clear_map_data') }}</a>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="text" name="address" value="" class="googlemap-address-input form-control" placeholder="{{ trans('common.address') }}" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" name="latitude" value="" class="googlemap-latitude-input form-control" placeholder="{{ trans('common.latitude') }}" readonly />
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="longitude" value="" class="googlemap-longitude-input form-control" placeholder="{{ trans('common.longitude') }}" readonly />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--}}
                <div class="form-group m-form__group">
                    <button type="button" id="btn-submit" class="btn btn-primary btn-block blue">{{ trans('common.create') }}</button>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@push('footer')
   {{-- <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?sensor=true&key={{ config('env.GOOGLE_MAP_API_KEY') }}&libraries=places"></script>
    <script type="text/javascript" src="/custom/gmap.js"></script>--}}
    <script type="text/javascript">
        +function() {
            $(document).ready(function() {
                $('#supplier-form').makeAjaxForm({
                    submitBtn: '#btn-submit',
                    redirectTo: '{{ route('admin.supplier') }}',
                });
                //Google Maps
                /*$('.gmap-container').makeGmapContainer();
                $('#add-map').on('click', function() {
                    var mapCount = $('.gmap-container').length;
                    var index = parseInt(mapCount) + parseInt(1);
                    var template = getMapSetupTemplate();
                    template = template.replace(/@@index@@/g, index);

                    $('#maps-container').append(template);
                    $('.gmap-container').makeGmapContainer();
                });*/
            });
        }(jQuery);
    </script>
@endpush

@push('modal') @endpush
