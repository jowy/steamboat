@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_announcement') }}
@endsection

@section('description')
    {{ trans('permission.manage_announcement') }}
@endsection

@section('author') @endsection

@section('metatag') @endsection

@push('header') @endpush

@section('action') @endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.lists') }}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin.announcement.create') }}" class="btn btn-primary"><i class="md-plus"></i> {{ trans('common.create') }}</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-hover table-bordered table-striped" id="data-dt">
                <thead>
                    <tr role="row" class="heading">
                        <th width="80">#</th>
                        <th width="150">{{ trans('common.actions') }}</th>
                        <th>{{ trans('common.title') }}</th>
                        <th width="200">{{ trans('common.created_at') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('footer')
    <script>
        +function () {
            $(document).ready(function () {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#data-dt'),
                    showIndex: true,
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('admin.announcement.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false},
                            {data: 'title', name: 'username', orderable: false, sortable: false},
                            {data: 'created_at', name: 'created_at'},
                        ]
                    }
                });
            });
        }(jQuery);
    </script>
@endpush

@push('modal') @endpush


