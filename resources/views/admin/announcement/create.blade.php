@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_announcement') }}
@endsection

@section('description')
    {{ trans('permission.manage_announcement') }}
@endsection

@section('author') @endsection

@section('metatag') @endsection

@push('header') @endpush

@push('action') @endpush

@section('content')
    {!! Form::open(['route' => 'admin.announcement.create.post', 'id' => 'announcement-form', 'role' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
        <div class="m-portlet m-portlet--mobile m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#tab-en" role="tab">
                                <i class="flag-icon {{ \App\Constants::getFlagClassByLanguage('en') }}" aria-hidden="true"></i> {{ trans('common.english') }}
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-cn" role="tab">
                                <i class="flag-icon {{ \App\Constants::getFlagClassByLanguage('cn') }}" aria-hidden="true"></i> {{ trans('common.chinese') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-en" role="tabpanel" aria-expanded="true">
                        <div class="form-body">
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.title')  }}</label>
                                {!! Form::text('title_en', '', ['class' => 'form-control m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.content') }}</label>
                                {!! Form::textarea('content_en', '', ['class' => 'form-control m-input editor-mce']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-cn" role="tabpanel" aria-expanded="false">
                        <div class="form-body">
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.title')  }}</label>
                                {!! Form::text('title_cn', '', ['class' => 'form-control m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label class="control-label">{{ trans('common.content') }}</label>
                                {!! Form::textarea('content_cn', '', ['class' => 'form-control m-input editor-mce']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="form-group m-form__group">
                    <button type="button" class="btn btn-primary btn-block " id="submit-btn"><i class="fa fa-check"></i> {{ trans('common.create') }}</button>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@push('footer')
    @include('tinymce', ['selector' => '.editor-mce'])
    <script>
        +function () {
            $(document).ready(function () {
                $('#announcement-form').makeAjaxForm({
                    submitBtn: '#submit-btn',
                    redirectTo: '{{ route('admin.announcement') }}'
                });
            });
        }(jQuery);
    </script>
@endpush

@push('modal') @endpush


