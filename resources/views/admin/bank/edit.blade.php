@extends('ajaxmodal')

@section('title')
    {{ trans('permission.manage_bank') }}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#data-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit" class="btn btn-primary">{{ trans('common.submit') }}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['admin.bank.edit.post', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'data-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
        <div class="form-group m-form__group">
            <label>{{ trans('common.country') }}</label>
            {!! Form::select('country_id', \App\Models\Country::GetCountries()->GetSelects(), $model->country_id, ['class' => 'form-control m-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.name_in_english') }}</label>
            {!! Form::text('name_en', $model->name_en, ['class' => 'form-control m-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.name_in_chinese') }}</label>
            {!! Form::text('name_cn', $model->name_cn, ['class' => 'form-control m-input']) !!}
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection