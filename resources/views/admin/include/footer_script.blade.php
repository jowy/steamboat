@if (config('env.APP_DEBUG') == true)
    <script src="{{ asset('custom/jquery.min.js?id=' . time()) }}"></script>
    <script src="{{ asset('assets/admin/vendors/base/vendors.bundle.js?id=' . time()) }}"></script>
    <script src="{{ asset('assets/admin/assets/demo/default/base/scripts.bundle.js?id=' . time()) }}"></script>
    <script src="{{ asset('custom/jquery.fileDownload.js?id=' . time()) }}"></script>
    <script src="{{ asset('custom/datatables/datatables.min.js?id=' . time()) }}"></script>
    <script src="{{ asset('custom/sweetalert.min.js?id=' . time()) }}"></script>
    <script src="{{ asset('custom/jquery.form.min.js?id=' . time()) }}"></script>
    <script src="{{ asset('custom/jquery.blockUI.js?id=' . time()) }}"></script>
    <script src="{{ asset('custom/functions.js?id=' . time()) }}"></script>
    <script src="{{ asset('custom/extension.js?id=' . time()) }}"></script>
    <script src="{{ asset('custom/datatable.js?id=' . time()) }}"></script>
    <script src="{{ asset('custom/app.js?id=' . time()) }}"></script>
    <script src="{{ asset('custom/global.js?id=' . time()) }}"></script>
    <script src="{{ asset('custom/admin.js?id=' . time()) }}"></script>
@else
    <script src="{{ mix('/js/admin_core.js') }}" type="text/javascript"></script>
@endif