<div class="modal fade modal-primary" id="remote-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">

        </div>
    </div>
</div>
<div class="modal fade modal-primary" id="remote-modal-large" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-simple modal-lg">
        <div class="modal-content">

        </div>
    </div>
</div>
<div class="modal fade modal-primary" id="remote-modal-full" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-simple modal-lg">
        <div class="modal-content">

        </div>
    </div>
</div>
<div class="modal fade modal-primary" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ trans('common.close') }}">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure " data-dismiss="modal">{{ trans('common.cancel') }}</button>
                <a class="btn btn-primary text-white  btn-ok">{{ trans('common.confirm') }}</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-primary" id="img-previewer-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ trans('common.close') }}">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <img src="" class="img-responsive" id="img-previewer" style="margin: 0 auto; text-align: center; max-width: 100%;" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure " data-dismiss="modal">{{ trans('common.close') }}</button>
            </div>
        </div>
    </div>
</div>