@if (config('env.APP_DEBUG') == true)
    <link rel="stylesheet" href="{{ asset('assets/admin/vendors/base/vendors.bundle.css?id=' . time()) }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/assets/demo/default/base/style.bundle.css?id=' . time()) }}">
    <link rel="stylesheet" href="{{ asset('custom/datatables/datatables.min.css?id=' . time()) }}">
    <link rel="stylesheet" href="{{ asset('custom/flag-icon-css/flag-icon.min.css?id=' . time()) }}">
    <link rel="stylesheet" href="{{ asset('custom/global.css?id=' . time()) }}">
    <link rel="stylesheet" href="{{ asset('custom/admin.css?id=' . time()) }}">
@else
    <link rel="stylesheet" href="{{ mix('css/admin_core.css') }}">
@endif