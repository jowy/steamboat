<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <li class="m-menu__item{{ $_G['route'] == 'admin.home' ? ' m-menu__item--active' : '' }}" aria-haspopup="true">
                <a href="{{ route('admin.home') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-line-graph"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">{{ trans('common.dashboard') }}</span>
                        </span>
                    </span>
                </a>
            </li>
            {{--@if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_user', 'manage_user_advance', 'manage_credit', 'manage_topup', 'manage_withdrawal']))
				<li class="m-menu__section ">
                    <h4 class="m-menu__section-text">{{ trans('common.user') }}</h4>
                    <i class="m-menu__section-icon flaticon-more-v2"></i>
                </li>
                <li class="m-menu__item m-menu__item--submenu{{ \Str::startsWith($_G['route'], ['admin.user', 'admin.credit', 'admin.user_topup', 'admin.user_withdrawal']) ? ' m-menu__item--open m-menu__item--expanded': '' }}">
                    <a href="javascript:void(0)" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-user"></i>
                        <span class="m-menu__link-text">{{ trans('common.user') }}</span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">{{ trans('common.user') }}</span></span></li>
                            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_user', 'manage_user_advance']))
                                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.user']) ? ' m-menu__item--active' : '' }}">
                                    <a class="m-menu__link" href="{{ route('admin.user') }}">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">{{ trans('permission.manage_user') }}</span>
                                    </a>
                                </li>
                            @endif
                            @if ($_G['admin'] && $_G['admin']->hasPermission('manage_credit'))
                                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.credit']) ? ' m-menu__item--active' : '' }}">
                                    <a class="m-menu__link" href="{{ route('admin.credit') }}">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">{{ trans('permission.manage_credit') }}</span>
                                    </a>
                                </li>
                            @endif
                            @if ($_G['admin'] && $_G['admin']->hasPermission('manage_topup'))
                                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.user_topup']) ? ' m-menu__item--active' : '' }}">
                                    <a class="m-menu__link" href="{{ route('admin.user_topup') }}">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">{{ trans('permission.manage_topup') }}</span>
                                    </a>
                                </li>
                            @endif
                            @if ($_G['admin'] && $_G['admin']->hasPermission('manage_withdrawal'))
                                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.user_withdrawal']) ? ' m-menu__item--active' : '' }}">
                                    <a class="m-menu__link" href="{{ route('admin.user_withdrawal') }}">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">{{ trans('permission.manage_withdrawal') }}</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </li>
            @endif--}}
            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_admin', 'manage_permission_group']))
                <li class="m-menu__section ">
                    <h4 class="m-menu__section-text">{{ trans('common.admin') }}</h4>
                    <i class="m-menu__section-icon flaticon-more-v2"></i>
                </li>
                <li class="m-menu__item m-menu__item--submenu{{ \Str::startsWith($_G['route'], ['admin.admin', 'admin.permission']) ? ' m-menu__item--open m-menu__item--expanded': '' }}">
                    <a href="javascript:void(0)" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-user-ok"></i>
                        <span class="m-menu__link-text">{{ trans('common.admin') }}</span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">{{ trans('common.bank') }}</span></span></li>
                            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_admin']))
                                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.admin']) ? ' m-menu__item--active' : '' }}">
                                    <a class="m-menu__link" href="{{ route('admin.admin') }}">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">{{ trans('permission.manage_admin') }}</span>
                                    </a>
                                </li>
                            @endif
                            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_permission_group']))
                                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.permission']) ? ' m-menu__item--active' : '' }}">
                                    <a class="m-menu__link" href="{{ route('admin.permission') }}">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">{{ trans('permission.manage_permission_group') }}</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </li>
            @endif
            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_category', 'manage_product']))
                <li class="m-menu__section ">
                    <h4 class="m-menu__section-text">{{ trans('common.product') }}</h4>
                    <i class="m-menu__section-icon flaticon-more-v2"></i>
                </li>
            @endif
            {{--@if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_category']))
                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.category']) ? ' m-menu__item--active' : '' }}" aria-haspopup="true">
                    <a href="{{ route('admin.category') }}" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-cogwheel"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">{{ trans('permission.manage_category') }}</span>
                            </span>
                        </span>
                    </a>
                </li>
            @endif--}}
            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_product']))
                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.product']) ? ' m-menu__item--active' : '' }}" aria-haspopup="true">
                    <a href="{{ route('admin.product') }}" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-shopping-basket"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">{{ trans('permission.manage_product') }}</span>
                            </span>
                        </span>
                    </a>
                </li>
            @endif
            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_order']))
                <li class="m-menu__item{{ starts_with($_G['route'], ['admin.order']) ? ' m-menu__item--active' : '' }}" aria-haspopup="true">
                    <a href="{{ route('admin.order') }}" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-paper-plane"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">{{ trans('permission.manage_order') }}</span>
                            </span>
                        </span>
                    </a>
                </li>
            @endif

            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['sale_report']))
                <li class="m-menu__section ">
                    <h4 class="m-menu__section-text">{{ trans('common.report') }}</h4>
                    <i class="m-menu__section-icon flaticon-more-v2"></i>
                </li>
            @endif
            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_sale']))
                <li class="m-menu__item{{ starts_with($_G['route'], ['admin.report.sale']) ? ' m-menu__item--active' : '' }}" aria-haspopup="true">
                    <a href="{{ route('admin.report.sale') }}" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-browser"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">{{ trans('permission.sale_report') }}</span>
                            </span>
                        </span>
                    </a>
                </li>
            @endif


            <li class="m-menu__section ">
                <h4 class="m-menu__section-text">{{ trans('common.other') }}</h4>
                <i class="m-menu__section-icon flaticon-more-v2"></i>
            </li>
            {{--@if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_announcement']))
                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.announcement']) ? ' m-menu__item--active' : '' }}" aria-haspopup="true">
                    <a href="{{ route('admin.announcement') }}" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-info"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">{{ trans('permission.manage_announcement') }}</span>
                            </span>
                        </span>
                    </a>
                </li>
            @endif--}}
            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_bank', 'manage_company_bank']))
                <li class="m-menu__item m-menu__item--submenu{{ in_array($_G['route'], ['admin.bank', 'admin.companybank']) ? ' m-menu__item--open m-menu__item--expanded': '' }}">
                    {{--<a href="javascript:void(0)" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-layers"></i>
                        <span class="m-menu__link-text">{{ trans('common.bank') }}</span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>--}}
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">{{ trans('common.bank') }}</span></span></li>
                            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_bank']))
                                <li class="m-menu__item{{ in_array($_G['route'], ['admin.bank']) ? ' m-menu__item--active' : '' }}">
                                    <a class="m-menu__link" href="{{ route('admin.bank') }}">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">{{ trans('permission.manage_bank') }}</span>
                                    </a>
                                </li>
                            @endif
                            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_company_bank']))
                                <li class="m-menu__item{{ in_array($_G['route'], ['admin.companybank']) ? ' m-menu__item--active' : '' }}">
                                    <a class="m-menu__link" href="{{ route('admin.companybank') }}">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">{{ trans('permission.manage_company_bank') }}</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </li>
            @endif
            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_branch']))
                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.branch']) ? ' m-menu__item--active' : '' }}" aria-haspopup="true">
                    <a href="{{ route('admin.branch') }}" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-map"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">{{ trans('permission.manage_branch') }}</span>
                            </span>
                        </span>
                    </a>
                </li>
            @endif
            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_staff']))
                <li class="m-menu__item{{ starts_with($_G['route'], ['admin.staff']) ? ' m-menu__item--active' : '' }}" aria-haspopup="true">
                    <a href="{{ route('admin.staff') }}" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-users-1"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">{{ trans('permission.manage_staff') }}</span>
                            </span>
                        </span>
                    </a>
                </li>
            @endif
            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_supplier']))
                <li class="m-menu__item{{ starts_with($_G['route'], ['admin.supplier']) ? ' m-menu__item--active' : '' }}" aria-haspopup="true">
                    <a href="{{ route('admin.supplier') }}" class="m-menu__link ">
                        <i class="m-menu__link-icon fa fa-building"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">{{ trans('permission.manage_supplier') }}</span>
                            </span>
                        </span>
                    </a>
                </li>
            @endif
            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_setting', 'manage_price', 'manage_country', 'manage_page', 'manage_menu', 'manage_slider']))
                <li class="m-menu__item m-menu__item--submenu{{ \Str::startsWith($_G['route'], ['admin.setting', 'admin.price', 'admin.country', 'admin.page', 'admin.menu', 'admin.slider']) ? ' m-menu__item--open m-menu__item--expanded': '' }}">
                    <a href="javascript:void(0)" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-cogwheel"></i>
                        <span class="m-menu__link-text">{{ trans('common.others') }}</span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">{{ trans('common.bank') }}</span></span></li>
                            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_setting']))
                                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.setting']) ? ' m-menu__item--active' : '' }}">
                                    <a class="m-menu__link" href="{{ route('admin.setting') }}">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">{{ trans('permission.manage_setting') }}</span>
                                    </a>
                                </li>
                            @endif
                            {{--@if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_slider']))
                                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.slider']) ? ' m-menu__item--active' : '' }}">
                                    <a class="m-menu__link" href="{{ route('admin.slider') }}">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">{{ trans('permission.manage_slider') }}</span>
                                    </a>
                                </li>
                            @endif--}}
                            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_price']))
                                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.price']) ? ' m-menu__item--active' : '' }}">
                                    <a class="m-menu__link" href="{{ route('admin.price') }}">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">{{ trans('permission.manage_price') }}</span>
                                    </a>
                                </li>
                            @endif
                            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_country']))
                                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.country']) ? ' m-menu__item--active' : '' }}">
                                    <a class="m-menu__link" href="{{ route('admin.country') }}">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">{{ trans('permission.manage_country') }}</span>
                                    </a>
                                </li>
                            @endif
                            @if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_page']))
                                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.page']) ? ' m-menu__item--active' : '' }}">
                                    <a class="m-menu__link" href="{{ route('admin.page') }}">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">{{ trans('permission.manage_page') }}</span>
                                    </a>
                                </li>
                            @endif
                            {{--@if ($_G['admin'] && $_G['admin']->hasAnyPermission(['manage_menu']))
                                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.menu']) ? ' m-menu__item--active' : '' }}">
                                    <a class="m-menu__link" href="{{ route('admin.menu') }}">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">{{ trans('permission.manage_menu') }}</span>
                                    </a>
                                </li>
                            @endif--}}
                        </ul>
                    </div>
                </li>
            @endif
            {{--@if ($_G['admin'] && $_G['admin']->hasAnyPermission(['audit_log']))
                <li class="m-menu__item{{ \Str::startsWith($_G['route'], ['admin.audit_log']) ? ' m-menu__item--active' : '' }}" aria-haspopup="true">
                    <a href="{{ route('admin.audit_log') }}" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-analytics"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">{{ trans('permission.audit_log') }}</span>
                            </span>
                        </span>
                    </a>
                </li>
            @endif--}}
        </ul>
    </div>
</div>
