<div id="body-alert-container">
    @if (Session::has('flash_success'))
        @foreach (Session::get('flash_success') as $msg)
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="{{ trans('common.close') }}"></button>
                <i class="fa fa-check"></i> {{ $msg }}
            </div>
        @endforeach
    @endif
    @if (Session::has('flash_info'))
        @foreach (Session::get('flash_info') as $msg)
            <div class="alert alert-info" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="{{ trans('common.close') }}"></button>
                <i class="fa fa-exclamation-circle"></i> {{ $msg }}
            </div>
        @endforeach
    @endif
    @if (Session::has('flash_warning'))
        @foreach (Session::get('flash_warning') as $msg)
            <div class="alert alert-warning" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="{{ trans('common.close') }}"></button>
                <i class="fa fa-times"></i> {{ $msg }}
            </div>
        @endforeach
    @endif
    @if (Session::has('flash_error'))
        @foreach (Session::get('flash_error') as $msg)
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="{{ trans('common.close') }}"></button>
                <i class="fa fa-times"></i> {{ $msg }}
            </div>
        @endforeach
    @endif
</div>