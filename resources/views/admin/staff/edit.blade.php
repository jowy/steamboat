@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_staff') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@section('metatag') @endsection

@push('header') @endpush

@push('action') @endpush

@section('content')
    {!! Form::open(['route' => ['admin.staff.edit.post', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'staff-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="form-group m-form__group">
                    <label class="control-label">{{ trans('common.branch') }}</label>
                    {!! Form::select('branch_id', $branches, $model->branch_id, ['class' => 'form-control m-input', 'placeholder' => trans('common.please_select'), 'disabled']) !!}
                </div>
                {{--<div class="form-group m-form__group">
                    <label>{{ trans('common.branch_name') }} ( {{ trans('common.chinese') }} )</label>
                    {!! Form::text('name_cn', $model->name_cn, ['class' => 'form-control m-input']) !!}
                </div>--}}
                <div class="form-group m-form__group">
                    <label>{{ trans('common.username') }}</label>
                    {!! Form::text('username', $model->username, ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.password') }}</label>
                    {!! Form::password('password', ['class' => 'form-control m-input']) !!}
                    <span class="help-block">{{ trans('common.leave_blank_not_change') }}</span>
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.password_confirmation') }}</label>
                    {!! Form::password('password_confirmation', ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.name') }}</label>
                    {!! Form::text('name', $model->name, ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.contact_number') }}</label>
                    {!! Form::text('contact_number', $model->contact_number, ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.email') }}</label>
                    {!! Form::text('email', $model->email, ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <button type="button" id="btn-submit" class="btn btn-primary btn-block blue">{{ trans('common.edit') }}</button>
                </div>
            </div>
        </div>
    {!! Form::close() !!}

@endsection

@push('footer')
    <script type="text/javascript">
        +function() {
            $(document).ready(function() {
                $('#staff-form').makeAjaxForm({
                    submitBtn: '#btn-submit',
                    redirectTo: '{{ route('admin.staff') }}',
                });
            });
        }(jQuery);
    </script>
@endpush

@push('modal') @endpush
