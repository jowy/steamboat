@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_staff') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@section('metatag') @endsection

@push('header') @endpush

@push('action')
@endpush

@section('content')
    {!! Form::open(['route' => ['admin.staff.create.post'], 'method' => 'post', 'role' => 'form', 'id' => 'staff-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="form-group m-form__group">
                    <label class="control-label">{{ trans('common.branch') }}</label>
                    {!! Form::select('branch_id', $branches, null, ['class' => 'form-control m-input', 'placeholder' => trans('common.please_select')]) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.username') }}</label>
                    {!! Form::text('username', '', ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.password') }}</label>
                    {!! Form::password('password', ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.password_confirmation') }}</label>
                    {!! Form::password('password_confirmation', ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.name') }}</label>
                    {!! Form::text('name', '', ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.contact_number') }}</label>
                    {!! Form::text('contact_number', '', ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.email') }}</label>
                    {!! Form::text('email', '', ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <button type="button" id="btn-submit" class="btn btn-primary btn-block blue">{{ trans('common.create') }}</button>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@push('footer')
    <script type="text/javascript">
        +function() {
            $(document).ready(function() {
                $('#staff-form').makeAjaxForm({
                    submitBtn: '#btn-submit',
                    redirectTo: '{{ route('admin.staff') }}',
                });
            });
        }(jQuery);
    </script>
@endpush

@push('modal') @endpush
