@extends('admin.layout')

@section('title')
    {{ trans('common.dashboard') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header') @endpush

@section('content')
    @if (config('env.APP_DEBUG') == true && $_G['admin']->isDev())
        <a href="{{ route('admin.test.reset', ['test' => 1]) }}" class="btn btn-primary btn-block waves waves-classic">Reset Data</a>
    @endif
	@if ($_G['admin']->hasPermission('dashboard_statistics'))
		
	@endif
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.dashboard') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            {{ trans('common.welcome') }}, {{ $_G['admin']->name }}
        </div>
    </div>
@endsection

@push('footer')
    <script type="text/javascript">
        (function ($) {
            $(document).ready(function () {

            });
        }(jQuery));
    </script>
@endpush

@push('modal') @endpush