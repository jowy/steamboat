@extends('ajaxmodal')

@section('title')
    {{ trans('common.details') }}
@endsection

@section('script')

@endsection

@section('footer') @endsection

@section('content')
    <div class="form-body">
        @if ($model->admin)
            <div class="form-group m-form__group">
                <label>{{ trans('common.admin') }}</label>
                {!! Form::text('', $model->admin ? $model->admin->username : '-', ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
            </div>
        @endif
        @if ($model->admin_response_date != null)
            <div class="form-group m-form__group">
                <label>{{ trans('common.handle_date') }}</label>
                {!! Form::text('', $model->admin_response_date, ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
            </div>
        @endif
        <div class="form-group m-form__group">
            <label>{{ trans('common.user') }}</label>
            {!! Form::text('', $model->user ? $model->user->username : '-', ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.bank') }}</label>
            {!! Form::text('', $model->getBankName(), ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.bank_account_name') }}</label>
            {!! Form::text('', $model->bank_account_name, ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.bank_account_number') }}</label>
            {!! Form::text('', $model->bank_account_number, ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.credit_' . $model->credit_type) }}</label>
            {!! Form::text('', fundFormat($model->amount), ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.admin_fees') }}</label>
            {!! Form::text('', fundFormat($model->admin_fees, 2), ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.fiat_currency') }}</label>
            {!! Form::text('', $model->country ? $model->country->explainCurrency(fundFormat($model->local_currency_amount)) : fundFormat($model->local_currency_amount), ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        @if ($model->user_remark != null)
            <div class="form-group m-form__group">
                <label>{{ trans('common.remark') }}</label>
                {!! Form::textarea('', $model->user_remark, ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled', 'rows' => 4]) !!}
            </div>
        @endif
        @if ($model->receipts)
            <div class="form-group m-form__group">
                <label>{{ trans('common.receipt') }}</label>
                <div>
                    @foreach ($model->receipts as $receipt)
                        @if (isImageExtension($receipt->receipt, true))
                            <img src="{{ asset($receipt->receipt) }}" class="img-fluid text-center" />
                        @else
                            <a href="{{ asset($receipt->receipt) }}" target="_blank" class="btn btn-primary waves-button waves-classic"><i class="la la-file-o"></i></a>
                        @endif
                    @endforeach
                </div>
            </div>
        @endif
        @if ($model->admin_remark != null)
            <div class="form-group m-form__group">
                <label>{{ trans('common.admin_remark') }}</label>
                {!! Form::textarea('', $model->admin_remark, ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
            </div>
        @endif
        <div class="form-group m-form__group">
            <label>{{ trans('common.status') }}</label>
            {!! Form::text('', $model->explainStatus(), ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
    </div>
@endsection

@section('modal')

@endsection