@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_topup') }}
@endsection

@section('description')
    {{ trans('permission.manage_topup') }}
@endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header') @endpush

@section('action') @endsection

@section('content')
    <div class="m-portlet m-portlet--mobile" id="filter-container">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.filter') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m-form m-form--fit m-form--label-align-right">
                <div class="form-group m-form__group">
                    <label>{{ trans('common.date') }}</label>
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            {!! Form::text('filter_created_after_date', '', ['class' => 'form-control m-input form-filter input-sm date-picker', 'placeholder' => trans('common.from')]) !!}
                        </div>
                        <div class="col-12 col-lg-6">
                            {!! Form::text('filter_created_before_date', '', ['class' => 'form-control m-input form-filter input-sm date-picker', 'placeholder' => trans('common.to')]) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.admin') }}</label>
                    {!! Form::text('filter_admin_username', '', ['class' => 'form-control m-input form-filter input-sm', 'placeholder' => trans('common.all')]) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.username') }}</label>
                    {!! Form::text('filter_username', '', ['class' => 'form-control m-input form-filter input-sm', 'placeholder' => trans('common.all')]) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.country') }}</label>
                    {!! Form::select('filter_country_id', \App\Models\Country::GetCountries()->GetSelects(), null, ['class' => 'form-control m-input form-filter input-sm', 'placeholder' => trans('common.all')]) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.status') }}</label>
                    {!! Form::select('filter_status', \App\Models\UserTopup::getStatusLists(), null, ['class' => 'form-control m-input form-filter input-sm', 'placeholder' => trans('common.all')]) !!}
                </div>
                <div class="form-group m-form__group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <button class="btn btn-danger btn-block  filter-submit">
                                <i class="fa fa-search"></i> {{ trans('common.filter') }}
                            </button>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <button class="btn btn-info btn-block  filter-cancel">
                                <i class="fa fa-times"></i> {{ trans('common.reset') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.lists') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-hover table-bordered table-striped" id="data-dt">
                <thead>
                    <tr role="row" class="heading">
                        <th>#</th>
                        <th>{{ trans('common.actions') }}</th>
                        <th>{{ trans('common.user') }}</th>
                        <th>{{ trans('common.admin') }}</th>
                        <th>{{ trans('common.bank') }}</th>
                        <th>{{ trans('common.bank_account_name') }}</th>
                        <th>{{ trans('common.bank_account_number') }}</th>
                        <th>{{ trans('common.credit_type') }}</th>
                        <th>{{ trans('common.amount') }}</th>
                        <th>{{ trans('common.fiat_currency') }}</th>
                        <th>{{ trans('common.status') }}</th>
                        <th>{{ trans('common.created_at') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('footer')
    <script>
        +function ($) {
            $(document).ready(function () {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#data-dt'),
                    showIndex: true,
                    filterContainer: '#filter-container',
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('admin.user_topup.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id', sortable: true, orderable: true},
                            {data: 'actions', name: 'actions', sortable: false, orderable: false},
                            {data: 'user_id', name: 'user_id'},
                            {data: 'admin_id', name: 'admin_id'},
                            {data: 'bank_name_en', name: 'bank_name_en'},
                            {data: 'bank_account_name', name: 'bank_account_name'},
                            {data: 'bank_account_number', name: 'bank_account_number'},
                            {data: 'credit_type', name: 'credit_type'},
                            {data: 'amount', name: 'amount'},
                            {data: 'local_currency_amount', name: 'local_currency_amount'},
                            {data: 'status', name: 'status'},
                            {data: 'created_at', name: 'created_at'},
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endpush

@push('modal') @endpush


