@extends('ajaxmodal')

@section('title')
    {{ trans('common.my_profile') }}
@endsection

@section('footer')
    <button type="button" id="btn-submit" class="btn btn-primary">{{ trans('common.update') }}</button>
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#profile-form').makeAjaxForm({
                    closeModal: true,
                    inModal: true,
                    submitBtn: '#btn-submit',
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    {!! Form::open(['method' => 'post', 'id' => 'profile-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
    <div class="form-body">
        <div class="form-group m-form__group">
            <label class="control-label">{{ trans('common.language') }}</label>
            {!! Form::select('lang', $_G['langs'], $_G['admin']->lang, ['class' => 'form-control m-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label class="control-label">{{ trans('common.current_password') }}</label>
            {!! Form::password('current_password', ['class' => 'form-control m-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label class="control-label">{{ trans('common.new_password') }}</label>
            {!! Form::password('new_password', ['class' => 'form-control m-input']) !!}
            <span class="help-block">{{ trans('common.leave_blank_not_change') }}</span>
        </div>
        <div class="form-group m-form__group">
            <label class="control-label">{{ trans('common.new_password_confirmation') }}</label>
            {!! Form::password('new_password_confirmation', ['class' => 'form-control m-input']) !!}
            <span class="help-block">{{ trans('common.leave_blank_not_change') }}</span>
        </div>
    </div>
    {!! Form::close() !!}
@endsection