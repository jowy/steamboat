@extends('ajaxmodal')

@section('title')
    {{ trans('permission.manage_admin') }}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#admin-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit" class="btn btn-primary blue">{{ trans('common.submit') }}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['admin.admin.edit.post', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'admin-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
    <div class="form-body">
        <div class="form-group m-form__group">
            <label>{{ trans('common.username') }}</label>
            {!! Form::text('username', $model->username, ['class' => 'form-control m-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.email') }}</label>
            {!! Form::text('email', $model->email, ['class' => 'form-control m-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.name') }}</label>
            {!! Form::text('name', $model->name, ['class' => 'form-control m-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.group_name') }}</label>
            {!! Form::select('permission_group_id', $permissions, $model->permissionGroup ? $model->permissionGroup->id : null, ['class' => 'form-control m-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.password') }}</label>
            {!! Form::password('password', ['class' => 'form-control m-input']) !!}
            <span class="help-block">{{ trans('common.leave_blank_not_change') }}</span>
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.password_confirmation') }}</label>
            {!! Form::password('password_confirmation', ['class' => 'form-control m-input']) !!}
            <span class="help-block">{{ trans('common.leave_blank_not_change') }}</span>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection