@extends('ajaxmodal')

@section('title')
    @if (isset($model))
        {{ trans('common.edit') }}
    @else
        {{ trans('common.add') }}
    @endif
@endsection

@section('script')
    @if (!isset($model) || (isset($model) && $model != null))
        <script>
            +function() {
                $(document).ready(function() {
                    @if (isset($father) && $father)
                    $('#sub-form').makeAjaxForm({
                        inModal: true,
                        closeModal: true,
                        submitBtn: '#btn-submit-sub',
                        afterSuccessFunction: function (response, $el, $this) {
                            if (typeof response.model !== 'undefined') {
                                var father_id = {{ $father->id }};
                                var id = response.model.id;

                                @if (isset($model) && $model != null)
                                    $(".dd-item[data-id='" + id + "']").children('.dd-handle').first().html(response.text);
                                @else
                                    var menu_html =
                                        '<li class="dd-item" data-id="__id__">' +
                                        '<a data-href="{{ route('admin.country.location.sub.delete.post', ['id' => '__fatherid__', 'id2' => '__id__']) }}" class="btn btn-danger btn-sm" data-redirect="no" data-toggle="modal" data-target="#confirm-modal" data-header="{{ trans('common.confirm') }}?" data-body="{{ trans('common.are_you_sure') }}?" data-successcall="reloadMenu"><i class="fa fa-trash"></i></a>' +
                                        '<a href="{{ route('admin.country.location.sub.edit', ['id' => '__fatherid__', 'id2' => '__id__']) }}" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#remote-modal"><i class="fa fa-eye"></i></a>' +
                                        '<div class="dd-handle">__text__</div>' +
                                        '</li>';

                                    menu_html = menu_html.replace(/__fatherid__/g, father_id);
                                    menu_html = menu_html.replace(/__id__/g, id);
                                    menu_html = menu_html.replace(/__text__/g, response.text);

                                    $('#nestable > ol').append(menu_html);
                                @endif
                            }

                            $('#remote-modal').modal('toggle');
                        }
                    });

                    $('#type-selector').trigger('update');
                    @endif
                });
            }(jQuery);
        </script>
    @endif
@endsection

@section('footer')
    @if (!isset($model) || (isset($model) && $model != null))
        @if (isset($father) && $father)
            <button type="button" id="btn-submit-sub" class="btn btn-primary">
                @if (isset($model))
                    {{ trans('common.edit') }}
                @else
                    {{ trans('common.create') }}
                @endif
            </button>
        @endif
    @endif
@endsection

@section('content')
    @if (!isset($model) || (isset($model) && $model != null))
        @if (isset($father) && $father)
            @if (isset($model) && $model != null)
                {!! Form::open(['route' => ['admin.country.location.sub.edit.post', 'id' => $father->id, 'id2' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'sub-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
            @else
                {!! Form::open(['route' => ['admin.country.location.sub.create.post', 'id' => $father->id], 'method' => 'post', 'role' => 'form', 'id' => 'sub-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
            @endif
            {!! Form::hidden('country_id', $father->id) !!}
                <div class="form-group">
                    <label>{{ trans('common.name_in_english') }}</label>
                    {!! Form::text('location_name_en', isset($model) ? $model->location_name_en : '', ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group">
                    <label>{{ trans('common.name_in_chinese') }}</label>
                    {!! Form::text('location_name_cn', isset($model) ? $model->location_name_cn : '', ['class' => 'form-control m-input']) !!}
                </div>
            {!! Form::close() !!}
        @else
            <h4>{{ trans('common.record_not_found') }}</h4>
        @endif
    @else
        <h4>{{ trans('common.record_not_found') }}</h4>
    @endif
@endsection

@section('modal')

@endsection