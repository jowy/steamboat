@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_product') }}
@endsection

@section('description')
    {{ trans('permission.manage_product') }}
@endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header') @endpush

@section('action')

@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile" id="filter-container">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.filter') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m-form m-form--fit m-form--label-align-right">
                <div class="form-group m-form__group">
                    <label>{{ trans('common.date') }}</label>
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            {!! Form::text('filter_created_after_date', '', ['class' => 'form-control m-input form-filter input-sm date-picker', 'placeholder' => trans('common.from')]) !!}
                        </div>
                        <div class="col-12 col-lg-6">
                            {!! Form::text('filter_created_before_date', '', ['class' => 'form-control m-input form-filter input-sm date-picker', 'placeholder' => trans('common.to')]) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.product_name') }}</label>
                    {!! Form::text('filter_product_name', '', ['class' => 'form-control form-filter', 'placeholder' => trans('common.name')]) !!}
                </div>
                <div class="form-group m-form__group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <button class="btn btn-danger btn-block  filter-submit">
                                <i class="fa fa-search"></i> {{ trans('common.filter') }}
                            </button>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <button class="btn btn-info btn-block  filter-cancel">
                                <i class="fa fa-times"></i> {{ trans('common.reset') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.lists') }}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li><a href="{{ route('admin.product.create') }}" class="btn btn-primary text-white"><i class="fa fa-plus"></i> {{ trans('common.add_product') }}</a></li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-hover table-bordered table-striped" id="data-dt">
                <thead>
                    <tr role="row" class="heading">
                        <th width="60">#</th>
                        <th>{{ trans('common.product_name') }}</th>
                        <th width="200">{{ trans('common.quantity_label') }}</th>
                        <th width="200">{{ trans('common.ala_carte') }}</th>
                        <th width="200">{{ trans('common.ala_carte_price') }}</th>
                        <th width="145">{{ trans('common.created_at') }}</th>
                        <th width="65">{{ trans('common.action') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#data-dt'),
                    filterContainer: $('#filter-container'),
                    showIndex: true,
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('admin.product.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'product_name_en', name: 'product_name_en'},
                            {data: 'quantity_label', name: 'quantity_label'},
                            {data: 'is_ala_carte', name: 'is_ala_carte'},
                            {data: 'ala_carte_price', name: 'ala_carte_price'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false},
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endpush

@push('modal')

@endpush
