@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_product') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@push('metatag') @endpush

@push('header')
    <style>
        #images-container > div {
            margin-bottom: 20px;
        }
    </style>
@endpush

@section('action') @endsection

@section('content')
    {!! Form::model($model, ['role' => 'form', 'method' => 'post', 'id' => 'form', 'files' => true]) !!}
        <div class="m-portlet m-portlet--mobile m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#tab-en" role="tab">
                                <i class="flag-icon {{ \App\Constants::getFlagClassByLanguage('en') }}" aria-hidden="true"></i> {{ trans('common.english') }}
                            </a>
                        </li>
                        {{--<li class="nav-item m-tabs__item">--}}
                            {{--<a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-cn" role="tab">--}}
                                {{--<i class="flag-icon {{ \App\Constants::getFlagClassByLanguage('cn') }}" aria-hidden="true"></i> {{ trans('common.chinese') }}--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="form-body">
                   {{-- <div class="form-group">
                        <label class="control-label">{{ trans('common.sorting') }} ({{ trans('common.ascending_order') }})</label>
                        {!! Form::text('sorting', null, ['class' => 'form-control number-input']) !!}
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('common.product_code') }}</label>
                        {!! Form::text('product_code', null, ['class' => 'form-control']) !!}
                    </div>--}}
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-en" role="tabpanel" aria-expanded="true">
                            <div class="form-group">
                                <label class="control-label">{{ trans('common.product_name') }}</label>
                                {!! Form::text('product_name_en', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label>{{ trans('common.ala_carte') }}</label>
                                {!! Form::select('is_ala_carte', [true => trans('common.yes'), false => trans('common.no')], null, ['class' => 'form-control m-input', 'id' => 'is_ala_carte']) !!}
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{ trans('common.ala_carte_price') }}</label>
                                {!! Form::text('ala_carte_price', null, ['class' => 'form-control fund-input', 'id' => 'ala_carte_price']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label>{{ trans('common.quantity_label') }}</label>
                                {!! Form::select('quantity_label', config('product.label'), null, ['class' => 'form-control m-input', 'id' => 'is_ala_carte']) !!}
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{ trans('common.product_short_description') }}</label>
                                {!! Form::text('product_short_description_en', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{ trans('common.product_description') }}</label>
                                {!! Form::textarea('product_description_en', null, ['class' => 'form-control editor-mce']) !!}
                            </div>
                            {{--<div class="form-group row">
                                <label class="control-label col-12">
                                    {{ trans('common.image') }} ({{ trans('common.recommend_image_size', ['width' => \App\Models\Product::IMAGE_WIDTH, 'height' => \App\Models\Product::IMAGE_HEIGHT]) }})
                                </label>
                                <div class="col-12">
                                    @if ($model->exists && $model->product_image_en != null)
                                        <div>
                                            <img src="{{ asset($model->product_image_en) }}" class="img-fluid" />
                                        </div>
                                    @endif
                                    {!! Form::file('product_image_en', ['class' => 'form-control m-input']) !!}
                                </div>
                            </div>--}}
                            <hr>
                            <div class="form-group">
                                <label class="control-label">{{ trans('common.subtract_product') }}</label>
                                {!! Form::select('subtract_product_id', $ala_carte_products, null, ['class' => 'form-control m-input', 'id' => 'subtract-product', 'placeholder' => 'Please select']) !!}
                                <span>If no subtract product, just leave it</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{ trans('common.subtract_quantity') }}</label>
                                {!! Form::text('subtract_quantity', null, ['class' => 'form-control']) !!}
                                <span>If no subtract product, just leave it</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ trans('common.category') }}
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">

                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="form-group">
                    <div class="row">
                        @foreach ($categories as $cid => $cname)
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                <label class="m-checkbox m-checkbox--bold m-checkbox--state-success">
                                    <input type="checkbox" name="categories[]" value="{{ $cid }}"{{ $model->exists && isset($selected) && array_key_exists($cid, $selected) ? ' checked' : '' }}> {{ $cname }}
                                    <span></span>
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>--}}
        {{--<div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ trans('common.gallery') }}
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">

                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="form-group row">
                    <label class="control-label col-12">
                        {{ trans('common.image') }} ({{ trans('common.recommend_image_size', ['width' => \App\Models\ProductImages::IMAGE_WIDTH, 'height' => \App\Models\ProductImages::IMAGE_HEIGHT]) }})
                    </label>
                    <div class="col-12">
                        @if ($model->exists && $model->images && $model->images->count())
                            <div class="row mb-30">
                                @foreach ($model->images as $img)
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-3" id="img-{{ $img->id }}">
                                        <div>
                                            <img src="{{ asset($img->path) }}" class="img-responsive img-fluid" />
                                        </div>
                                        <button type="button" class="btn btn-danger btn-block btn-delete-img" data-imgid="{{ $img->id }}">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        <div>
                            {!! Form::file('images[]', ['class' => 'form-control m-input', 'data-multiple' => 1]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>--}}

        <div id="delete-container" style="display: none;">

        </div>

        <div class="row">
            <div class="col-12">
                <button type="button" class="btn btn-primary blue text-white btn-block" id="submit-btn"><i class="fa fa-check"></i> {{ trans('common.submit') }}</button>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@push('footer')
    @include('tinymce', ['selector' => '.editor-mce'])
    <script>
        +function ($) {
            $(document).ready(function () {
                $('#form').makeAjaxForm({
                    submitBtn: '#submit-btn',
                    redirectTo: '{{ route('admin.product') }}',
                });

                $('#subtract-product').select2();

                if ($('#is_ala_carte').val() == true) {
                    $('#ala_carte_price').prop('disabled', false);
                } else {
                    $('#ala_carte_price').prop('disabled', true);
                }

                $('#is_ala_carte').change(function() {
                    if ($(this).val() == true) {
                        $('#ala_carte_price').prop('disabled', false);
                    } else {
                        $('#ala_carte_price').prop('disabled', true);
                    }
                });
                /*$(document).on('click', '.btn-delete-img', function () {
                    let i = parseInt($(this).data('imgid'));
                    if (i > 0) {
                        let str = '<input name="delete_image[]" value="' + i + '" type="hidden" />';
                        $('#delete-container').append(str);
                        $('#img-' + i).remove();
                    }
                });*/
            });
        }(jQuery);
    </script>
@endpush

@push('modal') @endpush


