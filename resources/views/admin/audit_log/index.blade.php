@extends('admin.layout')

@section('title')
    {{ trans('permission.audit_log') }}
@endsection

@section('description')
    {{ trans('permission.audit_log') }}
@endsection

@push('header') @endpush

@section('content')
    <div class="m-portlet m-portlet--mobile" id="filter-container">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.filter') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m-form m-form--fit m-form--label-align-right">
                <div class="form-group m-form__group">
                    <label>{{ trans('common.date') }}</label>
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            {!! Form::text('filter_created_after_date', '', ['class' => 'form-control m-input form-filter input-sm date-picker', 'placeholder' => trans('common.from')]) !!}
                        </div>
                        <div class="col-12 col-lg-6">
                            {!! Form::text('filter_created_before_date', '', ['class' => 'form-control m-input form-filter input-sm date-picker', 'placeholder' => trans('common.to')]) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group m-form__group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <button class="btn btn-danger btn-block  filter-submit">
                                <i class="fa fa-search"></i> {{ trans('common.filter') }}
                            </button>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <button class="btn btn-info btn-block  filter-cancel">
                                <i class="fa fa-times"></i> {{ trans('common.reset') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.lists') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-hover table-bordered table-striped" id="data-dt">
                <thead>
                    <tr role="row" class="heading">
                        <th width="80">#</th>
                        <th width="120">{{ trans('common.actions') }}</th>
                        <th width="180">{{ trans('common.admin') }}</th>
                        <th>{{ trans('common.description') }}</th>
                        {{--<th width="200">{{ trans('common.function') }}</th>--}}
                        {{--<th width="200">{{ trans('common.operation') }}</th>--}}
                        <th width="180">{{ trans('common.created_at') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('footer')
    <script>
        +function ($) {
            $(document).ready(function () {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#data-dt'),
                    showIndex: true,
                    filterContainer: $('#filter-container'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('admin.audit_log.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id', sortable: true, searchable: true},
                            {data: 'actions', name: 'actions', sortable: false, searchable: false},
                            {data: 'admin_id', name: 'admin_id'},
                            {data: 'description', name: 'description'},
                            // {data: 'function_tag', name: 'function_tag'},
                            // {data: 'operation', name: 'operation'},
                            {data: 'created_at', name: 'created_at'},
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endpush

@push('modal') @endpush