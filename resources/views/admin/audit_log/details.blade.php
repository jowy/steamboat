@extends('ajaxmodal')

@section('title')
    @if ($model)
        {{ $model->description }}
    @else
        {{ trans('common.record_not_found') }}
    @endif
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {

            });
        }(jQuery);
    </script>
@endsection

@section('footer')

@endsection

@section('content')
    @if ($model)
        @if ($model->operation == \App\Models\AuditRecords::OPERATION_CREATE)
            @if ($model->created_data)
                <div class="table-responsive">
                    <table class="table table-condensed table-bordered table-stripped table-hover">
                        <thead>
                            <tr>
                                <th colspan="2" class="text-center bold uppercase font-red">{{ trans('common.created_data') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($model->convertCreatedData() as $key => $var)
                                <tr>
                                    <td class="text-right">{{ $key }}</td>
                                    <td class="text-center">{{ is_string($var) ? $var : json_encode($var) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <h3 class="bold">{{ trans('common.record_not_found') }}</h3>
            @endif
        @elseif ($model->operation == \App\Models\AuditRecords::OPERATION_UPDATE)
            @if ($model->before_data && $model->after_data)
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="table-responsive">
                            <table class="table table-condensed table-bordered table-stripped table-hover">
                                <thead>
                                    <tr>
                                        <th colspan="2" class="text-center bold uppercase font-red">{{ trans('common.before_data') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($model->convertBeforeData() as $key => $var)
                                        <tr>
                                            <td class="text-right bold uppercase">{{ $key }}</td>
                                            <td class="text-center">{{ is_string($var) ? $var : json_encode($var) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="table-responsive">
                            <table class="table table-condensed table-bordered table-stripped table-hover">
                                <thead>
                                    <tr>
                                        <th colspan="2" class="text-center bold uppercase font-red">{{ trans('common.after_data') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($model->convertAfterData() as $key => $var)
                                    <tr>
                                        <td class="text-right bold uppercase">{{ $key }}</td>
                                        <td class="text-center">{{ is_string($var) ? $var : json_encode($var) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <h3 class="bold">{{ trans('common.record_not_found') }}</h3>
            @endif
        @elseif ($model->operation == \App\Models\AuditRecords::OPERATION_DELETE)
            @if ($model->deleted_data)
                <div class="table-scrollable table-responsive">
                    <table class="table table-condensed table-bordered table-stripped table-hover">
                        <thead>
                            <tr>
                                <th colspan="2" class="text-center bold uppercase font-red">{{ trans('common.deleted_data') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($model->convertDeletedData() as $key => $var)
                            <tr>
                                <td class="text-right">{{ $key }}</td>
                                <td class="text-center">{{ is_string($var) ? $var : json_encode($var) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <h3 class="bold">{{ trans('common.record_not_found') }}</h3>
            @endif
        @else
            <h3 class="bold">{{ trans('common.unknown_error') }}</h3>
        @endif
    @else
        <h3 class="bold">{{ trans('common.record_not_found') }}</h3>
    @endif
@endsection

@section('modal')

@endsection