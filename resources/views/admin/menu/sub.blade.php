@extends('ajaxmodal')

@section('title')
    @if (isset($model))
        {{ trans('common.edit_menu') }}
    @else
        {{ trans('common.add_menu') }}
    @endif
@endsection

@section('script')
    @if (!isset($model) || (isset($model) && $model != null))
    <script>
        +function() {
            $(document).ready(function() {
                @if (isset($father) && $father)
                    $('#sub-menu-form').makeAjaxForm({
                        inModal: true,
                        closeModal: true,
                        submitBtn: '#btn-submit-sub-menu',
                        afterSuccessFunction: function (response, $el, $this) {
                            if (typeof response.model !== 'undefined') {
                                var father_id = {{ $father->id }};
                                var id = response.model.id;

                                @if (isset($model) && $model != null)
                                    $(".dd-item[data-id='" + id + "']").children('.dd-handle').first().html(response.text);
                                @else
                                    var menu_html =
                                        '<li class="dd-item" data-id="__id__">' +
                                            '<a data-href="{{ route('admin.menu.sub.delete.post', ['id' => '__fatherid__', 'id2' => '__id__']) }}" class="btn btn-danger btn-sm " data-redirect="no" data-toggle="modal" data-target="#confirm-modal" data-header="{{ trans('common.confirm') }}?" data-body="{{ trans('common.are_you_sure') }}?" data-successcall="reloadMenu"><i class="fa fa-trash"></i></a>' +
                                            '<a href="{{ route('admin.menu.sub.edit', ['id' => '__fatherid__', 'id2' => '__id__']) }}" class="btn btn-primary btn-sm " data-toggle="modal" data-target="#remote-modal"><i class="fa fa-eye"></i></a>' +
                                            '<div class="dd-handle">__text__</div>' +
                                        '</li>';

                                    menu_html = menu_html.replace(/__fatherid__/g, father_id);
                                    menu_html = menu_html.replace(/__id__/g, id);
                                    menu_html = menu_html.replace(/__text__/g, response.text);

                                    $('#nestable > ol').append(menu_html);
                                @endif
                            }
                        }
                    });

                    $('#type-selector').trigger('update');
                @endif
            });
        }(jQuery);
    </script>
    @endif
@endsection

@section('footer')
    @if (!isset($model) || (isset($model) && $model != null))
        @if (isset($father) && $father)
            <button type="button" id="btn-submit-sub-menu" class="btn btn-primary">
                @if (isset($model))
                    {{ trans('common.edit_menu') }}
                @else
                    {{ trans('common.add_menu') }}
                @endif
            </button>
        @endif
    @endif
@endsection

@section('content')
    @if (!isset($model) || (isset($model) && $model != null))
        @if (isset($father) && $father)
            @if (isset($model) && $model != null)
            {!! Form::open(['route' => ['admin.menu.sub.edit.post', 'id' => $father->id, 'id2' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'sub-menu-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
            @else
            {!! Form::open(['route' => ['admin.menu.sub.create.post', 'id' => $father->id], 'method' => 'post', 'role' => 'form', 'id' => 'sub-menu-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
            @endif
                {!! Form::hidden('menu_id', $father->id) !!}
                <div class="form-group m-form__group">
                    <label>{{ trans('common.link_text_in_english') }}</label>
                    {!! Form::text('text_en', isset($model) ? $model->text_en : '', ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.link_text_in_chinese') }}</label>
                    {!! Form::text('text_cn', isset($model) ? $model->text_cn : '', ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.open_in_new_tab') }} ?</label>
                    {!! Form::select('open_in_new_tab', \App\Models\MenuItems::getOpenInNewTabLists(), isset($model) ? $model->open_in_new_tab : '', ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.type') }}</label>
                    {!! Form::select('type', \App\Models\MenuItems::getTypeLists(), isset($model) ? $model->type : '', ['class' => 'form-control m-input', 'id' => 'type-selector']) !!}
                </div>
                <div class="form-group type-related" id="type-related-{{ \App\Models\MenuItems::TYPE_PAGE }}">
                    <label>{{ trans('common.page') }}</label>
                    {!! Form::select('page_id', \App\Models\Page::getPagesForSelect(), isset($model) ? $model->page_id : '', ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group type-related" id="type-related-{{ \App\Models\MenuItems::TYPE_LINK }}">
                    <label>{{ trans('common.url') }}</label>
                    {!! Form::text('url', isset($model) ? $model->url : '', ['class' => 'form-control m-input']) !!}
                </div>
            {!! Form::close() !!}
        @else
            <h4>{{ trans('common.record_not_found') }}</h4>
        @endif
    @else
        <h4>{{ trans('common.record_not_found') }}</h4>
    @endif
@endsection

@section('modal')

@endsection