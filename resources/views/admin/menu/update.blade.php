@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_menu') }}
@endsection

@section('description')
    {{ trans('permission.manage_menu') }}
@endsection

@section('author') @endsection

@section('metatag') @endsection

@push('header')
    <link href="/custom/nestable/nestable.css" rel="stylesheet">
    <style>
        .portlet-body {
            display: table;
            width: 100%;
        }
        .type-related {
             display: none;
        }
        #menu-form .btn {
            background-color: #009688 !important;
        }
        #menu-form .btn.btn-danger {
            background-color: #f44336 !important;
        }
        #menu-form .btn.btn-danger > i {
            color: #fff !important;
        }
    </style>
@endpush

@push('action') @endpush

@section('content')
    {!! Form::open(['route' => ['admin.menu.update.post', 'id' => $model->id], 'id' => 'menu-form', 'role' => 'form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ $model->getName() }}
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('admin.menu.sub.create', ['id' => $model->id]) }}" class="btn btn-primary" data-target="#remote-modal" data-toggle="modal"><i class="md-plus"></i> {{ trans('common.add_menu') }}</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="dd" id="nestable">
                    <ol class="dd-list">
                        @foreach ($model->childrens as $k1 => $l1)
                            <li class="dd-item" data-id="{{ $l1->id }}">
                                <a data-href="{{ route('admin.menu.sub.delete.post', ['id' => $model->id, 'id2' => $l1->id]) }}" class="btn btn-danger btn-sm " data-redirect="no" data-toggle="modal" data-target="#confirm-modal" data-header="{{ trans('common.confirm') }}?" data-body="{{ trans('common.are_you_sure') }}?" data-successcall="reloadMenu"><i class="fa fa-trash"></i></a>
                                <a href="{{ route('admin.menu.sub.edit', ['id' => $model->id, 'id2' => $l1->id]) }}" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#remote-modal"><i class="fa fa-eye"></i></a>
                                <div class="dd-handle">{{ $l1->getTextWithLink() }}</div>
                                @if ($model->tier >= 2)
                                    <ol class="dd-list">
                                        @foreach ($l1->sons as $k2 => $l2)
                                            <li class="dd-item" data-id="{{ $l2->id }}">
                                                <a data-href="{{ route('admin.menu.sub.delete.post', ['id' => $model->id, 'id2' => $l2->id]) }}" class="btn btn-danger btn-sm " data-redirect="no" data-toggle="modal" data-target="#confirm-modal" data-header="{{ trans('common.confirm') }}?" data-body="{{ trans('common.are_you_sure') }}?" data-successcall="reloadMenu"><i class="fa fa-trash"></i></a>
                                                <a href="{{ route('admin.menu.sub.edit', ['id' => $model->id, 'id2' => $l2->id]) }}" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#remote-modal"><i class="fa fa-eye"></i></a>
                                                <div class="dd-handle">{{ $l2->getTextWithLink() }}</div>
                                                @if ($model->tier >= 3)
                                                    <ol class="dd-list">
                                                        @foreach ($l2->sons as $k3 => $l3)
                                                            <li class="dd-item" data-id="{{ $l3->id }}">
                                                                <a data-href="{{ route('admin.menu.sub.delete.post', ['id' => $model->id, 'id2' => $l3->id]) }}" class="btn btn-danger btn-sm " data-redirect="no" data-toggle="modal" data-target="#confirm-modal" data-header="{{ trans('common.confirm') }}?" data-body="{{ trans('common.are_you_sure') }}?" data-successcall="reloadMenu"><i class="fa fa-trash"></i></a>
                                                                <a href="{{ route('admin.menu.sub.edit', ['id' => $model->id, 'id2' => $l3->id]) }}" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#remote-modal"><i class="fa fa-eye"></i></a>
                                                                <div class="dd-handle">{{ $l3->getTextWithLink() }}</div>
                                                                @if ($model->tier >= 4)
                                                                    <ol class="dd-list">
                                                                        @foreach ($l3->sons as $k4 => $l4)
                                                                            <li class="dd-item" data-id="{{ $l4->id }}">
                                                                                <a data-href="{{ route('admin.menu.sub.delete.post', ['id' => $model->id, 'id2' => $l4->id]) }}" class="btn btn-danger btn-sm " data-redirect="no" data-toggle="modal" data-target="#confirm-modal" data-header="{{ trans('common.confirm') }}?" data-body="{{ trans('common.are_you_sure') }}?" data-successcall="reloadMenu"><i class="fa fa-trash"></i></a>
                                                                                <a href="{{ route('admin.menu.sub.edit', ['id' => $model->id, 'id2' => $l4->id]) }}" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#remote-modal"><i class="fa fa-eye"></i></a>
                                                                                <div class="dd-handle">{{ $l4->getTextWithLink() }}</div>
                                                                                @if ($model->tier >= 5)
                                                                                    <ol class="dd-list">
                                                                                        @foreach ($l4->sons as $k5 => $l5)
                                                                                            <li class="dd-item" data-id="{{ $l5->id }}">
                                                                                                <a data-href="{{ route('admin.menu.sub.delete.post', ['id' => $model->id, 'id2' => $l5->id]) }}" class="btn btn-danger btn-sm " data-redirect="no" data-toggle="modal" data-target="#confirm-modal" data-header="{{ trans('common.confirm') }}?" data-body="{{ trans('common.are_you_sure') }}?" data-successcall="reloadMenu"><i class="fa fa-trash"></i></a>
                                                                                                <a href="{{ route('admin.menu.sub.edit', ['id' => $model->id, 'id2' => $l5->id]) }}" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#remote-modal"><i class="fa fa-eye"></i></a>
                                                                                                <div class="dd-handle">{{ $l5->getTextWithLink() }}</div>
                                                                                            </li>
                                                                                        @endforeach
                                                                                    </ol>
                                                                                @endif
                                                                            </li>
                                                                        @endforeach
                                                                    </ol>
                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    </ol>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ol>
                                @endif
                            </li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@push('footer')
    <script type="text/javascript" src="/custom/nestable/jquery.nestable.js"></script>
    <script>
        window['reloadMenu'] = function (resp) {
            if (typeof resp.data.model_id !== 'undefined') {
                $('.dd-item[data-id="' + resp.data.model_id + '"]').remove();
                alertSuccess('{{ trans('common.operation_success') }}');
            }
        };

        +function ($) {
            $(document).ready(function () {

                $(document).on('change update blur', '#type-selector', function () {
                    var value = $(this).val();

                    $('.type-related').hide();
                    $('#type-related-' + value).show();
                });

                $('#nestable').nestable({
                    group: 1,
                    maxDepth: {{ $model->tier }}
                });

                //Reorder
                $(document).on('change', '.dd-item', function(e) {
                    e.stopPropagation();

                    var id = $(this).data('id'),
                        parentId = $(this).parents('.dd-item').data('id');

                    if (typeof parentId === 'undefined') {
                        parentId = 0;
                    }

                    var brothers = $(this).closest('.dd-list').find('.dd-item');

                    var ids = [];
                    $(brothers).each(function () {
                        ids.push($(this).data('id'));
                    });

                    var data = {
                        parent_id: parentId,
                        target_id: id,
                        ids: ids
                    };

                    $.ajax({
                        url: "{{ route('admin.menu.sub.updatesorting', ['id' => $model->id]) }}",
                        type: "post",
                        dataType: 'json',
                        data: data,
                        beforeSend: function () {
                            blockBody();
                        },
                        success: function (resp) {
                            if (typeof resp.message !== 'undefined') {
                                alertSuccess(resp.message);
                            }
                        },
                        error: function (resp) {
                            alertError('{{ trans('common.unknown_error') }}');
                        },
                        complete: function () {
                            unblockBody();
                        }
                    });
                });
            });
        }(jQuery);
    </script>
@endpush

@push('modal') @endpush


