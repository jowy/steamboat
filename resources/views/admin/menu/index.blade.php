@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_menu') }}
@endsection

@section('description')
    {{ trans('permission.manage_menu') }}
@endsection

@section('author') @endsection

@section('metatag') @endsection

@push('header') @endpush

@push('action') @endpush

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.lists') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-hover table-bordered table-striped" id="data-dt">
                <thead>
                    <tr role="row" class="heading">
                        <th width="80">#</th>
                        <th width="200">{{ trans('common.actions') }}</th>
                        <th>{{ trans('common.name') }}</th>
                        <th width="200">{{ trans('common.updated_at') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('footer')
    <script type="text/javascript">
        (function ($) {
            $(document).ready(function () {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#data-dt'),
                    showIndex: true,
                    filterContainer: '#filter-container',
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('admin.menu.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id', sortable: true},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false},
                            {data: 'name', name: 'name', orderable: false, searchable: false},
                            {data: 'updated_at', name: 'updated_at'},
                        ],
                    }
                });
            });
        }(jQuery));
    </script>
@endpush

@push('modal') @endpush