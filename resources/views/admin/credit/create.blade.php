@extends('ajaxmodal')

@section('title')
    {{ trans('permission.manage_credit') }}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#credit-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit-credit'
                });

                var $form = $('#credit-form');
                var loading = false;

                var blockForm = function () {
                    loading = true;
                    blockBody();
                };
                var unblockForm = function () {
                    unblockBody();
                    loading = false;
                };

                var resetUsername = function (clear) {
                    $('.username-related').hide();

                    if (typeof clear !== 'undefined' && clear === true) {
                        $('#user-name-input').val('');
                    }
                };

                var showUsername = function () {
                    $('.username-related').show();
                };

                $(document).on('click', '#username-search', function () {
                    $('#username-input').trigger('blur');
                });

                $(document).on('blur', '#username-input', function () {
                    if (loading === false && $(this).val() != '') {
                        var data = { username: $('#username-input').val() };

                        $.ajax({
                            url: '{{ route('admin.credit.checkusername') }}',
                            method: 'post',
                            dataType: 'json',
                            data: data,
                            beforeSend: function () {
                                blockForm();
                                resetUsername(false);
                            },
                            success: function (resp) {
                                if (typeof resp.name !== 'undefined') {
                                    $('#user-name-input').val(resp.name);
                                    showUsername();
                                }
                            },
                            error: function (resp, statusText, xhr, formElm) {
                                handleError(resp);
                                resetUsername(true);
                            },
                            complete: function () {
                                unblockForm();
                            }
                        });
                    } else {
                        resetUsername(true);
                    }
                });

                $('#adjust-description').on('change', function () {
                    $('.description-related').hide();
                    if ($(this).val() == 1) {
                        $('.description-related').show();
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit-credit" class="btn btn-primary blue">{{ trans('common.submit') }}</button>
@endsection

@section('content')
    <style>
        .description-related { display: none; }
        .username-related { display: none; }
    </style>
    {!! Form::open(['route' => 'admin.credit.create.post', 'method' => 'post', 'role' => 'form', 'id' => 'credit-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
        <div class="form-group m-form__group">
            <label>{{ trans('common.username') }}</label>
            <div class="input-group">
                {!! Form::text('username', '', ['class' => 'form-control m-input', 'id' => 'username-input']) !!}
                <span class="input-group-append">
                    <button type="button" class="btn btn-primary " id="username-search"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </div>
        <div class="form-group m-form__group username-related" id="user-name-container">
            <label>{{ trans('common.name') }}</label>
            {!! Form::text('', '', ['class' => 'form-control m-input', 'id' => 'user-name-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
            <span class="help-block text-danger">{{ trans('common.please_make_sure_name_is_correct') }}</span>
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('permission.manage_credit') }}</label>
            {!! Form::select('action_type', \App\Models\CreditManage::getActionTypeLists(), '', ['class' => 'form-control m-input', 'placeholder' => trans('common.please_select')]) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.amount') }}</label>
            {!! Form::text('amount', '', ['class' => 'form-control m-input fund-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.cash_received') }}</label>
            {!! Form::text('cash_received', 0, ['class' => 'form-control m-input fund-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.adjust_description') }}</label>
            {!! Form::select('adjust_description', \App\Constants::getYesNoForSelect(), 0, ['class' => 'form-control m-input', 'placeholder' => trans('common.please_select'), 'id' => 'adjust-description']) !!}
        </div>
        <div class="form-group m-form__group description-related">
            <label>{{ trans('common.english_description') }}</label>
            {!! Form::text('description_en', '', ['class' => 'form-control m-input']) !!}
        </div>
        <div class="form-group m-form__group description-related">
            <label>{{ trans('common.chinese_description') }}</label>
            {!! Form::text('description_cn', '', ['class' => 'form-control m-input']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.remark') }}</label>
            {!! Form::textarea('remark', '', ['class' => 'form-control m-input']) !!}
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection