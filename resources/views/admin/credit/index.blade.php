@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_credit') }}
@endsection

@section('description')
    {{ trans('permission.manage_credit') }}
@endsection

@push('header')

@endpush

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#data-dt'),
                    filterContainer: $('#filter-container'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('admin.credit.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id', sortable: true},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false},
                            {data: 'user_id', name: 'user_id'},
                            {data: 'admin_id', name: 'admin_id'},
                            {data: 'action_type', name: 'action_type'},
                            {data: 'amount', name: 'amount'},
                            {data: 'cash_received', name: 'cash_received'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'updated_at', name: 'updated_at'},
                        ],
                        fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
                            /*
                             * Calculate the total market share for all browsers in this table (ie inc. outside
                             * the pagination)
                             */
                            var total = parseFloat(0);
                            for ( var i=0 ; i<aaData.length ; i++ )
                            {
                                total += parseFloat(fundConvert(aaData[i].amount));
                            }

                            $('#result-total').html(addCommas(fundFormat(parseFloat(total).toFixed(2))));
                        },
                    }
                });
            });
        }(jQuery);
    </script>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="dashboard-stat info">
                <div class="visual">
                    <i class="fa fa-money"></i>
                </div>
                <div class="details">
                    <div class="number" id="result-total">0.00</div>
                    <div class="desc"> {{ trans('common.filtered_total') }} </div>
                </div>
                <a class="more" href="javascript:;"> &nbsp;
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile" id="filter-container">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.filter') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m-form m-form--fit m-form--label-align-right">
                <div class="form-group m-form__group">
                    <label>{{ trans('common.date') }}</label>
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            {!! Form::text('filter_created_after_date', '', ['class' => 'form-control m-input form-filter input-sm date-picker', 'placeholder' => trans('common.from')]) !!}
                        </div>
                        <div class="col-12 col-lg-6">
                            {!! Form::text('filter_created_before_date', '', ['class' => 'form-control m-input form-filter input-sm date-picker', 'placeholder' => trans('common.to')]) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.action') }}</label>
                    {!! Form::select('filter_action_type', \App\Models\CreditManage::getActionTypeLists(), '', ['class' => 'form-control m-input form-filter', 'placeholder' => trans('common.all')]) !!}
                </div>
                <div class="form-group m-form__group">
                    <labe>{{ trans('common.username') }}</labe>
                    {!! Form::text('filter_username', '', ['class' => 'form-control m-input form-filter', 'placeholder' => trans('common.username')]) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.operator') }}</label>
                    <input type="text" class="form-control m-input form-filter" name="filter_admin" placeholder="{{ trans('common.operator') }}">
                </div>
                <div class="form-group m-form__group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <button class="btn btn-danger btn-block  filter-submit">
                                <i class="fa fa-search"></i> {{ trans('common.filter') }}
                            </button>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <button class="btn btn-info btn-block  filter-cancel">
                                <i class="fa fa-times"></i> {{ trans('common.reset') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.lists') }}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin.credit.create') }}" class="btn btn-primary" data-target="#remote-modal" data-toggle="modal"><i class="md-plus"></i> {{ trans('common.create') }}</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-hover table-bordered table-striped" id="data-dt">
                <thead>
                    <tr role="row" class="heading">
                        <th width="50">#</th>
                        <th width="65">{{ trans('common.actions') }}</th>
                        <th width="140">{{ trans('common.username') }}</th>
                        <th width="140">{{ trans('common.operator') }}</th>
                        <th>{{ trans('common.action') }}</th>
                        <th width="130">{{ trans('common.amount') }}</th>
                        <th width="130">{{ trans('common.cash_received') }}</th>
                        <th width="150">{{ trans('common.created_at') }}</th>
                        <th width="150">{{ trans('common.updated_at') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('modal')

@endpush