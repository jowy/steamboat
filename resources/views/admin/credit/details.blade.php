@extends('ajaxmodal')

@section('title')
    {{ trans('permission.manage_credit') }} #{{ $model->id }}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {

            });
        }(jQuery);
    </script>
@endsection

@section('footer')

@endsection

@section('content')
    <div class="form-group form-material">
        <label>{{ trans('common.username') }}</label>
        {!! Form::text('', $model->user ? $model->user->username : trans('common.user_not_found'), ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
    </div>
    <div class="form-group form-material">
        <label>{{ trans('common.operator') }}</label>
        {!! Form::text('', $model->admin ? $model->admin->username : trans('common.admin_not_found'), ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
    </div>
    <div class="form-group form-material">
        <label>{{ trans('permission.manage_credit') }}</label>
        {!! Form::select('', \App\Models\CreditManage::getActionTypeLists(), $model->action_type, ['class' => 'form-control m-input', 'placeholder' => trans('common.all'), 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
    </div>
    <div class="form-group form-material">
        <label>{{ trans('common.amount') }}</label>
        {!! Form::text('', $model->amount, ['class' => 'form-control m-input fund-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
    </div>
    <div class="form-group form-material">
        <label>{{ trans('common.cash_received') }}</label>
        {!! Form::text('', $model->cash_received, ['class' => 'form-control m-input fund-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
    </div>
    <div class="form-group form-material">
        <label>{{ trans('common.adjust_description') }}</label>
        {!! Form::text('', $model->explainAdjustDescription(), ['class' => 'form-control m-input fund-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
    </div>
    @if ($model->adjust_description == 1)
        <div class="form-group m-form__group">
            <label>{{ trans('common.english_description') }}</label>
            {!! Form::text('', $model->description_en, ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
        <div class="form-group m-form__group">
            <label>{{ trans('common.chinese_description') }}</label>
            {!! Form::text('', $model->description_cn, ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
    @endif
    @if ($model->remark && $model->remark != null)
        <div class="form-group m-form__group">
            <label>{{ trans('common.remark') }}</label>
            {!! Form::textarea('', $model->remark, ['class' => 'form-control m-input', 'readonly' => 'readonly', 'disabled' => 'disabled']) !!}
        </div>
    @endif
@endsection

@section('modal')

@endsection