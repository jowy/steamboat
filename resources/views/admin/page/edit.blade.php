@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_page') }}
@endsection

@section('description')
    {{ trans('permission.manage_page') }}
@endsection

@section('author') @endsection

@section('metatag') @endsection

@push('header') @endpush

@push('action')
@endpush

@section('content')
    {!! Form::open(['route' => ['admin.page.edit.post', 'id' => $page->id], 'role' => 'form', 'method' => 'post', 'id' => 'page-form', 'files' => true, 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
        <div class="m-portlet m-portlet--mobile m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#tab-en" role="tab">
                                <i class="flag-icon {{ \App\Constants::getFlagClassByLanguage('en') }}" aria-hidden="true"></i> {{ trans('common.english') }}
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-cn" role="tab">
                                <i class="flag-icon {{ \App\Constants::getFlagClassByLanguage('cn') }}" aria-hidden="true"></i> {{ trans('common.chinese') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="form-group m-form__group">
                    @if ($page->is_system == 1)
                        <label>{{ trans('common.url') }}</label>
                        <div class="form-control m-input-static">{{ route('user.page', ['page_tag' => $page->page_tag]) }}</div>
                    @else
                        <label>{{ trans('common.url') }}</label>
                        {!! Form::text('page_tag', $page->page_tag, ['class' => 'form-control m-input slug-input', 'id' => 'tag-input', 'data-target' => '#tag-target']) !!}
                        <span class="help-block" id="tag-target"></span>
                    @endif
                </div>
                <hr />
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-en" role="tabpanel">
                        @if ($page->has_share == 1)
                            <div class="form-group m-form__group">
                                <label>{{ trans('common.sharing_image') }}</label>
                                @if ($page->sharing_image_en != null)
                                    <img src="{{ asset($page->sharing_image_en) }}" alt="" />
                                @else
                                    <img src="http://www.placehold.it/1200x630/EFEFEF/AAAAAA&amp;text=1200x630" alt="" />
                                @endif
                                {!! Form::file('sharing_image_en', ['class' => 'form-controm m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label>{{ trans('common.sharing_title') }}</label>
                                {!! Form::text('sharing_title_en', $page->sharing_title_en, ['class' => 'form-control m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label>{{ trans('common.sharing_content') }}</label>
                                {!! Form::textarea('sharing_content_en', $page->sharing_content_en, ['class' => 'form-control m-input']) !!}
                            </div>
                        @endif
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.title') }}</label>
                            {!! Form::text('title_en', $page->title_en, ['class' => 'form-control m-input']) !!}
                        </div>
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.description') }}</label>
                            {!! Form::text('page_description_en', $page->page_description_en, ['class' => 'form-control m-input']) !!}
                        </div>
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.content') }}</label>
                            {!! Form::textarea('content_en', $page->content_en, ['class' => 'form-control m-input editor']) !!}
                        </div>
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.mobile_content') }}</label>
                            {!! Form::textarea('mobile_content_en', $page->mobile_content_en, ['class' => 'form-control m-input editor']) !!}
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-cn" role="tabpanel">
                        @if ($page->has_share == 1)
                            <div class="form-group m-form__group">
                                <label>{{ trans('common.sharing_image') }}</label>
                                @if ($page->sharing_image_cn != null)
                                    <img src="{{ asset($page->sharing_image_cn) }}" alt="" />
                                @else
                                    <img src="http://www.placehold.it/1200x630/EFEFEF/AAAAAA&amp;text=1200x630" alt="" />
                                @endif
                                {!! Form::file('sharing_image_cn', ['class' => 'form-controm m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label>{{ trans('common.sharing_title') }}</label>
                                {!! Form::text('sharing_title_cn', $page->sharing_title_cn, ['class' => 'form-control m-input']) !!}
                            </div>
                            <div class="form-group m-form__group">
                                <label>{{ trans('common.sharing_content') }}</label>
                                {!! Form::textarea('sharing_content_cn', $page->sharing_content_cn, ['class' => 'form-control m-input']) !!}
                            </div>
                        @endif
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.title') }}</label>
                            {!! Form::text('title_cn', $page->title_cn, ['class' => 'form-control m-input']) !!}
                        </div>
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.description') }}</label>
                            {!! Form::text('page_description_cn', $page->page_description_cn, ['class' => 'form-control m-input']) !!}
                        </div>
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.content') }}</label>
                            {!! Form::textarea('content_cn', $page->content_cn, ['class' => 'form-control m-input editor']) !!}
                        </div>
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.mobile_content') }}</label>
                            {!! Form::textarea('mobile_content_cn', $page->mobile_content_cn, ['class' => 'form-control m-input editor']) !!}
                        </div>
                    </div>
                </div>
                <hr />
                <div class="form-group m-form__group">
                    <button type="button" class="btn btn-primary btn-block" id="submit-btn"><i class="fa fa-check"></i> {{ trans('common.update') }}</button>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@push('footer')
    @include('tinymce', ['selector' => '.editor'])
    <script type="text/javascript" src="/custom/speakingurl/speakingurl.js"></script>
    <script>
        +function ($) {
            $(document).ready(function () {
                $('#page-form').makeAjaxForm({
                    submitBtn: '#submit-btn',
                    redirectTo: '{{ route('admin.page') }}',
                });

                $('#tag-input').trigger('change');
            });
        }(jQuery);
    </script>
@endpush

@push('modal') @endpush


