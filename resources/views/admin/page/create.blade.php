@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_page') }}
@endsection

@section('description')
    {{ trans('permission.manage_page') }}
@endsection

@section('author') @endsection

@section('metatag') @endsection

@push('header') @endpush

@section('content')
    {!! Form::open(['route' => 'admin.page.create.post', 'role' => 'form', 'method' => 'post', 'id' => 'page-form', 'files' => true, 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
        <div class="m-portlet m-portlet--mobile m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#tab-en" role="tab">
                                <i class="flag-icon flag-icon-{{ \App\Constants::getFlagClassByLanguage('en') }}" aria-hidden="true"></i> {{ trans('common.english') }}
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-cn" role="tab">
                                <i class="flag-icon flag-icon-{{ \App\Constants::getFlagClassByLanguage('cn') }}" aria-hidden="true"></i> {{ trans('common.chinese') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="form-group m-form__group">
                    <label>{{ trans('common.url') }}</label>
                    {!! Form::text('page_tag', '', ['class' => 'form-control m-input slug-input', 'id' => 'tag-input', 'data-target' => '#tag-target']) !!}
                    <span class="help-block" id="tag-target"></span>
                </div>
                <hr />
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-en" role="tabpanel">
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.title') }}</label>
                            {!! Form::text('title_en', '', ['class' => 'form-control m-input']) !!}
                        </div>
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.description') }}</label>
                            {!! Form::text('page_description_en', '', ['class' => 'form-control m-input']) !!}
                        </div>
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.content') }}</label>
                            {!! Form::textarea('content_en', '', ['class' => 'form-control m-input editor']) !!}
                        </div>
                        <hr />
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.mobile_content') }}</label>
                            {!! Form::textarea('mobile_content_en', '', ['class' => 'form-control m-input editor']) !!}
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-cn" role="tabpanel">
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.title') }}</label>
                            {!! Form::text('title_cn', '', ['class' => 'form-control m-input']) !!}
                        </div>
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.description') }}</label>
                            {!! Form::text('page_description_cn', '', ['class' => 'form-control m-input']) !!}
                        </div>
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.content') }}</label>
                            {!! Form::textarea('content_cn', '', ['class' => 'form-control m-input editor']) !!}
                        </div>
                        <hr />
                        <div class="form-group m-form__group">
                            <label>{{ trans('common.mobile_content') }}</label>
                            {!! Form::textarea('mobile_content_cn', '', ['class' => 'form-control m-input editor']) !!}
                        </div>
                    </div>
                </div>
                <hr />
                <div class="form-group m-form__group">
                    <button type="button" class="btn btn-primary btn-block" id="submit-btn"><i class="fa fa-check"></i> {{ trans('common.submit') }}</button>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@push('footer')
    @include('tinymce', ['selector' => '.editor'])
    <script type="text/javascript" src="/custom/speakingurl/speakingurl.js"></script>
    <script>
        +function ($) {
            $(document).ready(function () {
                $('#page-form').makeAjaxForm({
                    submitBtn: '#submit-btn',
                    redirectTo: '{{ route('admin.page') }}',
                });

                $('#tag-input').trigger('change');
            });
        }(jQuery);
    </script>
@endpush

@push('modal') @endpush


