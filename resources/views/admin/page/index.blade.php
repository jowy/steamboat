@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_page') }}
@endsection

@section('description')
    {{ trans('permission.manage_page') }}
@endsection

@push('header')

@endpush

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#data-dt'),
                    filterContainer: $('#filter-container'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('admin.page.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id', sortable: true},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false},
                            {data: 'page_description', name: 'page_description', orderable: false, searchable: false},
                            {data: 'page_tag', name: 'page_tag'},
                            {data: 'updated_at', name: 'updated_at'},
                        ]
                    }
                });
            });
        }(jQuery);
    </script>
@endpush

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.lists') }}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin.page.create') }}" class="btn btn-primary"><i class="md-plus"></i> {{ trans('common.create') }}</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-hover table-bordered table-striped" id="data-dt">
                <thead>
                    <tr role="row" class="heading">
                        <th width="80">#</th>
                        <th width="150">{{ trans('common.actions') }}</th>
                        <th>{{ trans('common.description') }}</th>
                        <th width="400">{{ trans('common.url') }}</th>
                        <th width="200">{{ trans('common.updated_at') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('modal')

@endpush