<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <title>
        @if ($_G['setting']['site_name_' . $_G['lang']])
            {{ $_G['setting']['site_name_' . $_G['lang']] }}
        @endif
        @yield('title')
        @if ($_G['setting']['slogan_' . $_G['lang']])
            {{ $_G['setting']['slogan_' . $_G['lang']] }}
        @endif
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta content="@yield('description')" name="description" />
    <meta content="@yield('author')" name="author" />
    <script src="//ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CRoboto:300,400,500,600,700" media="all">
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    @stack('metatag')
    @include('admin.include.header_script')
    <link rel="shortcut icon" href="{{ $_G['setting']['favicon'] }}" />
    @stack('header')
</head>
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default{{ $_G['admin']->hasPermission('can_export') ? ' has-export' : '' }}">
    <div class="m-grid m-grid--hor m-grid--root m-page">
        @include('admin.include.header')

        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
            @include('admin.include.sidebar')

            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title ">@yield('title')</h3>
                        </div>
                    </div>
                </div>
                <div class="m-content">
                    <div class="row">
                        <div class="col-12">
                            @include('admin.include.flash')
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('admin.include.footer')
    </div>

    <div id="m_scroll_top" class="m-scroll-top">
        <i class="la la-arrow-up"></i>
    </div>

    @include('admin.include.footer_script')
    @stack('footer')
    @include('admin.include.modal')
    @stack('modal')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script type="text/javascript">
        +function ($) {
            $(document).ready(function () {
                $(document).on('change', 'input[type="file"][data-multiple=1]', function() {
                    var empty = 0;
                    var $this = $(this);
                    var n = $this.attr('name');
                    $('input[name="' + n + '"]').each(function (index, value) {
                        if (!$(this).val()) {
                            empty++;
                        }
                    });
                    if (empty <= 0) {
                        $(this).parent().append($(this).clone().val(''));
                    }
                });
            });
        }(jQuery);
    </script>
    <div id="dtBox"></div>
</body>
</html>