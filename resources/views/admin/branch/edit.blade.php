@extends('admin.layout')

@section('title')
    {{ trans('permission.manage_branch') }}
@endsection

@section('description') @endsection

@section('author') @endsection

@section('metatag') @endsection

@push('header') @endpush

@push('action') @endpush

@section('content')
    {!! Form::open(['route' => ['admin.branch.edit.post', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'branch-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="form-group m-form__group">
                    <label>{{ trans('common.branch_name') }} ( {{ trans('common.english') }} )</label>
                    {!! Form::text('name_en', $model->name_en, ['class' => 'form-control m-input']) !!}
                </div>
                {{--<div class="form-group m-form__group">
                    <label>{{ trans('common.branch_name') }} ( {{ trans('common.chinese') }} )</label>
                    {!! Form::text('name_cn', $model->name_cn, ['class' => 'form-control m-input']) !!}
                </div>--}}
                <div class="form-group m-form__group">
                    <label>{{ trans('common.username') }}</label>
                    {!! Form::text('username', $model->username, ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.password') }}</label>
                    {!! Form::password('password', ['class' => 'form-control m-input']) !!}
                    <span class="help-block">{{ trans('common.leave_blank_not_change') }}</span>
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.password_confirmation') }}</label>
                    {!! Form::password('password_confirmation', ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.contact_number') }}</label>
                    {!! Form::text('contact_number', $model->contact_number, ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.email') }}</label>
                    {!! Form::text('email', $model->email, ['class' => 'form-control m-input']) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.country') }}</label>
                    {!! Form::select('country_id', $countries, $model->country_id, ['class' => 'form-control m-input', 'placeholder' => trans('common.please_select')]) !!}
                </div>
                <div class="form-group m-form__group">
                    <label>{{ trans('common.receipt_address') }}</label>
                    {!! Form::textarea('receipt_address', $model->receipt_address, ['class' => 'form-control m-input']) !!}
                </div>
                <hr />
                <div class="form-group m-form__group">
                    <div class="panel panel-bordered" id="maps-container">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('common.map') }}</h3>
                        </div>
                        <div class="panel-body gmap-container">
                            <div class="form-body" style="display: table; width: 100%;">
                                <div class="form-group google-map-setup row">
                                    <div class="col-sm-12 col-md-7">
                                        <h3>{{ trans('common.search_map_prefix') }}<img src="{{ asset('img/gmapmarker.png') }}" style="width: 21px;"> to arrange your location</h3>
                                        <div class="searchmap-map-container" style="height: 350px;"></div>
                                    </div>
                                    <div class="col-sm-12 col-md-5">
                                        <h3>{{ trans('common.search_address') }}:</h3>
                                        <a href="javascript:;" class="clear-map">{{ trans('common.clear_map_data') }}</a>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="text" name="address" value="{{ $model->address }}" class="googlemap-address-input form-control" placeholder="{{ trans('common.address') }}" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" name="latitude" value="{{ $model->latitude }}" class="googlemap-latitude-input form-control" placeholder="{{ trans('common.latitude') }}" readonly />
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="longitude" value="{{ $model->longitude }}" class="googlemap-longitude-input form-control" placeholder="{{ trans('common.longitude') }}" readonly />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group m-form__group">
                    <button type="button" id="btn-submit" class="btn btn-primary btn-block blue">{{ trans('common.edit') }}</button>
                </div>
            </div>
        </div>
    {!! Form::close() !!}

@endsection

@push('footer')
    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?sensor=true&key={{ config('env.GOOGLE_MAP_API_KEY') }}&libraries=places"></script>
    <script type="text/javascript" src="/custom/gmap.js"></script>
    <script type="text/javascript">
        +function() {
            $(document).ready(function() {
                $('#branch-form').makeAjaxForm({
                    submitBtn: '#btn-submit',
                    redirectTo: '{{ route('admin.branch') }}',
                });

                //Google Maps
                $('.gmap-container').makeGmapContainer();
                $('#add-map').on('click', function() {
                    var mapCount = $('.gmap-container').length;
                    var index = parseInt(mapCount) + parseInt(1);
                    var template = getMapSetupTemplate();
                    template = template.replace(/@@index@@/g, index);

                    $('#maps-container').append(template);
                    $('.gmap-container').makeGmapContainer();
                });
            });
        }(jQuery);
    </script>
@endpush

@push('modal') @endpush
