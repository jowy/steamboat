@extends('ajaxmodal')

@section('title')
    {{ trans('common.stock_manage') }}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#stock-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    successRefresh: true,
                    submitBtn: '#btn-submit-stock'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit-stock" class="btn btn-primary blue">{{ trans('common.submit') }}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['admin.branch.stock.manage', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'stock-form', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
        <div class="form-body">
            <div class="form-group m-form__group">
                <label>{{ trans('common.product') }}</label>
                {!! Form::select('product', $products, '', ['class' => 'form-control m-input', 'placeholder' => trans('common.please_select')]) !!}
            </div>
            <div class="form-group m-form__group">
                <label>{{ trans('common.action') }}</label>
                {!! Form::select('transaction_type', [1 => trans('common.add_stock'), 2 => trans('common.deduct_stock')], '', ['class' => 'form-control m-input', 'placeholder' => trans('common.please_select')]) !!}
            </div>
            <div class="form-group m-form__group">
                <label>{{ trans('common.quantity') }}</label>
                {!! Form::text('quantity', 0, ['class' => 'form-control number-input m-input']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>{{ trans('common.remark') }}</label>
                {!! Form::text('remark', '', ['class' => 'form-control m-input']) !!}
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection