@extends('admin.layout')

@section('title')
    {{ $model->getName() }}
@endsection

@section('description')
    {{ $model->getName() }}
@endsection

@push('header')

@endpush

@push('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#data-dt'),
                    showIndex: true,
                    dataTable: {
                        @include('custom.datatable.common', ['order' => 0, 'sorting' => 'desc'])
                        "ajax": {
                            "url": "{{ route('admin.branch.stock.dt', ['id' => $model->id]) }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id', orderable: true},
                            {data: 'product_id', name: 'product_id'},
                            {data: 'transaction_type', name: 'transaction_type'},
                            {data: 'quantity', name: 'quantity'},
                            {data: 'remark', name: 'remark'},
                            {data: 'created_at', name: 'created_at'},
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endpush

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ $model->getName() }} {{ trans('common.stock') }}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-hover table-bordered table-striped">
                <thead>
                    <tr>
                        <th>{{ trans('common.product_name') }}</th>
                        <th width="200">{{ trans('common.quantity') }}</th>
                        <th width="200">{{ trans('common.amount') }}</th>
                        <th width="200">{{ trans('common.total_amount') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($stocks as $key => $var)
                        @if ($var->quantity > 0)
                            <tr>
                                <td>{{ $var->getName() }}</td>
                                <td>{{ fundFormat($var->quantity, 0) }}</td>
                                <td>$ {{ fundFormat($var->cost) }}</td>
                                <td>$ {{ fundFormat($var->cost * $var->quantity) }}</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="sbold text-danger text-right">{{ fundFormat($total_quantity, 0) }}</td>
                        <td colspan="2" class="text-left bold text-danger">$ {{ fundFormat($total_cost) }}</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ trans('common.stock') }}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin.branch.stock.manage', ['id' => $model->id]) }}" data-toggle="modal" data-target="#remote-modal" class="btn btn-danger"><i class="md-plus"></i> {{ trans('common.stock_manage') }}</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-hover table-bordered table-striped" id="data-dt">
                <thead>
                    <tr role="row" class="heading">
                        <th width="80">#</th>
                        <th width="300">{{ trans('common.product_name') }}</th>
                        <th>{{ trans('common.description') }}</th>
                        <th width="200">{{ trans('common.quantity') }}</th>
                        <th width="200">{{ trans('common.remark') }}</th>
                        <th width="200">{{ trans('common.created_at') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('modal')

@endpush