<html>
<head>
    <meta charset="UTF-8">
    <title>Image Uploader</title>
    <style>
        .fileUpload {
            display: table;
            width: 100%;
            vertical-align: middle;
            margin-top: 50%;
        }
        .fileUpload input.upload {
            display: none;
        }
        .btn {
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
            display: inline-block;
            padding: 6px 12px;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            cursor: pointer;
            display: block;
            margin: 0 auto;
            vertical-align: middle;
            text-align: center;
            margin-top: -34px;
        }
        .btn:hover {
            color: #fff;
            background-color: #286090;
            border-color: #204d74;
        }
        .container, .container > form {
            display: table;
            width: 100%;
            height: 100%;
            margin: 0 auto;
        }
    </style>
</head>
<body>
<div class="container">
    <!-- Change the url here to reflect your image handling controller -->
    {!! Form::open(['route' => 'admin.image.post', 'method' => 'POST', 'files' => true, 'id' => $id]) !!}
        <div class="fileUpload">
            <label>
                <a class="btn btn-primary">
                    <span>{{ trans('common.upload') }}</span>
                    {!! Form::file('imagefile', ['class' => 'upload', 'id' => $id . '-fileinput']) !!}
                </a>
            </label>
        </div>
    {!! Form::close() !!}
</div>
<script src="/custom/jquery.min.js" type="text/javascript"></script>
<script src="/custom/jquery.form.min.js" type="text/javascript"></script>
<script>
    +function ($) {
        $(document).ready(function ($) {
            $('#{{ $id }}-fileinput').on('change', function () {
                $('#{{ $id }}').submit();
            });
            $('#{{ $id }}').ajaxForm({
                success: function (resp) {
                    parent.tinymce.EditorManager.activeEditor.insertContent('<img src="' + resp.message +'">');
                    parent.tinymce.EditorManager.activeEditor.windowManager.close(window);
                    parent.tinymce.EditorManager.activeEditor.execCommand('mceAutoResize');
                },
                error: function (response, statusText, xhr, formElm) {
                    if (typeof response.message !== 'undefined') {
                        alert(response.message);
                    } else {
                        alert('{{ trans('common.unknown_error') }}');
                    }
                }
            });
        });
    }(jQuery);
</script>
</body>
</html>