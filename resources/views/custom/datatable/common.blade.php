"bStateSave": true,
"lengthMenu": [
    [10, 30, 50, 100, 150, 300, 1000, 5000],
    [10, 30, 50, 100, 150, 300, 1000, 5000]
],
"pageLength": 30,
"order": [
    [{{ isset($order) ? $order : 0 }}, "{{ isset($sorting) ? $sorting : 'desc' }}"]
],