@extends('ajaxmodal')

@section('title')
    {{ trans('common.record_not_found') }}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {

            });
        }(jQuery);
    </script>
@endsection

@section('footer')
@endsection

@section('content')
    <h4>{{ trans('common.record_not_found') }}</h4>
@endsection

@section('modal')

@endsection