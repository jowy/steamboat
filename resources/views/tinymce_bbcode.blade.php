@push('footer')
    <script src="/custom/tinymce/tinymce.min.js"></script>
@endpush
<script>
    +function() {
        $(document).ready(function() {
            /*
             * TinyMCE Setup
             */
            tinymce.init({
                selector: "{{ $selector }}",
                body_id: 'content',
                content_css: [

                ],
                convert_urls: false,
                plugins: [
                    'bbcode advlist autolink lists link image charmap print preview hr anchor pagebreak autoresize',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'paste textcolor colorpicker textpattern code imageupload'
                ],
                toolbar: 'insertfile undo redo forecolor backcolor | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link media image imageupload',
                bbcode_dialect: "punbb",
                setup: function (editor) {
                    editor.on('change', function () {
                        editor.save();
                    });
                },
            });
        });
    }(jQuery);
</script>