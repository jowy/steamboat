<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
        @if ($_G['setting']['site_name_' . $_G['lang']])
            {{ $_G['setting']['site_name_' . $_G['lang']] }}
        @endif
        @yield('title')
        @if ($_G['setting']['slogan_' . $_G['lang']])
            {{ $_G['setting']['slogan_' . $_G['lang']] }}
        @endif
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="{{ mix('css/staff_auth.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{ $_G['setting']['user_navigation_logo_' . $_G['lang']] }}" />
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
<!-- begin:: Page -->
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style="background-image: url(../../img/bg-3.jpg);">
        <div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
            <div class="m-login__container">
                <div class="m-login__logo">
                    <a href="#">
                        <img src="{{ $_G['setting']['user_login_logo_' . $_G['lang']] }}" style="max-width: 150px;">
                    </a>
                </div>
                <div class="m-login__signin">
                    <div class="m-login__head m-section__sub">
                        <h3 class="m-login__title">
                            {{ trans('common.choose_to_login') }}
                        </h3>
                    </div>
                    <div class="m-section__content" style="margin-top: 30px;">
                        <div class="m-demo m-demo--last" data-code-preview="true" data-code-html="true" data-code-js="false">
                            <div class="m-demo__preview">
                                <a href="{{ route('admin.login') }}" class="btn btn-info btn-block">
                                    <i class="fa fa-user-secret"></i>
                                    {{ trans('common.admin_login') }}
                                </a>
                                <a href="{{ route('branch.login') }}" class="btn btn-danger btn-block">
                                    <i class="fa fa-building"></i>
                                    {{ trans('common.branch_login') }}
                                </a>
                                <a href="{{ route('staff.login') }}" class="btn btn-primary btn-block">
                                    <i class="fa fa-user"></i>
                                    {{ trans('common.staff_login') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Page -->
<!--begin::Base Scripts -->
<script src="{{ mix('js/staff_auth.js') }}" type="text/javascript"></script>
<script src="{{ mix('js/all_custom.js') }}" type="text/javascript"></script>
<!--end::Base Scripts -->
</body>
<!-- end::Body -->
</html>
