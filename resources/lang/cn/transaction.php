<?php

return [

    'credit_transfer_out' => '转帐至: :to_username',
    'credit_transfer_in' => '转帐来自: :from_username',

    'topup' => '充值',
    'withdrawal' => '提现',
    'withdrawal_refund' => '提现取消（退款）',
];
