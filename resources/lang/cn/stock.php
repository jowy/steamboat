<?php

return [
    'type_admin_add' => '管理员添加产品',
    'type_admin_deduct' => '管理员扣除产品',
    'type_user_order' => '用户下单 (:username)',
    'type_user_refund' => '用户取消订单返还 (:username)',
    'type_restock_out' => '出货 (:username)',
    'type_restock_in' => '进货',
    'type_restock_refund' => '出货取消 (:username)',
];