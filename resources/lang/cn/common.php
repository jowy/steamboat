<?php

return [

    /** Credit related --- START */
    'credit_1' => '现金点',
    'credit_2' => '注册积分',
    'credit_3' => '小红利',
    'credit_4' => '大红利',
    'credit_1_wallet' => '现金钱包',
    'credit_2_wallet' => '注册钱包',
    'credit_3_wallet' => '小红利钱包',
    'credit_4_wallet' => '大红利钱包',
    'invalid_credit_type' => '错误的积分类型',
    'admin_add_credit_1' => '管理员添加 现金点',
    'admin_deduct_credit_1' => '管理员扣除 现金点',
    'admin_add_credit_2' => '管理员添加注册积分',
    'admin_deduct_credit_2' => '管理员扣除注册积分',
    'admin_add_credit_3' => '管理员添加小红利',
    'admin_deduct_credit_3' => '管理员扣除小红利',
    'admin_add_credit_4' => '管理员添加大红利',
    'admin_deduct_credit_4' => '管理员扣除大红利',
    'insufficient_credit_1' => '现金点不足',
    'insufficient_credit_2' => '注册积分不足',
    'insufficient_credit_3' => '小红利不足',
    'insufficient_credit_4' => '大红利不足',
    'credit' => '增加',
    'debit' => '扣除',
    'admin_related' => '管理员相关',
    'credit_1_statement' => '现金点报表',
    'credit_2_statement' => '注册积分报表',
    'credit_3_statement' => '小红利报表',
    'credit_4_statement' => '大红利报表',
    'wallet_1_statement' => '现金点报表',
    'wallet_2_statement' => '注册积分报表',
    'wallet_3_statement' => '小红利报表',
    'wallet_4_statement' => '大红利报表',
    'credit_type' => '积分类型',
    'transfer_credit_1' => '现金点转帐',
    'transfer_credit_2' => '注册积分转帐',
    'transfer_credit_3' => '小红利转帐',
    'transfer_credit_4' => '大红利转帐',
    'my_wallets' => '我的钱包',
    'credit_1_max_entitlement' => '现金点最高收获',
    'credit_2_max_entitlement' => '注册积分最高收获',
    'credit_3_max_entitlement' => '小红利最高收获',
    'credit_4_max_entitlement' => '大红利最高收获',
    'credit_1_total_received' => '已收获现金点',
    'credit_2_total_received' => '已收获注册积分',
    'credit_3_total_received' => '已收获小红利',
    'credit_4_total_received' => '已收获大红利',
    /** Credit related --- END */

    /** Language related --- START */
    'english' => '英文',
    'english_description' => '中文简介',
    'english_description_is_required' => '英文简介为必填项',
    'link_text_in_english' => '英文链接文字',
    'chinese' => '中文',
    'chinese_description' => '中文简介',
    'chinese_description_is_required' => '中文简介为必填项',
    'link_text_in_chinese' => '中文链接文字',
    'name_in_english' => '英文名称',
    'name_in_chinese' => '中文名称',
    /** Language related --- END */

    'small_bonus' => '小红利',
    'big_bonus' => '大红利',

    'adjust_description' => '调整简介',
    'avatar' => '头像',
    'user' => '用户',
    'admin' => '管理员',
    'administrator' => '管理员',
    'unknown_error' => '未知错误',
    'operation_success' => '操作成功',
    'country' => '国籍',
    'gender' => '性别',
    'male' => '男性',
    'female' => '女士',
    'record_not_found' => '记录不存在',
    'user_not_found' => '用户不存在',
    'admin_not_found' => '管理员不存在',
    'unknown' => '未知',
    'please_select' => '请选择',
    'yes' => '是',
    'no' => '否',
    'confirm' => '确定',
    'are_you_sure' => '您是否确定',
    'cancel' => '取消',
    'please_fix_the_following_error' => '请修复以下错误',
    'permission' => '权限',
    'permission_denied' => '权限不足',
    'account_banned' => '帐号已查封',
    'authentication_failed' => '认证失败',
    'unauthenticated' => '未认证',
    'login' => '登入',
    'login_now' => '马上登入',
    'signin' => '登入',
    'signin_now' => '马上登入',
    'register' => '注册',
    'register_now' => '马上注册',
    'signup' => '注册',
    'signup_now' => '马上注册',
    'forgot' => '忘记',
    'forgot_password' => '忘记密码',
    'forget' => '忘记',
    'forget_password' => '忘记密码',
    'email' => '电子邮件',
    'username' => '用户名',
    'password' => '密码',
    'password_confirmation' => '确认密码',
    'password2' => '钱包密码',
    'password2_confirmation' => '确认钱包密码',
    'current_password' => '当前密码',
    'current_password2' => '当前钱包密码',
    'new_password' => '新密码',
    'new_password2' => '新钱包密码',
    'new_password_confirmation' => '新密码确认',
    'new_password2_confirmation' => '新钱包密码确认',
    'incorrect_current_password' => '错误的当前密码',
    'incorrect_current_password2' => '错误的当前钱包密码',
	'please_keyin_new_password' => '请输入新密码',
    'please_keyin_new_password2' => '请输入新安全密码',
    'default_password' => '默认密码',
    'default_password2' => '默认钱包密码',
    'set_new_password' => '设置新密码',
    'leave_blank_not_change' => '如果不更改请留空',
    'please_keyin_current_password2' => '请输入当前钱包密码',
    'pin' => 'PIN',
    'address' => '地址',
    'remember' => '记住',
    'remember_me' => '记住我',
    'successfully_login' => '成功登入',
    'successfully_register' => '成功注册',
    'dashboard' => '首页',
    'home_page' => '首页',
    'index' => '首页',
    'welcome' => '欢迎',
    'profile' => '档案',
    'my_profile' => '我的档案',
    'nationality' => '国籍',
    'national_id' => '身份证',
    'national_id_number' => '身份证号码',
    'setting' => '设置',
    'settings' => '设置',
    'logout' => '登出',
    'logoff' => '登出',
    'lock' => '锁定',
    'active' => '活跃',
    'activated' => '活跃',
    'banned' => '已查封',
    'unban' => '解除封锁',
    'freeze' => '冻结',
    'inactive' => '不活跃',
    'promotion' => '活动',
    'package' => '配套',
    'packages' => '配套',
    'currency' => '货币',
    'invalid_currency' => '错误的货币',
    'invalid_amount' => '错误的金额',
    'amount' => '金额',
    'amounts' => '金额',
    'unfreeze' => '解封',
    'free' => '免费',
    'charges' => '收费',
    'edit' => '编辑',
    'update' => '更新',
    'delete' => '删除',
    'create' => '新增',
    'add' => '新增',
    'lang' => '语言',
    'language' => '语言',
    'username_exists' => '用户名已存在',
    'username_not_found' => '用户名不存在',
    'email_exists' => '电子邮件地址已存在',
    'email_not_found' => '电子邮件地址不存在',
    'superadmin_cannot_edit' => '超级管理员不能被编辑',
    'close' => '关闭',
    'open' => '打开',
    'date' => '日期',
    'time' => '时间',
    'datetime' => '日期与时间',
    'group' => '群组',
    'group_name' => '群组名称',
    'filter' => '过滤',
    'reset' => '重置',
    'lists' => '列表',
    'action' => '操作',
    'actions' => '操作',
    'created_at' => '创建于',
    'updated_at' => '修改于',
    'name' => '全名',
    'from' => '自',
    'to' => '至',
    'not_selected' => '未选中',
    'selected' => '已选中',
    'please_fill_in_the_form' => '请填上表格',
    'search' => '搜索',
    'status' => '状态',
    'pending' => '等待中',
    'complete' => '完成',
    'completed' => '已完成',
    'reject' => '拒绝',
    'rejected' => '已拒绝',
    'approved' => '已通过',
    'decline' => '回绝',
    'declined' => '已回绝',
    'blank_account' => '空白帐号',
    'loan_account' => '借贷帐号',
    'operator' => '操作员',
    'all' => '全部',
    'filtered_total' => '已筛选总数',
    'remark' => '备注',
    'please_make_sure_name_is_correct' => '请确保名称正确',
    'submit' => '提交',
    'announcement' => '公告',
    'title' => '标题',
    'content' => '内容',
    'country_name' => '国家名称',
    'actual_exchange_rate' => '实际兑换率',
    'buy_in' => '买入',
    'sell_out' => '卖出',
    'phone_extension' => '电话前缀',
    'currency_prefix' => '货币前缀',
    'currency_suffix' => '货币后缀',
    'value' => '值',
    'description' => '简介',
    'width' => '宽度',
    'height' => '高度',
    'recommended_image_size' => '推荐图像尺寸',
    'select' => '选择',
    'choose' => '选择',
    'preview' => '预览',
    'upload' => '上传',
    'download' => '下载',
    'duplicated_page' => '重复页面',
    'page' => '页面',
    'url' => '链接',
	'url_text' => '链接内容',
    'share' => '分享',
    'sharing_image' => '分享图',
    'remove' => '移除',
    'sharing_title' => '分享标题',
    'sharing_content' => '分享内容',
    'mobile_content' => '手机内容',
    'slider' => '横幅',
    'start_date' => '开始日期',
    'start_datetime' => '开始日期与时间',
    'end_date' => '结束日期',
    'end_datetime' => '结束日期与时间',
    'open_in_new_tab' => '新窗口打开',
    'note' => '注释',
    'recommend_image_size' => '推荐图像尺寸: :width px x :height px',
    'image' => '图像',
    'add_menu' => '添加菜单',
    'edit_menu' => '编辑菜单',
    'enable' => '激活',
    'enabled' => '已激活',
    'disable' => '禁用',
    'disabled' => '已禁用',
    'type' => '类型',
    'other' => '其它',
    'others' => '其它',
    'sharing_image_suggestion' => '建议使用图像尺寸为最少 1200 x 630 px',
    'invalid_user_transaction' => '错误的用户交易',
    'page_not_found' => '页面不存在',
    'your_last_login_at' => '您的最后登入在: :datetime',
    'facebook_login' => 'Facebook 登入',
    'username_registered' => '用户名已注册！',
    'email_registered' => '电子邮件已注册！',
    'reset_now' => '马上重置',
    'name_as_per_national_id' => '国名身份证上之全名',
    'contact' => '联络',
    'date_of_birth' => '出生日期',
    'invite' => '邀请',
    'user_scan_qrcode_in_wechat_to_share' => '使用微信 <strong>扫一扫</strong> 扫描以进行分享',
    'or' => '或',
    'copy' => '复制',
    'share_your_link' => '分享您的链接',
    'invite_now' => '马上邀请',
    'invitation_from' => '来自 :username 的邀请',
    'introducer' => '介绍人',
    'optional' => '非必填项',
    'my_community' => '我的社群',
    'the_username_cannot_upgrade_anymore' => ':username 已无法继续升级',
    'upgrade' => '升级',
    'verify' => '验证',
    'verified' => '已验证',
    'target_username' => '目标用户名',
    'statement' => '报表',
    'report' => '报告',
    'total_bonus' => '总奖金',
    'total_sales' => '总销售',
    'package_price' => '配套价格',
    'bonus' => '奖金',
    'transaction' => '交易',
    'refer_new_member' => '注册新会员',
    'i_confirm_the_above_is_correct' => '我确认上述资料完全正确',
    'please_confirm_the_above_is_correct' => '请确认上述资料完全正确',
    'no_account_register_now' => '没有帐号？马上注册',
    'first_login' => '首次登入',
    'terms_and_conditions' => '使用条款与协议',
    'i_agree_the_terms_and_conditions_above' => '我同意并已阅读使用条款与协议',
    'i_agree_the_terms_and_conditions' => '我已阅读并同意使用条款与协议',
    'please_agree_the_terms_and_conditions_first' => '请先阅读并同意使用条款与协议',
    'back' => '返回',
    'next' => '下一步',
    'agree' => '同意',
    'continue' => '继续',
    'please_confirm_upgrade_target_username' => '请确认要升级的用户名的准确，操作后将不可复原',
    'registration_type' => '注册类型',
    'referrer' => '推荐人',
    'cannot_transfer_to_yourself' => '您不能转帐给自己',
    'please_select_atleast_1_permission' => '请至少选择一项权限',
    'exists' => '已存在',
    'logo' => '商标',
    'photo' => '图像',
    'bank' => '银行',
    'bank_name' => '银行名称',
    'bank_not_found' => '银行不存在',
    'bank_account_name' => '银行户口名称',
    'bank_account_number' => '银行户口号码',
	'please_select_bank' => '请选择银行',
    'please_keyin_bank_account_name' => '请输入银行户口持有人名称',
    'please_keyin_bank_account_number' => '请输入银行户口号码',
    'please_setup_bank_account' => '请设置银行户口',
    'please_setup_country_first' => '请先设置国籍',
    'receipt' => '收据',
    'please_select_bank_account_first' => '请先选择银行户口',
    'please_keyin_amount' => '请输入金额',
    'details' => '详情',
    'admin_remark' => '管理员备注',
    'rebate' => '回馈',
    'group_incentive' => '团队奖励',
    'staff' => '工作人员',
    'manager' => '经理',
    'contact_number' => '联络号码',
    'pending_approval' => '等待验证',
    'approve' => '通过',
    'approve_all' => '全数通过',
    'reject_all' => '全数回绝',
    'my_community_size' => '我的社群规模',
    'email_invite_success' => '电子邮件邀请成功',
    'username_not_available_try_again' => '用户名不可使用，请再尝试',
    'company_bank_details' => '公司银行户口资料',
    'please_upload_receipt' => '请上传收据',
    'purchase_amount_myr' => '购买金额 (MYR)',
    'calculate' => '计算',
    'bank_in_slip' => '银行汇票',
    'please_upload_bank_in_slip' => '请上传银行汇票',
    'state' => '州属',
    'please_select_state' => '请选择州属',
    'pending_bonus' => '待验证奖金',
    'pending_rebate' => '待验证回馈',
    'rebate_type' => '回馈类型',
    'start_with_http_or_https' => '始于 http:// 或 https://',
    'invalid_url_for_field' => '错误的链接 :field, 链接必须始于 http:// 或 https://',
    'facebook' => 'Facebook',
    'twitter' => 'Twitter',
    'instagram' => 'Instagram',
    'youtube' => 'Youtube',
    'website' => 'Website',
    'gallery' => '图集',
    'reset_filter' => '充值过滤器',
    'member_login' => '会员登入',
    'company_name' => '公司名称',
    'refunded' => '已退款',
    'withdrawal' => '提现',
    'currency_myr' => 'MYR',
    'minimum_withdraw' => '最低单次提现金额为 :amount',
    'withdrawal_successfully' => '您的提现已成功提交，我们将会在3个工作日内完成处理',
    'admin_fees' => '管理费',
    'nothing_has_done' => '无操作',
    'withdrawal_hint' => '请注意：每次提现将会被征收10%管理费用.',
    'product' => '产品',
    'product_name' => '产品名称',
    'product_image' => '产品图',
    'product_description' => '产品简介',
    'category' => '目录',
    'product_code' => '产品编号',
    'certificate' => '证书',
    'display_home' => '首页显示',
    'display_category' => '目录显示',
    'display_profile' => '个人主页显示',
    'file_extension_not_allowed' => '只能上传 :exts 档案',
    'size' => '尺寸',
    'colour' => '颜色',
    'material' => '材质',
    'subscribe' => '订购',
    'apply' => '使用',
	'created_data' => '新增资料',
    'deleted_data' => '已删资料',
    'updated_data' => '更新资料',
    'before_data' => '改前资料',
    'after_data' => '改后资料',
    'id' => 'ID',
    'function' => '功能',
    'operation' => '操作',
    'invalid_attribute' => '错误的 :attribute',
    'created' => '新增',
    'updated' => '编辑',
    'deleted' => '删除',
    'updated_by' => '更新自',
    'event' => '活动',
    'old_values' => '旧数据',
    'new_values' => '新数据',
	'unread' => '未读',
    'readed' => '已读',
    'terms_and_condition' => '使用条款与协议',
    'first_time_login' => '首次登入',
    'i_agree_tnc' => '我同意该使用条款与协议',
    'you_must_read_and_agree_tnc' => '您必须先同意使用条款与协议',
    'history_record' => '历史记录',
    'agent_id' => '代理ID',
    'me' => '我',
    'welcome_message' => ':username, 欢迎回来',
    'your_last_login_time_is' => '您上次的登入时间是 :datetime',
	'create_branch' => '新增分行',
    'branch_name' => '分行名称',
    'map' => '地图',
    'search_map_prefix' => '搜索并拖拽 ',
    'search_map_suffix' => ' 以调整您的位置',
    'search_address' => '搜索地址',
    'clear_map_data' => '清除地图',
    'latitude' => '纬度',
    'longitude' => '经度',
	'cash_out' => '支出现金',
	'cash_received' => '收到现金',
    'are_your_sure' => '您是否确定',
	'fiat_currency' => '法定货币',
	'stock' => '库存',
    'quantity' => '数量',
    'total_amount' => '总数',
    'stock_manage' => '库存管理',
    'add_stock' => '添加库存',
    'deduct_stock' => '扣除库存',
    'price' => '价格',
    'is_blank_account' => '是否空白帐号',
    'add_product' => '新增产品',
    'sorting' => '排序',
    'ascending_order' => '升序',
    'descending_order' => '降序',
    'product_short_description' => '产品简介',
    'total' => '总数',
    'discount' => '折扣',
    'payment_method' => '付款方式',
    'cash' => '现金',
    'credit_card' => '信用卡',
    'sub_total' => '小计',
    'tax' => '税务',
    'discount_too_much' => '折扣太多',
    'close_account' => '关帐号',
    'close_account_history' => '关帐号历史',
    'cash_in' => '现金',
    'account_history' => '帐号历史',
    'cancel_bill' => '取消账单',
    'the_order_over_x_minutes_cannot_cancel_please_cancel_at_branch' => '该订单已超过 :minute 分钟限制，请使用分行系统取消',

];
