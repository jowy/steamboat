<?php

return [

    'manage_admin' => '管理员',
    'manage_permission_group' => '权限组',

    'login_user' => '登入用户帐号',
    'manage_user' => '查看用户帐号',
    'manage_user_advance' => '修改用户帐号',
    'manage_user_account_status' => '用户状态修改',
    'manage_credit' => '积分操作',
    'manage_announcement' => '公告管理',
    'manage_slider' => '横幅管理',
    'manage_setting' => '设置管理',
    'manage_country' => '国籍管理',
    'manage_page' => '页面管理',
    'manage_menu' => '菜单管理',
    'manage_bank' => '银行管理',
    'manage_company_bank' => '公司银行帐号',
    'manage_topup' => '充值管理',
    'manage_withdrawal' => '提现管理',
	'manage_branch' => '分行管理',

    'manage_category' => '分类管理',
	'manage_product' => '产品管理',

    'dashboard_statistics' => '首页数据',
	
	'audit_log' => '资料追踪',
    'has_export' => '可导出数据',
];
