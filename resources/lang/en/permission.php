<?php

return [

    'manage_admin' => 'Manage admin',
    'manage_permission_group' => 'Manage permission group',

    'login_user' => 'Login as user',
    'login_branch' => 'Login as branch',
    'login_staff' => 'Login as staff',
    'manage_user' => 'Manage user',
    'manage_user_advance' => 'Manage user advance',
    'manage_user_account_status' => 'Manage user account status',
    'manage_credit' => 'Manage credit',
    'manage_announcement' => 'Manage announcement',
    'manage_slider' => 'Manage slider',
    'manage_setting' => 'Manage setting',
    'manage_country' => 'Manage country',
    'manage_page' => 'Manage page',
    'manage_menu' => 'Manage menu',
    'manage_bank' => 'Manage bank',
    'manage_price' => 'Manage price',
    'manage_company_bank' => 'Manage company bank',
    'manage_topup' => 'Manage topup',
    'manage_withdrawal' => 'Manage withdrawal',
	'manage_branch' => 'Manage branch',
    'manage_supplier' => 'Manage supplier',
    'manage_staff' => 'Manage staff',
    'manage_order' => 'Manage order',
    'sale_report' => 'Sale report',

    'manage_category' => 'Manage category',
	'manage_product' => 'Manage product',

    'dashboard_statistics' => 'Dashboard statistics',
	
	'audit_log' => 'Audit log',
    'has_export' => 'Can Export',
];
