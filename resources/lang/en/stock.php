<?php

return [
    'type_admin_add' => 'Admin Add Product',
    'type_admin_deduct' => 'Admin Deduct Product',
    'type_user_order' => 'User Order',
    'type_user_refund' => 'User Order Cancelled (:username)',
    'type_restock_out' => 'Restock out',
    'type_restock_in' => 'Restock in',
    'type_restock_refund' => 'Restock cancelled (:username)',
    'type_staff_order' => 'Staff Order (:username)',
    'type_staff_refund' => 'Staff Order Cancelled (:username)',

    'type_admin_add_wallet' => 'Admin Add Wallet',
    'type_admin_deduct_wallet' => 'Admin Deduct Wallet',
    'type_branch_add_wallet' => 'Branch Add Wallet',
    'type_branch_deduct_wallet' => 'Branch Deduct Wallet',
    'type_daily_sales' => 'Daily Sales',
    'type_supplier_paid' => 'Paid supplier',
];
