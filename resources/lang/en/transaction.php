<?php

return [

    'credit_transfer_out' => 'Transfer to :to_username',
    'credit_transfer_in' => 'Received from :from_username',

    'topup' => 'Topup',
    'withdrawal' => 'Withdrawal',
    'withdrawal_refund' => 'Withdrawal Refund',

    'owe' => 'Owe credit',
    'paid' => 'Paid credit',

];
