# Core 4

1. copy .env.example to .env and update the parameters
2. composer install && php artisan key:generate
3. php artisan migrate
4. php artisan db:seed --class=InstallSeeder