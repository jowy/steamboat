let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.babel([
    'public/custom/jquery.min.js',
    'public/assets/admin/vendors/base/vendors.bundle.js',
    'public/assets/admin/assets/demo/default/base/scripts.bundle.js',
    'public/custom/jquery.fileDownload.js',
    'public/custom/datatables/datatables.min.js',
    'public/custom/sweetalert.min.js',
    'public/custom/jquery.form.min.js',
    'public/custom/jquery.blockUI.js',
    'public/custom/functions.js',
    'public/custom/extension.js',
    'public/custom/datatable.js',
    'public/custom/app.js',
    'public/custom/global.js',
    'public/custom/admin.js',
], 'public/js/admin_core.js').version();

mix.scripts([
    'public/assets/admin/vendors/base/vendors.bundle.css',
    'public/assets/admin/assets/demo/default/base/style.bundle.css',
    'public/custom/flag-icon-css/flag-icon.min.css',
    'public/custom/datatables/datatables.min.css',
    'public/custom/global.css',
    'public/custom/admin.css',
], 'public/css/admin_core.css').version();

// Branch
mix.styles([
    'public/assets/branch/css/vendors.bundle.css',
    'public/assets/branch/css/style.bundle.css',
], 'public/css/branch_auth.css').version();

mix.babel([
    'public/assets/branch/js/vendors.bundle.js',
    'public/assets/branch/js/scripts.bundle.js',
    'public/assets/branch/js/login.js',
], 'public/js/branch_auth.js').version();

mix.styles([
    'public/assets/branch/css/demo3/vendors.bundle.css',
    'public/assets/branch/css/demo3/style.bundle.css',
    'public/custom/datatables/datatables.min.css',
    'public/custom/global.css',
], 'public/css/branch.css');

mix.babel([
    'public/assets/branch/js/demo3/vendors.bundle.js',
    'public/assets/branch/js/demo3/scripts.bundle.js',
	'public/custom/jquery.fileDownload.js',
    'public/custom/datatable/datatables.min.js',
    'public/assets/functions.js',
    'public/assets/app.js',
    'public/assets/extension.js',
    'public/assets/datatable.js',
], 'public/js/branch.js');

// Staff
mix.styles([
    'public/assets/staff/css/vendors.bundle.css',
    'public/assets/staff/css/style.bundle.css',
], 'public/css/staff_auth.css').version();

mix.babel([
    'public/assets/staff/js/vendors.bundle.js',
    'public/assets/staff/js/scripts.bundle.js',
    'public/assets/staff/js/login.js',
], 'public/js/staff_auth.js').version();

mix.styles([
    'public/assets/staff/css/vendors.bundle.css',
    'public/assets/staff/css/style.bundle.css',
    'public/assets/staff/css/staff_custom.css',
    //'public/custom/datatable/datatables.min.css',
], 'public/css/staff.css');

mix.scripts([
    'public/assets/staff/js/vendors.bundle.js',
    'public/assets/staff/js/scripts.bundle.js',
], 'public/js/staff.js');


mix.babel([
    'public/custom/jquery.min.js',
    'public/assets/admin/vendors/base/vendors.bundle.js',
	'public/custom/jquery.fileDownload.js',
    'public/custom/datatables/datatables.min.js',
    'public/custom/sweetalert.min.js',
    'public/custom/jquery.form.min.js',
    'public/custom/jquery.blockUI.js',
    'public/custom/functions.js',
    'public/custom/extension.js',
    'public/custom/datatable.js',
    'public/custom/app.js',
    'public/custom/global.js',
    'public/custom/admin.js',
], 'public/js/all_custom.js');
