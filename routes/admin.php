<?php

/* Route for admin */

Route::group(['middleware' => 'language:admin'], function () {
//Image upload route
    Route::get('upload/image', 'ImageController@imageUploader')->name('admin.image');
    Route::post('upload/image', 'ImageController@imageUpload')->name('admin.image.post');

    Route::group(['middleware' => 'auth:admin'], function () {
        Route::get('/', 'HomeController@index')->name('admin.home');
        Route::get('/home', 'HomeController@index')->name('admin.home');

        //manage_permission_group related
        Route::get('permission', 'PermissionController@index')->name('admin.permission')->middleware('adminHasPermission:manage_permission_group');
        Route::get('permission/dt', 'PermissionController@indexDt')->name('admin.permission.dt')->middleware('adminHasPermission:manage_permission_group');
        Route::get('permission/create', 'PermissionController@create')->name('admin.permission.create')->middleware('adminHasPermission:manage_permission_group');
        Route::post('permission/create', 'PermissionController@createPost')->name('admin.permission.create.post')->middleware('adminHasPermission:manage_permission_group');
        Route::get('permission/edit/{id}', 'PermissionController@edit')->name('admin.permission.edit')->middleware('adminHasPermission:manage_permission_group');
        Route::post('permission/edit/{id}', 'PermissionController@editPost')->name('admin.permission.edit.post')->middleware('adminHasPermission:manage_permission_group');
        Route::post('permissions/delete/{id}', 'PermissionController@delete')->name('admin.permission.delete')->middleware('adminHasPermission:manage_permission_group');

        //manage_admin related
        Route::get('administrator', 'AdminController@index')->name('admin.admin')->middleware('adminHasPermission:manage_admin');
        Route::get('administrator/dt', 'AdminController@indexDt')->name('admin.admin.dt')->middleware('adminHasPermission:manage_admin');
        Route::get('administrator/create', 'AdminController@create')->name('admin.admin.create')->middleware('adminHasPermission:manage_admin');
        Route::post('administrator/create', 'AdminController@createPost')->name('admin.admin.create.post')->middleware('adminHasPermission:manage_admin');
        Route::get('administrator/edit/{id}', 'AdminController@edit')->name('admin.admin.edit')->middleware('adminHasPermission:manage_admin');
        Route::post('administrator/edit/{id}', 'AdminController@editPost')->name('admin.admin.edit.post')->middleware('adminHasPermission:manage_admin');
        Route::post('administrator/delete/{id}', 'AdminController@delete')->name('admin.admin.delete')->middleware('adminHasPermission:manage_admin');

        //manage_user or manage_user_advance related
        Route::get('user', 'UserController@index')->name('admin.user')->middleware('adminHasAnyPermission:manage_user|manage_user_advance');
        Route::get('user/dt', 'UserController@indexDt')->name('admin.user.dt')->middleware('adminHasAnyPermission:manage_user|manage_user_advance');
        Route::get('user/view/{id}', 'UserController@view')->name('admin.user.view')->middleware('adminHasAnyPermission:manage_user|manage_user_advance');
        Route::post('user/edit/{id}', 'UserController@update')->name('admin.user.update')->middleware('adminHasPermission:manage_user_advance');

        //manage_account_status related
        Route::post('user/accountmanage/{id}', 'UserController@accountStatusManage')->name('admin.user.accountstatusmanage')->middleware('adminHasPermission:manage_account_status');

        //manage_credit related
        Route::get('credit', 'CreditController@index')->name('admin.credit')->middleware('adminHasPermission:manage_credit');
        Route::get('credit/dt', 'CreditController@indexDt')->name('admin.credit.dt')->middleware('adminHasPermission:manage_credit');
        Route::get('credit/create', 'CreditController@create')->name('admin.credit.create')->middleware('adminHasPermission:manage_credit');
        Route::post('credit/create', 'CreditController@createPost')->name('admin.credit.create.post')->middleware('adminHasPermission:manage_credit');
        Route::get('credit/details/{id}', 'CreditController@details')->name('admin.credit.details')->middleware('adminHasPermission:manage_credit');
        Route::post('credit/check_username', 'CreditController@checkUserName')->name('admin.credit.checkusername')->middleware('adminHasPermission:manage_credit');

        //manage_setting related
        Route::get('setting', 'SettingController@index')->name('admin.setting')->middleware('adminHasPermission:manage_setting');
        Route::get('setting/dt', 'SettingController@indexDt')->name('admin.setting.dt')->middleware('adminHasPermission:manage_setting');
        Route::get('setting/edit/{id}', 'SettingController@edit')->name('admin.setting.edit')->middleware('adminHasPermission:manage_setting');
        Route::post('setting/edit/{id}', 'SettingController@editPost')->name('admin.setting.edit.post')->middleware('adminHasPermission:manage_setting');

        //manage_slider related
        Route::get('slider', 'SliderController@index')->name('admin.slider')->middleware('adminHasPermission:manage_slider');
        Route::get('slider/dt', 'SliderController@indexDt')->name('admin.slider.dt')->middleware('adminHasPermission:manage_slider');
        Route::get('slider/create', 'SliderController@create')->name('admin.slider.create')->middleware('adminHasPermission:manage_slider');
        Route::post('slider/create', 'SliderController@createPost')->name('admin.slider.create.post')->middleware('adminHasPermission:manage_slider');
        Route::get('slider/edit/{id}', 'SliderController@edit')->name('admin.slider.edit')->middleware('adminHasPermission:manage_slider');
        Route::post('slider/edit/{id}', 'SliderController@editPost')->name('admin.slider.edit.post')->middleware('adminHasPermission:manage_slider');
        Route::post('slider/delete/{id}', 'SliderController@deletePost')->name('admin.slider.delete.post')->middleware('adminHasPermission:manage_slider');

        //manage_country related
        Route::get('country', 'CountryController@index')->name('admin.country')->middleware('adminHasPermission:manage_country');
        Route::get('country/dt', 'CountryController@indexDt')->name('admin.country.dt')->middleware('adminHasPermission:manage_country');
        Route::get('country/update/{id}', 'CountryController@update')->name('admin.country.update')->middleware('adminHasPermission:manage_country');
        Route::post('country/update/{id}/post', 'CountryController@updatePost')->name('admin.country.update.post')->middleware('adminHasPermission:manage_country');
        Route::get('country/location/update/{id}', 'CountryLocationController@update')->name('admin.country.location.update')->middleware('adminHasPermission:manage_country');
        Route::post('country/location/update/{id}/post', 'CountryLocationController@updatePost')->name('admin.country.location.update.post')->middleware('adminHasPermission:manage_country');
        Route::get('country/location/sub/create/{id}', 'CountryLocationController@subCreate')->name('admin.country.location.sub.create')->middleware('adminHasPermission:manage_country');
        Route::post('country/location/sub/create/{id}', 'CountryLocationController@subCreatePost')->name('admin.country.location.sub.create.post')->middleware('adminHasPermission:manage_country');
        Route::get('country/location/sub/edit/{id}/{id2}', 'CountryLocationController@subEdit')->name('admin.country.location.sub.edit')->middleware('adminHasPermission:manage_country');
        Route::post('country/location/sub/edit/{id}/{id2}/post', 'CountryLocationController@subEditPost')->name('admin.country.location.sub.edit.post')->middleware('adminHasPermission:manage_country');
        Route::post('country/location/sub/delete/{id}/{id2}/post', 'CountryLocationController@subDeletePost')->name('admin.country.location.sub.delete.post')->middleware('adminHasPermission:manage_country');
        Route::post('country/location/update_sorting/{id}', 'CountryLocationController@updateSorting')->name('admin.country.location.sub.updatesorting')->middleware('adminHasPermission:manage_country');

        //manage_page related
        Route::get('page', 'PageController@index')->name('admin.page')->middleware('adminHasPermission:manage_page');
        Route::get('page/dt', 'PageController@indexDt')->name('admin.page.dt')->middleware('adminHasPermission:manage_page');
        Route::get('page/create', 'PageController@create')->name('admin.page.create')->middleware('adminHasPermission:manage_page');
        Route::post('page/create/post', 'PageController@createPost')->name('admin.page.create.post')->middleware('adminHasPermission:manage_page');
        Route::get('page/edit/{id}', 'PageController@edit')->name('admin.page.edit')->middleware('adminHasPermission:manage_page');
        Route::post('page/edit/{id}/post', 'PageController@editPost')->name('admin.page.edit.post')->middleware('adminHasPermission:manage_page');
        Route::post('page/delete/{id}', 'PageController@deletePost')->name('admin.page.delete.post')->middleware('adminHasPermission:manage_page');

        //manage_menu related
        Route::get('menu', 'MenuController@index')->name('admin.menu')->middleware('adminHasPermission:manage_menu');
        Route::get('menu/dt', 'MenuController@indexDt')->name('admin.menu.dt')->middleware('adminHasPermission:manage_menu');
        Route::get('menu/update/{id}', 'MenuController@update')->name('admin.menu.update')->middleware('adminHasPermission:manage_menu');
        Route::post('menu/update/{id}/post', 'MenuController@updatePost')->name('admin.menu.update.post')->middleware('adminHasPermission:manage_menu');
        Route::get('menu/sub/create/{id}', 'MenuController@subCreate')->name('admin.menu.sub.create')->middleware('adminHasPermission:manage_menu');
        Route::post('menu/sub/create/{id}', 'MenuController@subCreatePost')->name('admin.menu.sub.create.post')->middleware('adminHasPermission:manage_menu');
        Route::get('menu/sub/edit/{id}/{id2}', 'MenuController@subEdit')->name('admin.menu.sub.edit')->middleware('adminHasPermission:manage_menu');
        Route::post('menu/sub/edit/{id}/{id2}/post', 'MenuController@subEditPost')->name('admin.menu.sub.edit.post')->middleware('adminHasPermission:manage_menu');
        Route::post('menu/sub/delete/{id}/{id2}/post', 'MenuController@subDeletePost')->name('admin.menu.sub.delete.post')->middleware('adminHasPermission:manage_menu');
        Route::post('menu/update_sorting/{id}', 'MenuController@updateSorting')->name('admin.menu.sub.updatesorting')->middleware('adminHasPermission:manage_menu');

        //manage_announcement related
        Route::get('announcement', 'AnnouncementController@index')->name('admin.announcement')->middleware('adminHasPermission:manage_announcement');
        Route::get('announcement/dt', 'AnnouncementController@indexDt')->name('admin.announcement.dt')->middleware('adminHasPermission:manage_announcement');
        Route::get('announcement/create', 'AnnouncementController@create')->name('admin.announcement.create')->middleware('adminHasPermission:manage_announcement');
        Route::post('announcement/create', 'AnnouncementController@createPost')->name('admin.announcement.create.post')->middleware('adminHasPermission:manage_announcement');
        Route::get('announcement/edit/{id}', 'AnnouncementController@edit')->name('admin.announcement.edit')->middleware('adminHasPermission:manage_announcement');
        Route::post('announcement/edit/{id}', 'AnnouncementController@editPost')->name('admin.announcement.edit.post')->middleware('adminHasPermission:manage_announcement');
        Route::post('announcement/delete/{id}', 'AnnouncementController@deletePost')->name('admin.announcement.delete.post')->middleware('adminHasPermission:manage_announcement');

        //manage_bank related
        Route::get('bank', 'BankController@index')->name('admin.bank')->middleware('adminHasPermission:manage_bank');
        Route::get('bank/dt', 'BankController@indexDt')->name('admin.bank.dt')->middleware('adminHasPermission:manage_bank');
        Route::get('bank/create', 'BankController@create')->name('admin.bank.create')->middleware('adminHasPermission:manage_bank');
        Route::post('bank/create', 'BankController@createPost')->name('admin.bank.create.post')->middleware('adminHasPermission:manage_bank');
        Route::get('bank/edit/{id}', 'BankController@edit')->name('admin.bank.edit')->middleware('adminHasPermission:manage_bank');
        Route::post('bank/edit/{id}', 'BankController@editPost')->name('admin.bank.edit.post')->middleware('adminHasPermission:manage_bank');
        Route::post('bank/delete/{id}', 'BankController@delete')->name('admin.bank.delete')->middleware('adminHasPermission:manage_bank');

        //manage_company_bank related
        Route::get('company_bank', 'CompanyBankController@index')->name('admin.companybank')->middleware('adminHasPermission:manage_company_bank');
        Route::get('company_bank/dt', 'CompanyBankController@indexDt')->name('admin.companybank.dt')->middleware('adminHasPermission:manage_company_bank');
        Route::get('company_bank/create', 'CompanyBankController@create')->name('admin.companybank.create')->middleware('adminHasPermission:manage_company_bank');
        Route::post('company_bank/create', 'CompanyBankController@createPost')->name('admin.companybank.create.post')->middleware('adminHasPermission:manage_company_bank');
        Route::get('company_bank/edit/{id}', 'CompanyBankController@edit')->name('admin.companybank.edit')->middleware('adminHasPermission:manage_company_bank');
        Route::post('company_bank/edit/{id}', 'CompanyBankController@editPost')->name('admin.companybank.edit.post')->middleware('adminHasPermission:manage_company_bank');
        Route::post('company_bank/delete/{id}', 'CompanyBankController@delete')->name('admin.companybank.delete')->middleware('adminHasPermission:manage_company_bank');

        //manage_branch related
        Route::get('branch', 'BranchController@index')->name('admin.branch')->middleware('adminHasPermission:manage_branch');
        Route::get('branch/dt', 'BranchController@indexDt')->name('admin.branch.dt')->middleware('adminHasPermission:manage_branch');
        Route::get('branch/create', 'BranchController@create')->name('admin.branch.create')->middleware('adminHasPermission:manage_branch');
        Route::post('branch/create/post', 'BranchController@createPost')->name('admin.branch.create.post')->middleware('adminHasPermission:manage_branch');
        Route::get('branch/edit/{id}', 'BranchController@edit')->name('admin.branch.edit')->middleware('adminHasPermission:manage_branch');
        Route::post('branch/edit/{id}', 'BranchController@editPost')->name('admin.branch.edit.post')->middleware('adminHasPermission:manage_branch');
        Route::post('branch/delete/{id}/post', 'BranchController@delete')->name('admin.branch.delete')->middleware('adminHasPermission:manage_branch');
        Route::get('/branch/login/{id}', 'BranchController@loginBranch')->name('admin.branch.login')->middleware('adminHasPermission:login_branch');
        Route::get('branch/stock/{id}', 'BranchController@stock')->name('admin.branch.stock')->middleware('adminHasPermission:manage_branch');
        Route::get('branch/stock/{id}/dt', 'BranchController@stockDt')->name('admin.branch.stock.dt')->middleware('adminHasPermission:manage_branch');
        Route::get('branch/stock/manage/{id}', 'BranchController@stockManage')->name('admin.branch.stock.manage')->middleware('adminHasPermission:manage_branch');
        Route::post('branch/stock/manage/{id}', 'BranchController@stockManagePost')->name('admin.branch.stock.manage.post')->middleware('adminHasPermission:manage_branch');

        //manage_supplier related
        Route::get('supplier', 'SupplierController@index')->name('admin.supplier')->middleware('adminHasPermission:manage_supplier');
        Route::get('supplier/dt', 'SupplierController@indexDt')->name('admin.supplier.dt')->middleware('adminHasPermission:manage_supplier');
        Route::get('supplier/create', 'SupplierController@create')->name('admin.supplier.create')->middleware('adminHasPermission:manage_supplier');
        Route::post('supplier/create/post', 'SupplierController@createPost')->name('admin.supplier.create.post')->middleware('adminHasPermission:manage_supplier');
        Route::get('supplier/edit/{id}', 'SupplierController@edit')->name('admin.supplier.edit')->middleware('adminHasPermission:manage_supplier');
        Route::post('supplier/edit/{id}', 'SupplierController@editPost')->name('admin.supplier.edit.post')->middleware('adminHasPermission:manage_supplier');
        Route::post('supplier/delete/{id}/post', 'SupplierController@delete')->name('admin.supplier.delete')->middleware('adminHasPermission:manage_supplier');

        //manage_staff related
        Route::get('staff', 'StaffController@index')->name('admin.staff')->middleware('adminHasPermission:manage_staff');
        Route::get('staff/dt', 'StaffController@indexDt')->name('admin.staff.dt')->middleware('adminHasPermission:manage_staff');
        Route::get('staff/create', 'StaffController@create')->name('admin.staff.create')->middleware('adminHasPermission:manage_staff');
        Route::post('staff/create/post', 'StaffController@createPost')->name('admin.staff.create.post')->middleware('adminHasPermission:manage_staff');
        Route::get('staff/edit/{id}', 'StaffController@edit')->name('admin.staff.edit')->middleware('adminHasPermission:manage_staff');
        Route::post('staff/edit/{id}', 'StaffController@editPost')->name('admin.staff.edit.post')->middleware('adminHasPermission:manage_staff');
        Route::post('staff/delete/{id}/post', 'StaffController@delete')->name('admin.staff.delete')->middleware('adminHasPermission:manage_staff');
        Route::get('staff/login/{id}', 'StaffController@loginStaff')->name('admin.staff.login')->middleware('adminHasPermission:login_staff');

        //manage_product related
        Route::get('product', 'ProductController@index')->name('admin.product')->middleware('adminHasPermission:manage_product');
        Route::get('product/dt', 'ProductController@indexDt')->name('admin.product.dt')->middleware('adminHasPermission:manage_product');
        Route::get('product/create', 'ProductController@create')->name('admin.product.create')->middleware('adminHasPermission:manage_product');
        Route::post('product/create', 'ProductController@createPost')->name('admin.product.create.post')->middleware('adminHasPermission:manage_product');
        Route::get('product/edit/{id}', 'ProductController@edit')->name('admin.product.edit')->middleware('adminHasPermission:manage_product');
        Route::post('product/edit/{id}', 'ProductController@editPost')->name('admin.product.edit.post')->middleware('adminHasPermission:manage_product');
        Route::post('product/delete/{id}', 'ProductController@deletePost')->name('admin.product.delete.post')->middleware('adminHasPermission:manage_product');

        //manage_price related
        Route::get('price', 'PriceController@index')->name('admin.price')->middleware('adminHasPermission:manage_price');
        Route::get('price/create', 'PriceController@create')->name('admin.price.create')->middleware('adminHasPermission:manage_price');
        Route::post('price/store', 'PriceController@store')->name('admin.price.store')->middleware('adminHasPermission:manage_price');
        Route::get('price/update/{price}', 'PriceController@edit')->name('admin.price.update')->middleware('adminHasPermission:manage_price');
        Route::post('price/update/{price}/post', 'PriceController@update')->name('admin.price.update.post')->middleware('adminHasPermission:manage_price');
        Route::post('price/delete/{id}', 'PriceController@destroy')->name('admin.price.delete')->middleware('adminHasPermission:manage_price');

        //manage_order related
        Route::get('order', 'OrderController@index')->name('admin.order')->middleware('adminHasPermission:manage_order');
        Route::get('order/dt', 'OrderController@indexDt')->name('admin.order.dt')->middleware('adminHasPermission:manage_order');
        Route::get('order/{order}', 'OrderController@show')->name('admin.order.show')->middleware('adminHasPermission:manage_order');
        Route::get('order/invoice/{order}', 'OrderController@invoice')->name('admin.order.invoice')->middleware('adminHasPermission:manage_order');

        //login_user related
        Route::get('user/login/{id}', 'UserController@loginUser')->name('admin.user.login')->middleware('adminHasPermission:login_user');

        //manage_topup related
        Route::get('user_topup', 'UserTopupController@index')->name('admin.user_topup')->middleware('adminHasPermission:manage_topup');
        Route::get('user_topup/dt', 'UserTopupController@indexDt')->name('admin.user_topup.dt')->middleware('adminHasPermission:manage_topup');
        Route::get('user_topup/details/{id}', 'UserTopupController@details')->name('admin.user_topup.details')->middleware('adminHasPermission:manage_topup');
        Route::post('user_topup/update/{id}/post', 'UserTopupController@updatePost')->name('admin.user_topup.update.post')->middleware('adminHasPermission:manage_topup');

        //manage_withdrawal related
        Route::get('user_withdrawal', 'UserWithdrawalController@index')->name('admin.user_withdrawal')->middleware('adminHasPermission:manage_withdrawal');
        Route::get('user_withdrawal/dt', 'UserWithdrawalController@indexDt')->name('admin.user_withdrawal.dt')->middleware('adminHasPermission:manage_withdrawal');
        Route::get('user_withdrawal/details/{id}', 'UserWithdrawalController@details')->name('admin.user_withdrawal.details')->middleware('adminHasPermission:manage_withdrawal');
        Route::post('user_withdrawal/handle/{id}', 'UserWithdrawalController@withdrawalHandle')->name('admin.user_withdrawal.handle.post')->middleware('adminHasPermission:manage_withdrawal');

        //manage_category related
        Route::get('category', 'CategoryController@update')->name('admin.category')->middleware('adminHasPermission:manage_category');
        Route::post('category/update/post', 'CategoryController@updatePost')->name('admin.category.update.post')->middleware('adminHasPermission:manage_category');
        Route::get('category/sub/create', 'CategoryController@subCreate')->name('admin.category.sub.create')->middleware('adminHasPermission:manage_category');
        Route::post('category/sub/create', 'CategoryController@subCreatePost')->name('admin.category.sub.create.post')->middleware('adminHasPermission:manage_category');
        Route::get('category/sub/edit/{id}', 'CategoryController@subEdit')->name('admin.category.sub.edit')->middleware('adminHasPermission:manage_category');
        Route::post('category/sub/edit/{id}/post', 'CategoryController@subEditPost')->name('admin.category.sub.edit.post')->middleware('adminHasPermission:manage_category');
        Route::post('category/sub/delete/{id}/post', 'CategoryController@subDeletePost')->name('admin.category.sub.delete.post')->middleware('adminHasPermission:manage_category');
        Route::post('category/update_sorting', 'CategoryController@updateSorting')->name('admin.category.sub.updatesorting')->middleware('adminHasPermission:manage_category');

        //report
        Route::get('report/sale', 'ReportController@sale')->name('admin.report.sale')->middleware('adminHasPermission:sale_report');

        //audit_report related
        Route::get('audit_log', 'AuditReportController@index')->name('admin.audit_log')->middleware('adminHasPermission:audit_report');
        Route::get('audit_log/dt', 'AuditReportController@indexDt')->name('admin.audit_log.dt')->middleware('adminHasPermission:audit_report');
        Route::get('audit_log/details/{id}', 'AuditReportController@details')->name('admin.audit_log.details')->middleware('adminHasPermission:audit_report');

        Route::get('profile', 'MyProfileController@profile')->name('admin.profile');
        Route::post('profile', 'MyProfileController@profilePost')->name('admin.profile.post');

//        Route::get('storage/images', 'StorageController@images')->name('admin.storage.images');
//        Route::get('storage/files', 'StorageController@files')->name('admin.storage.files');

        //Reusable DT for user
        Route::get('user_bonus/dt/{uuid}', 'UserDtController@userBonusDt')->name('admin.userdt.bonus');
        Route::get('user_transaction/dt/{uuid}', 'UserDtController@userTransactionDt')->name('admin.userdt.transaction');

        //Test only route
        Route::get('test/reset', 'TestController@reset')->name('admin.test.reset');

        Route::get('setlang', 'EtcController@setLang')->name('admin.setlang');
    });

    Route::group(['middleware' => 'guest:admin'], function () {
        Route::get('login', 'LoginController@showLoginForm')->name('admin.login');
        Route::post('login/post', 'LoginController@login')->name('admin.login.post');
    });

    Route::get('logout', 'LoginController@logout')->name('admin.logout');
});
