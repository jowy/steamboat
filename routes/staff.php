<?php

$prefix = 'staff';

Route::group(['prefix' => 'auth', 'middleware' => 'guest:staff'], function () use ($prefix) {
    Route::get('login', 'AuthController@login')->name($prefix . '.login');
    Route::post('authenticate', 'AuthController@authenticate')->name($prefix . '.authenticate');
});

Route::group(['middleware' => 'auth:staff'], function () use ($prefix){
    # Home
    Route::get('/', 'SaleController@index')->name($prefix.'.sale.index');

    # Sale
    Route::get('sale', 'SaleController@index')->name($prefix.'.sale.index');
    Route::post('sale/store', 'SaleController@store')->name($prefix.'.sale.store');
    Route::post('sale/store-ala-carte', 'SaleController@storeAlaCarte')->name($prefix.'.sale.store-ala-carte');
    Route::get('sale/get-branch-table', 'SaleController@getBranchTable')->name($prefix.'.sale.get-branch-table');
    //Route::post('sale/add-pax', 'SaleController@addPax')->name($prefix.'.sale.add-pax');
    Route::get('sale/detail', 'SaleController@detail')->name($prefix.'.sale.detail');
    Route::get('sale/get-ala-carte-stock', 'SaleController@getAlaCarteStock')->name($prefix.'.sale.get-ala-carte-stock');
    Route::post('sale/add-ala-carte', 'SaleController@addAlaCarte')->name($prefix.'.sale.add-ala-carte');
    Route::get('sale/get-sale-order', 'SaleController@getSaleOrder')->name($prefix.'.sale.get-sale-order');
    Route::post('sale/paid', 'SaleController@paid')->name($prefix.'.sale.paid');
    Route::get('sale/history', 'SaleController@history')->name($prefix.'.sale.history');
    Route::get('sale/widget', 'SaleController@indexWidget')->name($prefix.'.sale.widget');
    Route::get('sale/history-detail/{sale}', 'SaleController@historyDetail')->name($prefix.'.sale.history-detail');
    Route::post('sale/history-detail/{sale}', 'SaleController@postHistoryDetail')->name($prefix.'.sale.history-detail.post');
    Route::get('sale/get-change-table', 'SaleController@getChangeTable')->name($prefix.'.sale.get-change-table');
    Route::post('sale/change-table', 'SaleController@changeTable')->name($prefix.'.sale.change-table');
    Route::get('sale/get-pax', 'SaleController@getPax')->name($prefix.'.sale.get-pax');
    Route::post('sale/edit-pax', 'SaleController@editPax')->name($prefix.'.sale.edit-pax');
    Route::post('sale/cancel', 'SaleController@cancelSale')->name($prefix.'.sale.cancel');

    # Join Sale
    Route::get('join-bill', 'SaleController@joinBill')->name($prefix.'.sale.join-bill');
    Route::post('join-bill', 'SaleController@joinBillPost')->name($prefix.'.sale.join-bill.post');

    Route::get('account', 'AccountController@index')->name($prefix.'.account');
    Route::get('account/dt', 'AccountController@indexDt')->name($prefix.'.account.dt');
    Route::get('account/submit', 'AccountController@submit')->name($prefix.'.account.submit');
    Route::post('account/submit', 'AccountController@submitPost')->name($prefix.'.account.submit.post');
    Route::get('account/summary', 'AccountController@summary')->name($prefix.'.account.summary');
    Route::post('account/summary', 'AccountController@summaryPost')->name($prefix.'.account.summary.post');
    Route::get('account/receipt/{id}', 'AccountController@receipt')->name($prefix.'.account.receipt');
    Route::get('account/{staff_account}', 'AccountController@edit')->name($prefix.'.account.edit');
    Route::put('account/{staff_account}', 'AccountController@update')->name($prefix.'.account.update');

    # Sale Order Statement
    Route::get('sale/order-statement/{sale}', 'SaleController@orderStatement')->name($prefix.'.sale.order-statement');

    # Sale Receipt
    Route::get('sale/receipt/{sale}', 'SaleController@receipt')->name($prefix.'.sale.receipt');

    # Profile
    Route::get('profile', 'StaffController@profile')->name($prefix.'.profile');
    Route::post('profile', 'StaffController@updateProfile')->name($prefix.'.update-profile');

    # Logout
    Route::get('logout', 'AuthController@logout')->name($prefix.'.logout');
});
