<?php

/* Route for branch */

$prefix = 'branch';

Route::group(['prefix' => 'auth', 'middleware' => 'guest:branch'], function () use ($prefix){
    Route::get('login', 'AuthController@login')->name($prefix.'.login');
    Route::post('authenticate', 'AuthController@authenticate')->name($prefix.'.authenticate');
});

Route::group(['middleware' => 'auth:branch'], function () use ($prefix){
    Route::get('/', 'HomeController@index')->name($prefix.'.dashboard');

    # Order Stock
    Route::resource('order', 'OrderController', array_merge(append_resource_names($prefix.'.order')), ['except' => ['edit', 'update', 'destroy']]);
    Route::get('order/print-invoice/{order}', 'OrderController@printInvoice')->name($prefix.'.order.print-invoice');

    # Branch Stock
    Route::resource('stock', 'StockController', array_merge(append_resource_names($prefix.'.stock')), ['except' => ['edit', 'update', 'destroy']]);

    # Table
    Route::resource('branch-table', 'BranchTableController', append_resource_names($prefix.'.branch-table'));
    Route::post('/branch-table/delete/{id}', 'BranchTableController@delete')->name($prefix.'.branch-table.delete');

    # Promotion
    //Route::resource('promotion', 'PromotionController', append_resource_names($prefix.'.promotion'));

    # Staff
    Route::resource('staff', 'StaffController', append_resource_names($prefix.'.staff'));
    Route::get('staff/login/{id}', 'StaffController@login')->name($prefix . '.staff.login');
    Route::post('/staff/delete/{staff}', 'StaffController@delete')->name($prefix.'.staff.delete');

    # Staff close account
    Route::get('account', 'StaffAccountController@index')->name($prefix.'.staff.account');
    Route::get('account/dt', 'StaffAccountController@indexDt')->name($prefix.'.staff.account.dt');
    Route::get('account/summary', 'StaffAccountController@summary')->name($prefix.'.staff.account.summary');
    Route::post('account/summary', 'StaffAccountController@summaryPost')->name($prefix.'.staff.account.summary.post');
    Route::get('account/receipt/{id}', 'StaffAccountController@receipt')->name($prefix.'.staff.account.receipt');

    # Stock manage
    Route::resource('stock-manage', 'StockManageController', array_merge(append_resource_names($prefix.'.stock-manage')), ['except' => ['edit', 'update', 'destroy']]);

    # Supplier
    Route::resource('supplier', 'SupplierController', append_resource_names($prefix.'.supplier'));

    # Sale
    Route::resource('sale', 'SaleController', array_merge(append_resource_names($prefix.'.sale')), ['except' => ['create', 'store', 'destroy']]);
    Route::get('sale-widget', 'SaleController@indexWidget')->name($prefix.'.sale.widget');
    Route::get('sale/receipt/{sale}', 'SaleController@receipt')->name($prefix.'.sale.receipt');
    Route::post('sale/delete/{sale}', 'SaleController@delete')->name($prefix.'.sale.delete');
//    Route::get('sale', 'SaleController@index')->name($prefix.'.sale.index');
//    Route::get('sale/{sale}', 'SaleController@show')->name($prefix.'.sale.show');

    # Profile
    Route::get('profile', 'BranchController@profile')->name($prefix.'.profile');
    Route::post('profile', 'BranchController@updateProfile')->name($prefix.'.update-profile');

    # Stock
    Route::get('branch-stock', 'BranchStockController@index')->name($prefix.'.branch-stock.index');
    Route::get('branch-stock/{branch_stock}', 'BranchStockController@edit')->name($prefix.'.branch-stock.edit');
    Route::put('branch-stock/{branch_stock}', 'BranchStockController@update')->name($prefix.'.branch-stock.update');

    # Wallet
    Route::get('wallet', 'WalletController@index')->name($prefix.'.wallet.index');
    Route::put('wallet/update', 'WalletController@update')->name($prefix.'.wallet.update');

    # Logout
    Route::get('logout', 'AuthController@logout')->name($prefix.'.logout');
});
