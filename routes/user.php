<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'language:user'], function () {
    Route::get('/', 'HomeController@index')->name('user.home');

    Route::group(['middleware' => ['auth:user', 'notFreeze:user']], function () {
        //Page
        Route::get('p/{page_tag}', 'PageController@page')->name('user.page');
    });

    Route::group(['middleware' => 'guest:user'], function () {
        Route::get('login', 'LoginController@showLoginForm')->name('user.login');
        Route::post('login/post', 'LoginController@login')->name('user.login.post');

        Route::get('password/forget', 'ForgotPasswordController@showForgetForm')->name('password.forget');
        Route::post('password/forget', 'ForgotPasswordController@sendResetLinkEmail')->name('password.forget.post');

        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'ResetPasswordController@reset')->name('password.reset.post');

        Route::get('register', 'RegisterController@index')->name('user.register');
        Route::post('register/post', 'RegisterController@indexPost')->name('user.register.post');
    });

    Route::get('share/{username}/{timestamp}', 'InviteController@share')->name('user.invite.share');
    Route::get('logout', 'LoginController@logout')->name('user.logout');
});
